/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle;

import java.io.*;

import javax.swing.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URLDecoder;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.cli.*;
import simpplle.comcode.*;
import simpplle.comcode.Process;
import simpplle.gui.PrintStatusReporter;
import simpplle.gui.SimpplleMain;
import simpplle.gui.SplashScreen;
import simpplle.gui.SystemKnowledgeFiler;

/** 
 * This is the Main starting point for the JSimpplle application.
 * This will determine whether to start the gui or command-line
 * based on properties.
 * This class also provides some utilities and certain data that
 * needs to be accessed throughout the application.
 *
 * Properties that OpenSIMPPLLE uses:
 *   simpplle.fixedrandom=enabled    ;; Read random #'s from file
 *   simpplle.fire.spotting=disabled ;; disable fire spotting
 */
public final class JSimpplle {

  private static simpplle.comcode.Simpplle    comcode;
  private static simpplle.gui.SimpplleMain    simpplleMain;
  private static simpplle.comcode.Properties  properties;

  private static File appDirectory;

  private static boolean isHeadless;

  private static final int EXIT_SUCCESS = 0;
  private static final int EXIT_FAILURE = 1;

  /**
   * The main entry point for OpenSIMPPLLE.
   *
   * @param arguments an array of program arguments
   * @throws java.io.IOException if an I/O exception occurs
   */
  public static void main(String[] arguments) throws IOException {
    // Create a new container for application state

    comcode = new Simpplle();

    // Read in application settings
    File homeDirectory = new File(System.getProperty("user.home"));
    properties = new Properties(homeDirectory, "opensimpplle.properties");
    properties.read();

    // Determine where the app is running from
    determineAppDirectory();

    // Define command line options
    Options options = new Options();
    options.addOption("allstatesreport", false, "produce an all states report");
    options.addOption("allstatesrules", true, "rules file for the all states report");
    options.addOption("area", true, "the area file to load");
    options.addOption("areasummary", false, "produce an area summary");
    options.addOption("boundarymultiplier", true, "rate of spread beyond the border of the area");
    options.addOption("discountedcost", true, "calculate and discount suppression cost at this rate");
    options.addOption("extremewindmultiplier", true, "wind speed multiplier for extreme fires");
    options.addOption("firespreadmodel", true, "method for spreading fires");
    options.addOption("firesuppression", false, "apply fire suppression logic");
    options.addOption("gisupdateandspread", false, "produce GIS update and spread files");
    options.addOption("help", false, "print usage information and exit");
    options.addOption("historic", false, "use historic pathways");
    options.addOption("invasivelogic", true, "apply invasive species logic (mesa-verde-np, msu, none, r1)");
    options.addOption("knowledge", true, "name of the system knowledge");
    options.addOption("probabilityreports", false, "produce probability reports");
    options.addOption("processschedule", true, "schedule processes with a *.sk_processsched file");
    options.addOption("recyclesteps", true, "number of time steps kept in memory at once");
    options.addOption("reportperiods", true, "Specific times steps the user would like in the output");
    options.addOption("runs", true, "number of times to run the simulation");
    options.addOption("savearea", true, "file to save the area to");
    options.addOption("saveknowledge", true, "file to save the knowledge to");
    options.addOption("saveresults", true, "simulation results directory");
    options.addOption("seed", true, "seed for the pseudo random number generator");
    options.addOption("simmethod", true, "method for selecting disturbance processes");
    options.addOption("simulate", false, "run a simulation");
    options.addOption("steps", true, "number of time steps in each run");
    options.addOption("trackingspeciesreport", false, "produce a tracking species report");
    options.addOption("trackownership", false, "stratify *-ls.txt output file by ownership");
    options.addOption("trackspecialarea", false, "stratify *-ls.txt output file by special area");
    options.addOption("treatmentschedule", true, "schedule treatments with a *.sk_treatsched file");
    options.addOption("version", false, "print the version and exit");
    options.addOption("winddirectionvariability", true, "variability of wind direction (degrees)");
    options.addOption("windspeedvariability", true, "variability of wind speed (miles per hour)");
    options.addOption("writetodatabase", false, "store vegetative states in database during run");
    options.addOption("yearly", false, "use a one year interval between time steps");
    options.addOption("zone", true, "regional zone name (westside-region-one, sierra-nevada, ..)");

    // Parse the arguments
    CommandLineParser parser = new DefaultParser();
    CommandLine commandLine;
    try {
      commandLine = parser.parse(options, arguments);
    } catch (ParseException ex) {
      System.err.println("Parsing Failed: " + ex.getMessage());
      return;
    }

    // Run without a GUI if there are arguments
    if (commandLine.getOptions().length > 0) {
      runAsCommand(commandLine, options, arguments);
    } else {
      runAsEditor();
    }
  }

  /**
   * Runs the application as a command. Options may be specified in any order, but the order that
   * they are processed is specific. Some options depend on other options. For example, the
   * "-savearea" flag requires a zone and an area to be loaded.
   *
   * @param commandLine a command line
   * @param options supported command line options
   */
  private static void runAsCommand(CommandLine commandLine, Options options, String args[]) {

    isHeadless = true;

    Simpplle.setStatusReporter(new PrintStatusReporter());

    if (commandLine.hasOption("help")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.setWidth(200);
      formatter.printHelp("java -jar opensimpplle.jar", options);
      System.exit(EXIT_SUCCESS);
    }

    if (commandLine.hasOption("version")) {
      System.out.println(SimpplleMain.VERSION);
      System.exit(EXIT_SUCCESS);
    }

    if (commandLine.hasOption("zone")) {
      String name = commandLine.getOptionValue("zone");
      Integer zone = zoneNameToIndex(name);
      if (zone == null) {
        System.err.println("Invalid zone name: " + name);
        System.exit(EXIT_FAILURE);
      }
      Simpplle.setStatusMessage("Loading " + RegionalZone.availableZones()[zone]);
      try {
        comcode.loadZone(zone, commandLine.hasOption("historic"));
      } catch (SimpplleError error) {
        System.err.println(error.getMessage());
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("knowledge")) {
      File file = new File(commandLine.getOptionValue("knowledge"));
      Simpplle.setStatusMessage("Loading knowledge from " + file.getAbsolutePath());
      try {
        SystemKnowledge.loadUserKnowledge(file);
      } catch (SimpplleError error) {
        System.err.println(error.getError());
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("saveknowledge")) {
      File file = new File(commandLine.getOptionValue("saveknowledge"));
      Simpplle.setStatusMessage("Saving knowledge to " + file.getAbsolutePath());
      try {
        SystemKnowledge.saveUserKnowledge(file);
      } catch (SimpplleError error) {
        System.err.println(error.getError());
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("area")) {
      File file = new File(commandLine.getOptionValue("area"));
      Simpplle.setStatusMessage("Loading area from " + file.getAbsolutePath());
      try {
        comcode.loadUserArea(file);
      } catch (SimpplleError error) {
        System.err.println(error.getError());
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("savearea")) {
      File file = new File(commandLine.getOptionValue("savearea"));
      Simpplle.setStatusMessage("Saving area to " + file.getAbsolutePath());
      try {
        comcode.saveCurrentArea(file);
      } catch (SimpplleError error) {
        System.err.println(error.getError());
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("processschedule")) {
      File file = new File(commandLine.getOptionValue("processschedule"));
      try {
        SystemKnowledge.loadUserKnowledge(file, SystemKnowledge.PROCESS_SCHEDULE);
      } catch (SimpplleError error) {
        error.printStackTrace();
        System.exit(EXIT_FAILURE);
      }
    }

    if (commandLine.hasOption("treatmentschedule")) {
      File file = new File(commandLine.getOptionValue("treatmentschedule"));
      try {
        SystemKnowledge.loadUserKnowledge(file, SystemKnowledge.TREATMENT_SCHEDULE);
      } catch (SimpplleError error) {
        error.printStackTrace();
        System.exit(EXIT_FAILURE);
      }

    }

    if (commandLine.hasOption("simulate")) {

      if (Simpplle.getCurrentZone() == null) {
        System.err.println("A zone must be loaded before running a simulation");
        System.exit(EXIT_FAILURE);
      }

      if (Simpplle.getCurrentArea() == null) {
        System.err.println("An area must be loaded before running a simulation");
        System.exit(EXIT_FAILURE);
      }

      String model;

      // Set the fire spread model: if keane attributes are present, use keane spread behavior.
      if(Simpplle.getCurrentArea().hasKeaneAttributes()) {

        model = commandLine.getOptionValue("firespreadmodel", "keane");

      } else {
        model = commandLine.getOptionValue("firespreadmodel", "simpplle");
      }

      switch (model.toLowerCase()) {
        case "keane":
          Simulation.fireSpreadModel = FireSpreadModel.KEANE;
          break;

        case "simpplle":
          Simulation.fireSpreadModel = FireSpreadModel.BASIC;
          break;

        default:
          System.err.println("Unknown fire spread model");
          System.exit(EXIT_FAILURE);
      }

      // Set Keane fire spread parameters
      KeaneFireEvent.boundarySpreadWeight = Double.parseDouble(commandLine.getOptionValue("boundarymultiplier", "0.0"));
      KeaneFireEvent.extremeWindMultiplier = Double.parseDouble(commandLine.getOptionValue("extremewindmultiplier", "0.0"));
      KeaneFireEvent.windDirectionVariability = Double.parseDouble(commandLine.getOptionValue("winddirectionvariability", "0.0"));
      KeaneFireEvent.windSpeedVariability = Double.parseDouble(commandLine.getOptionValue("windspeedvariability", "0.0"));

      // Parse enumerated options
      Simulation.InvasiveKind invasiveLogic = Simulation.InvasiveKind.NONE;
      switch (commandLine.getOptionValue("invasivelogic", "none")) {
        case "mesa-verde-np":
          invasiveLogic = Simulation.InvasiveKind.R1;
          break;

        case "msu":
          invasiveLogic = Simulation.InvasiveKind.MSU;
          break;

        case "none":
          invasiveLogic = Simulation.InvasiveKind.NONE;
          break;

        case "r1":
          invasiveLogic = Simulation.InvasiveKind.R1;
          break;

        default:
          System.err.println("Invalid invasive species logic");
          System.exit(EXIT_FAILURE);
      }

      String simMethod = "STOCHASTIC";
      switch (commandLine.getOptionValue("simmethod", "stochastic")) {
        case "highest":
          simMethod = "HIGHEST";
          break;

        case "stand-development":
          simMethod = "STAND DEVELOPMENT";
          break;

        case "stochastic":
          simMethod = "STOCHASTIC";
          break;

        default:
          System.err.println("Invalid simulation method");
          System.exit(EXIT_FAILURE);
      }

      File resultsPrefix = null;
      if (commandLine.hasOption("saveresults")) {
        resultsPrefix = new File(commandLine.getOptionValue("saveresults",""));
      }

      String repPeriodString = commandLine.getOptionValue("reportperiods");
      int size;
      int i;

      int numSteps = Integer.parseInt(commandLine.getOptionValue("steps"));
      int[] repPeriodArray;

      if(repPeriodString == null){
        // should start at 0 and include all timesteps
        size = numSteps+1;
        repPeriodArray = new int[size];
        for(i = 0; i < size; i++){
          repPeriodArray[i] = i;
        }
      }
      else{
        Scanner reportScanner = new Scanner(repPeriodString);

        // comma separates ranges
        reportScanner.useDelimiter(",");
        ArrayList<Integer> temp_periods = new ArrayList<>();
        parseReportPeriods(reportScanner, temp_periods);
        // Create array that is the proper size, and copy over the values
        repPeriodArray = new int[temp_periods.size()];
        for(i = 0; i < temp_periods.size(); i++){
          repPeriodArray[i] = temp_periods.get(i);
        }
      }

      // Technically we could catch all input errors with a function here and refactor the proceeding lines into it.
      int runs = Integer.parseInt(commandLine.getOptionValue("runs"));
      if (runs > 1 && !commandLine.hasOption("saveresults")) {
        System.out.println("\nAttention! " +
            "\nSimulations with a run parameter > 1 require an output directory. " +
            "\nAdd argument -saveresults \\my_directory to your command." +
            "**note directory must already exist**");
        System.exit(0);
      }

      // Perform the simulation
      Simulation simulation = new Simulation(
          Integer.parseInt(commandLine.getOptionValue("runs", "1")),
          Integer.parseInt(commandLine.getOptionValue("steps", "5")),
          repPeriodArray,
          Integer.parseInt(commandLine.getOptionValue("seed", "0")),
          Float.parseFloat(commandLine.getOptionValue("discountedcost", "0")),
          simMethod,
          invasiveLogic,
          commandLine.hasOption("recyclesteps"),
          commandLine.hasOption("allstatesreport"),
          commandLine.hasOption("areasummary"),
          commandLine.hasOption("gisupdateandspread"),
          commandLine.hasOption("firesuppression"),
          commandLine.hasOption("probabilityreports"),
          commandLine.hasOption("trackingspeciesreport"),
          commandLine.hasOption("trackspecialarea"),
          commandLine.hasOption("trackownership"),
          commandLine.hasOption("saveresults"),
          commandLine.hasOption("writetodatabase"),
          commandLine.hasOption("yearly"),
          commandLine.hasOption("seed"),
          Integer.parseInt(commandLine.getOptionValue("recyclesteps", "0")),
          new File(commandLine.getOptionValue("allstatesrules", "")),
          resultsPrefix
      );
      simulation.setCMDTrue();
      Simulation.setInstance(simulation);

      Simpplle.setStatusMessage("Running simulation");

      try {
        simulation.runSimulation();
      } catch (SimpplleError error) {
        error.printStackTrace();
        System.exit(EXIT_FAILURE);
      }
    }
  }

  /** Parses the report periods string.
   *
   *
   * @param simArrayScan A scanner that returns the line of the report period
   * @param temp_periods temporary array that holds the set of time periods to report.
   */
  public static void parseReportPeriods(Scanner simArrayScan, ArrayList<Integer> temp_periods) {
    while (simArrayScan.hasNext()) {
      String current_token = simArrayScan.next();
      // dash separates values in the range
      Scanner rangeScanner = new Scanner(current_token);
      rangeScanner.useDelimiter("-");
      while (rangeScanner.hasNext()) {
        int startValue = rangeScanner.nextInt();
        int endValue = startValue;
        // if there are more tokens, range is wider.
        // This handles the case of only one value between comma separations. i.e. won't have token
        while (rangeScanner.hasNext()) {
          endValue = rangeScanner.nextInt();
        }
        for (int n = startValue; n <= endValue; ++n) temp_periods.add(n);
      }
    }
  }

  /**
   * Converts a zone name from the command line to a predefined integer constant.
   *
   * @param name zone name
   * @return an integer constant for an existing zone, otherwise null
   */
  private static Integer zoneNameToIndex(String name) {
    switch (name.toLowerCase()) {
      case "colorado-front-range":
        return ValidZones.COLORADO_FRONT_RANGE;

      case "colorado-plateau":
        return ValidZones.COLORADO_PLATEAU;

      case "eastside-region-one":
        return ValidZones.EASTSIDE_REGION_ONE;

      case "gila":
        return ValidZones.GILA;

      case "great-plains-steppe":
        return ValidZones.GREAT_PLAINS_STEPPE;

      case "mixed-grass-prairie":
        return ValidZones.MIXED_GRASS_PRAIRIE;

      case "northern-central-rockies":
        return ValidZones.NORTHERN_CENTRAL_ROCKIES;

      case "sierra-nevada":
        return ValidZones.SIERRA_NEVADA;

      case "south-central-alaska":
        return ValidZones.SOUTH_CENTRAL_ALASKA;

      case "southern-california":
        return ValidZones.SOUTHERN_CALIFORNIA;

      case "southwest-utah":
        return ValidZones.SOUTHWEST_UTAH;

      case "teton":
        return ValidZones.TETON;

      case "western-great-plains-steppe":
        return ValidZones.WESTERN_GREAT_PLAINS_STEPPE;

      case "westside-region-one":
        return ValidZones.WESTSIDE_REGION_ONE;

      default:
        return null;
    }
  }

  /**
   * Runs the application as an editor with a graphical user interface.
   */
  private static void runAsEditor() {

    isHeadless = false;

    // Define a handler for unhandled exceptions
    System.setProperty("sun.awt.exception.handler","simpplle.gui.MyErrorHandler");

    // Launch the GUI creation task
    javax.swing.SwingUtilities.invokeLater(JSimpplle::createAndShowGUI);

  }

  public JSimpplle() {
    comcode = new simpplle.comcode.Simpplle();
    simpplleMain = new SimpplleMain();
  }

  /**
   * This will give access to all the classes in the comcode,
   * which is the main engine for OpenSimpplle
   * @return comcode from OpenSimpplle.Comcode package.
   */
  public static simpplle.comcode.Simpplle getComcode() {
    return comcode;
  }

  /**
   *
   * @return gui for OpenSimpplle interface
   */
  public static simpplle.gui.SimpplleMain getSimpplleMain() {
    return simpplleMain;
  }

  /**
   * Gets the current working directory
   * @return working directory file in current working directory.
   */
  public static File getWorkingDir() { return properties.getWorkingDirectory(); }

  /**  
   * Returns the directory that the application is running from.
   *
   * @return the application directory
   */
  public static File getAppDirectory() {
    return appDirectory;
  }

  /**  
   * Puts Simpplle into developer mode to allow editing.  Sets the system property to development mode. 
   * @return boolean true if System property is not null and developer equals true. 
   */
  public static boolean developerMode() {
    String developer = System.getProperty("simpplle.development");
    return (developer != null && developer.equals("true"));
  }

  public static boolean simLoggingFile() {
    return properties.getSimulationLogging();
  }

  public static boolean invasiveSpeciesMSUProbFile() {
    return properties.getInvasiveMSU();
  }

  /**
   * Determines the directory that the application is running from.
   */
  private static void determineAppDirectory() {

    URL url = JSimpplle.class.getProtectionDomain().getCodeSource().getLocation();

    String path;
    try {
      path = URLDecoder.decode(url.getFile(), "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      path = null;
    }

    if (path == null) {
      appDirectory = new File(System.getProperty("user.home"));
    } else {
      appDirectory = new File(path).getParentFile();
    }
  }

  private static void createAndShowGUI() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (ClassNotFoundException
        | UnsupportedLookAndFeelException
        | IllegalAccessException
        | InstantiationException e) {
      e.printStackTrace();
    }

    SplashScreen splashScreen = new SplashScreen();
    splashScreen.setVisible(true);
    simpplleMain = new SimpplleMain();

    try {

      simpplleMain.loadDefaultZone(properties);

    } catch (SimpplleError e) {

      JOptionPane.showMessageDialog(simpplleMain, e.getError(),
          "Error Loading Default Zone",
           JOptionPane.ERROR_MESSAGE);
    }

    splashScreen.setVisible(false);
  }

  public static Properties getProperties() {
    return properties;
  }
}
