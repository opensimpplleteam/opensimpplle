package simpplle.model;

/**
 * A coordinate represents a point on a two-dimensional plane
 */
public class Coordinate {

    public int x;
    public int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
