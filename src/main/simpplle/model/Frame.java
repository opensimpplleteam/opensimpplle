package simpplle.model;

import javafx.scene.paint.Color;

import java.util.List;

/**
 * @see Frame stores a table of indices for a single time step. Indices within a frame correspond to entries in a
 * color map. The indices are organized in a table and accessed by row and column, which begin at the upper left
 * corner.
 */
public class Frame {

    private int numRows;
    private int numCols;

    private Dimensions dim;

    private int[][] indices;

    /**
     * Create a frame with the requested dimensions.
     *
     * @param dimensions dimensions of the frame
     */
    public Frame(Dimensions dimensions) {
        dim = dimensions;
        this.numRows = dimensions.getRows();
        this.numCols = dimensions.getColumns();
        this.indices = new int[numRows][numCols];
    }

    /**Comment by Eduard Shokur:
     *  Copy by value of the passedframe is made
     *  so that we could later rotate the copied object to proper orientation
     *  and use it to render
     * This is a copy constructor for the new frame
     * @param passedFrame
     */
    public Frame(Frame passedFrame){
        this.dim = new Dimensions(passedFrame.getDim().getRows(), passedFrame.getDim().getColumns());
        this.numRows = dim.getRows();
        this.numCols = dim.getColumns();
        this.indices = new int[numRows][numCols];
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                //had to create getIndices, and must be public
               this.indices[row][col] = passedFrame.getIndices()[row][col];
            }
        }
    }

    /** Comment by Eduard Shokur:
     * Here we reverse the rows and cols in the indices array
     * This results in image rotation of the frame object around the x-axis
     */
    public void transpose(){
        this.dim = new Dimensions(numCols,numRows);
        this.numRows = dim.getRows();
        this.numCols = dim.getColumns();
        int[][] oldIndices = indices;
        this.indices = new int[numRows][numCols];
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                this.indices[row][col] = oldIndices[col][row];
            }
        }
    }

    /** Comment by Eduard Shokur:
     * Here we rotate around the y-axis
     */
    public void rotate(){
        this.dim = new Dimensions(numCols,numRows);
        this.numRows = dim.getRows();
        this.numCols = dim.getColumns();
        int[][] oldIndices = indices;
        this.indices = new int[numRows][numCols];
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                this.indices[row][col] = oldIndices[(numCols - col) - 1][row];
            }
        }
    }

    /**
     * Loads discrete data into a frame. Data values are replaced with indices in the frame using the color map. If
     * a value doesn't exist in the color map, then a new color assignment is added with the specified default color.
     * Values in the data table are expected to be accessed by row and column starting from the upper left corner.
     *
     * @param data a table of strings
     * @param colorMap a color map for indexing values in the color map
     * @param missing a default color for added color entries
     */
    public void load(String[][] data, ColorMap colorMap, Color missing) {
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                String value = data[row][col];
                int index = colorMap.indexOf(value);
                if (index == -1) {
                    index = colorMap.add(value, missing);
                }
                indices[row][col] = index;
            }
        }
    }

    /**
     * Creates a colored bitmap for a region of a frame.
     *
     * @param top index of the top row
     * @param bottom index of the bottom row
     * @param left index of the left column
     * @param right index of the right column
     * @param colorMap a color map
     * @param missing a color to fill in for missing values
     * @return a table of colors in row-major order from the top left corner
     */
    public Color[][] render(int top, int bottom, int left, int right, ColorMap colorMap, Color missing) {

        int nrows = 1 + bottom - top;
        int ncols = 1 + right - left;

        Color[][] result = new Color[nrows][ncols];

        for (int row = top; row <= bottom; row++) {
            for (int col = left; col <= right; col++) {

                Color color = missing;

                if (row >= 0 && row < numRows &&
                    col >= 0 && col < numCols) {

                    int index = indices[row][col];
                    color = colorMap.getColor(index, missing);

                }

                result[row - top][col - left] = color;

            }
        }

        return result;

    }

    /**
     * Creates a mask for a region of a frame.
     *
     * @param top index of the top row
     * @param bottom index of the bottom row
     * @param left index of the left column
     * @param right index of the right column
     * @param colorMap a color map
     * @param values a list of values to match
     * @return a table of booleans in row-major order from the top left corner
     */
    public boolean[][] mask(int top, int bottom, int left, int right, ColorMap colorMap, List<String> values) {

        int nrows = 1 + bottom - top;
        int ncols = 1 + right - left;

        boolean[][] result = new boolean[nrows][ncols];

        for (int row = top; row <= bottom; row++) {
            for (int col = left; col <= right; col++) {
                int index = indices[row][col];
                result[row - top][col - left] = values.contains(colorMap.getValue(index, null));
            }
        }

        return result;

    }

    /**
     * Returns a histogram, which is an array containing the frequencies of values in this frame.
     *
     * @param colorMap a color map associated with this frame's indices
     * @return an array of frequencies
     */
    public int[] histogram(ColorMap colorMap) {
        int[] frequency = new int[colorMap.size()];
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                int index = indices[row][col];
                frequency[index]++;
            }
        }
        return frequency;
    }

    /**
     * Returns the value of a cell.
     *
     * @param row the row of the cell
     * @param col the column of the cell
     * @param colorMap a color map associated with this frame's indices
     * @return the cell's value, or an empty string
     */
    public String valueAt(int row, int col, ColorMap colorMap) {
        if (row < numRows && col < numCols) {
            int index = indices[row][col];
            return colorMap.getValue(index, "");
        } else {
            return "";
        }
    }

    public Dimensions getDim() { return dim; }
    public int[][] getIndices() { return indices; }
}
