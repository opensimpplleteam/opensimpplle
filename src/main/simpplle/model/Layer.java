package simpplle.model;

/**
 * A layer stores animated discrete raster data as a collection of frames.
 */
public class Layer {

    private Frame[] frames;

    /**
     * Creates a layer with the requested number of frames with the specified dimensions.
     *
     * @param numFrames number of animation frames
     * @param dimensions dimensions of each frame
     */
    public Layer(int numFrames, Dimensions dimensions) {
        frames = new Frame[numFrames];
        for (int i = 0; i < numFrames; i++) {
            frames[i] = new Frame(dimensions);
        }
    }

    /**
     * Returns a single animation frame.
     *
     * @param index the index of the frame to retrieve
     * @return an animation frame
     */
    public Frame getFrame(int index) {
        return frames[index];
    }

    /**
     * Returns the number of animation frames.
     *
     * @return the number of animation frames
     */
    public int numFrames() {
        return frames.length;
    }

}
