package simpplle.model;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * A color map associates values with colors.
 */
public class ColorMap {

    private List<Color> colors;
    private List<String> values;

    /**
     * Creates a new color map.
     */
    public ColorMap() {
        this.colors = new ArrayList<>();
        this.values = new ArrayList<>();
    }

    /**
     * Maps a value to a color. If the value already exists, then the existing index is returned.
     *
     * @param value a value to add
     * @param color a color to associate with the value
     * @return an index for a new or existing mapping
     *
     * @param value
     * @param color
     * @return
     * Comment by Eduard Shokur:
     * Initially the colorMap is empty.
     * the information passed from each frame fills out a color map.
     * If the value is missing, then a default one is filled in at the location and the index where
     * the new color was added is returned to be placed in the frame's indices array.
     * If the value3 is not missing, the the index where the color is located is returned to
     * be placed in the frame's indices array
     */

    public int add(String value, Color color) {
        int index = values.indexOf(value);
        if (index == -1) {
            index = colors.size();
            colors.add(color);
            values.add(value);
        }
        return index;
    }

    /**
     * Returns the index of a mapped value, or -1 if a mapping doesn't exist.
     *
     * @param value a mapped value
     * @return the index of the value, or -1
     */
    public int indexOf(String value) {
        return values.indexOf(value);
    }

    /**
     * Returns the number of entries in this color map.
     *
     * @return the number of entries in this color map
     */
    public int size() {
        return values.size();
    }

    /**
     * Assigns a color to an existing value or adds a new entry.
     *
     * @param value a value whose color will be reassigned
     * @param color the new color
     */
    public void setColor(String value, Color color) {
        int index = values.indexOf(value);
        if (index >= 0) {
            colors.set(index, color);
        } else {
            add(value, color);
        }
    }

    /**
     * Returns a color from the color map at the given index, or a missing color.
     *
     * @param index an index for an entry
     * @param missing color to return if index out of bounds
     * @return the color at the index, or the missing color
     */
    public Color getColor(int index, Color missing) {
        if (index >= 0 && index < values.size()) {
            return colors.get(index);
        } else {
            return missing;
        }
    }

    /**
     * Returns the color assigned to the value, or a missing color.
     *
     * @param value a mapped value
     * @param missing color to return if value not mapped
     * @return the color for the mapped value, or the missing color
     */
    public Color getColor(String value, Color missing) {
        int index = values.indexOf(value);
        if (index == -1) {
            return missing;
        } else {
            return colors.get(index);
        }
    }

    /**
     * Returns the value at the specified index.
     *
     * @param index the position of an entry in this color map
     * @param missing a value to return if the index is invalid
     * @return the value if it exists, otherwise the specified missing value
     */
    public String getValue(int index, String missing) {
        if (index >= 0 && index < values.size()) {
            return values.get(index);
        } else {
            return missing;
        }
    }

    public void toContrastingColors() {
        double goldenRatio = ((1.0+Math.sqrt(5))/2.0);
        for (int i = 0; i < this.size(); i++ ) {
            double hue = (goldenRatio * (i * 360 / size())) % 360.0;
            this.setColor(this.getValue(i, "NOT FOUND"), Color.hsb(hue, 0.85, 0.925));
        }
    }

}
