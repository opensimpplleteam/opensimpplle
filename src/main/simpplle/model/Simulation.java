package simpplle.model;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A simulation stores a collection of animated layers.
 */
public class Simulation {

    public String regionName;
    public String areaName;
    public String runName;
    public String creationTime;

    private int numFrames;
    private Dimensions dimensions;
    private Map<String,Layer> layers;

    /**
     * Creates a new simulation.
     *
     * @param dimensions dimensions of each layer
     */
    public Simulation(int numFrames, Dimensions dimensions) {
        this.numFrames = numFrames;
        this.dimensions = dimensions;
        this.layers = new TreeMap<>();
    }

    /**
     * Adds a layer to the simulation, unless one with the same name already exists.
     *
     * @param name the name of the layer
     * @return the new layer, or an existing layer with the same name
     */
    public Layer addLayer(String name) {
        Layer layer = layers.get(name);
        if (layer == null) {
            layer = new Layer(numFrames, dimensions);
            layers.put(name,layer);
        }
        return layer;
    }

    /**
     * Returns a layer from the simulation.
     *
     * @param name the name of the layer
     * @return the layer, or null if a layer isn't found
     */
    public Layer getLayer(String name) {
        return layers.get(name);
    }

    /**
     * Returns the set of layer names.
     *
     * @return the set of layer names
     */
    public Set<String> getLayerNames() {
        return layers.keySet();
    }

    public Dimensions getDimensions() { return dimensions; }

    public int getNumFrames() { return numFrames; }

}
