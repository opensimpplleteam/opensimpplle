package simpplle.model;

/**
 * Dimensions store the number of rows and columns in a rectangular array.
 */
public class Dimensions {

    private int rows;
    private int columns;

    public Dimensions(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }
}
