package simpplle.model;

import javafx.scene.paint.Color;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class Landscape {
    /** Notes by Eduard Shokur
     * @see Landscape stores a collection of simulations that share a set of color maps.
     * Landscape contains the dimensions object as well String->ColorMap dictionary and String->Simulation dictionary
     */
    public static final Color DEFAULT_COLOR = Color.web("#666");

    private Dimensions dimensions;
    private Map<String, ColorMap> colorMaps;
    private Map<String, Simulation> simulations;

    /**
     * Creates a new landscape with the specified layer dimensions.
     *
     * @param dimensions dimensions of layers in this landscape
     */
    public Landscape(Dimensions dimensions) {
        this.dimensions = dimensions;
        this.colorMaps = new TreeMap<>();
        this.simulations = new TreeMap<>();
    }

    /**
     * Returns the number of rows in the contained layers.
     *
     * @return the number of rows in the contained layers
     */
    public int getNumRows() {
        return dimensions.getRows();
    }

    /**
     * Returns the number of columns in the contained layers.
     *
     * @return the number of columns in the contained layers
     */
    public int getNumCols() {
        return dimensions.getColumns();
    }

    /**
     * Adds a color map to the landscape, unless one with the same name already exists.
     *
     * @param layerName the name of the associated layer
     * @return the new color map, or an existing color map with the same name
     */
    public ColorMap addColorMap(String layerName) {
        // colormaps is a map of strings and their associated colorMap objects
        ColorMap colorMap = colorMaps.get(layerName);
        if (colorMap == null) {
            colorMap = new ColorMap();
            colorMaps.put(layerName, colorMap);
        }
        return colorMap;
    }

    /**
     * Returns a color map from the landscape.
     *
     * @param layerName the name of the associated layer
     * @return the color map, or null if it isn't found
     */
    public ColorMap getColorMap(String layerName) {
        return colorMaps.get(layerName);
    }

    /**
     * Returns the set of color map names.
     *
     * @return the set of color map names
     */
    public Set<String> getColorMapNames() {
        return colorMaps.keySet();
    }

    /**
     * Adds a simulation to the landscape, unless one with the same name already exists.
     *
     * @param name the name of the simulation
     * @param numFrames the number of frames in layers of the simulation
     * @return the new simulation, or an existing simulation with the same name
     */
    public Simulation addSimulation(String name, int numFrames) {
        Simulation simulation = simulations.get(name);
        if (simulation == null) {
            simulation = new Simulation(numFrames, dimensions);
            simulations.put(name, simulation);
        }
        return simulation;
    }

    /**
     * Returns a simulation from the landscape.
     *
     * @param name the name of the simulation
     * @return the simulation, or null if it isn't found
     */
    public Simulation getSimulation(String name) {
        return simulations.get(name);
    }

    /**
     * Returns the set of simulation names.
     *
     * @return the set of simulation names
     */
    public Set<String> getSimulationNames() {
        return simulations.keySet();
    }

    /**
     * Generates colors for color maps that have more than one color that is the default grey color.
     */
    public void generateMissingColors() {

        for (ColorMap colorMap : colorMaps.values()) {
            int numMissing = 0;
            for (int i = 0; i < colorMap.size(); i++) {
                Color c = colorMap.getColor(i, DEFAULT_COLOR);
                if (c == DEFAULT_COLOR) {
                    numMissing++;
                }
            }
            if (numMissing > 1) {
                colorMap.toContrastingColors();
                colorMap.setColor(colorMap.getValue(0,""), DEFAULT_COLOR); // Temporary - Used as BG fill color
            }
        }
    }
}
