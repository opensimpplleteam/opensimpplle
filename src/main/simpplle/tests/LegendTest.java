package simpplle.tests;

import simpplle.controller.MainController;
import javafx.scene.paint.Color;
import simpplle.model.*;

/**
 * Created by John on 4/10/2017.
 */
public class LegendTest {

    public static void testLegend(MainController main) {
        Landscape land = new Landscape(new Dimensions(2,2));
        Simulation sim = land.addSimulation("test", 2);

        Layer process = sim.addLayer("process");
        Layer species = sim.addLayer("species");

        Frame process0 = process.getFrame(0);
        Frame process1 = process.getFrame(1);
        Frame species0 = species.getFrame(0);
        Frame species1 = species.getFrame(1);

        String[][] pdata0 = new String[2][2];
        String[][] pdata1 = new String[2][2];
        String[][] sdata0 = new String[2][2];
        String[][] sdata1 = new String[2][2];

        pdata0[0][0] = "fire";
        pdata0[0][1] = "fire";
        pdata0[1][0] = "beetles";
        pdata0[1][1] = "beetles";

        pdata1[0][0] = "fire";
        pdata1[0][1] = "fire";
        pdata1[1][0] = "fire";
        pdata1[1][1] = "fire";

        sdata0[0][0] = "oak";
        sdata0[0][1] = "oak";
        sdata0[1][0] = "oak";
        sdata0[1][1] = "redwood";

        sdata1[0][0] = "oak";
        sdata1[0][1] = "redwood";
        sdata1[1][0] = "redwood";
        sdata1[1][1] = "redwood";

        ColorMap pcolor = land.addColorMap("process");
        ColorMap scolor = land.addColorMap("species");

        pcolor.add("fire", Color.RED);
        pcolor.add("beetles", Color.CYAN);
        scolor.add("oak", Color.YELLOW);
        scolor.add("redwood", Color.BLUE);

        process0.load(pdata0, pcolor, Color.WHITE);
        process1.load(pdata1, pcolor, Color.WHITE);
        species0.load(sdata0, scolor, Color.WHITE);
        species1.load(sdata1, scolor, Color.WHITE);

        main.makeViewer(land, sim);
    }
}
