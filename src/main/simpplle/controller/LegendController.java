package simpplle.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import simpplle.model.*;

import java.io.IOException;
import java.util.Set;

public class LegendController {

    /**
     * Selectable list of different attributes/layers
     */
    @FXML
    private ComboBox<String> comboBox;

    /**
     * Colored legend of values of a particular attribute/layer
     */
    @FXML
    private ListView<Label> listView;

    /**
     * The active viewer window
     */
    private ViewerController viewer;

    /**
     * The active viewer's landscape, needed to get colorMap info
     */
    private Landscape land;

    /**
     * The active viewer's simulation
     */
    private Simulation sim;

    /**
     * The active viewer's timestep
     */
    private int timestep;

    private boolean ignoreAction = false;

    /**
     * Called when an attribute is selected in the comboBox
     * @param event
     */
    @FXML
    void selectAttribute(ActionEvent event) {
        if (!ignoreAction) {
            String attrib = comboBox.getValue();
            setAttrib(attrib, true);
        }
    }

    /**
     * Set the active viewer window
     * @param viewer the active viewer window's controller
     */
    public void setViewer(ViewerController viewer) {
        if (this.viewer == viewer) {
            return;
        }
        if (viewer == null) {
            clear();
            return;
        }
        this.viewer = viewer;

        ignoreAction = true;
        setSimulation(viewer.getLandscape(), viewer.getSimulation());
        timestep = viewer.getTimestep();
        String attrib = viewer.getMapAttrib();
        comboBox.setValue(attrib);
        setAttrib(attrib, false);
        ignoreAction = false;
    }

    /**
     * Set the simulation, populate the attributes comboBox
     * @param land the landscape, needed to get colorMap info
     * @param sim the simulation
     */
    private void setSimulation(Landscape land, Simulation sim) {
        this.land = land;
        this.sim = sim;

        // attribute layers in combo box
        Set<String> layerNames = sim.getLayerNames();
        ObservableList<String> attribs = FXCollections.observableArrayList();
        for (String name: layerNames) {
            attribs.add(name);
        }
        comboBox.setItems(attribs);
    }

    /**
     * Populate the legend with names, colors, and counts
     * @param timestep the timestep needed to get counts
     */
    public void setTimestep(int timestep) {
        this.timestep = timestep;

        String attrib = viewer.getMapAttrib();
        setAttrib(attrib, false);
    }

    private void setAttrib(String attrib, boolean sendToViewer) {
        if (attrib == null) { return; }
        ColorMap colorMap = land.getColorMap(attrib);

        if (sendToViewer) { viewer.setMapAttrib(attrib); }

        int[] counts = sim.getLayer(attrib).getFrame(timestep).histogram(colorMap);

        ObservableList<Label> values = FXCollections.observableArrayList();
        for (int i = 0; i < counts.length; i++) {
            String name = colorMap.getValue(i, "");
            Color color = colorMap.getColor(i, Color.GRAY);
            values.add(makeLabel(name, counts[i], color));
        }
        listView.setItems(values);
    }

    /**
     * Make a colored legend label
     * @param name
     * @param count
     * @param color
     * @return a label
     */
    private Label makeLabel(String name, int count, Color color) {
        Label result = new Label(name + " - " + Integer.toString(count));
        result.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        result.setTextFill(textColor(color));
        result.setMaxWidth(Double.MAX_VALUE);
        result.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() == 2) {
                // set color
                try {
                    // Load a view from an FXML file
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/fxml/SetColor.fxml"));
                    Parent root = loader.load();

                    SetColorController controller = loader.getController();
                    controller.setColor(color);
                    Color newColor = controller.display(root);
                    changeColor(name, newColor);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        return result;
    }

    /**
     * Generate a text color that's readable on a particular background
     * @param bgColor the background color
     * @return the text color
     */
    private Color textColor(Color bgColor) {
        Color result;

        // from http://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
        double a = 1 - (
                (0.299 * bgColor.getRed())
                + (0.587 * bgColor.getGreen())
                + (0.114 * bgColor.getBlue())
        );

        result = (a < 0.5) ? Color.BLACK : Color.WHITE;
        return result;
    }

    private void clear() {
        ignoreAction = true;

        // combobox
        ObservableList<String> attribs = FXCollections.observableArrayList();
        comboBox.setItems(attribs);

        // legend
        ObservableList<Label> values = FXCollections.observableArrayList();
        listView.setItems(values);

        ignoreAction = false;
    }

    private void changeColor(String val, Color newColor) {
        String attrib = comboBox.getValue();
        ColorMap colorMap = land.getColorMap(attrib);

        colorMap.setColor(val, newColor);
        setAttrib(attrib, true);
    }

}
