package simpplle.controller;

import simpplle.io.LavaColorLoader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import simpplle.model.Landscape;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class MenuController {

    @FXML
    private MenuBar menuBar;

    @FXML
    public MenuItem saveColorsMenuItem;

    ///////////////
    // File Menu //
    ///////////////

    private MainController mainWindow;

    @FXML
    void loadMap(ActionEvent event) {

        try {

            // Load a view from an FXML file
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Load.fxml"));
            Parent root = loader.load();

            // Display the view as a dialog
            LoadController controller = loader.getController();
            controller.setMainWindow(mainWindow);
            boolean loadedLandscape = controller.display(root);

            if (loadedLandscape) {
                saveColorsMenuItem.setDisable(false);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void saveColors(ActionEvent event) throws IOException {

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select a Color File");
        chooser.setInitialDirectory(new File(mainWindow.getLastUsedDirectory()));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Color (*.color)","*.color")
        );

        Stage stage = (Stage) menuBar.getScene().getWindow();

        File colorFile = chooser.showSaveDialog(stage);
        if (colorFile == null) {
            return;
        }

        // add file extension if it doesn't exist
        if (!colorFile.getName().endsWith(".color")) {
            colorFile = new File(colorFile.getAbsolutePath() + ".color");
            mainWindow.setLastUsedDirectory(colorFile.getParent());
        }

        // save color information from the landscape
        LavaColorLoader colorLoader;
        Landscape landscape = mainWindow.getLandscape();

        try {
            colorLoader = new LavaColorLoader(colorFile);
            colorLoader.save(landscape);
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Unable to save color file");
            alert.setContentText(colorFile.getAbsolutePath());
            alert.showAndWait();
        }

        // Load missing Colors

        landscape.generateMissingColors();


        mainWindow.setLandscape(landscape);
    }

    @FXML
    void exportImage(ActionEvent event) {

    }

    ///////////////
    // View Menu //
    ///////////////

    @FXML
    void zoomIn(ActionEvent event) {

    }

    @FXML
    void zoomOut(ActionEvent event) {

    }

    @FXML
    void zoomExtents(ActionEvent event) {

    }

    @FXML
    void outline(ActionEvent event) {

    }

    @FXML
    void openView(ActionEvent event) {

    }

    @FXML
    void duplicateView(ActionEvent event) {

    }

    //////////////////
    // Analyze Menu //
    //////////////////

    @FXML
    void inspect(ActionEvent event) {

    }

    @FXML
    void highlight(ActionEvent event) {

    }

    @FXML
    void clearHighlight(ActionEvent event) {

    }

    ///////////////
    // Help Menu //
    ///////////////

    @FXML
    void userGuide(ActionEvent event) {

    }

    @FXML
    void about(ActionEvent event) {

        Alert aboutDialog = new Alert(Alert.AlertType.INFORMATION);
        aboutDialog.setTitle("About LAVA");
        aboutDialog.setHeaderText("Developed by Greg McMann, John Poje, and Robin Lockwood\n" +
                "Students at the University of Montana\n" +
                "Last revision: 2017/05/08");
        aboutDialog.setContentText("Additional credits:\n" +
                "Icons: http://www.fatcow.com/free-icons");
        aboutDialog.show();
    }

    public void setMainWindow(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

}
