package simpplle.controller;

import simpplle.io.ColorMapLoader;
import simpplle.io.LandscapeLoader;
import simpplle.io.LoaderFactory;
import simpplle.io.SimulationLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.*;
import simpplle.model.Landscape;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class LoadController {

    @FXML
    private Button btnAccept;

    @FXML
    private TextField txtColors;

    @FXML
    private TextField txtLandscape;

    @FXML
    private ListView<String> lsvSimulations;

    private Stage stage;

    private ObservableList<String> simulations;

    private MainController mainWindow;

    private boolean loadedLandscape;

    @FXML
    private void initialize() {

        simulations = FXCollections.observableArrayList();

        lsvSimulations.setItems(simulations);
        lsvSimulations.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }

    @FXML
    void accept(ActionEvent event) {

        stage.close();

        // Load the landscape

        Alert loadAlert = new Alert(Alert.AlertType.INFORMATION);
        loadAlert.setTitle("Loading Files");
        loadAlert.setHeaderText("Loading...");
        loadAlert.setContentText("Please wait for simulations to appear in the Simulation List.\n\n" +
                "Closing this dialog doesn't affect the load process.");
        loadAlert.show();
        Landscape landscape = null;
        File landscapeFile = new File(txtLandscape.getText());
        LandscapeLoader loader = LoaderFactory.createLandscapeLoader(landscapeFile);
/**
 *
 */
        try {
            landscape = loader.load();
            loadedLandscape = true;
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Unable to load landscape");
            alert.setContentText(landscapeFile.getAbsolutePath());
            alert.showAndWait();
        }

        // Load simulations into the landscape

        for (String simDirectory : simulations) {

            SimulationLoader simLoader = LoaderFactory.createSimulationLoader(new File(simDirectory), landscapeFile);

            try {
                simLoader.load(landscape);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Unable to load simulation");
                alert.setContentText(simDirectory);
                alert.showAndWait();
            }

        }

        // Load colors into the landscape's color maps

        if (!txtColors.getText().isEmpty()) {

            File colorFile = new File(txtColors.getText());
            ColorMapLoader colorLoader;

            try {
                colorLoader = LoaderFactory.createColorMapLoader(colorFile);
                colorLoader.load(landscape);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Unable to load color file");
                alert.setContentText(colorFile.getAbsolutePath());
                alert.showAndWait();
            }
        }

        // Generate missing Colors

        landscape.generateMissingColors();

        loadAlert.close();

        mainWindow.setLandscape(landscape);


    }

    @FXML
    void cancel(ActionEvent event) {
        stage.close();
    }

    @FXML
    void chooseColors(ActionEvent event) throws IOException {

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select a Color File");
        chooser.setInitialDirectory(new File(mainWindow.getLastUsedDirectory()));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Colors (*.color)","*.color")
        );

        File selection = chooser.showOpenDialog(stage);
        if (selection != null) {
            txtColors.setText(selection.getAbsolutePath());
            mainWindow.setLastUsedDirectory(selection.getParent());
        }
    }

    @FXML
    void chooseLandscape(ActionEvent event) throws IOException {

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select a Landscape File");
        chooser.setInitialDirectory(new File(mainWindow.getLastUsedDirectory()));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("OpenSIMPPLLE (*.attributesall)","*.attributesall")
        );

        File selection = chooser.showOpenDialog(stage);
        if (selection != null) {
            txtLandscape.setText(selection.getAbsolutePath());
            if (!simulations.isEmpty()) {
                btnAccept.setDisable(false);
            }
            mainWindow.setLastUsedDirectory(selection.getParent());
        }
    }

    @FXML
    void addSimulations(ActionEvent event) throws IOException {

        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select a Simulation Directory");
        chooser.setInitialDirectory(new File(mainWindow.getLastUsedDirectory()));

        File selection = chooser.showDialog(stage);
        if (selection != null) {

            if (!simulations.contains(selection.getAbsolutePath())) {
                simulations.add(selection.getAbsolutePath());
                mainWindow.setLastUsedDirectory(selection.getParent());
            }

            FXCollections.sort(simulations);

            if (!txtLandscape.getText().isEmpty()) {
                btnAccept.setDisable(false);
            }
        }
    }

    @FXML
    void removeSimulations(ActionEvent event) {

        List<String> selection = lsvSimulations.getSelectionModel().getSelectedItems();

        simulations.removeAll(selection);

        if (simulations.isEmpty()) {
            btnAccept.setDisable(true);
        }
    }

    boolean display(Parent root) {

        // Create a stage for the dialog
        stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setTitle("Load a Map");
        stage.setMinWidth(root.minWidth(-1));
        stage.setMinHeight(root.minHeight(-1));
        stage.setAlwaysOnTop(true);
        stage.setAlwaysOnTop(false);

        // Show the dialog and wait until closed
        stage.showAndWait();

        return loadedLandscape;

    }

    public void setMainWindow(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

}
