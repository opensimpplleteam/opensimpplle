package simpplle.controller;

import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import simpplle.model.Landscape;
import simpplle.model.Simulation;
import simpplle.view.MapEvent;
import simpplle.view.TileMap;

import java.io.IOException;
import java.util.ArrayList;

public class MainController {

    @FXML
    private MenuController menuController;

    @FXML
    private LegendController legendController;

    @FXML
    private SimulationsController simulationsController;

    @FXML
    private ToolsController toolsController;

    private String lastUsedDirectory = "./";

    private boolean syncNavigation = false;

    ArrayList<ViewerController> viewers;
    ViewerController active;
    Landscape land;

    @FXML
    void initialize() {
        viewers = new ArrayList<>();
        menuController.setMainWindow(this);
        simulationsController.setMainWindow(this);
        toolsController.setMainWindow(this);
    }

    /**
     * Handles a map event sent by a tile map.
     *
     * @param event a map event
     */
    public void handleMapEvent(MapEvent event) {

        if (syncNavigation) {

            TileMap source = (TileMap) event.getSource();

            for (ViewerController controller : viewers) {
                if (controller.tileMap != source) {
                    controller.tileMap.panTo(event.leftOffset, event.topOffset);
                }
            }
        }
    }

    /**
     * Make a new viewer window for the given simulation
     * @param land
     * @param sim
     * @return the viewer window controller
     */
    public ViewerController makeViewer(Landscape land, Simulation sim) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Viewer.fxml"));
            Parent root = loader.load();
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.initModality(Modality.NONE);
            stage.setTitle(sim.runName);
            stage.setScene(new Scene(root));
            stage.setMinWidth(root.minWidth(-1));
            stage.setMinHeight(root.minHeight(-1));
            stage.getIcons().add(new Image("icon/logo_32.png"));
            stage.getIcons().add(new Image("icon/logo_64.png"));

            //Brings the window to the front
            stage.setAlwaysOnTop(true);
            stage.setAlwaysOnTop(false);

            stage.show();

            ViewerController result = loader.getController();
            result.setSimulation(land, sim);
            result.setMainWindow(this);
            result.setLegend(legendController);
            result.tileMap.setHandler(this::handleMapEvent);
            viewers.add(result);
            setActiveViewer(result);

            stage.focusedProperty().addListener((Observable o) -> {
                if (!result.getClosed()) {
                    setActiveViewer(result);
                }
            });

            stage.setOnCloseRequest((WindowEvent e) -> {
                viewers.remove(result);
                result.close();
                if (viewers.isEmpty()) {
                    setActiveViewer(null);
                } else {
                    setActiveViewer(viewers.get(0));
                }
            });

            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Set the topmost viewer window, and update the main window info to match it
     * @param viewer a viewer window controller
     */
    public void setActiveViewer(ViewerController viewer) {
        active = viewer;
        legendController.setViewer(viewer);
        toolsController.setViewer(viewer);
    }

    public void setLandscape(Landscape land) {
        this.land = land;
        simulationsController.setLandscape(land);
    }

    public Landscape getLandscape() { return land; }

    public String getLastUsedDirectory() {
        return lastUsedDirectory;
    }

    public void setLastUsedDirectory(String path) {
        lastUsedDirectory = path;
    }

    public void setSyncNavigation(boolean syncNavigation) {
        this.syncNavigation = syncNavigation;
    }

    public boolean getSyncNavigation() {
        return syncNavigation;
    }

}
