package simpplle.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ToggleButton;

public class NavigationController {

    @FXML
    private ToggleButton btnNavigate;

    @FXML
    private ToggleButton btnZoom;

    @FXML
    private ToggleButton btnInspect;

    @FXML
    void toggleInspection(ActionEvent event) {

    }

    @FXML
    void toggleNavigation(ActionEvent event) {

    }

    @FXML
    void toggleZoom(ActionEvent event) {

    }

    @FXML
    void zoomExtents(ActionEvent event) {

    }

    @FXML
    void zoomIn(ActionEvent event) {

    }

    @FXML
    void zoomOut(ActionEvent event) {

    }

}
