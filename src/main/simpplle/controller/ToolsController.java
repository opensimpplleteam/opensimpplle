package simpplle.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

import java.io.IOException;

public class ToolsController {

    @FXML
    private CheckBox cbxSyncNavigation;

    private MainController mainWindow;
    private ViewerController viewer;

    @FXML
    void syncNavigation(ActionEvent event) {
        mainWindow.setSyncNavigation(cbxSyncNavigation.isSelected());
    }

    public void setMainWindow(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setViewer(ViewerController viewer) {
        this.viewer = viewer;
    }

}
