package simpplle.controller;

import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by John on 4/25/2017.
 */
public class SetColorController {

    @FXML
    Rectangle rect;

    @FXML
    Slider hueSlider;

    @FXML
    Slider satSlider;

    @FXML
    Slider brightSlider;

    Color result, newColor;

    Stage stage;

    @FXML
    private void initialize() {
        hueSlider.setMax(270);
        satSlider.setMax(1);
        brightSlider.setMax(1);
    }

    @FXML
    void submitColor(ActionEvent e) {
        result = newColor;
        stage.close();
    }

    @FXML
    void cancel(ActionEvent e) {
        stage.close();
    }

    public void setColor(Color oldColor) {
        this.result = oldColor;
        this.newColor = oldColor;

        // populate sliders, rect
        rect.setFill(oldColor);
        hueSlider.setValue(oldColor.getHue());
        satSlider.setValue(oldColor.getSaturation());
        brightSlider.setValue(oldColor.getBrightness());

        hueSlider.valueProperty().addListener((Observable v) -> {
            newColor = Color.hsb(hueSlider.getValue(), newColor.getSaturation(), newColor.getBrightness());
            rect.setFill(newColor);
        });

        satSlider.valueProperty().addListener((Observable v) -> {
            newColor = Color.hsb(newColor.getHue(), satSlider.getValue(), newColor.getBrightness());
            rect.setFill(newColor);
        });

        brightSlider.valueProperty().addListener((Observable v) -> {
            newColor = Color.hsb(newColor.getHue(), newColor.getSaturation(), brightSlider.getValue());
            rect.setFill(newColor);
        });
    }

    Color display(Parent root) {

        // Create a stage for the dialog
        stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setTitle("Set Color");
        stage.setMinWidth(root.minWidth(-1));
        stage.setMinHeight(root.minHeight(-1));

        // Show the dialog and wait until closed
        stage.showAndWait();

        return result;
    }
}
