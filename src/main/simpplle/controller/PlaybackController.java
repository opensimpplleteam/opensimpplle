package simpplle.controller;

import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import simpplle.model.Simulation;

public class PlaybackController {

    @FXML
    private Button playPauseButton;

    @FXML
    private Slider speedSlider;

    @FXML
    private TextField timestepText;

    private ViewerController viewer;

    private Simulation sim;

    private int timestep;

    private boolean animating = false;

    @FXML
    private void initialize() {
        timestep = 0;
        speedSlider.valueProperty().addListener((Observable o) -> {
            viewer.setSpeed(speedSlider.getValue());
        });
    }

    @FXML
    void goToFirst(ActionEvent event) {
        setTimestep(0);
    }

    @FXML
    void goToLast(ActionEvent event) {
        setTimestep(sim.getNumFrames()-1);
    }

    @FXML
    void goToNext(ActionEvent event) {
        setTimestep(timestep + 1);
    }

    @FXML
    void goToPrevious(ActionEvent event) {
        setTimestep(timestep - 1);
    }

    @FXML
    void playPause(ActionEvent event) {
        animating = !animating;

        if (animating) {
            viewer.playAnim();
        } else {
            viewer.stopAnim();
        }
    }

    public void setViewer(ViewerController viewer) {
        this.viewer = viewer;
    }

    public void setSimulation(Simulation sim) {
        this.sim = sim;
    }

    private void setTimestep(int timestep) {
        timestep = timestep % sim.getNumFrames();
        if (timestep < 0) {
            timestep = timestep + sim.getNumFrames();
        }
        this.timestep = timestep;
        viewer.setTimestep(timestep);
        timestepText.setText("" + timestep);
    }

}
