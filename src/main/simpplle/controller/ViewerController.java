package simpplle.controller;

import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import simpplle.model.Landscape;
import simpplle.view.AnimMap;
import simpplle.view.TileMap;
import simpplle.model.Dimensions;
import simpplle.model.Layer;
import simpplle.model.Simulation;

/**
 * The map window, containing a map pane and some playback controls.
 */
public class ViewerController {

    @FXML
    private Pane pane;

    @FXML
    private PlaybackController playbackController;

    @FXML
    private NavigationController navigationController;

    private MainController mainWindow;
    private LegendController legend;

    /**
     * A tile map for drawing the current frame
     */
    public TileMap tileMap;

    private AnimMap animMap;

    /**
     * The landscape associated with this view
     */
    private Landscape land;

    /**
     * The simulation associated with this view
     */
    private Simulation simulation;

    /**
     * The name of the current attribute/layer
     */
    private String attrib;

    /**
     * The current time step
     */
    private int timestep;

    /**
     * The current layer
     */
    private Layer layer;

    private boolean isClosed = false;

    @FXML
    private void initialize() {

        tileMap = new TileMap(1000, 1000, 50, 50);
        animMap = new AnimMap();

        pane.getChildren().add(tileMap);
        pane.getChildren().add(animMap);

        animMap.setVisible(false);

        // resize
        pane.widthProperty().addListener(e -> {
            tileMap.resizeMap(pane.getWidth(), pane.getHeight());
            animMap.setSize(pane.getWidth(), pane.getHeight());
        });
        pane.heightProperty().addListener(e -> {
            tileMap.resizeMap(pane.getWidth(), pane.getHeight());
            animMap.setSize(pane.getWidth(), pane.getHeight());
        });

        playbackController.setViewer(this);
    }

    public Landscape getLandscape() {
        return land;
    }

    public Simulation getSimulation() { return simulation; }

    public void setSimulation(Landscape land, Simulation sim) {
        this.land = land;
        this.simulation = sim;

        attrib = simulation.getLayerNames().iterator().next();
        setMapAttrib(attrib);

        playbackController.setSimulation(simulation);
    }

    public void setMapAttrib(String attrib) {
        this.attrib = attrib;
        layer = simulation.getLayer(attrib);
        tileMap.setFrame(layer.getFrame(timestep), land.getColorMap(attrib));
        animMap.setViewer(this);
    }

    public String getMapAttrib() { return attrib; }

    public void setTimestep(int timestep) {
        this.timestep = timestep;
        tileMap.setFrame(layer.getFrame(timestep), land.getColorMap(attrib));
        legend.setTimestep(timestep);
    }

    public int getTimestep() { return timestep; }

    public void setMainWindow(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setLegend(LegendController legend) {
        this.legend = legend;
    }

    public void playAnim() {
        tileMap.setVisible(false);
        animMap.setVisible(true);
        animMap.start();
    }

    public void stopAnim() {
        animMap.setVisible(false);
        tileMap.setVisible(true);
        animMap.stop();
    }

    public void close() { isClosed = true; }

    public boolean getClosed() { return isClosed; }

    public void setSpeed(double speed) {
        animMap.setSpeed(speed);
    }

}
