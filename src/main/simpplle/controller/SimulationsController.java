package simpplle.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import simpplle.model.Landscape;

import java.util.Set;

public class SimulationsController {

    /**
     * Clickable list of simulation names
     */
    @FXML
    private ListView<Label> listView;

    /**
     * The main tool window, so we can create new viewer windows
     */
    private MainController mainWindow;

    /**
     * The loaded landscape
     */
    private Landscape land;

    /**
     * Populate the simulation list for the given landscape
     * @param land
     */
    public void setLandscape(Landscape land) {
        this.land = land;

        Set<String> names = land.getSimulationNames();

        ObservableList<Label> sims = FXCollections.observableArrayList();
        for (String name: names) {
            Label l = new Label(name);
            l.setOnMouseClicked((MouseEvent e) -> {
                if (e.getClickCount() == 2) {
                    // make a new viewer window
                    String selected = ((Label) e.getSource()).getText();
                    mainWindow.makeViewer(land, land.getSimulation(selected));
                }
            });
            l.setMaxWidth(Double.MAX_VALUE);
            sims.add(l);
        }

        listView.setItems(sims);
    }

    /**
     * Set the main tool window
     * @param mainWindow the main window controller
     */
    public void setMainWindow(MainController mainWindow) {
        this.mainWindow = mainWindow;
    }

}
