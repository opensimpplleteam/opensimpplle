package simpplle.comcode;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An INI file stores grouped key-value pairs in plain text. Each line contains a group name, a
 * key-value pair, or a comment. Group names are surrounded by square brackets. Keys and values are
 * separated by an equals sign and zero or more white space characters. Comments begin with a
 * semicolon. Group names and the keys within each group must be unique.
 *
 * <pre>
 *   [Zone]
 *   name = Westside Region One
 *
 *   [Area]
 *   name = Sweathouse Creek
 *   acres = 5668000
 *   hasAquatic = true
 *   hasLand = false
 *   hasRoads = false
 *   hasTrails = false
 *   hasVegetation = true
 * </pre>
 *
 * References
 * https://en.wikipedia.org/wiki/INI_file
 * https://stackoverflow.com/questions/190629/what-is-the-easiest-way-to-parse-an-ini-file-in-java
 */
public class Ini {

  private Pattern sectionRegex = Pattern.compile("\\s*\\[(.*)\\]");
  private Pattern keyRegex = Pattern.compile("(.*?)=(.*)");
  private Map<String, Map<String, String>> entries = new LinkedHashMap<>();

  public Ini() {}

  public Ini(File file) throws IOException {
    load(file);
  }

  public Ini(InputStream stream) throws IOException {
    load(stream);
  }

  public void load(File file) throws IOException {
    try (InputStream stream = new FileInputStream(file)) {
      load(stream);
    }
  }

  public void load(InputStream stream) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(stream));
    String line;
    String section = null;
    while ((line = br.readLine()) != null) {
      if (line.startsWith(";")) continue;
      Matcher m = sectionRegex.matcher(line);
      if (m.matches()) {
        section = m.group(1).trim();
      } else if (section != null) {
        m = keyRegex.matcher(line);
        if (m.matches()) {
          String key = m.group(1).trim();
          String value = m.group(2).trim();
          putString(section, key, value);
        }
      }
    }
  }

  public void save(File file) throws IOException {
    try (Writer writer = new FileWriter(file)) {
      save(writer);
    }
  }

  public void save(Writer writer) throws IOException {
    for (String section : sections()) {
      writer.write("\n[" + section + "]\n\n");
      for (Map.Entry<String, String> entry : entries.get(section).entrySet()) {
        writer.write(entry.getKey() + " = " + entry.getValue() + "\n");
      }
    }
    writer.flush();
  }

  public boolean hasKey(String section, String key) {
    Map<String, String> map = entries.get(section);
    if (map == null) {
      return false;
    } else {
      return map.get(key) != null;
    }
  }

  public boolean hasSection(String section) {
    return entries.get(section) != null;
  }

  public Set<String> keys(String section) {
    Map<String, String> map = entries.get(section);
    if (map == null) {
      return null;
    } else {
      return map.keySet();
    }
  }

  public Set<String> sections() {
    return entries.keySet();
  }

  public boolean getBoolean(String section, String key, boolean defaultValue) {
    return Boolean.parseBoolean(getString(section, key, Boolean.toString(defaultValue)));
  }

  public byte getByte(String section, String key, byte defaultValue) {
    return Byte.parseByte(getString(section, key, Byte.toString(defaultValue)));
  }

  public char getChar(String section, String key, char defaultValue) {
    return getString(section, key, Character.toString(defaultValue)).charAt(0);
  }

  public double getDouble(String section, String key, double defaultValue) {
    return Double.parseDouble(getString(section, key, Double.toString(defaultValue)));
  }

  public float getFloat(String section, String key, float defaultValue) {
    return Float.parseFloat(getString(section, key, Float.toString(defaultValue)));
  }

  public int getInteger(String section, String key, int defaultValue) {
    return Integer.parseInt(getString(section, key, Integer.toString(defaultValue)));
  }

  public long getLong(String section, String key, long defaultValue) {
    return Long.parseLong(getString(section, key, Long.toString(defaultValue)));
  }

  public short getShort(String section, String key, short defaultValue) {
    return Short.parseShort(getString(section, key, Short.toString(defaultValue)));
  }

  public String getString(String section, String key, String defaultValue) {
    Map<String, String> map = entries.get(section);
    if (map == null) return defaultValue;
    return map.getOrDefault(key, defaultValue);
  }

  public void putBoolean(String section, String key, boolean value) {
    putString(section, key, Boolean.toString(value));
  }

  public void putByte(String section, String key, byte value) {
    putString(section, key, Byte.toString(value));
  }

  public void putChar(String section, String key, char value) {
    putString(section, key, Character.toString(value));
  }

  public void putDouble(String section, String key, double value) {
    putString(section, key, Double.toString(value));
  }

  public void putFloat(String section, String key, float value) {
    putString(section, key, Float.toString(value));
  }

  public void putInteger(String section, String key, int value) {
    putString(section, key, Integer.toString(value));
  }

  public void putLong(String section, String key, long value) {
    putString(section, key, Long.toString(value));
  }

  public void putShort(String section, String key, short value) {
    putString(section, key, Short.toString(value));
  }

  public void putString(String section, String key, String value) {
    Map<String, String> map = entries.get(section);
    if (map == null) {
      map = new LinkedHashMap<>();
      entries.put(section, map);
    }
    map.put(key, value);
  }
}
