package simpplle.comcode;

/**
 * Defines an area's type. The type refers to the location of the data and if simulation data is
 * associated with the area.
 */
public enum AreaType {

  SAMPLE,
  SIMULATED,
  USER

}
