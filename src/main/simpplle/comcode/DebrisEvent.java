/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class contains methods for Debris Event, a type of Process.
 * This process occurs in all regions therefore none of the doProbability or doSpread methods are default set to 0 and false respectively. 
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class DebrisEvent extends Process {
  private static final String printName = "DEBRIS-EVENT";

  /**
   * Constructor.  initializes variables inherited from Process superclass.
   */
  public DebrisEvent() {
    super();

    spreading   = false;
    description = "DEBRIS EVENT";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }
/**
 * outputs "DEBRIS-EVENT"
 */
  public String toString() { return printName; }

}
