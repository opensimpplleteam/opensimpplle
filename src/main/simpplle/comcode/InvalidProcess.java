/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class is used to create an initialProcess from a process name
 * that is not valid.  This allows the user to later correct the Evu's
 * process to a valid one.
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class InvalidProcess extends Process {
  private static final String printName = "UNKNOWN";
  private String name;

  /**
   * InvalidProcess constructor.  Inherits from Process superclass and initializes spreading to false, name and description to "UNKNOWN"
   * 
   */
  public InvalidProcess() {
    super();

    spreading   = false;
    name        = "UNKNOWN";
    description = "Unknown";
  }

  /**
   * Overloaded constructor which references default, primary constructor 
   * @param processType new process type to be entered
   * @param name new name of process 
   */
  public InvalidProcess(ProcessType processType, String name) {
    this();
    this.name        = name;
    this.processType = processType;
  }

  /**
   * outputs "UNKNOWN"
   */
  public String toString () {
    return printName;
  }
}