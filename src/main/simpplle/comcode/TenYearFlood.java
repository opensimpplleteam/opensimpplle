/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/** 
 * This class contains methods for Ten Year Flood, a type of process.
 * Since this method like many of the flood processes can occur in any zone, there 
 * are methods for all regional zones. 
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 * 
 * 
 * @see simpplle.comcode.Process
 */

public class TenYearFlood extends Process {
  private static final String printName = "TEN-YEAR-FLOOD";
/**
 * Constructor for ten year flood.  Inherits from process superclass and initializes spreading to false and sets default visible columns for row and probability.  
 */
  public TenYearFlood() {
    super();

    spreading   = false;
    description = "TEN YEAR FLOOD";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  /**
   * outputs  "TEN-YEAR-FLOOD"
   */
  public String toString() { return printName; }

}
