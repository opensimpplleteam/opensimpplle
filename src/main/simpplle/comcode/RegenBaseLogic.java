/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import simpplle.comcode.RegenerationLogic.DataKinds;

import static simpplle.comcode.RegenerationData.SPECIES_CODE_COL;
import static simpplle.comcode.RegenerationLogic.FIRE;
import static simpplle.comcode.RegenerationLogic.SUCCESSION;
import static simpplle.comcode.SystemKnowledge.Kinds.REGEN_LOGIC_FIRE;
import static simpplle.comcode.SystemKnowledge.Kinds.REGEN_LOGIC_SUCC;

/**
 * RegenBaseLogic holds tabular logic relevant to vegetation regeneration.
 */
public class RegenBaseLogic extends BaseLogic {

  private DataKinds kind;

  /**
   * Creates regeneration logic.
   *
   * @param kind the kind of regeneration logic
   */
  public RegenBaseLogic(DataKinds kind) {

    super(new String[] {kind.toString()});

    this.kind = kind;

    switch (kind) {

      case FIRE:
        sysKnowKind = REGEN_LOGIC_FIRE;
        break;

      case SUCCESSION:
        sysKnowKind = REGEN_LOGIC_SUCC;
        break;

    }

    //showColumn(kind.toString(), ROW_COL);
    //showColumn(kind.toString(), SIZE_CLASS_COL);
    //showColumn(kind.toString(), DENSITY_COL);

    addColumn(kind.toString(), "Species");
    showColumn(kind.toString(), SPECIES_CODE_COL);

    if (kind == FIRE) {

      for (String name : FireRegenerationData.getColumnNames()) {
        addColumn(kind.toString(), name);
      }

      for (int column : FireRegenerationData.getColumns()) {
        showColumn(kind.toString(), column);
      }

    } else if (kind == SUCCESSION) {

      for (String name : SuccessionRegenerationData.getColumnNames()) {
        addColumn(kind.toString(), name);
      }

      for (int column : SuccessionRegenerationData.getColumns()) {
        showColumn(kind.toString(), column);
      }
    }
  }

  public void addRow(int insertPos, String kind) {
    RegenerationLogic.addDataRow(insertPos,DataKinds.valueOf(kind));
  }

  public void duplicateRow(int row, int insertPos, String kind) {

    AbstractLogicData newRow = getAllRows(kind).get(row);
    if (newRow == null){
      return;
    }
    super.addRow(insertPos, kind, newRow.duplicate());
  }

  public String getColumnName(String kind, int col) {
    return RegenerationLogic.getColumnName(DataKinds.valueOf(kind),col);
  }

  public int getColumnNumFromName(String name) {
    int index = RegenerationLogic.getColumnNumFromName(kind,name);
    if (index == -1) {
      index = super.getColumnNumFromName(name);
    }
    return index;
  }

}
