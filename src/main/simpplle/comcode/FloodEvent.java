/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class contains methods for Flood Event, a type of Process.
 * Unlike Flood process all regions can have a Flood Event.  Although they all return 0 for probability  and false for spread.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *  
 * @see simpplle.comcode.Process
 */

public class FloodEvent extends Process {
  private static final String printName = "FLOOD-EVENT";

  public FloodEvent() {
    super();

    spreading   = false;
    description = "FLOOD EVENT";
  }

  public String toString() { return printName; }

}