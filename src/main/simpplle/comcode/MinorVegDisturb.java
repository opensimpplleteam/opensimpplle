/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class contains methods for Minor Vegetation Disturbance, a type of Process.  This can happen in all regions,
 * therefore it has overloaded doProbability and doSpread methods for all zones, as well as doProbabilityCommon and doSpreadCommon for the evu
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class MinorVegDisturb extends Process {
  private static final String printName = "MINOR-VEG-DISTURB";
/**
 * Constructor for Minor Vegetative Disturbance.  Inherites from Process superclass and initializes spreading to false, sets description and default visible columns.  
 */
  public MinorVegDisturb() {
    super();

    spreading   = false;
    description = "MINOR VEG DISTURB";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }
/**
 * Returns "MINOR VEG DISTURB"
 */
  public String toString() { return printName; }

}
