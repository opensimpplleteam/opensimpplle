/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class has methods for Bark Beetles, a type of Process
 * 
 * @author Documentation by Brian Losi
 * <p> Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class BarkBeetles extends Process {
  private static final String printName = "BARK-BEETLES";
  public BarkBeetles () {
    super();

    spreading   = false;
    description = "Bark Beetles";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  /**
   * Passes to the doProbability method of simpplle.comcode.Process.doProbability(Evu evu) where it will do the Bark Beetle probability
   * for a specified Evu.
   */
  public int doProbability (WestsideRegionOne zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }
  /**
   * Passes to the doProbability method of simpplle.comcode.Process.doProbability(Evu evu) where it will do the Bark Beetle probability
   * for a specified Evu.
   */
  public int doProbability (EastsideRegionOne zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }
  /**
   * Passes to the doProbability method of simpplle.comcode.Process.doProbability(Evu evu) where it will do the Bark Beetle probability
   * for a specified Evu.
   */
  public int doProbability (Teton zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }
  /**
   * Passes to the doProbability method of simpplle.comcode.Process.doProbability(Evu evu) where it will do the Bark Beetle probability
   * for a specified Evu.
   */
  public int doProbability (NorthernCentralRockies zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

  /**
   * to string returns "BARK-BEETLES"
   */
  
  public String toString () {
    return printName;
  }
}

