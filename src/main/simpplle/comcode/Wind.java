package simpplle.comcode;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Wind implements Externalizable {

  /**
   * Wind speed is in meters per second (m/s)
   */
  private double speed;

  /**
   * Direction (Azimuth) that the wind is coming from
   */
  private double direction;

  public Wind () {
    speed = 0.0;
    direction = 0.0;
  }

  public Wind(double speed, double direction) {
    this.speed = speed;
    this.direction = direction;
  }

  // Getters and setters

  public double getSpeed() {
    return speed;
  }

  public void setSpeed(double speed) {
    this.speed = speed;
  }

  public double getDirection() {
    return direction;
  }

  public void setDirection(double direction) {
    this.direction = direction;
  }

  // Interface methods

  /**
   * The object implements the writeExternal method to save its contents
   * by calling the methods of DataOutput for its primitive values or
   * calling the writeObject method of ObjectOutput for objects, strings,
   * and arrays.
   *
   * @param out the stream to write the object to
   * @throws java.io.IOException Includes any I/O exceptions that may occur
   * @serialData Saves speed and direction
   */
  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    out.writeDouble(speed);
    out.writeDouble(direction);
  }

  /**
   * The object implements the readExternal method to restore its
   * contents by calling the methods of DataInput for primitive
   * types and readObject for objects, strings and arrays.  The
   * readExternal method must read the values in the same sequence
   * and with the same types as were written by writeExternal.
   *
   * @param in the stream to read data from in order to restore the object
   * @throws java.io.IOException    if I/O errors occur
   * @throws ClassNotFoundException If the class for an object being
   *                                restored cannot be found.
   */
  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    speed = in.readDouble();
    direction = in.readDouble();
  }
}
