/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class contains methods for Flood, a type of Process.
 * The following regions do not have this process: Westside Region1, Eastside Region 1, Sierra Nevada, Southern California, Gila
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 * 
 * @see simpplle.comcode.Process
 */

public class Flood extends Process {
  private static final String printName = "FLOOD";
 /**
  * constructor.  initializes spreading to false and description to flood and references superclass Process
  */
  public Flood() {
    super();

    spreading   = false;
    description = "Flood";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbability (SouthCentralAlaska zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

  /**
   * outputs "FLOOD"
   */
  public String toString () {
    return printName;
  }

}
