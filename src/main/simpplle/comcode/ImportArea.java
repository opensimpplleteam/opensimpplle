/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.io.*;

/**
 * The purpose of this class is to read an input file
 * that provides information needed to create a new area.
 * <p>This class handles all aspects of creating the new area
 * including verifying and correcting states if necessary.
 */

public class ImportArea {

  private static final int EVU = 0;
  private static final int ELU = 1;
  private static final int ERU = 2;
  private boolean hasAttributes;

  public boolean attributesAdded() {
    return hasAttributes;
  }
  /**
   * Check to see if this has both a row & a column.
   * Note: if Field #5 is a number than we have row & col, however if it is a string it is not a row & col
   * @param line
   * @return true if have both a row and column (field #5 is a number), false if do not have a row & col (field #5 is a string)
   * @throws ParseError is caught in GUI, if trouble parsing line, however if it throws a parse error on field #5 while expecting a number it means we have a string which is caught here
   */
  private boolean hasRowCol(String line) throws ParseError {

    StringTokenizerPlus strTok = new StringTokenizerPlus(line,",");

    // If field #5 is a number than we have row & col,
    // however if it is a string we do not have row & col.
    String str;
    try {
      str = strTok.getToken();
      str = strTok.getToken();
      str = strTok.getToken();
      str = strTok.getToken();
    }
    catch (ParseError e) {
      throw new ParseError(e.getMessage() + "\nError parsing line: " + line);
    }

    try {
      // Field #5: Either ACRES or SPECIES
      str = strTok.getToken();
      if (str == null) {
        throw new ParseError("Not enough fields in line: " + line);
      }
      float value = Float.valueOf(str);
      // if no exception, than field #5 was a number.
      return true;
    }
    catch (NumberFormatException err) {
      return false;
    }
  }
  /**
   * Checks to see if the string passed contains digits throughout.  If so it substrings from 0 to index-1
   * @param str string to be checked if has digits
   * @return a digit in string form, designates size class
   */
  private String parseSizeClass(String str) {
    if (str == null) { return str; }

    boolean foundNumber = false;

    int i = 0;
    while (i < str.length() && !foundNumber) {
      foundNumber = Character.isDigit(str.charAt(i));
      i++;
    }

    if (foundNumber) {
      return str.substring(0,(i-1));
    }
    else { return str; }
  }
  /**
   * Parses a passed string which is converted to an int and designates age
   * @param size
   * @return the age parsed from passed string
   * @throws NumberFormatException - caught here, used to designate if age is 1
   */
  private int parseAge(String size) {
    if (size == null) { return 1; }

    boolean foundNumber = false;
    int     age = 1;

    int i = 0;
    while (i < size.length() && !foundNumber) {
      foundNumber = Character.isDigit(size.charAt(i));
      i++;
    }

    if (foundNumber) {
      i--;
      try {
        age = Integer.parseInt(size.substring(i));
      }
      catch (NumberFormatException NFE) {
        age = 1;
      }
    }
    return age;
  }
  /**
   *
   *  Attributes of an area, they are stored in the file in the following order:
   * +slink, ++row#, ++col#, +unit#, +acres, +htgrp, +species, +size class,
   * +density, ownership, road status, ignition prob,
   * fmz, special area, landtype, initial process, initial Treatment
   * Longitude, Latitude.
   * +  = Required Field
   * ++ = Required only in GRASSLAND Zone.
   *
   * @param area whose attributes are being read in.
   * @param fin
   * @param logFile
   * @throws ParseError line where parse error occurred is added in message caught in GUI
   * @throws IOException caught in GUI
   */
  private void readAttributes(Area area, BufferedReader fin, PrintWriter logFile) throws ParseError, IOException {


    String              line, str;
    StringTokenizerPlus strTok;
    int                 value, count;
    float               fValue;
    Evu                 evu;
    RegionalZone        zone = Simpplle.getCurrentZone();
    HabitatTypeGroup    htGrp;
    VegetativeType      state = null;
    String              speciesStr, sizeClassStr, densityStr;
    int                 age, id;
    ProcessType         process;
    Treatment           treatment;
    TreatmentType       treatType;
    Fmz                 fmz;
    StringBuffer        strBuf;
    int                 begin, index;
    int                 numReqFields = -1;
    boolean             hasRowCol=false;

    boolean             processedAsMultipleLife=false;

    line = fin.readLine();
    if (line == null) {
      logFile.println("No Attribute data found in input file.");
      logFile.println();
      hasAttributes = false;
      return;
    }

    while(line != null && !line.trim().equals("END")) {
      try {
        line = line.trim();
        if (line.length() == 0) { line = fin.readLine(); continue; }

        line = Utility.preProcessInputLine(line);

        if (numReqFields == -1) {
          // Find out if we have row and col.
          hasRowCol = hasRowCol(line);
          numReqFields = (hasRowCol) ? 9 : 7;
        }

        strTok = new StringTokenizerPlus(line,",");
        count  = strTok.countTokens();
        if (count < numReqFields) {
          logFile.println(line);
          logFile.println("   Not enough fields in attribute data.");
          logFile.println("   Required fields are:");
          if (zone.getId() == ValidZones.GRASSLAND) {
            logFile.println("     slink, row #, col #, unit #, acres,");
            logFile.println("     habitat type group, species, size class, density");
          }
          else {
            logFile.println("     slink, unit #, acres,");
            logFile.println("     habitat type group, species, size class, density");
            logFile.println("   note: (row, col) can optionally be placed after slink:");
            logFile.println("    (e.g. slink, row, col, unit #, acres, ...");
          }
          logFile.println();
          line = fin.readLine();
          continue;
        }

        // Get the evu id
        id = strTok.getIntToken();
        if (id == -1) {
          logFile.println(line);
          logFile.println("  Invalid Slink");
          logFile.println();
          continue;
        }
        evu = area.getEvu(id);
        if (evu == null) {
          logFile.println(line);
          logFile.println("  An Evu with the id: " + id + " does not exist.");
          logFile.println();
          line = fin.readLine();
          continue;
        }

        if (hasRowCol) {
          // get the Location
          // Note: not all areas will have x,y values
          //       so we don't really need to check anything.
          //       Also, location is not really used right now.
          int row, col;
          row = Math.round(strTok.getFloatToken());
          col = Math.round(strTok.getFloatToken());

          evu.setLocationX(col);

          evu.setLocationY(row);
        }

        // Get the Unit Number (which is actually a string)
        str = strTok.getToken();
        if (str == null) {
          logFile.println(line);
          logFile.println("  In Evu-" + id + ":");
          logFile.println("  The Unit number is missing (a required field)");
          logFile.println("  The value \"UNKNOWN\" was assigned");

          str = "UNKNOWN";
        }
        str = str.trim();
        evu.setUnitNumber(str);

        // Get the Acres
        fValue  = strTok.getFloatToken();
        evu.setAcres(fValue);
        // Acres check is done later, because we need to know species for check.

        // Get the Habitat Type Group
        str = strTok.getToken();
        str = (str != null) ? str.trim().toUpperCase() : "UNKNOWN";
        htGrp = HabitatTypeGroup.findInstance(str);
        if (htGrp == null) {
          logFile.println(line);
          logFile.println("  In Evu-" + id + ":");
          logFile.println("  " + htGrp + " is not a valid Ecological Grouping.");
          logFile.println("  The value \"" + str + "\" was assigned");
          logFile.println();

          // Need to set this anyway so that user can correct it later.
          evu.setHabitatTypeGroup(new HabitatTypeGroup(str));
        }
        else {
          evu.setHabitatTypeGroup(htGrp);
        }

        // Get the species, size class, and density;
        boolean newFileFormat=false;
        speciesStr = strTok.getToken();
        if (speciesStr != null && speciesStr.equals("#")) {
          processedAsMultipleLife = true;
          area.setDisableMultipleLifeforms(false);
          readMultiLifeformState(logFile,strTok,line,evu,htGrp);
          newFileFormat = true;
        }
        else {

          if (speciesStr == null) {
            speciesStr = "UNKNOWN";
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Species value is missing.");
            logFile.println("  The value \"UNKNOWN\" was assigned");
            logFile.println();
          }
          else {
            speciesStr = speciesStr.trim().toUpperCase();
          }

          str = strTok.getToken();
          if (str != null) {
            str = str.trim().toUpperCase();
          }
          sizeClassStr = parseSizeClass(str);
          if (sizeClassStr == null) {
            sizeClassStr = "UNKNOWN";
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Size Class value is missing.");
            logFile.println("  The value \"UNKNOWN\" was assigned");
            logFile.println();
          }
          age = parseAge(str);

          densityStr = strTok.getToken();
          if (densityStr == null) {
            densityStr = "UNKNOWN";
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Density value is missing.");
            logFile.println("  The value \"UNKNOWN\" was assigned");
            logFile.println();
          }
          Species species = Species.get(speciesStr);
          if (species == null) {
            species = new Species(speciesStr);
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Species \"" + speciesStr +
                "\" is unknown");
          }

          SizeClass sizeClass = SizeClass.get(sizeClassStr);
          if (sizeClass == null) {
            sizeClass = new SizeClass(sizeClassStr, Structure.NON_FOREST);
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Size Class \"" + sizeClassStr +
                "\" is unknown");
          }

          Density density = Density.get(densityStr);
          if (density == null) {
            density = new Density(densityStr);
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Density \"" + densityStr +
                "\" is unknown");
          }

          state = null;
          if (species != null && sizeClass != null && density != null && htGrp != null) {
            state = htGrp.getVegetativeType(species, sizeClass, age, density);
          }
          if (state == null) {
            logFile.println(line);
            logFile.println("  In Evu-" + id + " Could not build a valid state.");
            logFile.println("  One or more of the following must be invalid:");
            logFile.println(
                "  Species, Size Class, Density, or Ecological Grouping");
            logFile.println();

            // Even invalid states need to be set in the evu, in order to
            // allow for later correction.
            state = new VegetativeType(species, sizeClass, age, density);
            evu.setInitialVegetativeType(state, Season.YEAR);
          }
          else {
            evu.setInitialVegetativeType(state, Season.YEAR);
          }

          // In case there is not Initial Process.
          evu.setInitialProcess(Evu.getDefaultInitialProcess());
        }

        // Acres check depends on species being available.
        if (evu.isAcresValid() == false) {
          logFile.println(line);
          logFile.println("  In Evu-" + id + " Acres is Invalid");
          logFile.println("  Acres must be a value greater than 0.0");
          logFile.println("    - acres can be equal to 0 if species=ND");
          logFile.println();
        }

        // End of required fields
        if (count == numReqFields) { line = fin.readLine(); continue; }

        // Get the Ownership information (if any)
        str = strTok.getToken();
        evu.setOwnership(str);
        if (count == (numReqFields+1)) { line = fin.readLine(); continue; }

        // Get the Road Status information (if any)
        str = strTok.getToken();
        if (str != null) { str = str.trim().toUpperCase(); }

        if (str == null) {
          evu.setRoadStatus(Roads.Status.UNKNOWN);
        }
        else {
          evu.setRoadStatus(Roads.Status.lookup(str));
        }
        if (count == (numReqFields+2)) { line = fin.readLine(); continue; }

        // Parse Ignition Prob
        // value not used anymore.
        strTok.getToken();
        evu.setIgnitionProb(0);

        if (count == (numReqFields+3)) { line = fin.readLine(); continue; }

        // Get the Fmz
        str = strTok.getToken();
        if (str == null) {
          fmz = zone.getDefaultFmz();
        }
        else {
          fmz = zone.getFmz(str);
        }
        if (fmz == null) {
          logFile.println(line);
          logFile.println("  In Evu-" + id + ":");
          logFile.println("  " + str + " is not a valid Fire Management Zone.");
          logFile.println();

          evu.setFmz(new Fmz(str));
        }
        else {
          evu.setFmz(fmz);
        }
        if (count == (numReqFields+4)) { line = fin.readLine(); continue; }

        // Get the Special Area
        str = strTok.getToken();
        evu.setSpecialArea(str);
        if (count == (numReqFields+5)) { line = fin.readLine(); continue; }

        // Read Landtype
        str = strTok.getToken();
        evu.setLandtype(str);
        if (count == (numReqFields+6)) { line = fin.readLine(); continue; }

        if (!newFileFormat) {
          // Read Initial Process
          str = strTok.getToken();
          if (str == null) {
            str = Evu.getDefaultInitialProcess().toString();
          }
          process = ProcessType.get(str.toUpperCase());
          if (process == null) {
            logFile.println(line);
            logFile.println("  In Evu-" + id + ":");
            logFile.println("  " + str + " is not a Process.");
            logFile.println();

            evu.setInitialProcess(str);
          }
          else {
            evu.setInitialProcess(process);
          }
          if (count == (numReqFields + 7)) { line = fin.readLine(); continue; }

          // Read Initial Treatment
          str = strTok.getToken();
          if (str != null) {
            treatType = TreatmentType.get(str.toUpperCase());
            if (treatType == null) {
              logFile.println(line);
              logFile.println("  In Evu-" + id + ":");
              logFile.println("  " + str + " is not a Treatment.");
              logFile.println();
            }
            else {
              // TODO need to update state later if it is an invalid one. */
              // TODO carry process & treatment as initial when make sim ready */
              treatment = Treatment.createInitialTreatment(treatType, state);
              evu.addTreatment(treatment);
            }
          }
        }
        else {
          double longitude = strTok.getDoubleToken(Double.NaN); // POINT_X Field
          evu.setLongitude(longitude);
          if (count == (numReqFields + 7)) { line = fin.readLine(); continue; }

          double latitude = strTok.getDoubleToken(Double.NaN); // POINT_Y Field
          evu.setLatitude(latitude);
        }

        // Get the Next Line.
        line = fin.readLine();
      }
      catch (ParseError err) {
        String msg = "The following error occurred in this line: \n" +
            line + "\n" + err.getMessage();
        throw new ParseError(msg);
      }
    }
    area.updateArea();

    area.setMultipleLifeformStatus();
    if ((area.hasMultipleLifeforms() == false) &&
        processedAsMultipleLife) {
      // Need to change Evu's to be single lifeform;
      Evu[] evus = area.getAllEvu();
      for (int i=0; i<evus.length; i++) {
        if (evus[i] != null) { evus[i].makeSingleLife(); }
      }
    }
  }
  /**
   * Reads in the Multi Life Form State.  Sets species, size Class, and density
   * @param logFile
   * @param strTok
   * @param line
   * @param evu
   * @param htGrp
   * @throws ParseError caught in GUI
   */
  private void readMultiLifeformState(PrintWriter logFile,
                                      StringTokenizerPlus strTok,
                                      String line, Evu evu,
                                      HabitatTypeGroup htGrp) throws ParseError {
    String              str;
    VegetativeType      vegState = null;
    String              speciesStr, sizeClassStr=null, densityStr;
    int                 age=1;
    ProcessType         process;
    Treatment           treatment;
    TreatmentType       treatType;
    boolean             emptyState=false;

    int numLifeforms = Math.round(strTok.getFloatToken());
    if (numLifeforms == -1) {
      logFile.println(line);
      logFile.println("  In Evu-" + evu.getId() + " Number of Lifeforms is missing.");
      logFile.println();
      throw new ParseError("Data for Evu-" + evu.getId() + " Missing, see logfile");
    }

    for (int lf=0; lf<numLifeforms; lf++) {
      speciesStr   = null;
      sizeClassStr = null;
      densityStr   = null;

      VegSimStateData state = new VegSimStateData(evu.getId());

      speciesStr = strTok.getToken();
      if (speciesStr != null) {
        speciesStr = speciesStr.trim().toUpperCase();
      }

      str = strTok.getToken();
      if (str != null) {
        str = str.trim().toUpperCase();
        sizeClassStr = parseSizeClass(str);
        age = parseAge(str);
      }

      densityStr = strTok.getToken();

      vegState = null;
      emptyState = (speciesStr == null && sizeClassStr == null && densityStr == null);


      if (!emptyState)
      {
        if (speciesStr == null) { speciesStr = "UNKNOWN"; }
        if (sizeClassStr == null) { sizeClassStr = "UNKNOWN"; }
        if (densityStr == null) { densityStr = "UNKNOWN"; }

        Species species = Species.get(speciesStr, true);
        SizeClass sizeClass = SizeClass.get(sizeClassStr, true);
        Density density = Density.getOrCreate(densityStr);

        if (species != null && sizeClass != null && density != null && htGrp != null) {
          vegState = htGrp.getVegetativeType(species, sizeClass, age, density);
        }

        if (vegState == null) {
          logFile.println(line);
          logFile.println("  In Evu-" + evu.getId() +
              " Could not build a valid state.");
          logFile.println("  One or more of the following must be invalid:");
          logFile.println("  Species, Size Class, Density, or Ecological Grouping");
          logFile.println();

          // Even invalid states need to be set in the evu, in order to
          // allow for later correction.
          vegState = new VegetativeType(species, sizeClass, age, density);
        }
        state.setVegType(vegState);
      }
      // Skip the rest of the tokens.
      if (emptyState) {
        strTok.nextToken();
        strTok.nextToken();
        strTok.nextToken();
        strTok.nextToken();
      }

      if (!emptyState)
      {
        // Read Initial Process
        str = strTok.getToken();
        if (str == null) {
          str = Evu.getDefaultInitialProcess().toString();
        }

        process = ProcessType.get(str.toUpperCase());
        if (process == null) {
          logFile.println(line);
          logFile.println("  In Evu-" + evu.getId() + ":");
          logFile.println("  " + str + " is not a Process.");
          logFile.println();

          process = ProcessType.makeInvalidInstance(str);
        }
        state.setProcess(process);

        // Read process time Steps in past.
        int ts = Math.round(strTok.getFloatToken());

        // Read Initial Treatment
        str = strTok.getToken();
        if (str != null) {
          treatType = TreatmentType.get(str.toUpperCase());
          if (treatType == null) {
            logFile.println(line);
            logFile.println("  In Evu-" + evu.getId() + ":");
            logFile.println("  " + str + " is not a Treatment.");
            logFile.println();
          }
          else {
            // TODO need to update state later if it is an invalid one. */
            // TODO carry process & treatment as initial when make sim ready */
            treatment = Treatment.createInitialTreatment(treatType, vegState);
            evu.addTreatment(treatment);
          }
        }
        // Read treatment time steps in past
        ts = Math.round(strTok.getFloatToken());
      }

      if (!emptyState) {
        evu.setInitialState(state, Season.YEAR);
      }

      int numTrackSpecies = Math.round(strTok.getFloatToken());

      // Skip the tracking species info if no valid state information.
      if (emptyState) {
        for (int i=0; i<numTrackSpecies; i++) {
          strTok.nextToken();  // Species
          strTok.nextToken();  // Percent
        }
        continue;
      }

      int percent;
      if (numTrackSpecies != -1) {
        state.clearTrackingSpecies();
        for (int i = 0; i < numTrackSpecies; i++) {
          str = strTok.getToken();
          if (str != null) {
            str = str.trim().toUpperCase();
          }

          percent = Math.round(strTok.getFloatToken());
          if (str == null || percent == -1) {
            continue;
          }

          InclusionRuleSpecies trk_species = InclusionRuleSpecies.get(str,true);

          Range range = vegState.getSpeciesRange(trk_species);
          if (range != null) {
            if (percent < range.getLower()) {
              percent = range.getLower();
            }
            else if (percent > range.getUpper()) {
              percent = range.getUpper();
            }
          }
          state.addTrackSpecies(trk_species, percent);
        }
        state.addMissingTrackSpecies();
        state.removeInvalidTrackSpecies();
      }

    }
    // This should be the ending #
    str = strTok.getToken();

    // If we have multiple lifeforms they use different columns and the
    // count will always be five.
    if (numLifeforms == 1) {
      evu.makeSingleLife();
    }
  }
  /**
   * reads in Land Attributes.  They are stored in file in the following order: slink (ID), acres, soilType, landform, aspect, slope, parent material, depth
   * @param area
   * @param fin
   * @param logFile
   * @throws ParseError
   * @throws IOException
   */
  private void readLandAttributes(Area area, BufferedReader fin, PrintWriter logFile) throws ParseError, IOException {

    String              line, str;
    StringTokenizerPlus strTok;
    int                 count;
    int                 id;
    ExistingLandUnit    elu;
    float               fValue;
    int                 numReqFields = 8;

    line = fin.readLine();
    if (line == null) {
      logFile.println("No Land Attribute data found in input file.");
      logFile.println();
      hasAttributes = false;
      return;
    }

    while(line != null && line.trim().equals("END") == false) {
      try {
        line = line.trim();
        if (line.length() == 0) { line = fin.readLine(); continue; }

        line = Utility.preProcessInputLine(line);

        strTok = new StringTokenizerPlus(line,",");
        count  = strTok.countTokens();
        if (count < numReqFields) {
          logFile.println(line);
          logFile.println("   Not enough fields in attribute data.");
          logFile.println("   Required fields are:");
          logFile.print("     slink, acres, name, landform,");
          logFile.println("   aspect, slope, parent material");
          logFile.println();
          line = fin.readLine();
          continue;
        }

        // Get the unit id
        id = strTok.getIntToken();
        if (id == -1) {
          logFile.println(line);
          logFile.println("  Invalid Slink");
          logFile.println();
          continue;
        }
        elu = area.getElu(id);
        if (elu == null) {
          logFile.println(line);
          logFile.println("  An Existing Land Unit with the id: " + id + " does not exist.");
          logFile.println();
          line = fin.readLine();
          continue;
        }

        // Get the Acres
        fValue  = strTok.getFloatToken();
        elu.setAcres(fValue);
        // Acres check is done later, because we need to know species for check.

        // Get the Soil Type
        str = strTok.getToken();
        if (str != null) {
          elu.setSoilType(SoilType.get(str,true));
        }

        // Get the Landform
        str = strTok.getToken();
        if (str != null) {
          elu.setLandform(str);
        }

        // Get the aspect (can be either a double or String)
        str = strTok.getToken();
        if (str != null) {
          try {
            double value = Double.parseDouble(str);
            elu.setAspect(value);
          }
          catch (NumberFormatException ex) {
            elu.setAspectName(str);
          }
        }

        // Get the slope
        fValue = strTok.getFloatToken(Float.NaN);
        if (Float.isNaN(fValue)) {
          logFile.println(line);
          logFile.println("  In Elu-" + id + " Slope is Invalid");
          logFile.println("  Value of 0.0 assigned");
          logFile.println();
          fValue = 0.0f;
        }
        elu.setSlope(fValue);

        // Get the parent material
        str = strTok.getToken();
        if (str != null) {
          elu.setParentMaterial(str);
        }

        // Get the Depth
        str = strTok.getToken();
        if (str != null) {
          elu.setDepth(str);
        }

        if (strTok.hasMoreTokens()) {
          double longitude = strTok.getDoubleToken(Double.NaN); // POINT_X Field
          elu.setLongitude(longitude);
        }

        if (strTok.hasMoreTokens()) {
          double latitude = strTok.getDoubleToken(Double.NaN); // POINT_Y Field
          elu.setLatitude(latitude);
        }

        // Acres check depends on species being available.
        if (elu.isAcresValid() == false) {
          logFile.println(line);
          logFile.println("  In Elu-" + id + " Acres is Invalid");
          logFile.println("  Acres must be a value greater than 0.0");
          logFile.println();
        }

        // Get the Next Line.
        line = fin.readLine();
      }
      catch (ParseError err) {
        String msg = "The following error occurred in this line: \n" +
            line + "\n" + err.getMessage();
        throw new ParseError(msg);
      }
    }
  }

  public void readAttributesFile(File prefix) throws SimpplleError {
    File file, log, logFile;
    Area area = Simpplle.getCurrentArea();

    hasAttributes = false;

    // Veg Attributes
    file = Utility.makeSuffixedPathname(prefix, "", "attributes");
    logFile = Utility.makeUniqueLogFile(prefix, "vegatt");
    read(area, file, logFile, EVU, true);

    hasAttributes = true;
  }

  private boolean read(Area newArea, File inputFile, File logFile, int kind, boolean attribOnly) {
    BufferedReader fin;
    PrintWriter    log;
    boolean        success;
    RelationParser parser;

    try {
      log = new PrintWriter(new FileWriter(logFile));
    }
    catch (IOException e) {
      e.printStackTrace();
      System.out.println("Could not Open log file for writing: logFile");
      return false;
    }

    try {
      fin = new BufferedReader(new FileReader(inputFile));

      if (!attribOnly) {
        switch (kind) {
          case EVU:
            parser = new ParseNeighbors();
            success = parser.readSection(newArea, fin, log);
            break;
          case ELU:
            parser = new ParseLandNeighbors();
            success = parser.readSection(newArea, fin, log);
            break;
          default:
            success = false;
        }
        if (!success) {
          fin.close();
          log.flush();
          log.close();
          return false;
        }
      }
      if (hasAttributes || attribOnly) {
        switch (kind) {
          case EVU:
            readAttributes(newArea,fin,log);
            break;
          case ELU:
            readLandAttributes(newArea,fin,log);
            break;
        }
      }
      fin.close();
    }
    catch (ParseError e) {
      log.println("The following error occurred while trying to create the area:");
      log.println(e.msg);
    }
    catch (FileNotFoundException e) {
      log.println("Could not open file: " + inputFile);
    }
    catch (IOException e) {
      log.println("The following error occurred while trying to create the area:");
      e.printStackTrace(log);
    }
    log.flush();
    log.close();

    return true;
  }
}
