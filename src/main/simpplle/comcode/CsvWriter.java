package simpplle.comcode;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.TreeMap;

/**
 * Writes records to a stream as comma separated values. Fields in each record are defined in the
 * constructor. Values are assigned to these fields to complete a single record before writing it
 * to the stream. Attempts to assign values to undefined fields are ignored. The field delimiter
 * character must not appear in any values.
 */
public class CsvWriter implements Closeable {

  private Writer writer;
  private String delimiter;
  private LinkedHashMap<String, String> record;

  /**
   * Creates a CSV writer.
   *
   * @param file the file to write to
   * @param fields the names of fields present in each record
   * @throws IOException if an I/O exception occurs
   */
  public CsvWriter(File file, String[] fields) throws IOException {
    this(new FileWriter(file), ',', fields);
  }

  /**
   * Creates a CSV writer.
   *
   * @param writer the writer to write to
   * @param fields the names of fields present in each record
   * @throws IOException if an I/O exception occurs
   */
  public CsvWriter(Writer writer, String[] fields) throws IOException {
    this(writer, ',', fields);
  }

  /**
   * Creates a CSV writer with an alternate delimiter.
   *
   * @param file a file to write to
   * @param delimiter the field separator character
   * @param fields the names of fields present in each record
   * @throws IOException if an I/O exception occurs
   */
  public CsvWriter(File file, char delimiter, String[] fields) throws IOException {
    this(new FileWriter(file), delimiter, fields);
  }

  /**
   * Creates a CSV writer with an alternate delimiter.
   *
   * @param writer a writer to write to
   * @param delimiter the field separator character
   * @param fields the names of fields present in each record
   * @throws IOException if an I/O exception occurs
   */
  public CsvWriter(Writer writer, char delimiter, String[] fields) throws IOException {

    this.writer    = writer;
    this.delimiter = Character.toString(delimiter);
    this.record    = new LinkedHashMap<>();

    // Store the field names in the record
    for (String field : fields) {
      record.put(field, field);
    }

    // Write the header
    storeRecord();
  }

  /**
   * Writes a record to the CSV table.
   *
   * @throws IOException if an I/O exception occurs
   */
  public void storeRecord() throws IOException {
    String delim = "";
    for (String field : record.keySet()) {
      writer.write(delim);
      writer.write(record.get(field));
      record.put(field, "");
      delim = delimiter;
    }
    writer.write("\n");
    writer.flush();
  }

  public void putBoolean(String field, boolean value) {
    putString(field, Boolean.toString(value));
  }

  public void putByte(String field, byte value) {
    putString(field, Byte.toString(value));
  }

  public void putChar(String field, char value) {
    putString(field, Character.toString(value));
  }

  public void putDouble(String field, double value) {
    putString(field, Double.toString(value));
  }

  public void putFloat(String field, float value) {
    putString(field, Float.toString(value));
  }

  public void putInteger(String field, int value) {
    putString(field, Integer.toString(value));
  }

  public void putLong(String field, long value) {
    putString(field, Long.toString(value));
  }

  public void putShort(String field, short value) {
    putString(field, Short.toString(value));
  }

  public void putString(String field, String value) {
    if (record.containsKey(field)) {
      record.put(field, (value == null) ? "" : value);
    }
  }

  /**
   * Closes resources used by this CSV writer.
   *
   * @throws IOException if an I/O exception occurs
   */
  @Override
  public void close() throws IOException {
    writer.close();
  }
}
