/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class defines the Winter Drought, a type of Process
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class WinterDrought extends Process {
  private static final String printName = "WINTER-DROUGHT";
  public WinterDrought () {
    super();

    spreading   = false;
    description = "Winter Drought";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbability (WestsideRegionOne zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

  public int doProbability (EastsideRegionOne zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }
  public int doProbability (Teton zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }
  public int doProbability (NorthernCentralRockies zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

  /**
   * outputs "WINTER-DROUGHT"
   */
  public String toString () {
    return printName;
  }

}

