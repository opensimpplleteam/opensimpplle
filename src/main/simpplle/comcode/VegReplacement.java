/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class contains methods which defines Vegetative Replacement, a type of Process.
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class VegReplacement extends Process {
  private static final String printName = "VEG-REPLACEMENT";
/**
 * Constructor for Vegetative Replacement.  Inherits from Process superclass and initializes spreading to false, and sets visible columns row_col and probability column.  
 */
  public VegReplacement() {
    super();

    spreading   = false;
    description = "VEG REPLACEMENT";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  /**
   *  outputs "VEG-REPLACEMENT"
   */
    public String toString() { return printName; }


}
