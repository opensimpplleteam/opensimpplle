/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class contains methods for High Spruce Beetle, a type of Process.
 * This process does not happen in zones: Westside Region 1, Eastside Region 1, Sierra Nevada, Southern California, nor Gila.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 * 
 * @see simpplle.comcode.Process
 */

public class HighSb extends Process {
  private static final String printName = "HIGH-SB";
  /**
   * Constructor for High Spruce Beetle.  Sets the description, base logic visible column and process probability visible column.  
   */
  public HighSb() {
    super();

    spreading   = false;
    description = "High Spruce Beetle";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbability (SouthCentralAlaska zone, Evu evu, Lifeform lifeform) {
    return SpruceBeetleRisk.getHighProbability();
  }

  /**
   * outputs "HIGH-SB"
   */
  public String toString () {
    return printName;
  }
}



