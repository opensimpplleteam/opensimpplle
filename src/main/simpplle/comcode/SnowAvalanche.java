/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class defines methods for Snow Avalanche, a type of Process.  This process is not tracked in many of the zones,
 * therefore returns 0 for probability and false for spread.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class SnowAvalanche extends Process {
  private static final String printName = "SNOW-AVALANCHE";
  /**
   * Constructor for snow avalanche.  Inherits from process superclass and initializes spreading to false, and visible columns for row and probability.  
   * 
   */
  public SnowAvalanche() {
    super();

    spreading   = false;
    description = "Snow Avalanche";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbability (SouthCentralAlaska zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

/**
 * outputs "SNOW-AVALANCHE"
 */
  public String toString () {
    return printName;
  }

}
