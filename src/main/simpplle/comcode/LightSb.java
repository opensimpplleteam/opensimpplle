/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class contains methods for Light Spruce Beetles, a type of Process
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class LightSb extends Process {
  private static final String printName = "LIGHT-SB";
  /**
   * Constructor for Light Spruce Beetle.  Inherits from Process superclass, and initializes spreading to false, sets description and initializes default visible columns
   */
  public LightSb() {
    super();

    spreading   = false;
    description = "Light Spruce Beetle";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbability (SouthCentralAlaska zone, Evu evu, Lifeform lifeform) {
    SpruceBeetleRisk.compute(evu, lifeform);
    return SpruceBeetleRisk.getLowProbability();
  }

/**
 * outputs "LIGHT-SB"
 */
  public String toString () {
    return printName;
  }

}
