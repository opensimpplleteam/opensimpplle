/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class contains method for Fifty Year Flood, a type of Process.  Since it can occur in all regions and all zones, the doProbability and doSpread methods
 * all return a value.
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class FiftyYearFlood extends Process {
  private static final String printName = "FIFTY-YEAR-FLOOD";

  public FiftyYearFlood() {
    super();

    spreading   = false;
    description = "FIFTY YEAR FLOOD";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public String toString() { return printName; }

}
