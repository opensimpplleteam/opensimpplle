/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.io.*;
import java.util.StringTokenizer;

/**
 * A container for application properties. These properties are read from and written to a
 * properties file, which has a different format from Java's properties format.
 */
public class Properties {

  private File file;
  private boolean debug;
  private boolean invasiveMSU;
  private boolean simulationLogging;
  private boolean historicPathways;
  private File defaultDirectory;
  private File workingDirectory;
  private String defaultZoneName;

  /**
   * Create a new container for application properties
   *
   * @param directory the directory that contains this properties file
   * @param fileName the name of the properties file
   */
  public Properties(File directory, String fileName) {
    this.defaultDirectory = directory;
    this.workingDirectory = directory;
    this.file = new File(directory, fileName);
  }

  /**
   * Reads application settings from a properties file. If the properties file is missing, then
   * this method returns without throwing an exception.
   *
   * @throws IOException if there is an I/O exception other than a missing file
   */
  public void read() throws IOException {

    if (!file.exists()) return;

    try (FileReader fileReader = new FileReader(file);
         BufferedReader reader = new BufferedReader(fileReader)) {

      String line = reader.readLine();

      while (line != null) {

        String[] split = line.split(",", -1);

        if (split.length == 2) {
          String key = split[0];
          String value = split[1];

          if (key == null) return;

          switch (key.toLowerCase().trim()) {

            case "debug":
              debug = Boolean.parseBoolean(value);
              break;

            case "default_zone":
              defaultZoneName = value;
              break;

            case "historic_pathways":
              historicPathways = Boolean.parseBoolean(value);
              break;

            case "invasivespecieslogicdatamsu_probfile":
              invasiveMSU = Boolean.parseBoolean(value);
              break;

            case "simulation_logging":
              simulationLogging = Boolean.parseBoolean(value);
              break;

            case "working_directory":
              if (value.equalsIgnoreCase("DEFAULT")) {
                workingDirectory = defaultDirectory;
              } else {
                workingDirectory = new File(value);
                if (!workingDirectory.exists()) {
                  workingDirectory = defaultDirectory;
                }
              }
              break;
          }
        }

        line = reader.readLine();

      }
    }
  }

  /**
   * Writes application settings to a properties file.
   *
   * @throws IOException if there is an I/O exception
   */
  public void write() throws IOException {

    try (FileWriter fileWriter = new FileWriter(file);
         PrintWriter writer = new PrintWriter(fileWriter)) {

      writer.println("DEFAULT_ZONE," + defaultZoneName);
      writer.println("HISTORIC_PATHWAYS," + historicPathways);
      writer.println("WORKING_DIRECTORY," + workingDirectory);
      writer.println("DEBUG," + debug);
      writer.println("SIMULATION_LOGGING," + Boolean.toString(simulationLogging));
      writer.println("InvasiveSpeciesLogicDataMSU_probFile," + invasiveMSU);

    }
  }

  public boolean isDebug() {
    return debug;
  }

  public String getDefaultZoneName() {
    return defaultZoneName;
  }

  public void setDefaultZoneName(String defaultZoneName) {
    this.defaultZoneName = defaultZoneName;
  }

  public boolean getHistoricPathways() {
    return historicPathways;
  }

  public void setHistoricPathways(boolean historicPathways) {
    this.historicPathways = historicPathways;
  }

  public boolean getInvasiveMSU() {
    return invasiveMSU;
  }

  public void setInvasiveMSU(boolean invasiveMSU) {
    this.invasiveMSU = invasiveMSU;
  }

  public boolean getSimulationLogging() {
    return simulationLogging;
  }

  public void setSimulationLogging(boolean simulationLogging) {
    this.simulationLogging = simulationLogging;
  }

  public File getWorkingDirectory() {
    return workingDirectory;
  }

  public void setWorkingDirectory(File directory) {
    this.workingDirectory = directory;
  }
}
