/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.map.Flat3Map;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * VegSimStateData describes the state of an existing vegetation unit (EVU) during a single time step.
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class VegSimStateData implements Externalizable {

  static final long serialVersionUID = -159138068111213685L;
  static final int version = 3;

  private long           dbid; // Database row id
  private int            slink;
  private short          timeStep;
  private short          run;
  private Lifeform       lifeform;
  private VegetativeType veg;
  private ProcessType    process;
  private short          prob;
  private Season season;
  private short          seasonOrd;  // Used for hibernate mapping only!

  private int fireSpreadRuleIndex = -1;
  private int fireRegenRuleIndex  = -1;
  private int succRegenRuleIndex  = -1;

  // Object[Species][Integer]
  // Flat3Map
  // Key: InclusionRuleSpecies, Value: Percent(float)
  private Map<InclusionRuleSpecies, Float> trackingSpecies = new Flat3Map();

  private static int writeCount=0;

  public VegSimStateData() {
    this.process         = ProcessType.SUCCESSION;
    this.prob            = Evu.NOPROB;
    this.season          = Season.YEAR;
  }

  public VegSimStateData(int slink) {
    this(slink,0,0);
  }

  public VegSimStateData(int slink, int timeStep, int run) {
    this();
    this.slink    = slink;
    this.timeStep = (short)timeStep;
    this.run      = (short)run;
  }

  public VegSimStateData(int slink, VegetativeType veg) {
    this(slink);
    this.veg = veg;
    if (Area.hasMultipleLifeforms()) {
      this.lifeform = veg.getSpecies().getLifeform();
    } else {
      this.lifeform = Lifeform.NA;
    }
  }

  public VegSimStateData(int slink, int timeStep, int run, VegetativeType veg, ProcessType process, int prob) {
    this(slink,veg);
    this.timeStep = (short)timeStep;
    this.run      = (short)run;
    this.process  = process;
    this.prob     = (short)prob;
  }

  public VegSimStateData(int slink, int timeStep, int run, VegetativeType vegType, ProcessType process, int prob, Season season) {
    this(slink,timeStep,run,vegType,process,prob);
    this.season = season;
  }

  public VegSimStateData(int slink,int timeStep, int run, VegSimStateData state) {
    this(slink,timeStep,run,state.veg,state.process,state.prob,state.season);
    copyTrackingSpecies(state.trackingSpecies);
  }

  public void clearTrackingSpecies() {
    trackingSpecies.clear();
  }

  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    int version = in.readInt();

    if (version > 2) {

      slink    = in.readInt();
      timeStep = (short)in.readInt();
      run      = (short)in.readInt();
      lifeform = (Lifeform)in.readObject();

    } else {

      // For old versions these values are set when this class is loaded
      // by simpplle.comcode.Evu

    }

    veg = VegetativeType.readExternalData(in);
    process = ProcessType.readExternalSimple(in);

    String str = (String)in.readObject();
    prob = (short)Evu.parseProbabilityString(str);

    int size = in.readInt();

    if (size > 0) {

      trackingSpecies.clear();

      for (int i = 0; i < size; i++) {

        String spStr = (String)in.readObject();

        float pct;
        if (version == 1) {
          pct = (float)in.readInt();
        } else {
          pct = in.readFloat();
        }

        trackingSpecies.put(InclusionRuleSpecies.get(spStr, true), pct);

      }
    }

    str = (String)in.readObject();

    switch (str) {

      case "SPRING": season = Season.SPRING; break;
      case "SUMMER": season = Season.SUMMER; break;
      case "FALL":   season = Season.FALL;   break;
      case "WINTER": season = Season.WINTER; break;

    }
  }

  public void writeExternal(ObjectOutput out) throws IOException {

    out.writeInt(version);
    out.writeInt(slink);
    out.writeInt(timeStep);
    out.writeInt(run);
    out.writeObject(lifeform);

    veg.writeExternal(out);
    process.writeExternalSimple(out);
    out.writeObject(getProbString());

    Flat3Map map = (Flat3Map)trackingSpecies;
    out.writeInt(map.size());

    MapIterator it = map.mapIterator();
    while (it.hasNext()) {
      InclusionRuleSpecies sp = (InclusionRuleSpecies)it.next();
      out.writeObject(sp.toString());
      out.writeFloat((Float)it.getValue());
    }
    out.writeObject(getSeasonString());
  }

  public VegetativeType getVegType() {
    return veg;
  }

  /**
   * Sets the simulation vegetative type.  If the area being evaluated has multiple life forms sets the life from for this area to 
   * @param veg
   */
  public void setVegType(VegetativeType veg) {

    this.veg = veg;

    if (Area.hasMultipleLifeforms()) {
      this.lifeform = this.veg.getSpecies().getLifeform();
    } else {
      this.lifeform = Lifeform.NA;
    }
  }

  /**
   * Gets the simulation vegetative type.
   * @return
   */
  public VegetativeType getVeg() {
    return veg;
  }

  /**
   * Used by hibernate
   */
  public void setVeg(VegetativeType veg) {
    Evu evu = Simpplle.getCurrentArea().getEvu(slink);
    HabitatTypeGroup group = evu.getHabitatTypeGroup();
    setVeg(group, veg);
  }

  public void setVeg(HabitatTypeGroup group, VegetativeType veg) {
    // Make sure we get the existing instance, not a newly created one.
    this.veg = group.getVegetativeType(veg.getSpecies(), veg.getSizeClass(), veg.getAge(), veg.getDensity());
  }

  /**
   * Gets the process type.
   * @return process type
   */
  public ProcessType getProcess() {
    return process;
  }

  /**
   * Gets the probability
   * @return probability
   */
  public short getProb() {
    return prob;
  }

  /**
   * Sets the probability for this vegetative simulation state data.  
   * @param prob probability
   */
  public void setProb(short prob) {
    this.prob = prob;
  }

  public void setProb(int prob) {
    this.prob = (short)prob;
  }

  public float getFloatProb() {
    return ((float)prob / (float)Utility.pow(10,Area.getAcresPrecision()));
  }

  public String getProbString() {
    switch (prob) {
      case Evu.D:      return Evu.D_STR;
      case Evu.L:      return Evu.L_STR;
      case Evu.S:      return Evu.S_STR;
      case Evu.SE:     return Evu.SE_STR;
      case Evu.SFS:    return Evu.SFS_STR;
      case Evu.SUPP:   return Evu.SUPP_STR;
      case Evu.COMP:   return Evu.COMP_STR;
      case Evu.GAP:    return Evu.GAP_STR;
      case Evu.NOPROB: return Evu.NOPROB_STR;
      default:         return IntToString.get(prob);
    }
  }

  public Season getSeason() {
    return season;
  }

  public void setSeason(Season season) {
    this.season = season;
  }

  public int getFireSpreadRuleIndex() {
    return fireSpreadRuleIndex;
  }

  public void setFireSpreadRuleIndex(int index) {
    fireSpreadRuleIndex = index;
  }

  public int getFireRegenerationRuleIndex() {
    return fireRegenRuleIndex;
  }

  public void setFireRegenerationRuleIndex(int index) {
    fireRegenRuleIndex = index;
  }

  public int getSuccessionRegenerationRuleIndex() {
    return succRegenRuleIndex;
  }

  public void setSuccessionRegenerationRuleIndex(int index) {
    succRegenRuleIndex = index;
  }

  public void resetRegenRules(int resetValue) {
    fireRegenRuleIndex = resetValue;
    succRegenRuleIndex = resetValue;
  }

  public String getSeasonString() {
    return season.toString();
  }

  public void setProcess(ProcessType process) {
    this.process = ProcessType.get(process.toString());
  }

  public void addTrackSpecies(InclusionRuleSpecies trk_species, float percent) {
    trackingSpecies.put(trk_species, percent);
  }

  public float getSpeciesPercent(InclusionRuleSpecies trk_species) {
    Float pct = trackingSpecies.get(trk_species);
    return (pct != null) ? pct : -1;

  }

  public void addMissingTrackSpecies() {
    Set<InclusionRuleSpecies> allSpecies = veg.getTrackingSpecies();
    if (allSpecies != null) {
      for (InclusionRuleSpecies sp : allSpecies) {
        if (getSpeciesPercent(sp) == -1) {
          addTrackSpecies(sp,0);
        }
      }
    }
  }

  public void removeInvalidTrackSpecies() {

    ArrayList<InclusionRuleSpecies> removeList = new ArrayList<>();

    for (Object spObj : trackingSpecies.keySet()) {

      InclusionRuleSpecies sp = (InclusionRuleSpecies)spObj;

      if (veg.isTrackingSpecies(sp)) continue;

      Float pct = trackingSpecies.get(sp);
      if (pct != null && pct == 0) {
        removeList.add(sp);
      }
    }

    for (int i=0; i<removeList.size(); i++) {
      trackingSpecies.remove(removeList.get(i));
    }
  }

  /** For backward compatibility
   *
   */
  public Map getAccumDataSpeciesMap() {
    return new HashMap<>(trackingSpecies);
  }

  public InclusionRuleSpecies[] getTrackingSpeciesArray() {
    return trackingSpecies.keySet().toArray(new InclusionRuleSpecies[trackingSpecies.size()]);
  }

  public Flat3Map getTrackingSpeciesMap() {
    return (Flat3Map)trackingSpecies;
  }

  public void copyTrackingSpecies(Map trkSpecies) {
    trackingSpecies.clear();
    for (Object elem : trkSpecies.keySet()) {
      InclusionRuleSpecies sp = (InclusionRuleSpecies)elem;
      Float pct = (Float)trkSpecies.get(sp);
      trackingSpecies.put(sp,pct);
    }
  }

  public float updateTrackingSpecies(InclusionRuleSpecies trkSpecies, float change) {

    Float pct = trackingSpecies.get(trkSpecies);
    float newPct = change;

    if (pct != null) {
      newPct += pct;
    }

    if (newPct < 0) {
      newPct = 0.0f;
    } else if (newPct > 100) {
      newPct = 100.0f;
    }

    trackingSpecies.put(trkSpecies,newPct);

    return newPct;
  }

  /**
   * This one is called only in the case of invasive species.
   * In addition to the change as Percent difference this one also removes
   * species that have been reduced to zero.
   */
  public float updateTrackingSpecies(InclusionRuleSpecies trkSpecies, float change, boolean changeAsPercent) {

    float newPct;

    if (changeAsPercent) {

      Float pct = trackingSpecies.get(trkSpecies);

      if (pct == null) {
        pct = 0.0f;
      }

      float pctChange = pct * (change / 100.0f);
      newPct = pct + pctChange;

      if (newPct < 0) {
        newPct = 0.0f;
      } else if (newPct > 100) {
        newPct = 100.0f;
      }

      trackingSpecies.put(trkSpecies, newPct);

    } else {

      newPct = updateTrackingSpecies(trkSpecies,change);

    }

    if (newPct < 0.005) {
      trackingSpecies.remove(trkSpecies);
      newPct = 0.0f;
    }
    return newPct;
  }

  public short getTimeStep() {
    return timeStep;
  }

  public void setTimeStep(short timeStep) {
    this.timeStep = timeStep;
  }

  public void setTimeStep(int timeStep) {
    this.timeStep = (short)timeStep;
  }

  public short getRun() {
    return run;
  }

  public void setRun(short run) {
    this.run = run;
  }

  public void setRun(int run) {
    this.run = (short) run;
  }

  public Lifeform getLifeform() {
    return lifeform;
  }

  public void setLifeform(Lifeform lifeform) {
    this.lifeform = Lifeform.findByName(lifeform.getName());
  }

  public int getSlink() {
    return slink;
  }

  public void setSlink(int slink) {
    this.slink = slink;
  }

  public static void clearWriteCount() {
    writeCount=0;
  }

  public static void writeDatabase(Session session, VegSimStateData state) throws SimpplleError {
    try {
      session.saveOrUpdate(state.lifeform);
      session.saveOrUpdate(state.veg.getSpecies());
      session.saveOrUpdate(state.veg.getSizeClass());
      session.saveOrUpdate(state.veg.getDensity());
      session.saveOrUpdate(state.process);

      if (state.trackingSpecies != null) {
        MapIterator it = ((Flat3Map) state.trackingSpecies).mapIterator();
        while (it.hasNext()) {
          InclusionRuleSpecies sp = (InclusionRuleSpecies) it.next();
          session.saveOrUpdate(sp);
        }
      }

      session.saveOrUpdate(state);
      writeCount++;

      if (writeCount % 30 == 0) {
        session.flush();
        session.clear();
      }
    } catch (HibernateException ex) {
      throw new SimpplleError("Problems writing database", ex);
    }
  }

  public static void writeAccessFiles(PrintWriter fout, PrintWriter trackOut, Evu evu, VegSimStateData state) {

    Simulation sim = Simulation.getInstance();

    int run = Simulation.getCurrentRun() + 1;  // TODO: Use VegSimStateData's value
    int ts  = Simulation.getCurrentTimeStep(); // TODO: Use VegSimStateData's value

    sim.addAccessLifeform(state.lifeform);
    sim.addAccessSpecies(state.veg.getSpecies());
    sim.addAccessSizeClass(state.veg.getSizeClass());
    sim.addAccessDensity(state.veg.getDensity());
    sim.addAccessProcess(state.process);
    sim.addAccessOwnership(Ownership.get(evu.getOwnership(),true));
    sim.addAccessSpecialArea(SpecialArea.get(evu.getSpecialArea(),true));

    Treatment treatment = evu.getTreatment(ts);
    int treatmentId = -1;
    if (treatment != null) {
      sim.addAccessTreatment(treatment.getType());
      treatmentId = treatment.getType().getSimId();
    }

    // State data
    int   lifeId    = state.lifeform.getSimId();
    int   speciesId = state.getVeg().getSpecies().getSimId();
    int   sizeId    = state.getVeg().getSizeClass().getSimId();
    int   age       = state.getVeg().getAge();
    int   densityId = state.getVeg().getDensity().getSimId();
    int   processId = state.process.getSimId();
    int   seasonId  = state.season.ordinal();
    int   prob      = state.getProb();
    float fProb     = state.getFloatProb();
    int   firerule  = state.getFireSpreadRuleIndex();
    int   fireRegenRuleIndex        = state.getFireRegenerationRuleIndex();
    int   successionRegenRuleIndex  = state.getSuccessionRegenerationRuleIndex();

    // Evu Data
    float acres     = evu.getFloatAcres();
    int originUnitId  = (evu.getOriginUnit() != null) ? evu.getOriginUnit().getId() : -1;
    int fromUnitId    = evu.fromEvuId;

    String probStr = "n/a";
    if (prob < 0) {
      fProb = 0.0f;
      probStr = state.getProbString();
    }

    fout.printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.1f,%s,%d,%d,%d,%d,%d,%d%n",
        run ,ts, seasonId, state.slink, lifeId, speciesId, sizeId, age, densityId, processId, fProb,
        probStr, treatmentId, originUnitId, fromUnitId, firerule, fireRegenRuleIndex, successionRegenRuleIndex);

    state.resetRegenRules(-1);

    if (state.trackingSpecies != null) {

      MapIterator it = ((Flat3Map) state.trackingSpecies).mapIterator();

      while (it.hasNext()) {

        InclusionRuleSpecies sp = (InclusionRuleSpecies) it.next();

        int spId = sp.getSimId();
        float pct = (Float)it.getValue();
        
        trackOut.printf("%d,%d,%d,%d,%d,%.1f%n",run,ts,state.slink,lifeId,spId,pct);
        
        sim.addAccessIncRuleSpecies(sp);
      }
    }
    writeCount++;

    if (writeCount % 30 == 0) {
      fout.flush();
    }
  }
}
