package simpplle.comcode;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Saves an area as a zipped collection of tables. Each table describes a set of elements or
 * relationships within the area. An overview file (area.ini) describes the area at a high level.
 */
public class AreaSaver implements Closeable {

  ZipOutputStream zip;
  PrintWriter writer;

  /**
   * Creates a new area saver.
   *
   * @param file the path of the file that will be saved
   * @throws IOException if an I/O exception occurs
   */
  public AreaSaver(File file) throws IOException {
    zip = new ZipOutputStream(new FileOutputStream(file));
    writer = new PrintWriter(zip);
  }

  /**
   * Closes resources opened by the area saver.
   *
   * @throws IOException if an I/O exception occurs
   */
  public void close() throws IOException {
    writer.close();
  }

  /**
   * Saves an area to a zip file.
   *
   * @param area the area to save to the file
   * @throws IOException if an I/O exception occurs
   */
  public void save(Area area) throws IOException {

    writeEntry("area.ini", this::writeOverview, area);

    if (area.hasAquaticUnits()) {

      writeEntry("aquatic-units.csv", this::writeAquaticUnits, area);
      writeEntry("aquatic-neighbors.csv", this::writeAquaticNeighbors, area);
      writeEntry("aquatic-predecessors.csv", this::writeAquaticPredecessors, area);
      writeEntry("aquatic-successors.csv", this::writeAquaticSuccessors, area);

      if (area.hasVegetationUnits()) {
        writeEntry("aquatic-adjacent.csv", this::writeAquaticAdjacent, area);
        writeEntry("aquatic-upland.csv", this::writeAquaticUpland, area);
      }
    }

    if (area.hasLandUnits()) {

      writeEntry("land-units.csv", this::writeLandUnits, area);
      writeEntry("land-neighbors.csv", this::writeLandNeighbors, area);

      if (area.hasVegetationUnits()) {
        writeEntry("land-veg.csv", this::writeLandVeg, area);
      }
    }

    if (area.hasRoads()) {

      writeEntry("road-neighbors.csv", this::writeRoadNeighbors, area);
      writeEntry("road-units.csv", this::writeRoadUnits, area);

      if (area.hasVegetationUnits()) {
        writeEntry("road-veg.csv", this::writeRoadVeg, area);
      }
    }

    if (area.hasTrails()) {

      writeEntry("trail-neighbors.csv", this::writeTrailNeighbors, area);
      writeEntry("trail-units.csv", this::writeTrailUnits, area);

      if (area.hasVegetationUnits()) {
        writeEntry("trail-veg.csv", this::writeTrailVeg, area);
      }
    }

    if (area.hasVegetationUnits()) {

      writeEntry("habitat-type-group-types.csv", this::writeHabitatTypeGroupTypes, area);
      writeEntry("veg-adjacencies.csv", this::writeVegAdjacencies, area);
      writeEntry("veg-initial.csv", this::writeVegInitial, area);
      writeEntry("veg-neighbors.csv", this::writeVegNeighbors, area);
      writeEntry("veg-tracking.csv", this::writeVegTrackingSpecies, area);
      writeEntry("veg-units.csv", this::writeVegUnits, area);

      if (area.hasAquaticUnits()) {
        writeEntry("veg-aquatic.csv", this::writeVegAquatic, area);
      }

      if (area.hasLandUnits()) {
        writeEntry("veg-land.csv", this::writeVegLand, area);
      }

      if (area.hasRoads()) {
        writeEntry("veg-roads.csv", this::writeVegRoads, area);
      }

      if (area.hasTrails()) {
        writeEntry("veg-trails.csv", this::writeVegTrails, area);
      }
    }
  }

  private interface EntryWriter {
    void write(Area area) throws IOException;
  }

  private void writeEntry(String name, EntryWriter entryWriter, Area area) throws IOException {
    ZipEntry entry = new ZipEntry(name);
    zip.putNextEntry(entry);
    entryWriter.write(area);
    zip.closeEntry();
  }

  private void writeOverview(Area area) throws IOException {

    Ini ini = new Ini();

    ini.putString("Zone", "name", Simpplle.getCurrentZone().getName());
    ini.putString("Area", "date", area.getDate());
    ini.putString("Area", "kind", area.getKind());
    ini.putString("Area", "name", area.getName());
    ini.putInteger("Area", "acres", area.getAcres());
    ini.putInteger("Area", "aquaticLength", area.getAquaticLength());
    ini.putInteger("Area", "elevRelPosition", area.getElevationRelativePosition());
    ini.putBoolean("Area", "hasKeaneAttributes", area.hasKeaneAttributes());
    ini.putInteger("Area", "polygonWidthFeet", area.getPolygonWidth());
    ini.putInteger("Area", "maxEauID", area.getMaxEauId());
    ini.putInteger("Area", "maxEluID", area.getMaxEluId());
    ini.putInteger("Area", "maxEvuID", area.getMaxEvuId());
    ini.putInteger("Units", "aquatic", area.countAquaticUnits());
    ini.putInteger("Units", "land", area.countLandUnits());
    ini.putInteger("Units", "road", area.countRoadUnits());
    ini.putInteger("Units", "trail", area.countTrailUnits());
    ini.putInteger("Units", "vegetation", area.countVegetationUnits());

    ini.save(writer);

  }

  private void writeHabitatTypeGroupTypes(Area area) throws IOException {

    String[] fields = new String[] {
        "name",
        "description",
        "userCreated"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (SimpplleType value : SimpplleType.allGroupHm.values()) {
      HabitatTypeGroupType type = (HabitatTypeGroupType) value;
      csv.putString("name", type.getName());
      csv.putString("description", type.getDescription());
      csv.putBoolean("userCreated", type.isUserCreated());
      csv.storeRecord();
    }
  }

  private void writeAquaticAdjacent(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "vegID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit unit : area.getAllEau()) {
      if (unit == null) continue;
      for (Evu evu : unit.getAdjacentEvus()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("vegID", evu.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeAquaticNeighbors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "neighbor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit unit : area.getAllEau()) {
      if (unit == null) continue;
      for (NaturalElement neighbor : unit.getNeighbors()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("neighbor", neighbor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeAquaticPredecessors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "predecessor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit unit : area.getAllEau()) {
      if (unit == null) continue;
      for (ExistingAquaticUnit predecessor : unit.getPredecessors()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("predecessor", predecessor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeAquaticSuccessors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "successor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit unit : area.getAllEau()) {
      if (unit == null) continue;
      for (ExistingAquaticUnit successor : unit.getSuccessors()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("successor", successor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeAquaticUnits(Area area) throws IOException {

    String[] fields = new String[]{
        "id",
        "acres",
        "elevation",
        "aspectName",
        "aspect",
        "slope",
        "group",
        "segmentNumber",
        "currentState",
        "initialProcess",
        "length",
        "status"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit unit : area.getAllEau()) {
      if (unit == null) continue;

      csv.putInteger("id", unit.getId());
      csv.putInteger("acres", unit.getAcres());
      csv.putInteger("elevation", unit.getElevation());
      csv.putString("aspectName", unit.getAspectName());
      csv.putDouble("aspect", unit.getAspect());
      csv.putFloat("slope", unit.getSlope());

      csv.putString("group", unit.getLtaValleySegmentGroup().toString());
      csv.putInteger("segmentNumber", unit.getSegmentNumber());
      csv.putString("currentState", unit.getCurrentState().toString());
      csv.putString("initialProcess", unit.getInitialProcess().getType().getProcessName());
      csv.putInteger("length", unit.getLength());
      csv.putString("status", unit.getStatus().toString());

      csv.storeRecord();

    }
  }

  private void writeAquaticUpland(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "vegID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingAquaticUnit eau : area.getAllEau()) {
      if (eau == null) continue;
      for (Evu evu : eau.getUplandEvus()) {
        csv.putInteger("id", eau.getId());
        csv.putInteger("vegID", evu.getId());
      }
    }
  }

  private void writeLandNeighbors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "neighbor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingLandUnit unit : area.getAllElu()) {
      if (unit == null) continue;
      for (NaturalElement neighbor : unit.getNeighbors()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("neighbor", neighbor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeLandUnits(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "acres",
        "elevation",
        "aspectName",
        "aspect",
        "slope",
        "soilType",
        "parentMaterial",
        "landform",
        "depth",
        "latitude",
        "longitude"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingLandUnit elu : area.getAllElu()) {
      if (elu == null) continue;

      csv.putInteger("id", elu.getId());
      csv.putInteger("acres", elu.getAcres());
      csv.putInteger("elevation", elu.getElevation());
      csv.putString("aspectName", elu.getAspectName());
      csv.putDouble("aspect", elu.getAspect());
      csv.putFloat("slope", elu.getSlope());

      csv.putString("soilType", elu.getSoilType().getSoilType());
      csv.putString("parentMaterial", elu.getParentMaterial());
      csv.putString("landform", elu.getLandform());
      csv.putString("depth", elu.getDepth());
      csv.putDouble("latitude", elu.getLatitude());
      csv.putDouble("longitude", elu.getLongitude());

      csv.storeRecord();

    }
  }

  private void writeLandVeg(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "vegID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (ExistingLandUnit unit : area.getAllElu()) {
      if (unit == null) continue;
      for (Evu evu : unit.getAssociatedVegUnits()) {
        csv.putInteger("id", unit.getId());
        csv.putInteger("vegID", evu.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeRoadNeighbors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "neighbor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Roads roads : area.getAllRoads()) {
      if (roads == null) continue;
      for (ManmadeElement neighbor : roads.getNeighbors()) {
        csv.putInteger("id", roads.getId());
        csv.putInteger("neighbor", neighbor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeRoadUnits(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "status",
        "kind"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Roads roads : area.getAllRoads()) {
      if (roads == null) continue;
      csv.putInteger("id", roads.getId());
      csv.putString("status", roads.getStatus().toString());
      csv.putString("kind", roads.getKind().toString());
      csv.storeRecord();
    }
  }

  private void writeRoadVeg(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "vegID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Roads roads : area.getAllRoads()) {
      if (roads == null) continue;
      for (Evu evu : roads.getAssociatedVegUnits()) {
        csv.putInteger("id", roads.getId());
        csv.putInteger("vegID", evu.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeTrailNeighbors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "neighbor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Trails trail : area.getAllTrails()) {
      if (trail == null) continue;
      for (ManmadeElement neighbor : trail.getNeighbors()) {
        csv.putInteger("id", trail.getId());
        csv.putInteger("neighbor", neighbor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeTrailUnits(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "status",
        "kind"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Trails trails : area.getAllTrails()) {
      if (trails == null) continue;
      csv.putInteger("id", trails.getId());
      csv.putString("status", trails.getStatus().toString());
      csv.putString("kind", trails.getKind().toString());
      csv.storeRecord();
    }
  }

  private void writeTrailVeg(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "vegID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Trails trails : area.getAllTrails()) {
      if (trails == null) continue;
      for (Evu evu : trails.getAssociatedVegUnits()) {
        csv.putInteger("id", trails.getId());
        csv.putInteger("vegID", evu.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegAdjacencies(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "adjacent",
        "position",
        "wind",
        "spread",
        "slope"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (AdjacentData data : evu.getAdjacencies()) {
        csv.putInteger("id", evu.getId());
        csv.putInteger("adjacent", data.getEvu().getId());
        csv.putChar("position", data.getPosition());
        csv.putChar("wind", data.getWind());
        csv.putDouble("spread", data.getSpread());
        csv.putDouble("slope", data.getSlope());
        csv.storeRecord();
      }
    }
  }

  private void writeVegAquatic(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "aquaticID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (ExistingAquaticUnit eau : evu.getAssociatedAquaticUnits()) {
        if (eau == null) continue;
        csv.putInteger("id", evu.getId());
        csv.putInteger("aquaticID", eau.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegInitial(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "season",
        "lifeform",
        "species",
        "sizeClass",
        "age",
        "density",
        "habitatTypeGroup",
        "process",
        "probString",
        "treatment"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (VegSimStateData state : evu.getInitialVegStates()) {
        csv.putInteger("id", evu.getId());
        csv.putString("season", state.getSeasonString());
        csv.putString("lifeform", state.getLifeform().getName());
        csv.putString("species", state.getVegType().getSpecies().getSpecies());
        csv.putString("sizeClass", state.getVegType().getSizeClass().getSizeClass());
        csv.putInteger("age", state.getVegType().getAge());
        csv.putString("density", state.getVegType().getDensity().toString());
        csv.putString("habitatTypeGroup", state.getVegType().getHtGrp().getType().getName());
        csv.putString("process", state.getProcess().getProcessName());
        csv.putString("probString", state.getProbString());
        Treatment treatment = evu.getInitialTreatment();
        if (treatment != null) {
          csv.putString("treatment", treatment.getType().toString());
        }
        csv.storeRecord();
      }
    }
  }

  private void writeVegLand(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "landID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (ExistingLandUnit elu : evu.getAssociatedLandUnits()) {
        if (elu == null) continue;
        csv.putInteger("id", evu.getId());
        csv.putInteger("landID", elu.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegNeighbors(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "neighbor"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (NaturalElement neighbor : evu.getNeighbors()) {
        csv.putInteger("id", evu.getId());
        csv.putInteger("neighbor", neighbor.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegRoads(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "roadID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (Roads road : evu.getAssociatedRoadUnits()) {
        if (road == null) continue;
        csv.putInteger("id", evu.getId());
        csv.putInteger("roadID", road.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegTrackingSpecies(Area area) throws IOException {

    String[] fields = new String[] {
        "vegID",
        "season",
        "lifeform",
        "inclusionRuleSpecies",
        "percent"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (VegSimStateData state : evu.getInitialVegStates()) {
        for (InclusionRuleSpecies species : state.getTrackingSpeciesArray()) {
          csv.putInteger("vegID", evu.getId());
          csv.putString("season", state.getSeasonString());
          csv.putString("lifeform", state.getLifeform().getName());
          csv.putString("inclusionRuleSpecies", species.getName());
          csv.putFloat("percent", state.getSpeciesPercent(species));
          csv.storeRecord();
        }
      }
    }
  }

  private void writeVegTrails(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "trailID"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;
      for (Trails trail : evu.getAssociatedTrailUnits()) {
        if (trail == null) continue;
        csv.putInteger("id", evu.getId());
        csv.putInteger("trailID", trail.getId());
        csv.storeRecord();
      }
    }
  }

  private void writeVegUnits(Area area) throws IOException {

    String[] fields = new String[] {
        "id",
        "acres",
        "elevation",
        "slope",
        "habitatTypeGroup",
        "unitNumber",
        "ownership",
        "roadStatus",
        "fmz",
        "specialArea",
        "locationX",
        "locationY",
        "windSpeed",
        "windDirection"
    };

    CsvWriter csv = new CsvWriter(writer, fields);
    for (Evu evu : area.getAllEvu()) {
      if (evu == null) continue;

      HabitatTypeGroupType htGrpType = evu.getHabitatTypeGroup().getType();

      csv.putInteger("id", evu.getId());
      csv.putFloat("acres", evu.getFloatAcres());
      csv.putInteger("elevation", evu.getElevation());
      csv.putFloat("slope", evu.getSlope());

      csv.putString("habitatTypeGroup", htGrpType.getName());
      csv.putString("unitNumber", evu.getUnitNumber());
      csv.putString("ownership", evu.getOwnership());
      csv.putString("roadStatus", evu.getRoadStatus().toString());
      csv.putString("fmz", evu.getFmz().getName());
      csv.putString("specialArea", evu.getSpecialArea());
      csv.putInteger("locationX", evu.getLocationX());
      csv.putInteger("locationY", evu.getLocationY());
      csv.putDouble("windSpeed", evu.getWindSpeed());
      csv.putDouble("windDirection", evu.getWindDirection());

      csv.storeRecord();

    }
  }
}
