package simpplle.comcode;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static simpplle.comcode.SimpplleType.Types.GROUP;

/**
 * Loads an area from a zip file.
 */
public class AreaLoader implements Closeable {

  private ZipFile zipFile;

  /**
   * Creates a new area reader.
   *
   * @param file a zip file containing an area
   * @throws IOException if an I/O exception occurs
   */
  public AreaLoader(File file) throws IOException {
    zipFile = new ZipFile(file);
  }

  /**
   * Closes the zip file used by this reader.
   *
   * @throws IOException if an I/O exception occurs
   */
  public void close() throws IOException {
    zipFile.close();
  }

  /**
   * Loads an area.
   *
   * @return an area
   * @throws IOException if an I/O exception occurs
   * @throws SimpplleError if a required entry is missing
   */
  public Area load() throws IOException, SimpplleError {

    Area area = new Area();

    // Load high-level information about the area

    if (!readEntry("area.ini", this::readOverview, area)) {
      throw new SimpplleError("Area missing required area.ini file");
    }

    readEntry("habitat-type-group-types.csv", this::readHabitatTypeGroupTypes, area);

    // Load the natural and artificial (man-made) units

    readEntry("aquatic-units.csv", this::readAquaticUnits, area);
    readEntry("land-units.csv", this::readLandUnits, area);
    readEntry("road-units.csv", this::readRoadUnits, area);
    readEntry("trail-units.csv", this::readTrailUnits, area);
    readEntry("veg-units.csv", this::readVegUnits, area);

    // Establish relationships between the units

    readEntry("aquatic-adjacent.csv", this::readAquaticAdjacent, area);
    readEntry("aquatic-neighbors.csv", this::readAquaticNeighbors, area);
    readEntry("aquatic-predecessors.csv", this::readAquaticPredecessors, area);
    readEntry("aquatic-successors.csv", this::readAquaticSuccessors, area);
    readEntry("aquatic-upland.csv", this::readAquaticUpland, area);
    readEntry("land-neighbors.csv", this::readLandNeighbors, area);
    readEntry("land-veg.csv", this::readLandVeg, area);
    readEntry("road-neighbors.csv", this::readRoadNeighbors, area);
    readEntry("road-veg.csv", this::readRoadVeg, area);
    readEntry("trail-neighbors.csv", this::readTrailNeighbors, area);
    readEntry("trail-veg.csv", this::readTrailVeg, area);
    readEntry("veg-adjacencies.csv", this::readVegAdjacencies, area);
    readEntry("veg-aquatic.csv", this::readVegAquatic, area);
    readEntry("veg-land.csv", this::readVegLand, area);
    readEntry("veg-neighbors.csv", this::readVegNeighbors, area);
    readEntry("veg-roads.csv", this::readVegRoads, area);
    readEntry("veg-trails.csv", this::readVegTrails, area);

    // Read the initial vegetation state

    readEntry("veg-initial.csv", this::readVegInitial, area);

    // Read the tracking species

    readEntry("veg-tracking.csv", this::readVegTrackingSpecies, area);

    return area;

  }

  private interface EntryReader {
    void read(Area area, InputStream stream) throws IOException;
  }

  private boolean readEntry(String name, EntryReader reader, Area area) throws IOException {
    ZipEntry entry = zipFile.getEntry(name);
    if (entry != null) {
      reader.read(area, zipFile.getInputStream(entry));
      return true;
    } else {
      return false;
    }
  }

  private void readOverview(Area area, InputStream stream) throws IOException {

    Ini ini = new Ini(stream);

    // Zone ignored, but can be used for future zone restriction if needed

    area.setName(ini.getString("Area", "name", "unnamed"));
    area.setKind(ini.getString("Area", "kind", "USER"));
    area.setDate(ini.getString("Area", "date", ""));
    area.setAcres(ini.getInteger("Area", "acres", 0));
    area.setAquaticLength(ini.getInteger("Area", "aquaticLength", 0));
    area.setPolygonWidth(ini.getInteger("Area", "polygonWidthFeet", 0));
    area.setHasKeaneAttributes(ini.getBoolean("Area", "hasKeaneAttributes", false));
    area.setElevationRelativePosition(ini.getInteger("Area", "elevRelPosition", 10));

    // Store the max ID values

    area.setMaxEauId(ini.getInteger("Area", "maxEauID", -1));
    area.setMaxEluId(ini.getInteger("Area", "maxEluID", -1));
    area.setMaxEvuId(ini.getInteger("Area", "maxEvuID", -1));

    // Allocate the unit arrays

    area.setAllEau(new ExistingAquaticUnit[ini.getInteger("Units", "aquatic", 0)]);
    area.setAllElu(new ExistingLandUnit[ini.getInteger("Units", "land", 0)]);
    area.setAllRoads(new Roads[ini.getInteger("Units", "roads", 0)]);
    area.setAllTrails(new Trails[ini.getInteger("Units", "trails", 0)]);
    area.setAllEvu(new Evu[ini.getInteger("Units", "vegetation", 0)]);

    // Enable manual garbage collection if lots of vegetation units

    area.setManualGC(ini.getInteger("Units", "vegetation", 0) > 15000);

  }

  private void readHabitatTypeGroupTypes(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      String name = csv.getString("name", "");
      String description = csv.getString("description", "");
      boolean userCreated = csv.getBoolean("userCreated", false);

      HabitatTypeGroupType groupType = (HabitatTypeGroupType) SimpplleType.get(GROUP, name);
      if (groupType == null) {
        new HabitatTypeGroupType(name, description, userCreated);
      }
    }
  }

  private void readAquaticAdjacent(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingAquaticUnit eau = area.getEau(csv.getInteger("id", -1));
      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      eau.addAdjacentEvu(evu);
    }
  }

  private void readAquaticNeighbors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingAquaticUnit a = area.getEau(csv.getInteger("id", -1));
      ExistingAquaticUnit b = area.getEau(csv.getInteger("neighbor", -1));
      a.addNeighbor(b);
    }
  }

  private void readAquaticPredecessors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingAquaticUnit a = area.getEau(csv.getInteger("id", -1));
      ExistingAquaticUnit b = area.getEau(csv.getInteger("predecessor", -1));
      a.addPredecessor(b);
    }
  }

  private void readAquaticSuccessors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingAquaticUnit a = area.getEau(csv.getInteger("id", -1));
      ExistingAquaticUnit b = area.getEau(csv.getInteger("successor", -1));
      a.addSuccessor(b);
    }
  }

  private void readAquaticUnits(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      ExistingAquaticUnit eau = new ExistingAquaticUnit(csv.getInteger("id", 0));

      eau.setAcres(csv.getInteger("acres", 0));
      eau.setElevation(csv.getInteger("elevation", 0));
      eau.setAspectName(csv.getString("aspectName", ""));
      eau.setAspect(csv.getDouble("aspect", 0.0));
      eau.setSlope(csv.getFloat("slope", 0.0f));

      LtaValleySegmentGroup group = LtaValleySegmentGroup.findInstance(csv.getString("group", ""));
      ProcessType processType = ProcessType.get(csv.getString("initialProcess", ""), true);

      eau.setLtaValleySegmentGroup(group);
      eau.setSegmentNumber(csv.getInteger("segmentNumber", 0));
      eau.setCurrentState(group.getPotentialAquaticState(csv.getString("currentState", "")));
      eau.setInitialProcess(Process.findInstance(processType));
      eau.setLength(csv.getInteger("length", 0));
      eau.setStatus(csv.getString("status", ""));

      area.addEau(eau);

    }
  }

  private void readAquaticUpland(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingAquaticUnit eau = area.getEau(csv.getInteger("id", -1));
      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      eau.addUplandEvu(evu);
    }
  }

  private void readLandNeighbors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingLandUnit a = area.getElu(csv.getInteger("id", -1));
      ExistingLandUnit b = area.getElu(csv.getInteger("neighbor", -1));
      a.addNeighbor(b);
    }
  }

  private void readLandUnits(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      ExistingLandUnit elu = new ExistingLandUnit(csv.getInteger("id", 0));

      elu.setAcres(csv.getInteger("acres", 0));
      elu.setElevation(csv.getInteger("elevation", 0));
      elu.setAspectName(csv.getString("aspectName", ""));
      elu.setAspect(csv.getDouble("aspect", 0.0));
      elu.setSlope(csv.getFloat("slope", 0.0f));

      elu.setSoilType(SoilType.get(csv.getString("soilType", ""), true));
      elu.setParentMaterial(csv.getString("parentMaterial", ""));
      elu.setLandform(csv.getString("landform", ""));
      elu.setDepth(csv.getString("depth", ""));
      elu.setLatitude(csv.getDouble("latitude", 0.0));
      elu.setLongitude(csv.getDouble("longitude", 0.0));

      area.addElu(elu);

    }
  }

  private void readLandVeg(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      ExistingLandUnit elu = area.getElu(csv.getInteger("id", -1));
      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      elu.addAssociatedVegUnit(evu);
    }
  }

  private void readRoadNeighbors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Roads road = area.getRoadUnit(csv.getInteger("id", -1));
      Roads neighbor = area.getRoadUnit(csv.getInteger("neighbor", -1));
      road.addNeighbor(neighbor);
    }
  }

  private void readRoadUnits(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      Roads roads = new Roads(csv.getInteger("id", 0));
      roads.setStatus(Roads.Status.valueOf(csv.getString("status", "")));
      roads.setKind(Roads.Kind.valueOf(csv.getString("kind", "")));

      area.addRoadUnit(roads);

    }
  }

  private void readRoadVeg(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Roads road = area.getRoadUnit(csv.getInteger("id", -1));
      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      road.addAssociatedVegUnit(evu);
    }
  }

  private void readTrailNeighbors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Trails trail = area.getTrailUnit(csv.getInteger("id", -1));
      Trails neighbor = area.getTrailUnit(csv.getInteger("neighbor", -1));
      trail.addNeighbor(neighbor);
    }
  }

  private void readTrailUnits(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      Trails trails = new Trails(csv.getInteger("id", 0));
      trails.setStatus(Trails.Status.valueOf(csv.getString("status", "")));
      trails.setKind(Trails.Kind.valueOf(csv.getString("kind", "")));

      area.addTrailUnit(trails);

    }
  }

  private void readTrailVeg(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Trails trail = area.getTrailUnit(csv.getInteger("id", -1));
      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      trail.addAssociatedVegUnit(evu);
    }
  }

  private void readVegAdjacencies(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    // Adjacencies to vegetated units are stored in arrays which need to be pre-allocated. Since
    // we don't know how many adjacencies each EVU has, we will store a list of adjacencies for
    // each EVU in a temporary container.

    Map<Evu, List<AdjacentData>> map = new HashMap<>();

    boolean oldFormat = false;

    // check for windSpeed
    if(csv.hasField("windSpeed")) {
      oldFormat = true;
    }

    while (csv.nextRecord()) {

      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;

      Evu adjacent = area.getEvu(csv.getInteger("adjacent", -1));
      if (adjacent == null) continue;

      AdjacentData adjacentData = new AdjacentData(adjacent,
                                                   csv.getChar("position", 'N'),
                                                   csv.getChar("wind", 'N'),
                                                   csv.getDouble("spread", 0.0));

      // If wind speed / direction are present in the file, parse and put into evu
      if(oldFormat) {
        adjacent.setWindSpeed(csv.getDouble("windSpeed", 0.0));
        adjacent.setWindDirection(csv.getDouble("windDirection", 0.0));
      }

      adjacentData.setSlope(csv.getDouble("slope", 0.0));

      List<AdjacentData> adjacencies = map.get(evu);
      if (adjacencies == null) {
        adjacencies = new ArrayList<>();
        map.put(evu, adjacencies);
      }
      adjacencies.add(adjacentData);
    }

    // Allocate and populate the adjacency array in each EVU using entries in the hash map. If
    // attributes for the Keane fire spreading algorithm are present, then the units are expected
    // to lie on a grid with eight possible adjacencies. These units are hashed in the array by
    // spread angle in order to speed up adjacency searches during fire spread calculations.

    for (Evu evu : map.keySet()) {

      AdjacentData[] adjacencies;
      if (area.hasKeaneAttributes()) {
        adjacencies = new AdjacentData[8];
      } else {
        adjacencies = new AdjacentData[map.get(evu).size()];
      }

      evu.setNeighborhood(adjacencies);

      int offset = 0;
      for (AdjacentData adjacency : map.get(evu)) {
        if (area.hasKeaneAttributes()) {
          int index = evu.getNeighborIndex(adjacency.getSpread());
          adjacencies[index] = adjacency;
        } else {
          adjacencies[offset] = adjacency;
        }
        offset++;
      }
    }
  }

  private void readVegAquatic(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;
      ExistingAquaticUnit eau = area.getEau(csv.getInteger("aquaticID", -1));
      if (eau == null) continue;
      evu.addAssociatedAquaticUnit(eau);
    }
  }

  private void readVegInitial(Area area, InputStream stream) throws  IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;

      VegSimStateData state = new VegSimStateData(evu.getId());
      state.setTimeStep(0);
      state.setRun(0);
      state.setSeason(Season.valueOf(csv.getString("season", "YEAR")));
      state.setLifeform(Lifeform.findByName(csv.getString("lifeform", "")));
      state.setProcess(ProcessType.get(csv.getString("process", ""), true));
      state.setProb(Evu.parseProbabilityString(csv.getString("probString", "")));

      HabitatTypeGroup group = HabitatTypeGroup.findInstance(csv.getString("habitatTypeGroup", ""));
      if (group != null) {
        Species species = Species.get(csv.getString("species", "UNKNOWN"));
        SizeClass sizeClass = SizeClass.get(csv.getString("sizeClass", "UNKNOWN"));
        int age = csv.getInteger("age", 1);
        Density density = Density.get(csv.getString("density", "UNKNOWN"));
        VegetativeType vegetativeType = group.getVegetativeType(species, sizeClass, age, density);
        state.setVeg(group, vegetativeType);

        String treatmentName = csv.getString("treatment", null);
        if (treatmentName != null) {
          TreatmentType treatmentType = TreatmentType.get(treatmentName, true);
          Treatment treatment = Treatment.createInitialTreatment(treatmentType, vegetativeType);
          evu.addTreatment(treatment);
        }
      }
      evu.setInitialState(state, state.getSeason());
    }
  }

  private void readVegLand(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;
      ExistingLandUnit elu = area.getElu(csv.getInteger("landID", -1));
      if (elu == null) continue;
      evu.addAssociatedLandUnit(elu);
    }
  }

  private void readVegNeighbors(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu a = area.getEvu(csv.getInteger("id", -1));
      Evu b = area.getEvu(csv.getInteger("neighbor", -1));
      a.addNeighbor(b);
    }
  }

  private void readVegRoads(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;
      Roads road = area.getRoadUnit(csv.getInteger("roadID", -1));
      if (road == null) continue;
      evu.addAssociatedRoadUnit(road);
    }
  }

  private void readVegTrackingSpecies(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {

      Evu evu = area.getEvu(csv.getInteger("vegID", -1));
      if (evu == null) continue;

      Lifeform lifeform = Lifeform.findByName(csv.getString("lifeform", "NA"));
      Season season = Season.valueOf(csv.getString("season", "YEAR"));
      VegSimStateData state = evu.getInitialVegState(lifeform, season);
      if (state == null) continue;

      InclusionRuleSpecies species = InclusionRuleSpecies.get(csv.getString("inclusionRuleSpecies", ""), true);
      float percent = csv.getFloat("percent", 0.0f);
      state.addTrackSpecies(species, percent);

    }
  }

  private void readVegTrails(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    while (csv.nextRecord()) {
      Evu evu = area.getEvu(csv.getInteger("id", -1));
      if (evu == null) continue;
      Trails trail = area.getTrailUnit(csv.getInteger("trailID", -1));
      if (trail == null) continue;
      evu.addAssociatedTrailUnit(trail);
    }
  }

  private void readVegUnits(Area area, InputStream stream) throws IOException {

    CsvReader csv = new CsvReader(stream);

    boolean oldFormat = true;

    // check for windSpeed
    if(csv.hasField("windSpeed")) {
      oldFormat = false;
    }

    while (csv.nextRecord()) {

      Evu evu = new Evu(csv.getInteger("id", 0));
      evu.setAcres(csv.getFloat("acres", 0.0f));
      evu.setElevation(csv.getInteger("elevation", 0));
      evu.setSlope(csv.getFloat("slope", 0.0f));
      evu.setUnitNumber(csv.getString("unitNumber", ""));
      evu.setOwnership(csv.getString("ownership", ""));
      evu.setRoadStatus(Roads.Status.lookup(csv.getString("roadStatus", "")));
      evu.setSpecialArea(csv.getString("specialArea", ""));
      evu.setLocationX(csv.getInteger("locationX", -1));
      evu.setLocationY(csv.getInteger("locationY", -1));

      // If wind speed and direction are present, parse them
      if(!oldFormat) {
        evu.setWindSpeed(csv.getDouble("windSpeed", 0.0));
        evu.setWindDirection(csv.getDouble("windDirection", 0.0));
      }

      RegionalZone zone = Simpplle.getCurrentZone();
      String fmzName = csv.getString("fmz", zone.getDefaultFmzName());
      Fmz fmz = zone.getFmz(fmzName);
      if (fmz == null) {
        fmz = new Fmz(fmzName);
        zone.addFmz(fmz);
      }
      evu.setFmz(fmz);

      String habitatTypeGroupType = csv.getString("habitatTypeGroup", "");
      HabitatTypeGroup group = HabitatTypeGroup.findInstance(habitatTypeGroupType);
      if (group == null) {
        evu.setHabitatTypeGroup(new HabitatTypeGroup(habitatTypeGroupType));
      } else {
        evu.setHabitatTypeGroup(group);
      }
      area.addEvu(evu);
    }
  }
}
