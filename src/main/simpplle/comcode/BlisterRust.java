/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.awt.Color;

/**
 * This class has methods for Blister Rust, a type of process.  Blister rust is so common that it is included within the
 * succession pathways
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 * @see simpplle.comcode.Process
 */

public class BlisterRust extends Process { // Does not occur in Eastside.
  private static final String printName = "BLISTER-RUST";
  
  /**
   * BlisterRust constructor.  Inherits from Process superclass
   */
  public BlisterRust() {
    super();

    spreading   = false;
    description = "Blister Rust";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  public int doProbabilityCommon(RegionalZone zone, Evu evu, Lifeform lifeform) {
    return doProbability(evu, lifeform);
  }

  /**
   * If species is rust resistant white pine doProbability is called,
   * else returns 0 meaning no Blister Rust is pressent.
   */
  public int doProbability (WestsideRegionOne zone, Evu evu, Lifeform lifeform) {
    Species species = (Species)evu.getState(SimpplleType.SPECIES);
    if (species != null && species.contains(Species.RRWP)) {
      return doProbabilityCommon(zone,evu, lifeform);
    }
    return 0;
  }

  public int doProbability (SierraNevada zone, Evu evu, Lifeform lifeform) {
    return doProbabilityCommon(zone,evu, lifeform);
  }

  public int doProbability (SouthernCalifornia zone, Evu evu, Lifeform lifeform) {
    return doProbabilityCommon(zone,evu, lifeform);
  }

  public String toString () {
    return printName;
  }
}
