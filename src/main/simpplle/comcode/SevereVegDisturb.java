/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

/**
 * This class defines Severe Vegetative Disturbance, a type of process.
 * This can occur in all zones and therefore has methods for all.  BUT Probability for this is not applicable.  
 * <p>A severe disturbance is an unpredicted event. 
 * Therefore the doProbability common which all zones refer to returns 0.  Also a severe vegetative disturbance is not going to spread.  
 * So all zones return false for doSpreadCommon
 * 
 * @author Documentation by Brian Losi
 * <p>Original authorship: Kirk A. Moeller
 *
 * @see simpplle.comcode.Process
 */

public class SevereVegDisturb extends Process {
  private static final String printName = "SEVERE-VEG-DISTURB";
/**
 * Constructor for Severe Vegetative Disturbance.  Inherits from Process superclass and initializes spreading to false, and default visible columns row and probability columns.  
 */
  public SevereVegDisturb() {
    super();

    spreading   = false;
    description = "SEVERE VEG DISTURB";

    defaultVisibleColumns.add(BaseLogic.Columns.ROW_COL.toString());
    defaultVisibleColumns.add(ProcessProbLogic.Columns.PROB_COL.toString());
  }

  /**
   * outputs  "SEVERE-VEG-DISTURB"
   */
    public String toString() { return printName; }


}
