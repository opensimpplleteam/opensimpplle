/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.comcode;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * This class contains methods for a Regeneration Logic. Regeneration is highly variable.
 * It is dependent on spatial arrangement of plant communities, regeneration mechanisms of species,
 * and spatial arrangement of adjacent plant communities.
 * <p>Regeneration logic is applied under either succession or fire regeneration logic.  This class
 * covers both.
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public abstract class RegenerationLogic {
  private static final int version = 3;

  private static HashMap<HabitatTypeGroupType, RegenBaseLogic> fireData = new HashMap<>();
  private static HashMap<HabitatTypeGroupType, RegenBaseLogic> succData = new HashMap<>();

  public enum DataKinds { FIRE, SUCCESSION }
  public static final DataKinds FIRE = DataKinds.FIRE;
  public static final String FIRE_STR = FIRE.toString();

  public static final DataKinds SUCCESSION = DataKinds.SUCCESSION;
  public static final String SUCCESSION_STR = SUCCESSION.toString();

  private static HabitatTypeGroupType currentEcoGroup;

  /**
   * Sets the default habitat type group. If fire regeneration applies to any
   * habitat type group type in the fire data, uses ANY as default habitat type group type (eco group)
   * Otherwise sets it to the key (habitat type group type) in the fire data hashmap.
   * @param kind
   */
  public static void setDefaultEcoGroup(DataKinds kind) {
    switch (kind) {
      case FIRE:
        if (fireData.containsKey(HabitatTypeGroupType.ANY)) {
          currentEcoGroup = HabitatTypeGroupType.ANY;
        }
        else {
          for (HabitatTypeGroupType group : fireData.keySet()) {
            currentEcoGroup = group;
            break;
          }
        }
    }
  }
  /**
   * Gets the fire Logic Instance
   * @param kind will be either fire or succession.
   * @return
   */
  public static BaseLogic getLogicInstance(String kind) {
    return getData(DataKinds.valueOf(kind));
  }

  public static BaseLogic getData(DataKinds kind, HabitatTypeGroupType ecoGroup) {
    return getData(kind,ecoGroup,false);
  }
  /**
   * Gets the fire or succession regeneration data, if some exists.  If not will add the succession or fire regeneration info to the fireData or succData hashmaps.  
   * @param kind either fire or succession regeneration
   * @param ecoGroup 
   * @param addIfNull - if the data hashmap for either succession or fire is null will add the data to it.  
   * @return
   */
  public static BaseLogic getData(DataKinds kind, HabitatTypeGroupType ecoGroup, boolean addIfNull) {
    BaseLogic result;
    switch (kind) {
      case FIRE:
        result = fireData.get(ecoGroup);
        if (result == null && addIfNull) {
          fireData.put(ecoGroup,new RegenBaseLogic(kind));
        }
        return fireData.get(ecoGroup);
      case SUCCESSION:
        result = succData.get(ecoGroup);
        if (result == null && addIfNull) {
          succData.put(ecoGroup,new RegenBaseLogic(kind));
        }
        return succData.get(ecoGroup);
      default:
        return null;
    }
  }
  public static BaseLogic getData(DataKinds kind) {
    return getData(kind,false);
  }
  public static BaseLogic getData(DataKinds kind, boolean addIfNull) {
    return getData(kind,currentEcoGroup,addIfNull);
  }

  public static int getRowCount(DataKinds kind) {
    return getData(kind).getRowCount(kind.toString());
  }

  public static int getColumnCount(DataKinds kind) {
    return getData(kind).getVisibleColumnCount(kind.toString());
  }
  /**
   * Checks if there is either fire or succession regeneration data.
   * @return True if either fire or succession data hashmaps are not null.
   */
  public static boolean isDataPresent() {
    return isDataPresent(FIRE) && isDataPresent(SUCCESSION);
  }
  /**
   * Checks if a particular kind of regeneration data is present in the fire or succession regen data hashmaps.
   * @param kind either fire or succession regeneration data
   * @return true if there is data present
   */
  public static boolean isDataPresent(DataKinds kind) {
    return (getData(kind) != null && getData(kind).getRowCount(kind.toString()) > 0);
  }
  /**
   * Clears the regeneration hashmap for either fire or succession regeneration.
   * @param kind either fire or succession.
   */
  public static void clearData(DataKinds kind) {
    switch (kind) {
      case FIRE: fireData.clear(); break;
      case SUCCESSION: succData.clear(); break;
    }
  }

  public static Object getValueAt(int row, int col, DataKinds kind) {
    return getData(kind).getValue(row,col,kind.toString());
  }

  public static void setSpecies(int row, Species species, DataKinds kind) {
    RegenerationData regenData =
      (RegenerationData)getData(kind).getRow(row,kind.toString());
    regenData.setSpecies(species);
  }

  public static void addDataRows(int insertPos, String kind, Vector speciesList) {
    for (int i=0; i<speciesList.size(); i++) {
      addDataRow((Species)speciesList.get(i),insertPos,DataKinds.valueOf(kind));
    }
  }
  public static void addDataRow(int insertPos, DataKinds kind) {
    addDataRow(null,insertPos,kind);
  }
  private static void addDataRow(Species species, int insertPos, DataKinds kind) {
    RegenerationData regenData;
    switch(kind) {
      case FIRE:       regenData = new FireRegenerationData(currentEcoGroup);     break;
      case SUCCESSION: regenData = new SuccessionRegenerationData(false,currentEcoGroup); break;
      default: return;
    }
    if (species != null) {
      regenData.species = species;
    }
    getData(kind,true).addRow(insertPos,kind.toString(),regenData);
    markChanged(kind);
  }

  public static void deleteDataRow(int row, DataKinds kind) {
    getData(kind).removeRow(row,kind.toString());
    markChanged(kind);
  }

  public static boolean isSpeciesPresent(Species species, DataKinds kind) {
    BaseLogic logic = getData(kind);
    ArrayList<AbstractLogicData> dataList = logic.getAllRows(kind.toString());
    if (dataList != null) {
      for (AbstractLogicData data : dataList) {
        RegenerationData regenData = (RegenerationData)data;
        if (regenData.getSpecies() == species) { return true; }
      }
    }
    return false;
  }

  private static RegenerationData findRegenData(HabitatTypeGroupType ecoGroup, Evu evu, Lifeform lifeform, DataKinds kind) {
    int cStep = Simulation.getCurrentTimeStep();
    return findRegenData(ecoGroup,evu,cStep,lifeform,kind);
  }
  /**
   * This is one of the more important methods in regeneration logic.  It checks to see if there is regeneration data is present for a particular ecogropu
   * evu, time step, lifeform and kind
   * @param ecoGroup
   * @param evu
   * @param tStep time step.
   * @param lifeform
   * @param kind fire or succession
   * @return
   */
  private static RegenerationData findRegenData(HabitatTypeGroupType ecoGroup, Evu evu,
                                                int tStep, Lifeform lifeform, DataKinds kind) {

    BaseLogic logic = getData(kind,ecoGroup);
    if (logic != null) {
      ArrayList<AbstractLogicData> dataList = logic.getAllRows(kind.toString());
      if (dataList != null) {
        for (AbstractLogicData data : dataList) {
          RegenerationData regenData = (RegenerationData) data;
          if (regenData.isMatch(evu, tStep, lifeform)) {
            recordRuleIndex(evu, dataList.indexOf(regenData), kind, tStep, lifeform);
            return regenData;
          }
        }
      }
    }
    if (ecoGroup != HabitatTypeGroupType.ANY) {
      return findRegenData(HabitatTypeGroupType.ANY,evu,tStep,lifeform,kind);
    }
    return null;
  }

  private static void recordRuleIndex(Evu evu, int index, DataKinds kind, int timestep, Lifeform lifeform) {
    VegSimStateData state = evu.getState(timestep, lifeform);

    if (state == null) return;

    if (kind == SUCCESSION) {
      state.setSuccessionRegenerationRuleIndex(index);
    } else if (kind == FIRE) {
      state.setFireRegenerationRuleIndex(index);
    }
  }

  private static RegenerationData findRegenDataSuccInLandscapeSeed(HabitatTypeGroupType ecoGroup,
                                                                   Evu evu, VegSimStateData state,
                                                                   int tStep, Lifeform lifeform,
                                                                   DataKinds kind)
  {
    BaseLogic logic = getData(kind,ecoGroup);
    if (logic != null) {
      ArrayList<AbstractLogicData> dataList = logic.getAllRows(kind.toString());
      if (dataList != null) {
        for (AbstractLogicData data : dataList) {
          RegenerationData regenData = (RegenerationData) data;
          if (regenData.isMatch(evu, state, tStep, lifeform)) {
            return regenData;
          }
        }
      }
    }
    if (ecoGroup != HabitatTypeGroupType.ANY) {
      return findRegenDataSuccInLandscapeSeed(HabitatTypeGroupType.ANY,evu,state,tStep,lifeform,kind);
    }
    return null;
  }

  /**
   * Checks if a species is a regeneration succession species.  
   * @param ecoGroup
   * @param evu
   * @param lifeform
   * @return
   */
  public static boolean isSuccessionSpecies(HabitatTypeGroupType ecoGroup,
                                            Evu evu,Lifeform lifeform) {
    SuccessionRegenerationData regenData =
        (SuccessionRegenerationData)findRegenData(ecoGroup,evu,lifeform,SUCCESSION);
    if (regenData == null) { return false; }

    return (
        regenData.succession.booleanValue() ||
        (regenData.successionSpecies != null && regenData.successionSpecies.size() > 0));
  }

  public static boolean useSuccessionDominantSeed(HabitatTypeGroupType ecoGroup,
                                                  Evu evu,Lifeform lifeform) {
    SuccessionRegenerationData regenData =
      (SuccessionRegenerationData)findRegenData(ecoGroup,evu,lifeform,SUCCESSION);
    if (regenData == null) { return false; }

    return ((regenData != null) ? regenData.succession.booleanValue() : false);
  }

  public static VegetativeType getResproutingState(HabitatTypeGroupType ecoGroup, Evu evu,
                                                   Lifeform lifeform) {
    FireRegenerationData regenData =
      (FireRegenerationData)findRegenData(ecoGroup,evu,lifeform,FIRE);
    if (regenData == null) { return null; }

    // resprouting is only allowed one state, but is in a vector
    // to simplify the table logic.
    if (regenData.resprouting == null || regenData.resprouting.size() == 0) {
      return null;
    }
    return (VegetativeType)regenData.resprouting.get(0);
  }

  public static VegetativeType getAdjResproutingState(HabitatTypeGroupType ecoGroup, Evu evu,
                                                      int tStep, Lifeform lifeform) {

    FireRegenerationData regenData =
       (FireRegenerationData)findRegenData(ecoGroup,evu,tStep,lifeform,FIRE);
    if (regenData == null) { return null; }

    // Adjacent resprouting is only allowed one state, but is in a vector
    // to simplify the table logic.
    if (regenData.adjResprouting != null && (regenData.adjResprouting.size() != 0)) {
      return (VegetativeType)regenData.adjResprouting.get(0);
    }
    else { return null; }
  }

  public static VegetativeType getInPlaceSeedState(HabitatTypeGroupType ecoGroup, Evu evu,
                                                   Lifeform lifeform) {
    FireRegenerationData regenData =
        (FireRegenerationData)findRegenData(ecoGroup,evu,lifeform,FIRE);
    if (regenData == null) { return null; }

    // In Place Seed is only allowed one state, but is in a vector
    // to simplify the table logic.
    if (regenData.inPlaceSeed == null || regenData.inPlaceSeed.size() == 0) {
      return null;
    }
    return (VegetativeType)regenData.inPlaceSeed.get(0);
  }

  /**
   * Special version to allow the saved last Lifeform state to be passed
   * along into the chain of functions here.  This makes certain that we don't
   * end up trying to access a previous state in the unit that is no longer
   * in memory.
   * Should also note that it is only succession regen that that does this
   * looking way back in time to find out if there was seed in the past, say
   * if trees were wiped out by a stand-replacing fire.
   */
  public static VegetativeType getInLandscapeSeedState(HabitatTypeGroupType ecoGroup, Evu evu,
                                                       VegSimStateData state,
                                                       int tStep,
                                                       Lifeform lifeform) {

    FireRegenerationData regenData =
        (FireRegenerationData)findRegenDataSuccInLandscapeSeed(ecoGroup,evu,state,tStep,lifeform,FIRE);
    if (regenData == null) { return null; }

    // In Landscape Seed is only allowed one state, but is in a vector
    // to simplify the table logic.
    if (regenData.inLandscape == null || regenData.inLandscape.size() == 0) {
      return null;
    }
    return (VegetativeType)regenData.inLandscape.get(0);
  }

  public static VegetativeType getInLandscapeSeedState(HabitatTypeGroupType ecoGroup, Evu evu,
                                                       int tStep, Lifeform lifeform) {

    FireRegenerationData regenData =
        (FireRegenerationData)findRegenData(ecoGroup,evu,tStep,lifeform,FIRE);
    if (regenData == null) { return null; }

    // In Landscape Seed is only allowed one state, but is in a vector
    // to simplify the table logic.
    if (regenData.inLandscape == null || regenData.inLandscape.size() == 0) {
      return null;
    }
    return (VegetativeType)regenData.inLandscape.get(0);
  }

  public static ArrayList<VegetativeType> getAdjacentStates(HabitatTypeGroupType ecoGroup,Evu evu,
                                                            int tStep, Lifeform lifeform) {
    FireRegenerationData regenData =
       (FireRegenerationData)findRegenData(ecoGroup,evu,tStep,lifeform,FIRE);

    if (regenData == null) { return null; }
    return regenData.adjacent;
  }

  public static ArrayList<RegenerationSuccessionInfo> getSuccessionSpecies(
                  HabitatTypeGroupType ecoGroup, Evu evu,Lifeform lifeform) {

    SuccessionRegenerationData regenData =
      (SuccessionRegenerationData)findRegenData(ecoGroup,evu,lifeform,SUCCESSION);
    if (regenData == null) { return null; }
    return regenData.successionSpecies;
  }

//  public static void setData(Object value, int row, int col, DataKinds kind) {
//    switch (kind) {
//      case FIRE:       setFireData(value,row,col); break;
//      case SUCCESSION: setSuccData(value,row,col); break;
//      default:
//    }
//  }
//  private static void setFireData(Object value, int row, int col) {
//    switch (col) {
//      case FireRegenerationData.RESPROUTING_COL:
//      case FireRegenerationData.ADJ_RESPROUTING_COL:
//      case FireRegenerationData.IN_LANDSCAPE_COL:
//      case FireRegenerationData.IN_PLACE_SEED_COL:
//      case FireRegenerationData.ADJACENT_COL:
//        // Set in the table editor
//        break;
//    }
//    markChanged(FIRE);
//  }
//  private static void setSuccData(Object value, int row, int col) {
//    switch (col) {
//      case SuccessionRegenerationData.SUCCESSION_SPECIES_COL:
//        // Set in the table editor
//        break;
//      case SuccessionRegenerationData.SUCCESSION_COL:
//        BaseLogic logic = getData(SUCCESSION);
//        logic.getValueAt(row,SUCCESSION.toString());
//        SuccessionRegenerationData regenData =
//          (SuccessionRegenerationData)logic.getValueAt(row,SUCCESSION.toString());
//        regenData.succession = (Boolean)value;
//        break;
//    }
//    markChanged(SUCCESSION);
//  }

  public static void readDataLegacy(File infile) throws SimpplleError {
    GZIPInputStream gzip_in;
    BufferedReader fin;

    try {
      gzip_in = new GZIPInputStream(new FileInputStream(infile));
      fin = new BufferedReader(new InputStreamReader(gzip_in));
      readDataLegacy(fin);
      fin.close();
      gzip_in.close();
//
//      setFile(infile);
    }
    catch (IOException err) {
      throw new SimpplleError("Error reading file");
    }
    catch (ParseError err) {
      throw new SimpplleError(err.msg);
    }
    catch (Exception err) {
      err.printStackTrace();
      throw new SimpplleError("An Exception occurred while reading the input file.");
    }
  }

  public static void readDataLegacy(BufferedReader reader) throws ParseError, IOException {

    String line = reader.readLine();
    if (line == null) {
      throw new ParseError("No data in Regeneration Logic File");
    }

    // Line 1 : Number of rows in file
    int rowCount;
    try {
      rowCount = Integer.parseInt(line);
    } catch (NumberFormatException err) {
      throw new ParseError("Invalid row count in Regeneration Logic File.");
    }

    fireData.clear();
    succData.clear();

    // Line 2 : Comma-delimited list of preferred adjacent species
    line = reader.readLine();
    StringTokenizerPlus strTok = new StringTokenizerPlus(line,",");
    List<Species> prefSpecies = new ArrayList<>();
    while (strTok.hasMoreTokens()) {
      String token = strTok.nextToken();
      prefSpecies.add(Species.get(token, true));
    }
    for (HabitatTypeGroup group : HabitatTypeGroup.getAllLoadedGroups()) {
      List<Species> groupPrefSpecies = group.getPreferredAdjacentSpecies();
      for (Species species : prefSpecies) {
        groupPrefSpecies.add(species);
      }
    }

    // Line 3-END : Each line contains fire and succession regeneration rules for a species
    line = reader.readLine();
    for (int row=0; row < rowCount; row++) {

      strTok = new StringTokenizerPlus(line,",");

      Species species = Species.get(strTok.getToken(),true);

      FireRegenerationData regenData = new FireRegenerationData();
      regenData.setSpecies(species);
      regenData.setResprouting(strTok.getListValue());
      regenData.setAdjResprouting(strTok.getListValue());
      regenData.setInPlaceSeed(strTok.getListValue());
      regenData.setInLandscape(strTok.getListValue());
      regenData.setAdjacent(strTok.getListValue());

      SuccessionRegenerationData succRegenData = new SuccessionRegenerationData();
      succRegenData.setSpecies(species);
      succRegenData.setSuccession(Boolean.valueOf(strTok.getToken()));
      succRegenData.setSuccessionSpecies(strTok.getListValue());

      HabitatTypeGroupType ecoGroup;
      if (strTok.hasMoreTokens()) {
        ecoGroup = HabitatTypeGroupType.get(strTok.getToken());
        regenData.setEcoGroup(ecoGroup);
        succRegenData.setEcoGroup(ecoGroup);
      } else {
        ecoGroup = HabitatTypeGroupType.ANY;
      }

      BaseLogic fireLogic = getData(FIRE, ecoGroup, true);
      fireLogic.addRow(FIRE.toString(), regenData);

      BaseLogic succLogic = getData(SUCCESSION, ecoGroup, true);
      succLogic.addRow(FIRE.toString(), succRegenData);

      line = reader.readLine();
      if (line == null || line.trim().length() == 0) {
        break;
      }
    }

    setDefaultEcoGroup(SUCCESSION);
    setDefaultEcoGroup(FIRE);
    sortData();

  }

  private static void sortData() {
    for (HabitatTypeGroupType ecoGroup : fireData.keySet()) {
      sortData(getData(FIRE,ecoGroup).getAllRows(FIRE.toString()));
    }
    for (HabitatTypeGroupType ecoGroup : succData.keySet()) {
      sortData(getData(SUCCESSION,ecoGroup).getAllRows(SUCCESSION.toString()));
    }
  }
  private static void sortData(ArrayList<AbstractLogicData> dataList) {
    if (dataList == null) { return; }

    HashMap lifeformHm = new HashMap();

    RegenerationData regenData;
    ArrayList        list;
    Lifeform         lifeform;
    for (int i=0; i<dataList.size(); i++) {
      regenData = (RegenerationData)dataList.get(i);
      lifeform = regenData.species.getLifeform();
      list = (ArrayList)lifeformHm.get(lifeform.toString());
      if (list == null) {
        list = new ArrayList();
        lifeformHm.put(lifeform.toString(),list);
      }
      list.add(regenData);
    }

    dataList.clear();

    Lifeform[] allLives = Lifeform.getLifeformsByDominance();

    for (int i=0; i<allLives.length; i++) {
      list = (ArrayList)lifeformHm.get(allLives[i].toString());
      if (list == null) { continue; }

      Collections.sort(list);
      for (int j=0; j<list.size(); j++) {
        dataList.add((RegenerationData)list.get(j));
      }
      list.clear();
      list = null;
    }
  }

  public static void makeBlankLogic(DataKinds kind) {
    ArrayList<SimpplleType> list = Species.getList(SimpplleType.SPECIES);
    if (list == null || list.size() == 0) { return; }

    BaseLogic logic = getData(kind);
    if (logic != null) {
      logic.deleteAllRows(kind.toString());
    }
    switch(kind) {
      case FIRE:
        fireData.clear();
        for (HabitatTypeGroup group : HabitatTypeGroup.getAllLoadedGroups()) {
          group.getPreferredAdjacentSpecies().clear();
        }
        break;
      case SUCCESSION: succData.clear(); break;
    }

    logic = getData(kind,HabitatTypeGroupType.ANY,true);

    for (int i=0; i<list.size(); i++) {
      if (kind == FIRE) {
        logic.addRow(kind.toString(),new FireRegenerationData((Species) list.get(i)));
      }
      else if (kind == SUCCESSION) {
        logic.addRow(kind.toString(),new SuccessionRegenerationData((Species) list.get(i), false));
      }
    }
    setDefaultEcoGroup(kind);
  }

  public static boolean hasChanged(DataKinds kind) {
    if (kind == FIRE) {
      return SystemKnowledge.hasChangedOrUserData(SystemKnowledge.REGEN_LOGIC_FIRE);
    }
    else {
      return SystemKnowledge.hasChangedOrUserData(SystemKnowledge.REGEN_LOGIC_SUCC);
    }
  }

  public static void markChanged(DataKinds kind) {
    if (kind == FIRE) {
      SystemKnowledge.markChanged(SystemKnowledge.REGEN_LOGIC_FIRE);
    }
    else {
      SystemKnowledge.markChanged(SystemKnowledge.REGEN_LOGIC_SUCC);
    }
  }

  /**
   * Sets the current eco group for regeneration logic.
   * @param kind will be either REGEN_LOGIC_FIRE or REGEN_LOGIC_SUCC (fire or succession regeneration)
   * @param ecoGroup
   */
  public static void setCurrentEcoGroup(DataKinds kind,HabitatTypeGroupType ecoGroup) {
    currentEcoGroup = ecoGroup;
    if (kind != null) {
      getData(kind, currentEcoGroup, true);
    }
  }

  private static void setFile(File newFile, DataKinds kind) {
    if (kind == FIRE) {
      SystemKnowledge.setFile(SystemKnowledge.REGEN_LOGIC_FIRE, newFile);
      SystemKnowledge.markChanged(SystemKnowledge.REGEN_LOGIC_FIRE);
    }
    else {
      SystemKnowledge.setFile(SystemKnowledge.REGEN_LOGIC_SUCC, newFile);
      SystemKnowledge.markChanged(SystemKnowledge.REGEN_LOGIC_SUCC);
    }
  }

  public static void clearFile(DataKinds kind) {
    if (kind == FIRE) {
      SystemKnowledge.clearFile(SystemKnowledge.REGEN_LOGIC_FIRE);
    }
    else {
      SystemKnowledge.clearFile(SystemKnowledge.REGEN_LOGIC_SUCC);
    }
  }
  public static void closeFile(DataKinds kind) {
    if (kind == FIRE) {
      clearFile(kind);
      SystemKnowledge.setHasChanged(SystemKnowledge.REGEN_LOGIC_FIRE, false);
    }
    else {
      clearFile(kind);
      SystemKnowledge.setHasChanged(SystemKnowledge.REGEN_LOGIC_SUCC, false);
    }
  }

  public static void saveFire(ObjectOutputStream stream) throws SimpplleError {
    save(stream,FIRE);
  }
  public static void saveSuccession(ObjectOutputStream stream) throws SimpplleError {
    save(stream,SUCCESSION);
  }

  public static void save(ObjectOutputStream s, DataKinds kind) throws SimpplleError {
    try {
      s.writeInt(version);
      if (kind == FIRE) {
        List<HabitatTypeGroup> groups = HabitatTypeGroup.getAllLoadedGroups();
        s.writeInt(groups.size());
        for (HabitatTypeGroup group : groups) {
          s.writeObject(group.getName());
          List<Species> prefSpecies = group.getPreferredAdjacentSpecies();
          s.writeInt(prefSpecies.size());
          for (Species species : prefSpecies) {
            s.writeObject(species.getSpecies());
          }
        }
      }

      HashMap<HabitatTypeGroupType, RegenBaseLogic> data;
      switch (kind) {
        case FIRE:
          data = fireData;
          break;
        case SUCCESSION:
          data = succData;
          break;
        default:
          data = succData;
      }

      s.writeInt( (getData(kind) != null) ? data.size() : 0);
      for (HabitatTypeGroupType ecoGroup : data.keySet()) {
        ecoGroup.writeExternalSimple(s);
        BaseLogic logic = getData(kind,ecoGroup);
        logic.save(s);
      }

    }
    catch (IOException ex) {
    }
  }

  public static void readFire(ObjectInputStream stream) throws IOException, ClassNotFoundException
 {
    read(stream,FIRE);
    setDefaultEcoGroup(FIRE);
  }

  public static void readSuccession(ObjectInputStream stream) throws IOException, ClassNotFoundException
  {
    read(stream,SUCCESSION);
    setDefaultEcoGroup(SUCCESSION);
  }

  public static void read(ObjectInputStream in, DataKinds kind) throws IOException, ClassNotFoundException
  {
    int version = in.readInt();

    if (kind == FIRE) {

      if (version == 3) {

        // Add preferred adjacent species to specific loaded habitat type groups
        int numEcoGroups = in.readInt();
        for (int i = 0; i < numEcoGroups; i++) {
          String groupName = (String)in.readObject();
          HabitatTypeGroup group = HabitatTypeGroup.findInstance(groupName);
          List<Species> groupSpecies = group.getPreferredAdjacentSpecies();
          int numPrefSpecies = in.readInt();
          for (int j = 0; j < numPrefSpecies; j++) {
            String speciesName = (String)in.readObject();
            groupSpecies.add(Species.get(speciesName, true));
          }
        }

      } else {

        // Load all of the preferred adjacent species
        int numSpecies = in.readInt();
        List<Species> prefSpecies = new ArrayList<>(numSpecies);
        for (int i = 0; i < numSpecies; i++) {
          prefSpecies.add((Species) SimpplleType.readExternalSimple(in, SimpplleType.SPECIES));
        }

        // Add preferred adjacent species to all loaded habitat type groups
        for (HabitatTypeGroup group : HabitatTypeGroup.getAllLoadedGroups()) {
          List<Species> groupPrefSpecies = group.getPreferredAdjacentSpecies();
          for (Species species : prefSpecies) {
            groupPrefSpecies.add(species);
          }
        }
      }
    }
    if (version == 1) {
      readVersion1(in,kind);
      return;
    }

    switch (kind) {
      case FIRE:
        fireData.clear();
        break;
      case SUCCESSION:
        succData.clear();
        break;
    }

    int size = in.readInt();
    for (int i=0; i<size; i++) {
      HabitatTypeGroupType ecoGroup =
          (HabitatTypeGroupType)SimpplleType.readExternalSimple(in,SimpplleType.HTGRP);
      BaseLogic logic = getData(kind,ecoGroup,true);
      logic.read(in);
    }
  }

  public static void readVersion1(ObjectInputStream in, DataKinds kind)
      throws IOException, ClassNotFoundException
  {
    switch (kind) {
      case FIRE:
        fireData.clear();
        break;
      case SUCCESSION:
        succData.clear();
        break;
    }
    int size = in.readInt();
    for (int i=0; i<size; i++) {
      HabitatTypeGroupType ecoGroup =
          (HabitatTypeGroupType)SimpplleType.readExternalSimple(in,SimpplleType.HTGRP);
      BaseLogic logic = getData(kind,ecoGroup,true);
      int listSize = in.readInt();
      for (int l=0; l<listSize; l++) {
        if (kind == FIRE) {
          logic.addRow(kind.toString(),(FireRegenerationData)in.readObject());
        }
        else if (kind == SUCCESSION) {
          logic.addRow(kind.toString(),(SuccessionRegenerationData)in.readObject());
        }
      }
    }
  }

  public static HabitatTypeGroupType getCurrentEcoGroup() {
    return currentEcoGroup;
  }

  public static int getColumnNumFromName(DataKinds kind,String name) {
    BaseLogic logic = getData(kind);
    switch (kind) {
      case FIRE:       return FireRegenerationData.getColumnNumFromName(logic,name);
      case SUCCESSION: return SuccessionRegenerationData.getColumnNumFromName(logic,name);
      default: return -1;
    }

  }

  public static String getColumnName(DataKinds kind,int visibleCol) {
    BaseLogic logic = getData(kind);

    String colName = logic.getVisibleColumnName(kind.toString(),visibleCol);
    int col = logic.getColumnPosition(kind.toString(),colName);

    String name;
    switch (kind) {
      case FIRE:       name = FireRegenerationData.getColumnName(col);       break;
      case SUCCESSION: name = SuccessionRegenerationData.getColumnName(col); break;
      default:         name = logic.getColumnName(col);
    }
    return name;
  }

  public static void duplicateRow(int row,int insertPos, DataKinds kind) {
    AbstractLogicData logicData, newLogicData;

    switch (kind) {
      case FIRE:
        logicData = new FireRegenerationData();
        break;
      case SUCCESSION:
        logicData = new SuccessionRegenerationData(false);
        break;
      default:
        return;
    }
    newLogicData = logicData.duplicate();

    getData(kind).addRow(insertPos,kind.toString(),newLogicData);
  }
}
