package simpplle.comcode;

/**
 * Reports application status to users.
 */
public interface StatusReporter {

  void report(String status);

  void clear();

}
