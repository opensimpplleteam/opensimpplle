package simpplle.comcode;

import java.io.*;
import java.util.StringTokenizer;

/**
 * Loads a spatial relations file. Relationships are established between units within an existing
 * area. The current implementation does not require units to exist before loading the relations.
 */
public class SpatialRelateLoader {

  private File file;
  private File logFile;

  public SpatialRelateLoader(File file, File logFile) {
    this.file = file;
    this.logFile = logFile;
  }

  public void load(Area area) throws IOException, SimpplleError, ParseError {

    try (FileWriter fw = new FileWriter(logFile);
         FileReader fr = new FileReader(file);
         PrintWriter log = new PrintWriter(fw);
         BufferedReader reader = new BufferedReader(fr)) {

      String line = reader.readLine();
      while (line != null) {
        StringTokenizer tokenizer = new StringTokenizer(line.trim());
        String token = tokenizer.nextToken();

        // Ensure section starts with "begin" keyword
        if (token == null || !token.equalsIgnoreCase("begin")) {
          throw new SimpplleError("Invalid spatial relationships file: missing BEGIN keyword");
        }

        // Ensure that a relation keyword follows "begin"
        token = tokenizer.nextToken();
        if (token == null) {
          throw new SimpplleError("Invalid spatial relationships file: missing relation keyword");
        }

        // Parse the relations based on the keyword
        RelationParser parser;
        switch (token.toLowerCase()) {

          case "vegetation-vegetation":
            parser = new ParseNewNeighbors();
            break;

          case "vegetation-vegetation-keane":
            parser = new ParseNewNeighborsKeane();
            area.setHasKeaneAttributes(true);
            break;

          case "vegetation-vegetation-keane-new":
            parser = new ParseNeighborsKeaneNew();
            area.setHasKeaneAttributes(true);
            break;

          case "landform-landform":
            parser = new ParseLandNeighbors();
            break;

          case "aquatic-aquatic":
            parser = new ParseAquaticNeighbors();
            break;

          case "vegetation-landform":
            parser = new ParseVegLandRelations();
            break;

          case "vegetation-aquatic":
            parser = new ParseAquaticVegRelations();
            break;

          case "roads-roads":
            parser = new ParseRoadNeighbors();
            break;

          case "trails-trails":
            parser = new ParseTrailNeighbors();
            break;

          case "vegetation-roads":
            parser = new ParseVegRoadRelations();
            break;

          case "vegetation-trails":
            parser = new ParseVegTrailRelations();
            break;

          default:
            parser = null;

        }

        boolean success = false;
        if (parser == null) {
          reader.readLine();
        } else {
          success = parser.readSection(area, reader, log);
        }

        if (!success) {
          throw new SimpplleError("Could not load spatial relations. Please check log file.");
        }

        line = reader.readLine();

        // Skip blank lines at end of section
        while (line != null && !line.trim().toUpperCase().startsWith("BEGIN")) {
          line = reader.readLine();
        }
      }
    }
  }
}
