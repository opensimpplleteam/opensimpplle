package simpplle.comcode;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Reads records from a stream of comma separated values. The stream should contain one record per
 * line, with the first line defining the names of the fields in each record. Records are accessed
 * one at a time in sequential order. Fields are delimited by a single character, which must not
 * appear in any values. Users of this class must call nextRecord() to access the first record,
 * which enables looping through all records using while(reader.nextRecord()).
 */
public class CsvReader implements Closeable {

  private BufferedReader reader;
  private String delimiter;
  private String[] fields;
  private Map<String,String> values;

  /**
   * Creates a CSV reader.
   *
   * @param file a file to read from
   * @throws IOException if an I/O exception occurs
   */
  public CsvReader(File file) throws IOException {
    this(new FileInputStream(file), ',');
  }

  /**
   * Creates a CSV reader.
   *
   * @param stream a stream to read from
   * @throws IOException if an I/O exception occurs
   */
  public CsvReader(InputStream stream) throws IOException {
    this(stream, ',');
  }

  /**
   * Creates a CSV reader with an alternate delimiter.
   *
   * @param file a file to read from
   * @param delimiter the field separator character
   * @throws IOException if an I/O exception occurs
   */
  public CsvReader(File file, char delimiter) throws IOException {
    this(new FileInputStream(file), delimiter);
  }

  /**
   * Creates a CSV reader with an alternate delimiter.
   *
   * @param stream a stream to read from
   * @param delimiter the field separator character
   * @throws IOException if an I/O exception occurs
   */
  public CsvReader(InputStream stream, char delimiter) throws IOException {
    this.delimiter = Character.toString(delimiter);
    this.reader = new BufferedReader(new InputStreamReader(stream));
    this.values = new HashMap<>();

    // Store the fields from the header
    String header = reader.readLine();
    if (header == null) {
      fields = new String[0];
    } else {
      fields = header.split(this.delimiter);
    }
  }

  /**
   * Reads the next record from the file.
   *
   * @return true if there is a next record
   * @throws IOException if an I/O error occurs
   */
  public boolean nextRecord() throws IOException {
    String line = reader.readLine();
    if (line != null) {
      String[] split = line.split(delimiter);
      for (int i = 0; i < fields.length; i++) {
        try {
          values.put(fields[i], split[i]);
        } catch (ArrayIndexOutOfBoundsException e) {
          values.put(fields[i], null);
        }
      }
    }
    return line != null;
  }

  /**
   * Checks if a field is defined.
   *
   * @param column the name of a field
   * @return true if the field exists
   */
  public boolean hasField(String column) {
    for (String entry : fields) {
      if (entry.equals(column)) {
        return true;
      }
    }
    return false;
  }

  public boolean getBoolean(String field, boolean defaultValue) {
    String value = getString(field, "");
    if (value.isEmpty()) {
      return defaultValue;
    } else {
      return Boolean.parseBoolean(value);
    }
  }

  public byte getByte(String field, byte defaultValue) {
    String value = getString(field, "");
    try {
      return Byte.parseByte(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public char getChar(String field, char defaultValue) {
    String value = getString(field, "");
    if (value.length() < 1) {
      return defaultValue;
    } else {
      return value.charAt(0);
    }
  }

  public double getDouble(String field, double defaultValue) {
    String value = getString(field, "");
    try {
      return Double.parseDouble(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public float getFloat(String field, float defaultValue) {
    String value = getString(field, "");
    try {
      return Float.parseFloat(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public int getInteger(String field, int defaultValue) {
    String value = getString(field, "");
    try {
      return Integer.parseInt(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public long getLong(String field, long defaultValue) {
    String value = getString(field, "");
    try {
      return Long.parseLong(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public short getShort(String field, short defaultValue) {
    String value = getString(field, "");
    try {
      return Short.parseShort(value);
    } catch (NumberFormatException e) {
      return defaultValue;
    }
  }

  public String getString(String field, String defaultValue) {
    String token = values.get(field);
    if (token == null) {
      return defaultValue;
    } else {
      return token;
    }
  }

  @Override
  public void close() throws IOException {
    reader.close();
  }
}
