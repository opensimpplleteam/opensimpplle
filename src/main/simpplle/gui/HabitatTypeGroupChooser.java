package simpplle.gui;

import simpplle.comcode.HabitatTypeGroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class HabitatTypeGroupChooser extends JDialog {

  JList<String> list;
  List<HabitatTypeGroup> selectedGroups = new ArrayList<>();

  private HabitatTypeGroupChooser(JDialog owner) {

    super(owner, "Choose Habitat Type Groups", true);

    list = new JList<>(HabitatTypeGroup.getLoadedGroupNames());
    list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    JScrollPane scrollPane = new JScrollPane();
    scrollPane.getViewport().add(list, null);

    JButton acceptButton = new JButton("Accept");
    acceptButton.addActionListener(this::accept);

    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(this::cancel);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(acceptButton);
    buttonPanel.add(cancelButton);

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(scrollPane);
    mainPanel.add(buttonPanel);

    setContentPane(mainPanel);
    setPreferredSize(new Dimension(300, 300));

    pack();

  }

  private void accept(ActionEvent event) {
    ListModel<String> model = list.getModel();
    for (int index : list.getSelectedIndices()) {
      selectedGroups.add(HabitatTypeGroup.findInstance(model.getElementAt(index)));
    }
    setVisible(false);
  }

  private void cancel(ActionEvent event) {
    setVisible(false);
  }

  private List<HabitatTypeGroup> selection() {
    return selectedGroups;
  }

  public static List<HabitatTypeGroup> choose(JDialog owner) {
    HabitatTypeGroupChooser chooser = new HabitatTypeGroupChooser(owner);
    chooser.setLocationRelativeTo(owner);
    chooser.setVisible(true);
    return chooser.selection();
  }
}
