/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.comcode.FireResistance;
import simpplle.comcode.HabitatTypeGroupType;
import simpplle.comcode.HabitatTypeGroup;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * This class defines the FireLogic Resistance Conditional Editor, a type of JDialog.
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

class FireResistanceConditionalEditor extends JDialog {

  private static Color BORDER_HIGHLIGHT = Color.white;
  private static Color BORDER_SHADOW = new Color(148, 145, 140);

  private static final String protoTypeCellValue = "ABCDEFG  ";
  private HashMap<FireResistance, ArrayList<HabitatTypeGroupType>> data;

  private JPanel panel1 = new JPanel();
  private JPanel dropPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel buttonPanel = new JPanel();
  private JPanel highPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 0));
  private JPanel moderatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 0));
  private JPanel lowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 0));
  private JPanel lowListPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel moderateListPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel highListPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
  private JPanel groupScrollPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
  private JPanel groupDragSourcePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
  private JPanel groupSourcePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

  private JButton cancelPB = new JButton("Cancel");
  private JButton okPB = new JButton("Ok");

  private JList<String> groupSourceList = new JList<>();
  private JList<String> lowList = new JList<>();
  private JList<String> moderateList = new JList<>();
  private JList<String> highList = new JList<>();

  private JScrollPane groupSourceScroll = new JScrollPane(groupSourceList);
  private JScrollPane lowListScroll = new JScrollPane(lowList);
  private JScrollPane moderateListScroll = new JScrollPane(moderateList);
  private JScrollPane highListScroll = new JScrollPane(highList);

  /**
   * Constructor for fire resistance conditional editor.
   *
   * @param owner owner of this dialog
   * @param data  a hash map of the data with which this dialog is to be populated
   */
  FireResistanceConditionalEditor(Dialog owner, HashMap<FireResistance, ArrayList<HabitatTypeGroupType>> data) {
    super(owner, "Fire Resistance Conditionals editor", true);
    try {
      jbInit();
      this.data = data;
      initialize();
      pack();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Sets the border, panels, components, layouts, and lists used by the Fire Resistance Conditional Editor.
   *
   * @throws Exception
   */
  private void jbInit() throws Exception {

    panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
    cancelPB.addActionListener(this::cancelAction);
    okPB.addActionListener(this::okAction);

    groupSourceScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    lowListScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    moderateListScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    highListScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    groupSourceList.setToolTipText("Use Drag-n-Drop to move items");
    groupSourceList.setPrototypeCellValue(protoTypeCellValue);
    groupSourceList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    groupSourceList.setVisibleRowCount(8);

    lowList.setPrototypeCellValue(protoTypeCellValue);
    lowList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    moderateList.setPrototypeCellValue(protoTypeCellValue);
    moderateList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    highList.setPrototypeCellValue(protoTypeCellValue);

    dropPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW), "Fire Resistance by Ecological Grouping"));
    lowListPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW), "Low"));
    moderateListPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW), "Moderate"));
    highListPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW), "High"));
    groupDragSourcePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW), "Ecological Grouping Not Yet Assigned"));

    add(panel1);

    panel1.add(groupDragSourcePanel);
    panel1.add(dropPanel);
    panel1.add(buttonPanel);

    groupDragSourcePanel.add(groupSourcePanel);

    groupSourcePanel.add(groupScrollPanel);

    groupScrollPanel.add(groupSourceScroll);

    dropPanel.add(lowPanel);
    dropPanel.add(moderatePanel);
    dropPanel.add(highPanel);

    lowPanel.add(lowListPanel);
    lowListPanel.add(lowListScroll);
    moderatePanel.add(moderateListPanel);
    moderateListPanel.add(moderateListScroll);
    highPanel.add(highListPanel);
    highListPanel.add(highListScroll);

    buttonPanel.add(okPB);
    buttonPanel.add(cancelPB);
  }

  /**
   * Initializes the Fire Resistance Conditional Editor.  Sets the model based on the fire resistance either low, moderate, or high.
   */
  private void initialize() {
    ArrayList<HabitatTypeGroupType> groups;

    boolean hasLow;
    boolean hasModerate;
    boolean hasHigh;

    ArrayList<HabitatTypeGroupType> allGroups = new ArrayList<>();
    for (HabitatTypeGroup ht: HabitatTypeGroup.getAllLoadedGroups()) {
      allGroups.add(ht.getType());
    }

    hasLow = data.containsKey(FireResistance.LOW);
    hasModerate = data.containsKey(FireResistance.MODERATE);
    hasHigh = data.containsKey(FireResistance.HIGH);

    DefaultListModel<String> model;

    lowList.setModel(new DefaultListModel<>());
    moderateList.setModel(new DefaultListModel<>());
    highList.setModel(new DefaultListModel<>());
    groupSourceList.setModel(new DefaultListModel<>());

    if (hasLow) {
      model = (DefaultListModel<String>) lowList.getModel();
      groups = data.get(FireResistance.LOW);

      for (HabitatTypeGroupType group : groups) {
        model.addElement(group.toString());
        allGroups.remove(group);
      }
    }

    if (hasModerate) {
      model = (DefaultListModel<String>) moderateList.getModel();
      groups = data.get(FireResistance.MODERATE);

      for (HabitatTypeGroupType group : groups) {
        model.addElement(group.toString());
        allGroups.remove(group);
      }
    }

    if (hasHigh) {
      model = (DefaultListModel<String>) highList.getModel();
      groups = data.get(FireResistance.HIGH);

      for (HabitatTypeGroupType group : groups) {
        model.addElement(group.toString());
        allGroups.remove(group);
      }
    }

    model = (DefaultListModel<String>) groupSourceList.getModel();

    for (HabitatTypeGroupType group : allGroups) {
      model.addElement(group.toString());
    }

    ArrayListTransferHandler handler = new ArrayListTransferHandler();

    lowList.setDragEnabled(true);
    lowList.setTransferHandler(handler);

    moderateList.setDragEnabled(true);
    moderateList.setTransferHandler(handler);

    highList.setDragEnabled(true);
    highList.setTransferHandler(handler);

    groupSourceList.setDragEnabled(true);
    groupSourceList.setTransferHandler(handler);
  }

  private void okAction(ActionEvent e) {
    finishUp();
    setVisible(false);
    dispose();
  }

  private void cancelAction(ActionEvent e) {
    setVisible(false);
    dispose();
  }

  private void finishUp() {
    DefaultListModel model;
    ArrayList<HabitatTypeGroupType> groups;

    model = (DefaultListModel) lowList.getModel();
    if (model.size() > 0) {
      groups = data.get(FireResistance.LOW);

      if (groups == null) {
        groups = new ArrayList<>(model.size());
        data.put(FireResistance.LOW, groups);
      }
      copyModelData(model, groups);
    } else {
      groups = data.remove(FireResistance.LOW);
      if (groups != null) {
        groups.clear();
      }
    }

    model = (DefaultListModel) moderateList.getModel();
    if (model.size() > 0) {
      groups = data.get(FireResistance.MODERATE);

      if (groups == null) {
        groups = new ArrayList<>(model.size());
        data.put(FireResistance.MODERATE, groups);
      }
      copyModelData(model, groups);
    } else {
      groups = data.remove(FireResistance.MODERATE);
      if (groups != null) {
        groups.clear();
      }
    }

    model = (DefaultListModel) highList.getModel();
    if (model.size() > 0) {
      groups = data.get(FireResistance.HIGH);

      if (groups == null) {
        groups = new ArrayList<>(model.size());
        data.put(FireResistance.HIGH, groups);
      }
      copyModelData(model, groups);
    } else {
      groups = data.remove(FireResistance.HIGH);
      if (groups != null) {
        groups.clear();
      }
    }
  }

  private void copyModelData(DefaultListModel model, ArrayList<HabitatTypeGroupType> groups) {
    groups.clear();
    for (int i = 0; i < model.size(); i++) {
      groups.add(HabitatTypeGroupType.get((String) model.elementAt(i)));
    }
  }
}
