/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.comcode.AbstractBaseLogic;
import simpplle.comcode.BaseLogic;
import simpplle.comcode.SystemKnowledge;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

import static simpplle.comcode.BaseLogic.SPECIES_COL;
import static simpplle.comcode.SystemKnowledge.REGEN_LOGIC_FIRE;
import static simpplle.comcode.SystemKnowledge.REGEN_LOGIC_SUCC;

/**
 * A logic panel contains a table allowing users to edit logic rules for a single kind of knowledge.
 */

public class LogicPanel extends JPanel {

  protected LogicDialog dialog;
  protected String kind;
  public LogicDataModel dataModel;
  protected int selectedRow = -1;
  protected boolean inColumnInit = false;
  protected AbstractBaseLogic logicInst;
  protected SystemKnowledge.Kinds sysKnowKind;
  protected JPanel northPanel = new JPanel();
  protected JPanel centerPanel = new JPanel(new BorderLayout());
  protected JScrollPane tableScrollPane = new JScrollPane();
  protected JTable logicTable = new JTable();

  /**
   * Creates a logic panel.
   *
   * @param dialog the dialog that this panel will be placed in
   * @param kind the kind of knowledge edited within this panel
   * @param logicInst the logic edited within this panel
   * @param sysKnowKind the kind of knowledge edited within this panel
   */
  public LogicPanel(LogicDialog dialog,
                    String kind, AbstractBaseLogic logicInst,
                    SystemKnowledge.Kinds sysKnowKind) {

    this.dialog = dialog;
    this.dialog.menuAction.addActionListener(this::actionMenuListener);
    this.kind = kind;
    this.dataModel = new LogicDataModel(kind, logicInst, logicTable);
    this.logicInst = logicInst;
    this.sysKnowKind = sysKnowKind;

    try {
      jbInit();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  protected void jbInit() throws Exception {
    setLayout(new BorderLayout());
    add(centerPanel, BorderLayout.CENTER);
    centerPanel.add(tableScrollPane, BorderLayout.CENTER);
    tableScrollPane.getViewport().add(logicTable);
    add(northPanel, BorderLayout.NORTH);

    setActionEnabled(false);
  }

  /**
   * Initializes all columns.
   */
  public void initColumns() {
    Enumeration e = logicTable.getColumnModel().getColumns();
    while (e.hasMoreElements()) {
      TableColumn column = (TableColumn)e.nextElement();
      String name = (String)column.getHeaderValue();
      int index = dataModel.getLogicInst().getColumnNumFromName(name);
      initColumn(column, index);
    }
    updateColumnWidth();
  }

  /**
   * Initializes a column.
   *
   * @param column the table column to initialize
   * @param identifier the column identifier
   */
  protected void initColumn(TableColumn column, int identifier) {}

  /**
   * Initializes a base column, which is a column shared by multiple tables.
   *
   * @param column the table column to initialize
   * @param identifier the column identifier
   */
  protected void initBaseColumn(TableColumn column, int identifier) {
    if (identifier == BaseLogic.ROW_COL) {
      column.setIdentifier(BaseLogic.ROW_COL);
      column.setCellRenderer(new AlternateRowColorDefaultTableCellRenderer());
    }
  }

  /**
   * Initializes the table in this panel. Configuration involves assigning a model, configuring
   * allowed selections, and initializing column widths.
   */
  protected void initializeTable() {
    logicTable.setModel(dataModel);
    logicTable.setColumnSelectionAllowed(false);
    logicTable.setRowSelectionAllowed(true);

    initColumns();

    logicTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    if (dataModel.isDataPresent()) {
      Utility.initColumnWidth(logicTable);
    }

    logicTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    ListSelectionModel rowSM = logicTable.getSelectionModel();
    rowSM.addListSelectionListener(this::selectedRow);

    selectedRow = -1;
    updatePanel();
  }

  protected void updateColumnWidth() {
    Utility.initColumnWidth(logicTable);
  }

  public SystemKnowledge.Kinds getSystemKnowledgeKind() { return sysKnowKind; }

  /**
   * Adds a visible column and sets the fire table structure changed
   * @param col Index of column to be modified
   */
  public void showColumn(int col) {
    dataModel.showColumn(col);
    dataModel.fireTableStructureChanged();
  }

  /**
   * removes a visible column and sets the fire table structure changed
   * @param col Index of column to be modified
   */
  public void hideColumn(int col) {
    dataModel.hideColumn(col);
    dataModel.fireTableStructureChanged();
  }

  public boolean isVisibleColumn(int col) {
    return dataModel.isVisibleColumn(col);
  }

  /**
   * Updates the panel by refreshing the table and updating the graphics.
   */
  public void updatePanel() {
    refreshTable();
    update(getGraphics());
  }

  /**
   * Refreshes the table by notifying all listeners that the table has changed and the JTable should redraw from scratch
   */
  public void refreshTable() {
    dataModel.fireTableDataChanged();
  }

  /**
   * moves a row up by sending to LogicDataModel GUI class
   */
  public void moveRowUp() {
    int newRow = dataModel.moveRowUp(selectedRow);
    logicTable.setRowSelectionInterval(newRow,newRow);
    selectedRow = newRow;
    // Fixes a problem if moving a row right after editing something.
    logicTable.removeEditor();
  }

  /**
   * moves a row down by sending to LogicDataModel GUI class
   */
  public void moveRowDown() {
    int newRow = dataModel.moveRowDown(selectedRow);
    logicTable.setRowSelectionInterval(newRow,newRow);
    selectedRow = newRow;
    // Fixes a problem if moving a row right after editing something.
    logicTable.removeEditor();
  }
  /**
   * Inserts a duplicated at to the position of the selected row. If there is no selection, then the row is
   * appended to the end.
   */
  public void duplicateSelectedRow() {
    int position = (selectedRow != -1) ? selectedRow : dataModel.getRowCount() + 1;
    dataModel.duplicateRow(selectedRow,position);
  }
  /**
   * first checks if user really wants to delete row with a JOption Pane. If yes, deletes row by calling LogicDataModel class.
   */
  public void deleteSelectedRow() {
    int row = logicTable.getSelectedRow();
    String msg =
      "Delete Currently Selected Row!\n\n" +
      "Are You Sure?";
    int choice = JOptionPane.showConfirmDialog(dialog,msg,"Delete Selected Row",
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);

    if (choice == JOptionPane.YES_OPTION) {
      dataModel.deleteRow(row);
      logicTable.clearSelection();
    }
  }
  /**
   * Inserts a row according to position. The position is determined by a user row selection to be inserted above
   * or below, if there is no row selected then the row is appended to the end.
   */
  public void insertRow() {
    int position = (selectedRow != -1) ? selectedRow : dataModel.getRowCount() + 1;
    dataModel.addRow(position);
  }

  /**
   * Shows only columns that are not empty. Empty is defined as only containing empty brackets.
   */
  void hideEmptyColumns() {
    int count = logicInst.getTotalColumnCount(kind);
    for (int col = 0; col < count; col++) {
      if (logicInst.isColumnEmpty(kind, col)) {
        hideColumn(col);
      }
    }
  }

  /**
   * Shows all columns.
   */
  void showAllColumns() {
    int count = logicInst.getTotalColumnCount(kind);
    for (int col = 0; col < count; col++) {

      // The regeneration logic editor contains two species columns. The base column accepts
      // multiple species and should stay hidden. This ensures that the base column isn't shown.
      if (sysKnowKind == REGEN_LOGIC_FIRE && col == SPECIES_COL) continue;
      if (sysKnowKind == REGEN_LOGIC_SUCC && col == SPECIES_COL) continue;

      showColumn(col);
    }
  }

  public void selectedRow(ListSelectionEvent e) {
    ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    boolean isRowSelected = !lsm.isSelectionEmpty();
    if (isRowSelected) {
      selectedRow = lsm.getMinSelectionIndex();
      rowSelected();
    } else {
      selectedRow = -1;
    }
    setActionEnabled(isRowSelected);
  }

  private void actionMenuListener(ActionEvent e) {
    setActionEnabled(selectedRow != -1 && dataModel.isDataPresent());
  }

  protected void setActionEnabled(boolean isEnabled) {
    dialog.menuActionDeleteSelectedRule.setEnabled(isEnabled);
    dialog.menuActionMoveRuleUp.setEnabled(isEnabled);
    dialog.menuActionMoveRuleDown.setEnabled(isEnabled);
    dialog.menuActionDuplicateSelectedRule.setEnabled(isEnabled);
  }

  protected void rowSelected() {}
}
