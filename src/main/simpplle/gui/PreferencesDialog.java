/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.Properties;
import simpplle.comcode.RegionalZone;
import simpplle.comcode.Simpplle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * Edits application preferences. States are saved to an opensimpplle.properties file in your
 * working directory when the application is closed.
 */
public class PreferencesDialog extends JDialog implements FocusListener {

  private JCheckBox cbxDetailedLogging;
  private JCheckBox cbxUseMsuInvasive;
  private JComboBox<String> cmbDefaultZone;
  private JTextField txtWorkingDirectory;
  private JButton btnBrowse;
  private JCheckBox cbxHistoricPathways;

  private Properties properties = JSimpplle.getProperties();

  PreferencesDialog(Frame frame) {

    super(frame, "Preferences", true);

    initGUI();
    setupGUI();

    // Detailed Logging
    cbxDetailedLogging.setSelected(properties.getSimulationLogging());
    cbxDetailedLogging.addActionListener(this::toggleDetailedLogging);

    // MSU Invasive
    cbxUseMsuInvasive.setSelected(JSimpplle.getProperties().getInvasiveMSU());
    cbxUseMsuInvasive.addActionListener(this::toggleUseMsuInvasive);

    // Default Zone
    cmbDefaultZone.addItem("");
    for (String zone : Simpplle.availableZones()) {
      cmbDefaultZone.addItem(zone);
    }
    cmbDefaultZone.setSelectedItem(properties.getDefaultZoneName());
    cmbDefaultZone.addActionListener(this::chooseDefaultZone);

    // Historic Pathways
    cbxHistoricPathways.setSelected(JSimpplle.getProperties().getHistoricPathways());
    cbxHistoricPathways.addActionListener(this::toggleHistoricPathways);

    // Working Directory
    txtWorkingDirectory.setText(properties.getWorkingDirectory().getAbsolutePath());
    txtWorkingDirectory.addActionListener(this::editWorkingDirectory);
    txtWorkingDirectory.addFocusListener(this);
    btnBrowse.addActionListener(this::chooseWorkingDirectory);

    setModal(true);
    pack();

  }

  private void initGUI() {

    cbxDetailedLogging = new JCheckBox("Do Detailed Simulation Logging");
    cbxUseMsuInvasive = new JCheckBox("Use MSU Invasive Species Problem File");
    cbxHistoricPathways = new JCheckBox("Historic Pathways");

    cmbDefaultZone = new JComboBox<>();
    txtWorkingDirectory = new JTextField(20);
    btnBrowse = new JButton("Browse...");
  }

  private void setupGUI() {

    setPreferredSize(new Dimension(400, 290));
    FlowLayout panelLayout = new FlowLayout(FlowLayout.LEFT);


    cmbDefaultZone.setPreferredSize(new Dimension(350, 25));
    JPanel mainPanel = new JPanel();
    JPanel simLoggingPanel = new JPanel(panelLayout);
    JPanel invasivePanel = new JPanel(panelLayout);
    JPanel zonePanel = new JPanel(panelLayout);
    JPanel directoryPanel = new JPanel(panelLayout);
    JPanel cbxPanel = new JPanel();
    JPanel wrapperPanel = new JPanel(panelLayout);
    EtchedBorder border = new EtchedBorder(EtchedBorder.RAISED);
    TitledBorder zoneBorder = new TitledBorder(border, "Default Zone");
    TitledBorder dirBorder = new TitledBorder(border, "Working Directory");
    BoxLayout cbxLayout = new BoxLayout(cbxPanel, BoxLayout.PAGE_AXIS);
    BoxLayout layout = new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS);

    setLayout(new BorderLayout());
    cbxPanel.setLayout(cbxLayout);
    mainPanel.setLayout(layout);

    cbxPanel.setMaximumSize(new Dimension(305, 2));
    zonePanel.setPreferredSize(new Dimension(360, 30));
    wrapperPanel.setPreferredSize(new Dimension(100, 25));
    directoryPanel.setPreferredSize(new Dimension(360, 6));

    Dimension vertDim = new Dimension(10, 0);
    Dimension horizDim = new Dimension(0, 10);

    add(mainPanel, BorderLayout.CENTER);
    add(Box.createRigidArea(vertDim), BorderLayout.EAST);
    add(Box.createRigidArea(vertDim), BorderLayout.WEST);
    add(Box.createRigidArea(horizDim), BorderLayout.NORTH);
    add(Box.createRigidArea(horizDim), BorderLayout.SOUTH);

    mainPanel.add(wrapperPanel);
    mainPanel.add(zonePanel);
    mainPanel.add(Box.createRigidArea(horizDim));
    mainPanel.add(directoryPanel);

    wrapperPanel.add(cbxPanel);

    cbxPanel.add(simLoggingPanel);
    cbxPanel.add(invasivePanel);

    simLoggingPanel.add(cbxDetailedLogging);

    invasivePanel.add(cbxUseMsuInvasive);

    zonePanel.setBorder(zoneBorder);
    zonePanel.add(cmbDefaultZone);
    zonePanel.add(cbxHistoricPathways);

    directoryPanel.setBorder(dirBorder);
    directoryPanel.add(txtWorkingDirectory);
    directoryPanel.add(btnBrowse);
  }

  public void focusGained(FocusEvent event) {
    System.out.println(event);
  }

  public void focusLost(FocusEvent event) {
    System.out.println(event);
  }

  private void editWorkingDirectory(ActionEvent event) {
    System.out.println("edit");
  }

  private void chooseDefaultZone(ActionEvent event) {
    String zoneName = (String) cmbDefaultZone.getSelectedItem();
    properties.setDefaultZoneName(zoneName);
  }

  private void chooseWorkingDirectory(ActionEvent event) {

    JFileChooser chooser = new JFileChooser(JSimpplle.getWorkingDir());
    chooser.setDialogTitle("Change The Working Directory and Press OK");
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    chooser.setSelectedFile(JSimpplle.getWorkingDir());

    int result = chooser.showDialog(this, "Ok");
    if (result == JFileChooser.APPROVE_OPTION) {
      File file = chooser.getSelectedFile();
      txtWorkingDirectory.setText(file.toString());
      properties.setWorkingDirectory(file);
    }
    update(getGraphics());
  }

  private void toggleDetailedLogging(ActionEvent event) {
    properties.setSimulationLogging(cbxDetailedLogging.isSelected());
  }

  private void toggleUseMsuInvasive(ActionEvent event) {
    properties.setInvasiveMSU(cbxUseMsuInvasive.isSelected());
  }

  private void toggleHistoricPathways(ActionEvent event) {
    properties.setHistoricPathways(cbxHistoricPathways.isSelected());
  }
}
