/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.*;
import simpplle.comcode.RegenerationLogic.DataKinds;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

/**
 * This class creates the Regeneration Delay Logic Dialog, a type of VegLogicDialog.
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class RegenerationLogicDialog extends VegLogicDialog {

  /**
   * Creates a regeneration logic dialog.
   *
   * @param parent parent frame
   */
  public RegenerationLogicDialog(Frame parent) {

    super(parent, "Regeneration Logic", false);

    try {
      jbInit();
      pack();
    } catch(Exception ex) {
      ex.printStackTrace();
    }

    initialize();

    hideSpeciesMenuItem();

  }

  void jbInit() throws Exception {

    JMenuItem menuFileOldFormat = new JMenuItem("Import Old Format File");
    menuFileOldFormat.addActionListener(this::menuFileOldFormat_actionPerformed);

    menuFile.add(menuFileOldFormat);

    JMenuItem menuOptionsAddAllSpecies = new JMenuItem("Add All Species");
    menuOptionsAddAllSpecies.addActionListener(this::menuTableOptionsAddAllSpecies_actionPerformed);

    JMenuItem menuOptionsDelayLogic = new JMenuItem("Delay Logic");
    menuOptionsDelayLogic.addActionListener(this::menuOptionsDelayLogic_actionPerformed);

    JMenuItem menuOptionsPrefAdjSpecies = new JMenuItem("Preferred Adjacent Species");
    menuOptionsPrefAdjSpecies.addActionListener(this::choosePreferredAdjacentSpecies);

    JMenu menuOptions = new JMenu("Options");
    menuOptions.add(menuOptionsAddAllSpecies);
    menuOptions.add(menuOptionsDelayLogic);
    menuOptions.add(menuOptionsPrefAdjSpecies);

    menuBar.add(menuOptions);

  }

  private void initialize() {

    sysKnowKind = SystemKnowledge.REGEN_LOGIC_FIRE;

    String fireKind = RegenerationLogic.FIRE_STR;
    String succKind = RegenerationLogic.SUCCESSION_STR;
    BaseLogic fireLogic = RegenerationLogic.getLogicInstance(fireKind);
    BaseLogic succLogic = RegenerationLogic.getLogicInstance(succKind);

    addPanel(fireKind, new RegenerationLogicFirePanel(this, fireKind, fireLogic, SystemKnowledge.REGEN_LOGIC_FIRE));
    addPanel(succKind, new RegenerationLogicSuccPanel(this, SystemKnowledge.REGEN_LOGIC_SUCC, succLogic));

    updateDialog();

  }

  public void tabChanged(ChangeEvent e) {
    super.tabChanged(e);
    if (currentPanel != null) {
      sysKnowKind = ((VegLogicPanel) currentPanel).getSystemKnowledgeKind();
    }
  }

  protected void updateMenuItems() {
    super.updateMenuItems();
    if (currentPanelKind.equals(RegenerationLogic.FIRE_STR)) {
      menuFileOpen.setText("Open Fire");
      menuFileSave.setText("Save Fire");
      menuFileSaveAs.setText("Save Fire As");
      menuFileClose.setText("Close Fire");
      menuFileLoadDefault.setText("Load Default Fire");

    }
    else if (currentPanelKind.equals(RegenerationLogic.SUCCESSION_STR)){
      menuFileOpen.setText("Open Type of Succession");
      menuFileSave.setText("Save Type of Succession");
      menuFileSaveAs.setText("Save Type of Succession As");
      menuFileClose.setText("Close Type of Succession");
      menuFileLoadDefault.setText("Load Default Type of Succession");
    }
    else {
      menuFileOpen.setText("Open");
      menuFileSave.setText("Save");
      menuFileSaveAs.setText("Save As");
      menuFileClose.setText("Close");
      menuFileLoadDefault.setText("Load Default");
    }
  }

  private void menuFileOldFormat_actionPerformed(ActionEvent e) {
    File         infile;
    MyFileFilter extFilter;
    String       title = "Select a Regeneration Logic file.";

    extFilter = new MyFileFilter("regenlogic",
                                 "Regeneration Logic Files (*.regenlogic)");

    setCursor(Utility.getWaitCursor());

    infile = Utility.getOpenFile(this, title, extFilter);
    if (infile != null) {
      try {
        RegenerationLogic.readDataLegacy(infile);
        updateDialog();
        currentPanel.updatePanel();
        updateMenuItems();
      }
      catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this,err.getMessage(),
                                      "Error Loading Regeneration Logic File",
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
    setCursor(Utility.getNormalCursor());
    update(getGraphics());
  }
  /**
  * Quits if window closing event occurs
  * @param e
  */
  void this_windowClosing(WindowEvent e) {
    quit();
  }

  private void quit() {
    RegenerationLogic.setCurrentEcoGroup(null,null);
    setVisible(false);
    dispose();
  }

  private void choosePreferredAdjacentSpecies(ActionEvent e) {
    SuccessionSpeciesChooser dlg = new SuccessionSpeciesChooser(this);
    dlg.setLocationRelativeTo(this);
    dlg.setVisible(true);
    RegenerationLogic.markChanged(RegenerationLogic.FIRE);
  }

  private void menuTableOptionsAddAllSpecies_actionPerformed(ActionEvent e) {
    Vector  v = HabitatTypeGroup.getValidSpecies();
    Vector<Species> newSpecies = new Vector<>();
    Species species;
    for (int i=0; i<v.size(); i++) {
      species = (Species)v.elementAt(i);
      if (RegenerationLogic.isSpeciesPresent(species,DataKinds.valueOf(currentPanelKind)) == false) {
        newSpecies.addElement(species);
      }
    }
    if (currentPanelKind.equals(RegenerationLogic.FIRE_STR)) {
      ((RegenerationLogicFirePanel)currentPanel).addRows(newSpecies);
    }
    else if (currentPanelKind.equals(RegenerationLogic.SUCCESSION_STR)) {
      ((RegenerationLogicSuccPanel)currentPanel).addRows(newSpecies);
    }

    update(getGraphics());
  }

  private void menuOptionsDelayLogic_actionPerformed(ActionEvent e) {
    RegenDelayLogicDlg dlg =
      new RegenDelayLogicDlg(JSimpplle.getSimpplleMain(), "Regeneration Delay Logic",
                            true);
    dlg.setVisible(true);

  }

  RegenerationLogic.DataKinds getLogicKind() {
    return RegenerationLogic.DataKinds.valueOf(currentPanelKind);
  }
}
