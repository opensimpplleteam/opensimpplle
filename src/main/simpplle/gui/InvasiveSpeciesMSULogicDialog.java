/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.Frame;

import simpplle.comcode.BaseLogic;
import simpplle.comcode.InvasiveSpeciesLogic;
import simpplle.comcode.InvasiveSpeciesLogicMSU;
import simpplle.comcode.SystemKnowledge;

/** 
 *
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class InvasiveSpeciesMSULogicDialog extends VegLogicDialog {

  public InvasiveSpeciesMSULogicDialog(Frame owner, String title, boolean modal) {
    super(owner, title, modal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jbInit();
      initialize();
      pack();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  public InvasiveSpeciesMSULogicDialog() {
    this(new Frame(), "InvasiveSpeciesMSULogicDialog", false);
  }
  // TODO: Look into removing this...
  private void jbInit() throws Exception {}

  private void initialize() {

    sysKnowKind = SystemKnowledge.INVASIVE_SPECIES_LOGIC_MSU;

    String probKind = InvasiveSpeciesLogicMSU.PROBABILITY_STR;
    String changeKind = InvasiveSpeciesLogicMSU.CHANGE_RATE_STR;
    BaseLogic logic = InvasiveSpeciesLogicMSU.getInstance();

    addPanel(probKind, new InvasiveSpeciesMSULogicPanel(this, probKind, logic, sysKnowKind));
    addPanel(changeKind, new InvasiveSpeciesMSULogicPanel(this, changeKind, logic, sysKnowKind));

    updateDialog();

  }
}
