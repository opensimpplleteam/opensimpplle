/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JPanel;
import simpplle.comcode.VegetativeType;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import simpplle.comcode.HabitatTypeGroup;
import simpplle.comcode.Process;
import simpplle.comcode.ProcessType;
import javax.swing.ListSelectionModel;
import simpplle.comcode.InclusionRuleSpecies;

/** 
 *
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class PathwaySpeciesChangeEditDialog extends JDialog {

  private HabitatTypeGroup group;
  private PathwaySpeciesChangeDataModel dataModel = new PathwaySpeciesChangeDataModel();
  private JTable table = new JTable();
  private JMenuItem menuActionDeleteSelected = new JMenuItem("Delete Selected Row(s)");

  public PathwaySpeciesChangeEditDialog(Frame owner, String title,
                                        boolean modal) {
    super(owner, title, modal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jbInit();
      pack();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  public void initialize(VegetativeType vt, HabitatTypeGroup group) {
    dataModel.setData(vt);
    this.group = group;

    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    ListSelectionModel rowSM = table.getSelectionModel();
    rowSM.addListSelectionListener(e -> {{
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        menuActionDeleteSelected.setEnabled(!lsm.isSelectionEmpty());
      }
    });
  }

  public PathwaySpeciesChangeEditDialog() {
    this(new Frame(), "PathwaySpeciesChangeEditDialog", false);
  }

  private void jbInit() throws Exception {

    JPanel mainPanel = new JPanel(new BorderLayout());

    JMenuBar jMenuBar1 = new JMenuBar();
    setJMenuBar(jMenuBar1);

    JMenu menuAction = new JMenu("Action");

    JMenuItem menuActionNewEntry = new JMenuItem("New Entry");
    JMenuItem menuActionAddSpecies = new JMenuItem("Add Species");

    JScrollPane jScrollPane1 = new JScrollPane(table);

    mainPanel.setPreferredSize(new Dimension(750, 400));
    setLayout(new BorderLayout());
    table.setModel(dataModel);
    menuActionNewEntry.addActionListener(this::menuActionNewEntry_actionPerformed);
    menuActionAddSpecies.addActionListener(this::menuActionAddSpecies_actionPerformed);
    menuActionDeleteSelected.setEnabled(false);
    menuActionDeleteSelected.addActionListener(this::menuActionDeleteSelected_actionPerformed);
    jMenuBar1.add(menuAction);
    menuAction.add(menuActionNewEntry);
    menuAction.add(menuActionAddSpecies);
    menuAction.add(menuActionDeleteSelected);
    mainPanel.add(jScrollPane1, BorderLayout.CENTER);
    add(mainPanel, BorderLayout.CENTER);
  }

  private void menuActionAddSpecies_actionPerformed(ActionEvent e) {
    String name = JOptionPane.showInputDialog("Type a New Species");
    if (name == null) { return; }
    InclusionRuleSpecies species = new InclusionRuleSpecies(name);
    JOptionPane.showMessageDialog(this,species.toString() + " Added to Valid List",
                                  "Species Added",JOptionPane.INFORMATION_MESSAGE);
  }

  private void menuActionNewEntry_actionPerformed(ActionEvent e) {

    ProcessType[] processList = Process.getSummaryProcesses();
    ProcessType[] processes = new ProcessType[processList.length+5];
    int i=0;
    for (Object o : processList) {
      ProcessType p = (ProcessType)o;
      if (p.equals(ProcessType.SRF)) {
        processes[i++] = ProcessType.SRF_SPRING;
        processes[i++] = ProcessType.SRF_SUMMER;
        processes[i++] = ProcessType.SRF_FALL;
        processes[i++] = ProcessType.SRF_WINTER;
      }
      else if (p.equals(ProcessType.SUCCESSION)) {
        processes[i++] = p;
        processes[i++] = ProcessType.WET_SUCCESSION;
        processes[i++] = ProcessType.DRY_SUCCESSION;
      }
      else {
        processes[i++] = p;
      }
    }

    ProcessType process = (ProcessType)JOptionPane.showInputDialog(null,
        "Choose a Process", "Choose a Process",
        JOptionPane.INFORMATION_MESSAGE, null,
        processes, processes[0]);
    if (process == null) { return; }

    InclusionRuleSpecies[] allSpecies = InclusionRuleSpecies.getAllValues();
    InclusionRuleSpecies species =
      (InclusionRuleSpecies)JOptionPane.showInputDialog(null,
        "Choose a Species", "Choose a Species",
        JOptionPane.INFORMATION_MESSAGE, null,
        allSpecies, allSpecies[0]);

    if (process != null && species != null) {
      dataModel.addRow(process,species);
    }

  }

  private void menuActionDeleteSelected_actionPerformed(ActionEvent e) {
    int[] rows = table.getSelectedRows();
    String msg =
      "Delete Currently Selected Row(s)!\n\n" +
      "Are You Sure?";
    int choice = JOptionPane.showConfirmDialog(this,msg,"Delete Selected Row",
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);

    if (choice == JOptionPane.YES_OPTION) {
      dataModel.deleteRows(rows);
      table.clearSelection();
    }
    update(getGraphics());
  }
}
