package simpplle.gui;

import simpplle.comcode.StatusReporter;

import javax.swing.*;
import java.awt.*;

/**
 * Reports application status to users in a JLabel.
 */
public class LabelStatusReporter implements StatusReporter {

  private JLabel label;
  private JFrame owner;
  private Graphics graphics;

  public LabelStatusReporter(JLabel label, JFrame owner, Graphics graphics) {
    this.label = label;
    this.owner = owner;
    this.graphics = graphics;
  }


  @Override
  public void report(String status) {
    label.setText(status);
    owner.update(graphics);
  }

  @Override
  public void clear() {
    label.setText("");
    owner.update(graphics);
  }
}
