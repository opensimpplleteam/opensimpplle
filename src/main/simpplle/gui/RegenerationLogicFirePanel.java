/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.TableColumn;
import simpplle.JSimpplle;
import simpplle.comcode.HabitatTypeGroup;
import simpplle.comcode.FireRegenerationData;
import simpplle.comcode.RegenerationLogic;
import simpplle.comcode.Species;
import java.awt.event.*;
import simpplle.comcode.HabitatTypeGroupType;
import java.awt.Font;
import java.util.ArrayList;
import simpplle.comcode.*;
import simpplle.comcode.SystemKnowledge.Kinds;

/** 
 * This class is for fire regeneration.   The five components used to determine how a plant community can regenerate are
 * if the species resprots in-place, resprouts from adjacent communities, seed is produced by the burned plants, seed is provided from adjacent communities
 * or seed is transported from communities within the landscape.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class RegenerationLogicFirePanel extends VegLogicPanel {

  private static final int SPECIES_CODE_COL = FireRegenerationData.SPECIES_CODE_COL;
  private static final int RESPROUTING_COL = FireRegenerationData.RESPROUTING_COL;
  private static final int ADJ_RESPROUTING_COL = FireRegenerationData.ADJ_RESPROUTING_COL;
  private static final int IN_PLACE_SEED_COL = FireRegenerationData.IN_PLACE_SEED_COL;
  private static final int IN_LANDSCAPE_COL = FireRegenerationData.IN_LANDSCAPE_COL;
  private static final int ADJACENT_COL = FireRegenerationData.ADJACENT_COL;

  public RegenerationLogicFirePanel(LogicDialog dialog,
                                    String kind,
                                    AbstractBaseLogic logicInst,
                                    Kinds sysKnowKind) {

    super(dialog, kind, logicInst, sysKnowKind);

    try {
      initialize();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  protected void initialize() {
    dataModel.setIsRegen(true);
    initializeTable();
  }

  protected void initColumn(TableColumn column, int identifier) {

    // ** Resprouting Column **
    if (identifier == RESPROUTING_COL) {
      column.setIdentifier(RESPROUTING_COL);
      column.setCellEditor(new MyJButtonEditor(dialog, logicTable, RESPROUTING_COL,"Resprouting State", false));
      column.setCellRenderer(new MyJTextAreaRenderer());
    }
    // ** Adjacent Resprouting Column **
    else if (identifier == ADJ_RESPROUTING_COL) {
      column.setIdentifier(ADJ_RESPROUTING_COL);
      column.setCellEditor(new MyJButtonEditor(dialog, logicTable,ADJ_RESPROUTING_COL,"Adjacent Resprouting State", false));
      column.setCellRenderer(new MyJTextAreaRenderer());
    }
    // ** In-Place Seed Column **
    else if (identifier == IN_PLACE_SEED_COL) {
      column.setIdentifier(IN_PLACE_SEED_COL);
      column.setCellEditor(new MyJButtonEditor(dialog, logicTable,IN_PLACE_SEED_COL,"In Place Seed", false));
      column.setCellRenderer(new MyJTextAreaRenderer());
    }
    // ** In-Landscape Column **
    else if (identifier == IN_LANDSCAPE_COL) {
      column.setIdentifier(IN_LANDSCAPE_COL);
      column.setCellEditor(new MyJButtonEditor(dialog, logicTable,IN_LANDSCAPE_COL, "In Landscape", false));
      column.setCellRenderer(new MyJTextAreaRenderer());
    }
    // ** Adjacent Seed Source Priority State Column **
    else if (identifier == ADJACENT_COL) {
      column.setIdentifier(ADJACENT_COL);
      column.setCellEditor(new MyJButtonEditor(dialog, logicTable, ADJACENT_COL,"Adjacent State(s)", false));
      column.setCellRenderer(new MyJTextAreaRenderer());
    }
    else if (identifier == SPECIES_CODE_COL) {
      column.setIdentifier(SPECIES_CODE_COL);
      column.setCellRenderer(new AlternateRowColorDefaultTableCellRenderer());
    }
    else {
      super.initBaseColumn(column, identifier);
    }
  }

  void addRows(Vector speciesList) {
    int position = (selectedRow != -1) ? selectedRow : dataModel.getRowCount() + 1;
    RegenerationLogic.addDataRows(position,kind,speciesList);
    refreshTable();
    logicTable.clearSelection();
  }

  public void insertRow() {
    int position = (selectedRow != -1) ? selectedRow : dataModel.getRowCount();
    addRow(position);
  }

  private void addRow(int row) {
    Vector v  = HabitatTypeGroup.getValidSpecies();

    String[] values = new String[v.size()];
    for (int i=0; i<values.length; i++) {
      values[i] = (v.elementAt(i)).toString();
    }

    String title = "Select a Species";
    ListSelectionDialog dlg =
      new ListSelectionDialog(JSimpplle.getSimpplleMain(),title,true,values,false);
    dlg.setVisible(true);

    String value = (String)dlg.getSelection();
    if (value != null) {
      dataModel.addRow(row);
      RegenerationLogic.setSpecies(row,Species.get(value),RegenerationLogic.FIRE);
      logicTable.clearSelection();
    }
    update(getGraphics());
  }
}
