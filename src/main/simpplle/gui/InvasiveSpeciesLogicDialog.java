/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.Frame;

import simpplle.comcode.BaseLogic;
import simpplle.comcode.InvasiveSpeciesLogic;
import simpplle.comcode.SystemKnowledge;
import javax.swing.JMenuItem;

/**
 * This class defines the Invasive Species Logic Dialog.  There are only two zones with this logic, Eastside Region 1 and Colorado Plateau.
 * This class is specifically for the Colorado Plateau.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class InvasiveSpeciesLogicDialog extends VegLogicDialog {

  private JMenuItem menuActionSoilTypeEditor = new JMenuItem();

  public InvasiveSpeciesLogicDialog(Frame owner, String title, boolean modal) {
    super(owner, title, modal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jbInit();
      initialize();
      pack();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  public InvasiveSpeciesLogicDialog() {
    this(new Frame(), "InvasiveSpeciesLogic", false);
  }

  private void jbInit() throws Exception {
    menuActionSoilTypeEditor.setText("Soil Type Editor");
    menuActionSoilTypeEditor.addActionListener(e -> soilTypeEditor());
    menuAction.addSeparator();
    menuAction.add(menuActionSoilTypeEditor);
  }

  private void initialize() {

    sysKnowKind = SystemKnowledge.INVASIVE_SPECIES_LOGIC;

    BaseLogic logic = InvasiveSpeciesLogic.getInstance();
    String probKind = InvasiveSpeciesLogic.PROBABILITY_STR;
    String changeKind = InvasiveSpeciesLogic.CHANGE_RATE_STR;

    addPanel(probKind, new InvasiveSpeciesLogicPanel(this, probKind, logic, sysKnowKind));
    addPanel(changeKind, new InvasiveSpeciesLogicPanel(this, changeKind, logic, sysKnowKind));

    updateDialog();

  }

  private void soilTypeEditor() {
    SoilTypeChooser dlg = new SoilTypeChooser(this,"Soil Type Editor",true,null);
    dlg.setVisible(true);
  }
}
