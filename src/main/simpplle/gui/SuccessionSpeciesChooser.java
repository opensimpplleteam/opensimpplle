/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import simpplle.comcode.HabitatTypeGroup;
import simpplle.comcode.Species;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

/**
 * This class creates the Succession Species Chooser dialog, a type of JDialog.
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class SuccessionSpeciesChooser extends JDialog {

  private static Color BORDER_HIGHLIGHT = Color.white;
  private static Color BORDER_SHADOW = new Color(148, 145, 140);

  private JComboBox<String> comboBox;
  private DefaultListModel sourceListModel;
  private DefaultListModel targetListModel;

  private ListDataListener listDataListener = new ListDataListener() {

    @Override
    public void intervalAdded(ListDataEvent e) {
      addSpecies(e.getIndex0(), (String)targetListModel.get(e.getIndex0()));
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      removeSpecies(e.getIndex0());
    }

    @Override
    public void contentsChanged(ListDataEvent e) { }

  };

  public SuccessionSpeciesChooser(JDialog owner) {

    super(owner, "Choose Preferred Adjacent Species", true);

    try {
      jbInit();
      initialize();
      pack();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  void jbInit() throws Exception {

    setPreferredSize(new Dimension(500, 500));

    comboBox = new JComboBox<>();
    comboBox.addActionListener(this::selectEcoGroup);

    JPanel groupPanel = new JPanel();
    groupPanel.setLayout(new BoxLayout(groupPanel, BoxLayout.X_AXIS));
    groupPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT,BORDER_SHADOW),"Ecological Grouping"));
    groupPanel.add(comboBox);
    groupPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, groupPanel.getHeight()));

    sourceListModel = new DefaultListModel();
    DragSourceList sourceList = new DragSourceList();
    sourceList.setModel(sourceListModel);
    sourceList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    JScrollPane sourceScrollPane = new JScrollPane();
    sourceScrollPane.getViewport().add(sourceList, null);
    sourceScrollPane.setPreferredSize(new Dimension(120, 200));

    JPanel sourcePanel = new JPanel();
    sourcePanel.setLayout(new BoxLayout(sourcePanel, BoxLayout.Y_AXIS));
    sourcePanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT,BORDER_SHADOW),"Available Species"));
    sourcePanel.add(sourceScrollPane, null);

    targetListModel = new DefaultListModel();
    targetListModel.addListDataListener(listDataListener);

    DragDropList targetList = new DragDropList();
    targetList.setModel(targetListModel);
    targetList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    JScrollPane targetScrollPane = new JScrollPane();
    targetScrollPane.getViewport().add(targetList, null);
    targetScrollPane.setPreferredSize(new Dimension(220, 200));

    JPanel targetPanel = new JPanel();
    targetPanel.setLayout(new BoxLayout(targetPanel, BoxLayout.Y_AXIS));
    targetPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT,BORDER_SHADOW),"Chosen Species"));
    targetPanel.add(targetScrollPane, null);

    JPanel listPanel = new JPanel();
    listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.X_AXIS));
    listPanel.add(sourcePanel);
    listPanel.add(targetPanel);

    JButton clearButton = new JButton("Clear");
    clearButton.addActionListener(this::clearCurrent);
    clearButton.setToolTipText("Clear chosen species in this ecological grouping");

    JButton clearAllButton = new JButton("Clear All");
    clearAllButton.addActionListener(this::clearAll);
    clearAllButton.setToolTipText("Clear chosen species in every ecological grouping");

    JButton copyToButton = new JButton("Copy To…");
    copyToButton.addActionListener(this::copyTo);
    copyToButton.setToolTipText("Copy chosen species from this grouping to other groupings");

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(clearButton);
    buttonPanel.add(Box.createRigidArea(new Dimension(10,0)));
    buttonPanel.add(clearAllButton);
    buttonPanel.add(Box.createRigidArea(new Dimension(10,0)));
    buttonPanel.add(copyToButton);

    JLabel infoLabel = new JLabel(
        "<html>" +
            "Drag available species to chosen list on right.<br>" +
            "Order species as desired by dragging with the mouse.<br>" +
            "Remove chosen species with the 'Delete' key.<br>" +
            "The most preferred species is the first in the list." +
        "</html>"
    );

    JPanel infoPanel = new JPanel();
    infoPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
    infoPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, infoPanel.getHeight()));
    infoPanel.setAlignmentX(0.5f);
    infoPanel.add(infoLabel, null);

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(groupPanel);
    mainPanel.add(listPanel);
    mainPanel.add(buttonPanel);
    mainPanel.add(infoPanel);

    getContentPane().add(mainPanel);

  }

  private void initialize() {

    // Populate ecological groupings
    for (String name : HabitatTypeGroup.getLoadedGroupNames()) {
      comboBox.addItem(name);
    }

    // Populate available species
    Vector<Species> validSpecies = HabitatTypeGroup.getValidSpecies();
    for (Species species : validSpecies) {
      sourceListModel.addElement(species.getSpecies());
    }

    // Populate chosen species
    refreshChosenSpeciesList();
  }

  private void addSpecies(int index, String name) {
    String groupName = (String)comboBox.getSelectedItem();
    HabitatTypeGroup group = HabitatTypeGroup.findInstance(groupName);
    List<Species> species = group.getPreferredAdjacentSpecies();
    species.add(index, Species.get(name));
  }

  private void removeSpecies(int index) {
    String groupName = (String)comboBox.getSelectedItem();
    HabitatTypeGroup group = HabitatTypeGroup.findInstance(groupName);
    List<Species> species = group.getPreferredAdjacentSpecies();
    species.remove(index);
  }

  private void selectEcoGroup(ActionEvent event) {
    refreshChosenSpeciesList();
  }

  private void refreshChosenSpeciesList() {
    String name = (String)comboBox.getSelectedItem();
    HabitatTypeGroup group = HabitatTypeGroup.findInstance(name);
    targetListModel.removeListDataListener(listDataListener);
    targetListModel.clear();
    for (Species species : group.getPreferredAdjacentSpecies()) {
      targetListModel.addElement(species.getSpecies());
    }
    targetListModel.addListDataListener(listDataListener);
  }

  private void clearCurrent(ActionEvent event) {
    int choice = JOptionPane.showConfirmDialog(
        this,
        "Are you sure that you want to clear all\n" +
        "of the preferred species in the current\n" +
        "ecological grouping?",
        "Confirmation",
        JOptionPane.YES_NO_OPTION
    );
    if (choice == JOptionPane.YES_OPTION) {
      String name = (String) comboBox.getSelectedItem();
      HabitatTypeGroup group = HabitatTypeGroup.findInstance(name);
      group.getPreferredAdjacentSpecies().clear();
      refreshChosenSpeciesList();
    }
  }

  private void clearAll(ActionEvent event) {
    int choice = JOptionPane.showConfirmDialog(
        this,
        "Are you sure that you want to clear all\n" +
        "of the preferred species in the every\n" +
        "ecological grouping?",
        "Confirmation",
        JOptionPane.YES_NO_OPTION
    );
    if (choice == JOptionPane.YES_OPTION) {
      for (int i = 0; i < comboBox.getItemCount(); i++) {
        String name = comboBox.getItemAt(i);
        HabitatTypeGroup group = HabitatTypeGroup.findInstance(name);
        group.getPreferredAdjacentSpecies().clear();
      }
      refreshChosenSpeciesList();
    }
  }

  private void copyTo(ActionEvent event) {

    // Prompt the user for habitat type groups to copy to
    List<HabitatTypeGroup> groups = HabitatTypeGroupChooser.choose(this);

    if (groups.isEmpty()) return;

    String targetGroup = groups.get(0).toString();
    String sourceGroup = comboBox.getItemAt(comboBox.getSelectedIndex());

    if(sourceGroup.equalsIgnoreCase(targetGroup)) return;

    // Check if the selected groups contain values
    boolean willOverwrite = false;
    for (HabitatTypeGroup group : groups) {
      if (!group.getPreferredAdjacentSpecies().isEmpty()) {
        willOverwrite = true;
        break;
      }
    }

    // Display a warning if data will be overwritten
    if (willOverwrite) {
      int choice = JOptionPane.showConfirmDialog(
          this,
          "One or more of the selected ecological groupings\n" +
          "contains preferred species. Would you like to\n" +
          "continue and overwrite these values?",
          "Overwrite Warning",
          JOptionPane.YES_NO_OPTION
      );
      if (choice != JOptionPane.YES_OPTION) return;
    }

    // Copy preferred species from the active group
    HabitatTypeGroup source = HabitatTypeGroup.findInstance((String)comboBox.getSelectedItem());
    List<Species> sourceSpecies = source.getPreferredAdjacentSpecies();
    for (HabitatTypeGroup target : groups) {
      if (target == source) continue;
      List<Species> targetSpecies = target.getPreferredAdjacentSpecies();
      targetSpecies.clear();
      targetSpecies.addAll(sourceSpecies);
    }
  }
}



