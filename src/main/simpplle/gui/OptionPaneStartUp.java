package simpplle.gui;

import simpplle.JSimpplle;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.*;

public class OptionPaneStartUp extends JOptionPane {

  public static void Set_Memory(){
    File openSimpJar = new File(System.getProperty("java.class.path")); // needs location of JAR
    File dir = openSimpJar.getAbsoluteFile().getParentFile(); // dir that holds the JAR
    File memFile = new File(dir + "/memory.txt"); // text file to store memory option for restart

    int prev_max_memory = 0;
    try {
      List<String> mem_file_lines;
      mem_file_lines = Files.readAllLines(Paths.get(memFile.getAbsolutePath()), StandardCharsets.UTF_8);
      prev_max_memory = Integer.valueOf(mem_file_lines.get(0));

    } catch (Exception e){
      System.out.println("File memory.txt does not exists");
      prev_max_memory = 1;

    }
  }

  public static void Restart(){
    File openSimpJar = new File(System.getProperty("java.class.path"));
  }

  public static boolean checkForRestart(){
    File openSimpJar = new File(System.getProperty("java.class.path")); // needs location of JAR
    File dir = openSimpJar.getAbsoluteFile().getParentFile(); // dir that holds the JAR
    File memFile = new File(dir + "/memory.txt"); // text file to store memory option for restart

    try {
      List<String> mem_file_lines;
      mem_file_lines = Files.readAllLines(Paths.get(memFile.getAbsolutePath()), StandardCharsets.UTF_8);
      if(Integer.valueOf(mem_file_lines.get(0)) == -1){
        return true;
      }
    } catch (Exception e){
      System.out.println("File memory.txt does not exists");
    }
    return false;
  }

  class ScrollPane_Popup extends JScrollPane {
    JPanel popupPanel = new JPanel();

  }






}
