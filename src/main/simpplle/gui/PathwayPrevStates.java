/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.VegetativeType;
import simpplle.comcode.VegetativeTypeNextState;
import simpplle.comcode.Process;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

/** 
 * This class allows users to create a new vegetative state.  This is used in vegetative Pathways. Vegetative states include species, size class, age, density, and habitat type group.
 * The dialog is titled "Create a New State"
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class PathwayPrevStates extends JDialog {

  private static Color BORDER_HIGHLIGHT = Color.white;
  private static Color BORDER_SHADOW = new Color(148, 145, 140);

  private String protoCellValue = "RIPARIAN_GRASSES/CLOSED_TALL_SHRUB2/1 -- COLD-INJURY-BARK-BEETLES";
  private VegetativeType selectedState;
  private Process selectedProcess;
  private PathwayCanvas parentDlg;

  private GridLayout gridLayout1 = new GridLayout();
  private JButton editPB = new JButton("Edit State");
  private JButton displayDiagramPB = new JButton("Display Diagram");
  private JList prevStateList = new JList();
  private JPopupMenu listPopupMenu = new JPopupMenu();
  private JLabel currentLabel = new JLabel();

  public PathwayPrevStates(Frame frame, String title, boolean modal, PathwayCanvas parentDlg,
                           VegetativeType selectedState, Process selectedProcess) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    this.parentDlg       = parentDlg;
    this.selectedState   = selectedState;
    this.selectedProcess = selectedProcess;
    initialize();
  }

  public PathwayPrevStates() {
    this(null, "", false,null,null,null);
  }

  void jbInit() throws Exception {

    JPanel mainPanel = new JPanel(new BorderLayout());
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel prevStatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel PBPanel = new JPanel();
    JPanel southPanel = new JPanel(new FlowLayout());
    JPanel currentPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

    JButton displayCurrentPB = new JButton("Display Diagram");
    JButton editCurrentPB = new JButton("Edit");

    JMenuItem contextMenuDisplay = new JMenuItem("Display Diagram");
    JMenuItem contextMenuEdit = new JMenuItem("Edit");

    JScrollPane prevStateScroll = new JScrollPane(prevStateList);

    PBPanel.setLayout(gridLayout1);
    gridLayout1.setRows(2);
    gridLayout1.setVgap(5);
    editPB.setEnabled(false);
    editPB.addActionListener(this::editPB_actionPerformed);
    displayDiagramPB.setEnabled(false);
    displayDiagramPB.addActionListener(this::displayDiagramPB_actionPerformed);
    prevStateList.setPrototypeCellValue(protoCellValue);
    prevStateList.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        prevStateList_mouseClicked(e);
      }
      public void mousePressed(MouseEvent e) {
        prevStateList_mousePressed(e);
      }
      public void mouseReleased(MouseEvent e) {
        prevStateList_mouseReleased(e);
      }
    });
    contextMenuEdit.addActionListener(this::contextMenueditCurrentPB_actionPerformed);
    contextMenuDisplay.addActionListener(this::contextMenuDisplay_actionPerformed);
    this.addWindowListener(new java.awt.event.WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        this_windowClosing(e);
      }
    });
    displayCurrentPB.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    displayCurrentPB.addActionListener(this::displayCurrentPB_actionPerformed);
    editCurrentPB.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    editCurrentPB.addActionListener(this::editCurrentPB_actionPerformed);
    currentPanel.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED,
        BORDER_HIGHLIGHT, BORDER_SHADOW), "Current Vegetative Type"));

    add(mainPanel);
    mainPanel.add(northPanel, BorderLayout.NORTH);
    northPanel.add(prevStatePanel, null);
    prevStatePanel.add(prevStateScroll, null);
    prevStatePanel.add(PBPanel, null);
    PBPanel.add(editPB, null);
    PBPanel.add(displayDiagramPB, null);
    mainPanel.add(southPanel, BorderLayout.SOUTH);
    southPanel.add(currentPanel, null);
    currentPanel.add(currentLabel, null);
    currentPanel.add(editCurrentPB, null);
    currentPanel.add(displayCurrentPB, null);
    listPopupMenu.add(contextMenuEdit);
    listPopupMenu.add(contextMenuDisplay);
  }

  private void initialize() {
    prevStateList.setListData(selectedState.findPreviousStates());
    currentLabel.setText(selectedState.toString());
    update(getGraphics());
  }

  private void prevStateList_mouseClicked(MouseEvent e) {
   editPB.setEnabled((!prevStateList.isSelectionEmpty()));
   displayDiagramPB.setEnabled((!prevStateList.isSelectionEmpty()));
  }

  private void maybePopupMenu(MouseEvent e) {
    if (e.isPopupTrigger() && !prevStateList.isSelectionEmpty()) {
      listPopupMenu.show(e.getComponent(),e.getX(),e.getY());
    }
  }

  private void prevStateList_mousePressed(MouseEvent e) {
   editPB.setEnabled((!prevStateList.isSelectionEmpty()));
   displayDiagramPB.setEnabled((!prevStateList.isSelectionEmpty()));
   maybePopupMenu(e);
  }

  private void prevStateList_mouseReleased(MouseEvent e) {
   editPB.setEnabled((!prevStateList.isSelectionEmpty()));
   displayDiagramPB.setEnabled((!prevStateList.isSelectionEmpty()));
   maybePopupMenu(e);
  }

  private VegetativeType getSelectedNextState() {
    VegetativeTypeNextState selection = (VegetativeTypeNextState)prevStateList.getSelectedValue();
    return selection.getNextState();
  }

  private void editState() {
    editState(getSelectedNextState());
  }

  private void editState(VegetativeType selection) {
    PathwayEditor dlg = new PathwayEditor(JSimpplle.getSimpplleMain(),
                                          "Pathway Editor", true,
                                          selection);
    dlg.setVisible(true);
    parentDlg.getPathwayDlg().updateDialog();
    parentDlg.refreshDiagram();
  }

  private void contextMenueditCurrentPB_actionPerformed(ActionEvent e) {
    editState();
  }

  private void editPB_actionPerformed(ActionEvent e) {
    editState();
  }

  private void editCurrentPB_actionPerformed(ActionEvent e) {
    editState(selectedState);
  }

  private void displayDiagram() {
    VegetativeTypeNextState selection = (VegetativeTypeNextState)prevStateList.getSelectedValue();

    Pathway pathwayDlg = parentDlg.getPathwayDlg();
    pathwayDlg.setSpeciesAndProcess(selection.getNextState(),selection.getProcess());
  }

  private void displayDiagramPB_actionPerformed(ActionEvent e) {
    displayDiagram();
  }

  private void contextMenuDisplay_actionPerformed(ActionEvent e) {
    displayDiagram();
  }

  private void displayCurrentPB_actionPerformed(ActionEvent e) {
    Pathway pathwayDlg = parentDlg.getPathwayDlg();
    pathwayDlg.setSpeciesAndProcess(selectedState,selectedProcess);
  }

  void this_windowClosing(WindowEvent e) {
    parentDlg.setPrevDialogClosed();
    setVisible(false);
    dispose();
  }
}
