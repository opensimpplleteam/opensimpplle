/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Frame;
import simpplle.comcode.*;

/**
 * Defines UI elements and behavior common to all vegetation logic dialogs.
 */

public class VegLogicDialog extends LogicDialog {

  protected boolean inColumnInit = false;

  protected ArrayList<JMenuItem> colMenuItems = new ArrayList<>();

  protected JMenu menuColumns = new JMenu();
  private JMenuItem menuShowAllCols = new JMenuItem();
  private JMenuItem menuHideEmptyCols = new JMenuItem();
  private JCheckBoxMenuItem menuEcoGroup = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuSpecies = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuSizeClass = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuDensity = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuProcess = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuTreatment = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuSeason = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuMoisture = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuTemp = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuTrackingSpecies = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuOwnership = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuSpecialArea = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuRoadStatus = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuTrailStatus = new JCheckBoxMenuItem();
  private JCheckBoxMenuItem menuLandtype = new JCheckBoxMenuItem();

  /**
   * Creates a vegetation logic dialog.
   *
   * @param owner parent frame
   * @param title a title naming the logic being edited
   * @param modal whether dialog blocks user input to other top-level windows when shown
   */
  protected VegLogicDialog(Frame owner, String title, boolean modal) {

    super(owner, title, modal);

    try {
      jbInit();
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    colMenuItems.clear();
    colMenuItems.add(BaseLogic.ROW_COL,null);
    colMenuItems.add(BaseLogic.ECO_GROUP_COL,menuEcoGroup);
    colMenuItems.add(BaseLogic.SPECIES_COL,menuSpecies);
    colMenuItems.add(BaseLogic.SIZE_CLASS_COL,menuSizeClass);
    colMenuItems.add(BaseLogic.DENSITY_COL,menuDensity);
    colMenuItems.add(BaseLogic.PROCESS_COL,menuProcess);
    colMenuItems.add(BaseLogic.TREATMENT_COL,menuTreatment);
    colMenuItems.add(BaseLogic.SEASON_COL,menuSeason);
    colMenuItems.add(BaseLogic.MOISTURE_COL,menuMoisture);
    colMenuItems.add(BaseLogic.TEMP_COL,menuTemp);
    colMenuItems.add(BaseLogic.TRACKING_SPECIES_COL,menuTrackingSpecies);
    colMenuItems.add(BaseLogic.OWNERSHIP_COL,menuOwnership);
    colMenuItems.add(BaseLogic.SPECIAL_AREA_COL,menuSpecialArea);
    colMenuItems.add(BaseLogic.ROAD_STATUS_COL,menuRoadStatus);
    colMenuItems.add(BaseLogic.TRAIL_STATUS_COL,menuTrailStatus);
    colMenuItems.add(BaseLogic.LANDTYPE_COL,menuLandtype);

  }

  /**
   * Creates a vegetation logic dialog.
   */
  public VegLogicDialog() {
    super();
  }

  private void jbInit() throws Exception {

    menuShowAllCols.setText("Show All");
    menuShowAllCols.addActionListener(e -> showAllColumns());

    menuHideEmptyCols.setText("Hide Empty");
    menuHideEmptyCols.addActionListener(e -> hideEmptyColumns());

    menuEcoGroup.setText("Ecological Grouping");
    menuEcoGroup.addActionListener(e -> toggleVisibility(BaseLogic.ECO_GROUP_COL));

    menuSpecies.setText("Species");
    menuSpecies.addActionListener(e -> toggleVisibility(BaseLogic.SPECIES_COL));

    menuSizeClass.setText("Size Class");
    menuSizeClass.addActionListener(e -> toggleVisibility(BaseLogic.SIZE_CLASS_COL));

    menuDensity.setText("Density");
    menuDensity.addActionListener(e -> toggleVisibility(BaseLogic.DENSITY_COL));

    menuProcess.setText("Process");
    menuProcess.addActionListener(e -> toggleVisibility(BaseLogic.PROCESS_COL));

    menuTreatment.setText("Treatment");
    menuTreatment.addActionListener(e -> toggleVisibility(BaseLogic.TREATMENT_COL));

    menuSeason.setText("Season");
    menuSeason.addActionListener(e -> toggleVisibility(BaseLogic.SEASON_COL));

    menuMoisture.setText("Moisture");
    menuMoisture.addActionListener(e -> toggleVisibility(BaseLogic.MOISTURE_COL));

    menuTemp.setText("Temperature");
    menuTemp.addActionListener(e -> toggleVisibility(BaseLogic.TEMP_COL));

    menuTrackingSpecies.setText("Tracking Species");
    menuTrackingSpecies.addActionListener(e -> toggleVisibility(BaseLogic.TRACKING_SPECIES_COL));

    menuOwnership.setText("Ownership");
    menuOwnership.addActionListener(e -> toggleVisibility(BaseLogic.OWNERSHIP_COL));

    menuSpecialArea.setText("Special Area");
    menuSpecialArea.addActionListener(e -> toggleVisibility(BaseLogic.SPECIAL_AREA_COL));

    menuRoadStatus.setText("Road Status");
    menuRoadStatus.addActionListener(e -> toggleVisibility(BaseLogic.ROAD_STATUS_COL));

    menuTrailStatus.setText("Trail Status");
    menuTrailStatus.addActionListener(e -> toggleVisibility(BaseLogic.TRAIL_STATUS_COL));

    menuLandtype.setText("Land Type");
    menuLandtype.addActionListener(e -> toggleVisibility(BaseLogic.LANDTYPE_COL));

    menuColumns.setText("Columns");
    menuColumns.add(menuShowAllCols);
    menuColumns.add(menuHideEmptyCols);
    menuColumns.addSeparator();
    menuColumns.add(menuEcoGroup);
    menuColumns.add(menuSpecies);
    menuColumns.add(menuSizeClass);
    menuColumns.add(menuDensity);
    menuColumns.add(menuProcess);
    menuColumns.add(menuTreatment);
    menuColumns.add(menuSeason);
    menuColumns.add(menuMoisture);
    menuColumns.add(menuTemp);
    menuColumns.add(menuTrackingSpecies);
    menuColumns.add(menuOwnership);
    menuColumns.add(menuSpecialArea);
    menuColumns.add(menuRoadStatus);
    menuColumns.add(menuTrailStatus);
    menuColumns.add(menuLandtype);

    menuBar.add(menuColumns);

  }

  /**
   * Toggles the visibility of a column based on the state of the associated menu item. This method
   * returns immediately during initialization.
   *
   * @param columnIndex the column index
   */
  public void toggleVisibility(int columnIndex) {

    if (inColumnInit) { return; }

    if (colMenuItems.get(columnIndex).isSelected()) {
      currentPanel.showColumn(columnIndex);
    } else {
      currentPanel.hideColumn(columnIndex);
    }

    currentPanel.initColumns();

  }

  public void showAllColumns() {
    currentPanel.showAllColumns();
    updateMenuItems();
    currentPanel.initColumns();
  }

  public void hideEmptyColumns() {
    currentPanel.hideEmptyColumns();
    updateMenuItems();
    currentPanel.initColumns();
  }

  public void hideEmptyColumns(LogicPanel panel) {
    panel.hideEmptyColumns();
    updateMenuItems();
    panel.initColumns();
  }

  /**
   * Hides the species menu item from the 'Columns' menu. This is used by the Regeneration dialog,
   * which uses a different column that only allows one species per rule.
   */
  public void hideSpeciesMenuItem() {
    menuSpecies.setVisible(false);
  }

  /**
   * Enables or disables menu items based on current state. This enables or disables the save,
   * close, and column menu items. The selected state of the column menu items is set to match
   * the visibility of the columns in the current panel.
   */
  protected void updateMenuItems() {
    super.updateMenuItems();
    for (int i = 0; i < colMenuItems.size(); i++) {
      if (colMenuItems.get(i) == null) { continue; }
      colMenuItems.get(i).setSelected(currentPanel.isVisibleColumn(i));
    }
  }

  @Override
  public void tabChanged(ChangeEvent e) {
    super.tabChanged(e);
  }
}
