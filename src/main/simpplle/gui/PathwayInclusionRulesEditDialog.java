/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import simpplle.comcode.VegetativeType;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import simpplle.comcode.InclusionRuleSpecies;

/** 
 * This class allows users to edit an inclusion rule.
 * Inclusion rules consist of Inclusion Rule Species name, lower percent range, and upper percent range.  
 * This class allows users to create, add, or delete an inclusion rule by species name.  Much of this can be done using the Action JMenu.
 *  
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class PathwayInclusionRulesEditDialog extends JDialog {

  private PathwayInclusionRulesDataModel dataModel = new PathwayInclusionRulesDataModel();
  private JTable table = new JTable();
  private JMenuItem menuActionDeleteSelected = new JMenuItem("Delete Selected");

  /**
   * Constructor for inclusion rules Edit Dialog.
   *
   * @param owner frame that owns the inclusion rules editor
   * @param title name of the dialog
   * @param modal specifies whether dialog blocks user input to other top-level windows when shown
   */
  public PathwayInclusionRulesEditDialog(Frame owner, String title, boolean modal) {
    super(owner, title, modal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jbInit();
      pack();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  public PathwayInclusionRulesEditDialog() {
    this(new Frame(), "PathwayInclusionRulesEditDialog", false);
  }

  /**
   * Initialize the Pathway Inclusion Rule Editor
   *
   * @param vt
   */
  public void initialize(VegetativeType vt) {
    dataModel.setData(vt);

    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    ListSelectionModel rowSM = table.getSelectionModel();
    rowSM.addListSelectionListener(e -> {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        menuActionDeleteSelected.setEnabled(!lsm.isSelectionEmpty());
    });
  }

  /**
   * Sets up the inclusion rules with an Action menu, and data model which consists of Inclusion
   * Rule Species, Lower range and Upper Range.
   *
   */
  private void jbInit() {

    JPanel mainPanel = new JPanel();

    JMenuBar menuBar = new JMenuBar();

    JMenu actionMenu = new JMenu("Action");
    JMenuItem menuActionNew = new JMenuItem("New Entry");
    JMenuItem menuActionAdd = new JMenuItem("Add Entry");

    JScrollPane tablePane = new JScrollPane(table);

    setLayout(new BorderLayout());
    mainPanel.setLayout(new BorderLayout());
    mainPanel.setPreferredSize(new Dimension(750, 400));
    table.setModel(dataModel);
    menuActionNew.addActionListener(this::menuActionNew_actionPerformed);
    menuActionDeleteSelected.setEnabled(false);
    menuActionDeleteSelected.addActionListener(this::menuActionDeleteSelected_actionPerformed);
    setJMenuBar(menuBar);
    menuActionAdd.addActionListener(this::menuActionAdd_actionPerformed);
    menuBar.add(actionMenu);
    actionMenu.add(menuActionNew);
    actionMenu.add(menuActionAdd);
    actionMenu.add(menuActionDeleteSelected);
    mainPanel.add(tablePane, BorderLayout.CENTER);
    add(mainPanel, BorderLayout.CENTER);
  }

  /**
   * Allows users to create a new inclusion rule species by name into a JOptionPane.
   *
   * @param e
   */
  private  void menuActionNew_actionPerformed(ActionEvent e) {
    String name = JOptionPane.showInputDialog("Type a New Species");
    if (name != null) {
      dataModel.addRow(new InclusionRuleSpecies(name.toUpperCase()));
    }
  }

  /**
   * Adds an inclusion rule to the vegetative pathways.  Displays all the inclusion rule species and allows the user to choose one.  
   * @param e
   */
  private void menuActionAdd_actionPerformed(ActionEvent e) {
    InclusionRuleSpecies[] allSpecies = InclusionRuleSpecies.getAllValues();

    InclusionRuleSpecies species = (InclusionRuleSpecies)JOptionPane.showInputDialog(null,
        "Choose a Species", "Choose a Species",
        JOptionPane.INFORMATION_MESSAGE, null,
        allSpecies, allSpecies[0]);
    if (species != null) {
      dataModel.addRow(species);
    }
  }

  /**
   * Deletes an inclusion rule by deleting a selected row.
   *
   * @param e
   */
  private void menuActionDeleteSelected_actionPerformed(ActionEvent e) {
    int[] rows = table.getSelectedRows();
    String msg = "Delete Currently Selected Row(s)!\n\nAre You Sure?";
    int choice = JOptionPane.showConfirmDialog(this,msg,"Delete Selected Row",
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);

    if (choice == JOptionPane.YES_OPTION) {
      dataModel.deleteRows(rows);
      table.clearSelection();
    }
    update(getGraphics());
  }
}
