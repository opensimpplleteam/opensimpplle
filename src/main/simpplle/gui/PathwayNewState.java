/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.Area;
import simpplle.comcode.Simpplle;
import simpplle.comcode.RegionalZone;
import simpplle.comcode.HabitatTypeGroup;
import simpplle.comcode.VegetativeType;
import simpplle.comcode.Species;
import simpplle.comcode.SizeClass;
import simpplle.comcode.Density;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

/** 
 * This class allows users to create a new vegetative state.  This is used in vegetative Pathways. Vegetative states include species, size class, age, density, and habitat type group.
 * The dialog is titled "Create a New State"
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class PathwayNewState extends JDialog {

  private String[] htGrps;
  private Species species;
  private int age;
  private boolean focusLost = false;

  private GridLayout gridLayout1 = new GridLayout();
  private JLabel speciesValue = new JLabel("PP");
  private JComboBox<Density> densityCB = new JComboBox<>();
  private JComboBox<SizeClass> sizeClassCB = new JComboBox<>();
  private JTextField ageText = new JTextField(5);
  private JList<String> htGrpList = new JList<>();

  /**
   * Constructor for 'Create a New State' dialog.
   *
   * @param frame
   * @param title
   * @param modal
   * @param htGrp
   * @param species
   */
  public PathwayNewState(Frame frame, String title, boolean modal, String htGrp, Species species) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    this.htGrps = new String[1];
    this.htGrps[0] = htGrp;
    this.species = species;
    initialize();
  }

  public PathwayNewState() {
    this(null, "", false, null, null);
  }

  void jbInit() throws Exception {

    JPanel mainPanel = new JPanel(new BorderLayout());
    JPanel northPanel = new JPanel(new BorderLayout());
    JPanel valuesPanel1 = new JPanel(gridLayout1);
    JPanel speciesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel densityPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel buttonPanel = new JPanel(new FlowLayout());
    JPanel agePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel sizeClassPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel htGrpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    JPanel valuesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    JPanel htGrpPanel1 = new JPanel(new FlowLayout());

    JLabel speciesLabel = new JLabel("Species   ");
    JLabel densityLabel = new JLabel("Density   ");
    JLabel sizeClassLabel = new JLabel("Size Class");
    JLabel ageLabel = new JLabel("Age       ");

    JButton CancelPB = new JButton("Cancel");
    CancelPB.addActionListener(this::CancelPB_actionPerformed);

    JButton createPB = new JButton("Create");
    createPB.addActionListener(this::createPB_actionPerformed);

    JButton htGrpChangePB = new JButton("Change");
    htGrpChangePB.addActionListener(this::htGrpChangePB_actionPerformed);

    JScrollPane htGrpScroll = new JScrollPane(htGrpList);

    TitledBorder htGrpBorder = new TitledBorder(BorderFactory.createLineBorder(Color.black,2),"Ecological Grouping");
    valuesPanel1.setLayout(gridLayout1);
    gridLayout1.setRows(4);
    speciesValue.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    speciesValue.setForeground(Color.blue);
    speciesLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));
    densityLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));

    sizeClassLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));
    ageText.setText("1");
    ageText.setHorizontalAlignment(SwingConstants.RIGHT);
    ageText.addFocusListener(new java.awt.event.FocusAdapter() {
      public void focusLost(FocusEvent e) {
        ageText_focusLost(e);
      }
    });
    ageText.addActionListener(this::ageText_actionPerformed);
    ageLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));
    htGrpPanel1.setBorder(htGrpBorder);
    htGrpBorder.setTitleFont(new java.awt.Font("Monospaced", Font.BOLD, 12));

    this.setModal(true);
    htGrpList.setPrototypeCellValue("FTH-X     ");
    htGrpList.setVisibleRowCount(6);

    add(mainPanel);
    valuesPanel.add(valuesPanel1,BorderLayout.NORTH);
    valuesPanel1.add(speciesPanel, null);
    speciesPanel.add(speciesLabel, null);
    speciesPanel.add(speciesValue, null);
    valuesPanel1.add(sizeClassPanel, null);
    sizeClassPanel.add(sizeClassLabel, null);
    sizeClassPanel.add(sizeClassCB, null);
    valuesPanel1.add(agePanel, null);
    agePanel.add(ageLabel, null);
    agePanel.add(ageText, null);
    valuesPanel1.add(densityPanel, null);
    densityPanel.add(densityLabel, null);
    densityPanel.add(densityCB, null);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    buttonPanel.add(createPB, null);
    buttonPanel.add(CancelPB, null);
    mainPanel.add(northPanel, BorderLayout.NORTH);
    northPanel.add(htGrpPanel, BorderLayout.NORTH);
    htGrpPanel.add(htGrpPanel1, null);
    htGrpPanel1.add(htGrpScroll, null);
    htGrpPanel1.add(htGrpChangePB, null);
    northPanel.add(valuesPanel, BorderLayout.CENTER);
  }

  private void initialize() {
    RegionalZone  zone = Simpplle.getCurrentZone();

    // Fill the Habitat Type Group List
    htGrpList.setListData(htGrps);

    // Set the Species
    speciesValue.setText(species.toString());

    // Size Class
    for (Object sc : zone.getAllSizeClass()) {
      sizeClassCB.addItem((SizeClass)sc);
    }

    // Age
    age = 1;
    ageText.setText(Integer.toString(age));

    // Density
    for (Object d : zone.getAllDensity()) {
      densityCB.addItem((Density)d);
    }
    setSize(getPreferredSize());
    update(getGraphics());
  }

  /**
   * Method to change age of state.  Parses the age from the age text field.  As long as it is greater than 1, sets the age.
   */
  private void ageChanged() {
    if (focusLost) { return; }
    try {
      age = Integer.parseInt(ageText.getText());
      if (age < 1) {
        throw new NumberFormatException();
      }
    } catch (NumberFormatException nfe) {
      focusLost = true;
      String msg = "Invalid Age.\n" +
          "Please enter a number greater than 0";
      JOptionPane.showMessageDialog(this, msg, "Invalid value",
          JOptionPane.ERROR_MESSAGE);
      Runnable doRequestFocus = new Runnable() {
        public void run() {
          ageText.requestFocus();
          focusLost = false;
        }
      };
      SwingUtilities.invokeLater(doRequestFocus);
    }
  }

  /**
   * An age is typed in the Age text field.  (must be greater than 1)
   *
   * @param e
   */
  private void ageText_actionPerformed(ActionEvent e) {
    ageChanged();
  }

  /**
   * Focus is lost in Age text field.  (must be greater than 1)
   *
   * @param e
   */
  private void ageText_focusLost(FocusEvent e) {
    ageChanged();
  }

  /**
   * If user presses "Create" button, this will create a new vegetative state with (species, size class, age, and density), if it doesn't already exist.
   *
   * @param e
   */
  private void createPB_actionPerformed(ActionEvent e) {
    SizeClass sizeClass = (SizeClass)sizeClassCB.getSelectedItem();
    Density   density   = (Density)densityCB.getSelectedItem();

    RegionalZone     zone = Simpplle.getCurrentZone();
    HabitatTypeGroup htGrpInst;
    VegetativeType   vegType;

    for (int i = 0; i < htGrps.length; i++) {
      htGrpInst = HabitatTypeGroup.findInstance(htGrps[i]);
      vegType = htGrpInst.getVegetativeType(species, sizeClass, age, density);

      // Only create if it doesn't already exist.
      if (vegType == null) {
        vegType = new VegetativeType(htGrpInst, species, sizeClass, age, density);
        htGrpInst.addVegetativeType(vegType);

        // Update the units and check for invalid ones.
        Area area = Simpplle.getCurrentArea();
        if (area != null) {
          area.updatePathwayData();
          if (area.hasInvalidVegetationUnits() == false) {
            JSimpplle.getSimpplleMain().markAreaValid();
          }
        }
      }
    }
    setVisible(false);
    dispose();
  }

  /**
   * If user presses "Change" button this will load a List Selection Dialog with all the habitat type groups.  The user can then select a habitat type group.
   *
   * @param e
   */
  private void htGrpChangePB_actionPerformed(ActionEvent e) {
    RegionalZone        zone = Simpplle.getCurrentZone();
    Frame               theFrame = JSimpplle.getSimpplleMain();
    ListSelectionDialog dlg;
    Object[]            result;

    dlg = new ListSelectionDialog(theFrame,"Select Ecological Grouping",true,
                                  HabitatTypeGroup.getLoadedGroupNames(),true);

    dlg.setLocation(getLocation());
    dlg.setSelectedItems(htGrps);
    dlg.setVisible(true);
    result = (Object[])dlg.getSelections();
    if (result != null) {
      htGrps = new String[result.length];
      for (int i = 0; i < result.length; i++) {
        htGrps[i] = (String) result[i];
      }
      htGrpList.setListData(htGrps);
    }
  }

  /**
   * If user pushes 'Cancel' disposes the Pathway New State dialog.
   *
   * @param e
   */
  private void CancelPB_actionPerformed(ActionEvent e) {
    setVisible(false);
    dispose();
  }
}
