package simpplle.gui;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * The OpenSIMPPLLE splash screen. This is displayed as the application starts up.
 */
public class SplashScreen extends JWindow {

  public SplashScreen() {

    //URL url = simpplle.JSimpplle.class.getResource("src/resources/images/splash.jpg");
    //label below took 'url' as an argument for ImageIcon, wasnt working, will look into
    JLabel label = new JLabel(new ImageIcon("src/resources/images/splash.jpg"));
    getContentPane().add(label);
    pack();

    // Move to the center of the screen
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(screenSize.width/2 - getSize().width/2,
                screenSize.height/2 - getSize().height/2);
  }
}
