/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.Frame;

import simpplle.comcode.BaseLogic;
import simpplle.comcode.FireSuppClassALogic;
import simpplle.comcode.FireSuppEventLogic;
import simpplle.comcode.SystemKnowledge;

/**
 * This class sets up Fire Suppression Beyond Class A LogicPanel, a type of Vegetative Logic Panel, which inherits from Base Panel.
 * Class A fires are 0-.25 Acres.  
 * 
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class FireSuppEventLogicDlg extends VegLogicDialog {

  public FireSuppEventLogicDlg(Frame owner, String title, boolean modal) {
    super(owner, title, modal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      jbInit();
      initialize();
      pack();
    }
    catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  public FireSuppEventLogicDlg() {
    this(new Frame(), "FireSuppEventLogicDlg", false);
  }
  // TODO: Look into removing this...
  private void jbInit() throws Exception {}

  private void initialize() {

    sysKnowKind = SystemKnowledge.FIRE_SUPP_EVENT_LOGIC;

    String kind = FireSuppEventLogic.FIRE_SUPP_EVENT_LOGIC.toString();
    BaseLogic logic = FireSuppEventLogic.getInstance();
    addPanel(kind, new FireSuppEventLogicPanel(this, kind, logic, sysKnowKind));

    updateDialog();

  }
}
