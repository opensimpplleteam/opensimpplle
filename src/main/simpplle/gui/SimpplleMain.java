/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.hsqldb.util.DatabaseManagerSwing;
import simpplle.JSimpplle;
import simpplle.comcode.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.Vector;
/**
 *  ________  ________  _______   ________   ________  ___  _____ ______   ________  ________  ___       ___       _______
 * |\   __  \|\   __  \|\  ___ \ |\   ___  \|\   ____\|\  \|\   _ \  _   \|\   __  \|\   __  \|\  \     |\  \     |\  ___ \
 * \ \  \|\  \ \  \|\  \ \   __/|\ \  \\ \  \ \  \___|\ \  \ \  \\\__\ \  \ \  \|\  \ \  \|\  \ \  \    \ \  \    \ \   __/|
 *  \ \  \\\  \ \   ____\ \  \_|/_\ \  \\ \  \ \_____  \ \  \ \  \\|__| \  \ \   ____\ \   ____\ \  \    \ \  \    \ \  \_|/__
 *   \ \  \\\  \ \  \___|\ \  \_|\ \ \  \\ \  \|____|\  \ \  \ \  \    \ \  \ \  \___|\ \  \___|\ \  \____\ \  \____\ \  \_|\ \
 *    \ \_______\ \__\    \ \_______\ \__\\ \__\____\_\  \ \__\ \__\    \ \__\ \__\    \ \__\    \ \_______\ \_______\ \_______\
 *     \|_______|\|__|     \|_______|\|__| \|__|\_________\|__|\|__|     \|__|\|__|     \|__|     \|_______|\|_______|\|_______|
 *                                             \|_________|
 *
 *
 */

/**
 * This is the main screen for the OpenSimpplle gui.  It consists of a menu bar and the following menu items
 * File, System Knowledge, Import, Export, Reports, Intepretations, View Results, Utilities, Help.  Thre are also buttons to create new simulation, and
 * labels for current zone and area.
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

@SuppressWarnings("serial")
public class SimpplleMain extends JFrame {
  public static final String VERSION = "1.5.1";
  public static final String RELEASE_KIND = "Douglas Fir";
  public static final String BUILD_DATE = "August 2021";

  public static Color RESULT_COL_COLOR = new Color(90, 190, 190);
  public static Color ROW_HIGHLIGHT_COLOR = new Color(162, 200, 157);

  @SuppressWarnings("unused")
  private simpplle.comcode.Simpplle comcode;
  private static final int MIN_WIDTH = 718;
  private static final int MIN_HEIGHT = 300;
  private boolean vegPathwayDlgOpen = false;
  private boolean aquaticPathwayDlgOpen = false;
  private static simpplle.comcode.Properties properties;

  /**
   * Populates Combo Box dynamically. SIMPPLLE is the default and always available.
   */
  private Vector<String> fireSpreadModels = new Vector<>(2);

  final JFXPanel lavaPanel = new JFXPanel();

  JMenuBar mainMenuBar = new JMenuBar();
  JMenu menuFile = new JMenu();
  JMenuItem menuFileQuit = new JMenuItem();
  JToolBar toolBar = new JToolBar();
  JButton loadZone = new JButton();
  JButton loadArea = new JButton();
  JButton runSimulation = new JButton();
  JLabel statusBar = new JLabel();
  BorderLayout borderLayout1 = new BorderLayout();
  TitledBorder titledBorder1;
  JMenu menuSysKnow = new JMenu();
  JMenu menuImport = new JMenu();
  JMenu menuExport = new JMenu();
  JMenu menuReports = new JMenu();
  JMenu menuInterpretations = new JMenu();
  JMenu menuViewResult = new JMenu();
  JMenu menuHelp = new JMenu();
  JMenuItem menuSettings = new JMenuItem();
  JMenu menuUtility = new JMenu();
  JMenuItem menuUtilityReset = new JMenuItem();
  JMenuItem menuFileSave = new JMenuItem();
  JMenu menuSysKnowPath = new JMenu();
  JMenuItem menuSysKnowPathVeg = new JMenuItem();
  JMenuItem menuSysKnowPathAquatic = new JMenuItem();
  JMenu menuSysKnowVegProc = new JMenu();
  JMenuItem menuSysKnowVegProcLock = new JMenuItem();
  JMenuItem menuSysKnowFireEventLogic = new JMenuItem();
  JMenuItem menuSysKnowFireProb = new JMenuItem();
  JMenuItem menuItemAquaticProcess = new JMenuItem();
  JMenuItem menuSysKnowAquaticTreat = new JMenuItem();
  JMenu menuExportGIS = new JMenu();
  JMenuItem menuReportsSummary = new JMenuItem();
  JMenuItem menuReportsUnit = new JMenuItem();
  JMenu menuReportsMult = new JMenu();
  JMenuItem menuReportsMultNormal = new JMenuItem();
  JMenuItem menuReportsMultSpecial = new JMenuItem();
  JMenuItem menuReportsMultOwner = new JMenuItem();
  JMenuItem menuReportsFire = new JMenuItem();
  JMenuItem menuReportsFireCost = new JMenuItem();
  JMenuItem menuReportsEmission = new JMenuItem();
  JMenuItem menuInterpretProbCalc = new JMenuItem();
  JMenuItem menuInterpretWildlife = new JMenuItem();
  JMenuItem menuResultVegUnit = new JMenuItem();
  JMenuItem menuResultVegSum = new JMenuItem();
  JMenuItem menuResultAquaticUnit = new JMenuItem();
  JMenuItem menuResultAquaticSum = new JMenuItem();
  JMenuItem menuResultAquaticDiagram = new JMenuItem();
  JMenuItem menuUtilitySimReady = new JMenuItem();
  JMenuItem menuHelpAbout = new JMenuItem();
  JMenuItem menuHelpDocs = new JMenuItem();
  JMenuItem menuUtilityPrintArea = new JMenuItem();
  JPanel northPanel = new JPanel();
  JPanel currentPanel = new JPanel();
  JLabel areaLabel = new JLabel();
  JLabel zoneLabel = new JLabel();
  GridLayout gridLayout1 = new GridLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  JPanel zonePanel = new JPanel();
  FlowLayout flowLayout1 = new FlowLayout();
  JPanel areaPanel = new JPanel();
  FlowLayout flowLayout2 = new FlowLayout();
  JLabel zoneValueLabel = new JLabel();
  JLabel areaValueLabel = new JLabel();
  JMenuItem menuExportGISDecadeProb = new JMenuItem();
  JMenuItem menuImportCreate = new JMenuItem();
  JMenuItem menuImportFixStates = new JMenuItem();
  JMenuItem menuUtilityUnitEditor = new JMenuItem();
  JLabel areaInvalidLabel = new JLabel();
  JMenuItem menuSysKnowRegionalClimate = new JMenuItem();
  JMenuItem menuUtilitiesConsole = new JMenuItem();
  JMenuItem menuImportAttributeData = new JMenuItem();
  JMenuItem menuUtilityAreaName = new JMenuItem();
  JMenuItem menuInterpretRestoration = new JMenuItem();
  JMenuItem menuSysKnowOpen = new JMenuItem();
  JMenuItem menuSysKnowSave = new JMenuItem();
  JMenuItem menuInterpretRiskCalc = new JMenuItem();
  JMenuItem menuSysKnowRegen = new JMenuItem();
  JMenuItem menuSysKnowConiferEncroach = new JMenuItem();
  JMenu menuSysKnowVegTreat = new JMenu();
  JMenuItem menuSysKnowVegTreatSchedule = new JMenuItem();
  JMenuItem menuSysKnowVegTreatLogic = new JMenuItem();
  JMenuItem menuSysKnowRestoreDefaults = new JMenuItem();
  JMenuItem menuExportGISReburn = new JMenuItem();
  JMenuItem menuImportInvalidReport = new JMenuItem();
  JMenu menuSysKnowWeatherEvent = new JMenu();
  JMenuItem menuWeatherEventClassA = new JMenuItem();
  JMenuItem menuWeatherEventNotClassA = new JMenuItem();
  JMenuItem menuExportAttributes = new JMenuItem();
  JMenuItem menuUtilityDatabaseTest = new JMenuItem();
  JCheckBoxMenuItem menuSysKnowUseRegenPulse = new JCheckBoxMenuItem();
  JMenu menuSysKnowFireSuppLogic = new JMenu();
  JMenuItem menuSysKnowFireSuppLogicClassA = new JMenuItem();
  JMenuItem menuSysKnowFireSuppLogicBeyondClassA = new JMenuItem();
  JMenu menuMagis = new JMenu();
  JMenuItem menuMagisProcessTreatmentFiles = new JMenuItem();
  JMenuItem menuMagisAllVegStates = new JMenuItem();
  JMenuItem menuUtilityDeleteUnits = new JMenuItem();
  JMenuItem menuReportsFireSuppCostAll = new JMenuItem();
  JMenuItem menuSysKnowWSBWLogic = new JMenuItem();
  JMenuItem menuGisUpdateSpread = new JMenuItem();
  JMenuItem menuResultLandformSum = new JMenuItem();
  JMenuItem menuResultLandformUnit = new JMenuItem();
  JMenuItem menuSysKnowVegTreatDesired = new JMenuItem();
  JMenuItem menuUtilityGISFiles = new JMenuItem();
  JMenuItem menuSysKnowFireSeason = new JMenuItem();
  JMenu menuSysKnowFireSpread = new JMenu("Fire Spread Model");
  JMenuItem menuSysKnowCellPerc = new JMenuItem("Keane Cell Percolation");
  JMenuItem menuHelpUserGuide = new JMenuItem();
  JMenuItem menuSysKnowFireSuppSpreadRate = new JMenuItem();
  JMenuItem menuSysKnowFireSuppProdRate = new JMenuItem();
  JMenuItem menuSysKnowFireOccMgmtZone = new JMenuItem();
  JMenuItem menuSysKnowSpeciesKnowledgeEditor = new JMenuItem();
  JMenuItem menuImportFSLandscape = new JMenuItem();
  JMenuItem menuSysKnowImportProcesses = new JMenuItem();
  JMenuItem menuSysKnowShowLegalProcesses = new JMenuItem();
  JMenuItem menuSysKnowShowZoneTreatments = new JMenuItem();
  JMenuItem menuSysKnowImportZoneTreatments = new JMenuItem();
  JMenuItem menuFileSaveZone = new JMenuItem();
  JMenuItem menuReportsAllStates = new JMenuItem();
  JMenuItem buildSimpplleTypeFiles = new JMenuItem();
  JMenuItem buildSimpplleTypesSource = new JMenuItem();
  JMenuItem menuSysKnowWildlifeBrowsing = new JMenuItem();
  JMenuItem menuSysKnowWindthrow = new JMenuItem();
  JMenuItem menuSysKnowTussockMoth = new JMenuItem();
  private JMenuItem menuUtilityTestNewDialog = new JMenuItem();
  private JMenuItem menuSysKnowProcessProbLogic = new JMenuItem();
  private JCheckBoxMenuItem menuSysKnowDisableWsbw = new JCheckBoxMenuItem();
  private JMenuItem menuUtilityHibern8IDE = new JMenuItem();
  private JMenuItem menuUtilityDatabaseManager = new JMenuItem();
  JMenuItem menuBisonGrazingLogic = new JMenuItem();
  JMenuItem menuUtilityMemoryUse = new JMenuItem();
  private JMenuItem menuSysKnowDoCompetition = new JMenuItem();
  private JMenuItem menuSysKnowGapProcessLogic = new JMenuItem();
  private JMenuItem menuSysKnowProducingSeedLogic = new JMenuItem();
  private JMenuItem menuSysKnowVegUnitFireTypeLogic = new JMenuItem();
  private JMenu menuSysKnowInvasive = new JMenu();
  private JMenuItem menuSysKnowInvasiveLogicR1 = new JMenuItem();
  private JMenuItem menuSysKnowInvasiveLogicMSU = new JMenuItem();
  private JMenuItem menuSysKnowInvasiveLogicMesaVerdeNP = new JMenuItem();
  private JMenuItem menuUtilityMakeAreaMultipleLife = new JMenuItem();
  private JMenuItem menuReportsTrackingSpecies = new JMenuItem();
  private JMenuItem menuUtilityCombineLSFiles = new JMenuItem();

  //Construct the frame
  private final JMenuItem menuUtilityExportPathways = new JMenuItem();
  private final JMenuItem menuUtilityElevRelPos = new JMenuItem();
  private final JMenuItem menuUtilitySwapRowCol = new JMenuItem();
  private final JMenuItem menuSysKnowFireSuppEvent = new JMenuItem();

  //Change URL of Guide
  private static final String GUIDE_URL = "http://www.umt.edu/opensimpplle/guide/default.php";

  /**
   * This is the SimpplleMain constructor.
   */
  public SimpplleMain() {

    enableEvents(AWTEvent.WINDOW_EVENT_MASK);

    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    initialize();

    validate();

    // Make sure frame is centered on screen.
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(screenSize.width / 2 - getSize().width / 2,
        screenSize.height / 2 - getSize().height / 2);

    setVisible(true);

    Simpplle.setStatusReporter(new LabelStatusReporter(statusBar, this, getGraphics()));
  }

  /**
   * Initializes the simpplle main by getting the simpplle image and initializing some tests if in developer mode.  There is also a helpset file directory set, but it is not currently functional.
   */
  private void initialize() {
    // Set icon that is show in the title bar and minimized icon.
    ImageIcon tmpImage = new ImageIcon(
        simpplle.gui.SimpplleMain.class.getResource("/images/simpplle16.gif"));
    setIconImage(tmpImage.getImage());

    //HelpSet hs = null;
    comcode = JSimpplle.getComcode();

    menuHelpDocs.setVisible(false);
    menuUtilityDatabaseTest.setVisible(JSimpplle.developerMode());
    menuUtilityDatabaseTest.setEnabled(JSimpplle.developerMode());

    menuUtilityHibern8IDE.setVisible(JSimpplle.developerMode());
    menuUtilityHibern8IDE.setEnabled(JSimpplle.developerMode());
    menuUtilityDatabaseManager.setVisible(true);
    menuUtilityDatabaseManager.setEnabled(true);

    menuUtilityTestNewDialog.setVisible(JSimpplle.developerMode());
    menuUtilityTestNewDialog.setEnabled(JSimpplle.developerMode());

    menuSysKnowDisableWsbw.setState(false);

    menuReportsFireSuppCostAll.setVisible(false);
    fireSpreadModels.add("SIMPPLLE");
  }

  /**
   * Loads a default zone from the application properties.
   *
   * @param properties the properties to read the default zone from
   * @throws SimpplleError if the zone fails to load
   */
  public void loadDefaultZone(Properties properties) throws SimpplleError {

    int zoneId = Simpplle.zoneNameToIndex(properties.getDefaultZoneName());

    if (zoneId < 0) return;

    comcode.loadZone(zoneId, properties.getHistoricPathways());

    enableZoneControls();
    disableAreaControls();

    RegionalZone zone = Simpplle.getCurrentZone();
    String label = zone.getName();

    if (zone.isHistoric()) {

      label += " (Historic Pathways)";
    }

    zoneValueLabel.setText(label);
    areaValueLabel.setText("");
    refresh();
  }

  //Component initialization

  /**
   * Initializes the SImpplleMain frame with a host of components, the most important of which are all the JMenu's and their JMenuItems.
   *
   * @throws Exception
   */
  private void jbInit() throws Exception {
    titledBorder1 = new TitledBorder("");
    this.getContentPane().setLayout(borderLayout1);
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.setSize(new Dimension(1000, 700));
    this.setLocation(100, 100);
    this.setTitle("OpenSIMPPLLE " + SimpplleMain.VERSION + " (" + SimpplleMain.BUILD_DATE + ")");
    this.addComponentListener(new java.awt.event.ComponentAdapter() {

      public void componentResized(ComponentEvent e) {
        this_componentResized(e);
      }
    });
    statusBar.setBackground(Color.black);
    statusBar.setForeground(Color.white);
    statusBar.setMaximumSize(new Dimension(1280, 20));
    statusBar.setMinimumSize(new Dimension(718, 20));
    statusBar.setOpaque(true);
    statusBar.setPreferredSize(new Dimension(100, 20));
    menuFile.setText("File");
    menuFileQuit.setText("Quit");
    menuFileQuit.addActionListener(this::fileExit_actionPerformed);
    loadZone.setToolTipText("Loads logic for a geographic region");
    loadZone.setText("Load Zone");
    loadZone.addActionListener(this::loadZone_actionPerformed);
    loadArea.setEnabled(false);
    loadArea.setToolTipText("Loads a landscape containing aquatic, land, road, trail, and vegetation units");
    loadArea.setText("Load Area");
    loadArea.addActionListener(this::loadArea_actionPerformed);
    runSimulation.setEnabled(false);
    runSimulation.setText("Run Simulation");
    runSimulation.addActionListener(this::runSimulation_actionPerformed);
    menuSysKnow.setText("System Knowledge");
    menuImport.setText("Import");
    menuExport.setText("Export");
    menuReports.setText("Reports");
    menuInterpretations.setText("Interpretations");
    menuViewResult.setText("View Results");
    menuHelp.setText("Help");
    menuSettings.setText("Preferences");
    menuSettings.setActionCommand("Preferences");
    menuSettings.addActionListener(this::actionMenuSettings);
    menuUtility.setText("Utilities");
    menuUtilityReset.setEnabled(false);
    menuUtilityReset.setToolTipText("Discards simulation and restores area to initial conditions");
    menuUtilityReset.setText("Reset Area/Simulation");
    menuUtilityReset.addActionListener(this::menuUtilityReset_actionPerformed);
    menuFileSave.setEnabled(false);
    menuFileSave.setText("Save Area");
    menuFileSave.addActionListener(this::menuFileSave_actionPerformed);
    menuSysKnowPath.setEnabled(false);
    menuSysKnowPath.setText("Pathways");
    menuSysKnowPathVeg.setText("Vegetative Pathways");
    menuSysKnowPathVeg.addActionListener(this::menuSysKnowPathVeg_actionPerformed);
    menuSysKnowPathAquatic.setEnabled(false);
    menuSysKnowPathAquatic.setText("Aquatic Pathways");
    menuSysKnowPathAquatic.addActionListener(this::menuSysKnowPathAquatic_actionPerformed);
    menuSysKnowVegProc.setText("Vegetative Process");
    menuSysKnowVegProcLock.setEnabled(false);
    menuSysKnowVegProcLock.setText("Lock in Processes");
    menuSysKnowVegProcLock.addActionListener(this::menuSysKnowVegProcLock_actionPerformed);
    menuSysKnowFireEventLogic.setEnabled(false);
    menuSysKnowFireEventLogic.setText("Fire Spread, Type, and Spotting");
    menuSysKnowFireEventLogic.addActionListener(this::menuSysKnowFireEventLogic_actionPerformed);
    menuSysKnowFireProb.setEnabled(false);
    menuSysKnowFireProb.setText("Extreme Fire Spread Probability");
    menuSysKnowFireProb.addActionListener(this::menuSysKnowFireProb_actionPerformed);

    menuItemAquaticProcess.setEnabled(false);
    menuItemAquaticProcess.setText("Aquatic Process");
    menuSysKnowAquaticTreat.setEnabled(false);
    menuSysKnowAquaticTreat.setText("Aquatic Treatments");
    menuExportGIS.setEnabled(false);
    menuExportGIS.setText("GIS Simulation Files");
    menuReportsSummary.setEnabled(false);
    menuReportsSummary.setText("Summary");
    menuReportsSummary.addActionListener(this::menuReportsSummary_actionPerformed);
    menuReportsUnit.setEnabled(false);
    menuReportsUnit.setText("Individual Unit");
    menuReportsUnit.addActionListener(this::menuReportsUnit_actionPerformed);
    menuReportsMult.setEnabled(false);
    menuReportsMult.setText("Multiple Simulation");
    menuReportsMultNormal.setText("Normal");
    menuReportsMultNormal.addActionListener(this::menuReportsMultNormal_actionPerformed);
    menuReportsMultSpecial.setEnabled(false);
    menuReportsMultSpecial.setText("By Special Area");
    menuReportsMultSpecial.addActionListener(this::menuReportsMultSpecial_actionPerformed);
    menuReportsMultOwner.setEnabled(false);
    menuReportsMultOwner.setText("By Ownership");
    menuReportsMultOwner.addActionListener(this::menuReportsMultOwner_actionPerformed);
    menuReportsFire.setEnabled(false);
    menuReportsFire.setText("Detailed Fire");
    menuReportsFire.addActionListener(this::menuReportsFire_actionPerformed);
    menuReportsFireCost.setEnabled(false);
    menuReportsFireCost.setText("Fire Suppression Cost");
    menuReportsFireCost.addActionListener(this::menuReportsFireCost_actionPerformed);
    menuReportsEmission.setEnabled(false);
    menuReportsEmission.setToolTipText("Emissions based on adaptations of work by hardy, etc.");
    menuReportsEmission.setText("Emissions");
    menuReportsEmission.addActionListener(this::menuReportsEmission_actionPerformed);
    menuInterpretProbCalc.setEnabled(false);
    menuInterpretProbCalc.setText("Attribute Probability Calculator");
    menuInterpretProbCalc.addActionListener(this::menuInterpretProbCalc_actionPerformed);

    menuInterpretWildlife.setEnabled(false);
    menuInterpretWildlife.setText("Wildlife Habitat");
    menuInterpretWildlife.addActionListener(this::menuInterpretWildlife_actionPerformed);
    menuResultVegUnit.setEnabled(false);
    menuResultVegUnit.setText("Vegetative Unit Analysis");
    menuResultVegUnit.addActionListener(this::menuResultVegUnit_actionPerformed);
    menuResultVegSum.setEnabled(false);
    menuResultVegSum.setText("Vegetative Condition Summary");
    menuResultVegSum.addActionListener(this::menuResultVegSum_actionPerformed);
    menuResultAquaticUnit.setEnabled(false);
    menuResultAquaticUnit.setText("Aquatic Unit Analysis");
    menuResultAquaticUnit.addActionListener(this::menuResultAquaticUnit_actionPerformed);
    menuResultAquaticSum.setEnabled(false);
    menuResultAquaticSum.setText("Aquatic Condition Summary");
    menuResultAquaticSum.addActionListener(this::menuResultAquaticSum_actionPerformed);
    menuResultAquaticDiagram.setEnabled(false);
    menuResultAquaticDiagram.setText("Aquatic Stream Diagram");
    menuUtilitySimReady.setEnabled(false);
    menuUtilitySimReady.setText("Make Area Simulation Ready");
    menuUtilitySimReady.addActionListener(this::menuUtilitySimReady_actionPerformed);
    menuHelpAbout.setText("About");
    menuHelpAbout.addActionListener(this::menuHelpAbout_actionPerformed);
    menuHelpDocs.setText("Documentation");
    menuUtilityPrintArea.setEnabled(false);
    menuUtilityPrintArea.setText("Print Current Area to File");
    menuUtilityPrintArea.addActionListener(this::menuUtilityPrintArea_actionPerformed);
    areaLabel.setText("Current Area:");
    zoneLabel.setText("Current Zone");
    currentPanel.setLayout(gridLayout1);
    gridLayout1.setColumns(1);
    gridLayout1.setHgap(1);
    gridLayout1.setRows(2);
    northPanel.setLayout(borderLayout2);
    currentPanel.setBorder(BorderFactory.createEtchedBorder());
    zonePanel.setLayout(flowLayout1);
    flowLayout1.setAlignment(FlowLayout.LEFT);
    flowLayout1.setHgap(10);
    areaPanel.setLayout(flowLayout2);
    flowLayout2.setAlignment(FlowLayout.LEFT);
    flowLayout2.setHgap(10);
    areaValueLabel.setForeground(Color.blue);
    zoneValueLabel.setForeground(Color.blue);
    menuExportGISDecadeProb.setEnabled(false);
    menuExportGISDecadeProb.setText("Create GIS Probability Files by Decade");
    menuExportGISDecadeProb.addActionListener(this::menuExportGISDecadeProb_actionPerformed);
    menuImportCreate.setEnabled(false);
    menuImportCreate.setText("Create New Area");
    menuImportCreate.addActionListener(this::menuImportCreate_actionPerformed);
    menuImportFixStates.setEnabled(false);
    menuImportFixStates.setText("Fix Incorrect States");
    menuImportFixStates.addActionListener(this::menuImportFixStates_actionPerformed);
    menuUtilityUnitEditor.setEnabled(false);
    menuUtilityUnitEditor.setText("Open Unit Editor");
    menuUtilityUnitEditor.addActionListener(this::menuUtilityUnitEditor_actionPerformed);
    areaInvalidLabel.setFont(new java.awt.Font("Serif", Font.BOLD, 14));
    areaInvalidLabel.setPreferredSize(new Dimension(70, 20));
    menuSysKnowRegionalClimate.setEnabled(false);
    menuSysKnowRegionalClimate.setText("Regional Climate");
    menuSysKnowRegionalClimate.addActionListener(this::menuSysKnowRegionalClimate_actionPerformed);

    menuUtilitiesConsole.setText("Display Console Messages");
    menuUtilitiesConsole.addActionListener(this::menuUtilitiesConsole_actionPerformed);
    menuImportAttributeData.setEnabled(false);
    menuImportAttributeData.setText("Import Attribute Data");
    menuImportAttributeData.addActionListener(this::menuImportAttributeData_actionPerformed);
    menuUtilityAreaName.setEnabled(false);
    menuUtilityAreaName.setText("Change Area Name");
    menuUtilityAreaName.addActionListener(this::menuUtilityAreaName_actionPerformed);
    menuInterpretRestoration.setText("Ecosystem Restoration");
    menuInterpretRestoration.addActionListener(this::menuInterpretRestoration_actionPerformed);
    menuSysKnowOpen.setEnabled(false);
    menuSysKnowOpen.setToolTipText("Loads user-defined logic for a geographic region");
    menuSysKnowOpen.setText("Load User Knowledge");
    menuSysKnowOpen.addActionListener(this::menuSysKnowOpen_actionPerformed);
    menuSysKnowSave.setEnabled(false);
    menuSysKnowSave.setToolTipText("Saves user-defined logic for a geographic region");
    menuSysKnowSave.setText("Save User Knowledge");
    menuSysKnowSave.addActionListener(this::menuSysKnowSave_actionPerformed);
    menuInterpretRiskCalc.setEnabled(false);
    menuInterpretRiskCalc.setText("Risk Calculator");

    menuSysKnowRegen.setEnabled(false);
    menuSysKnowRegen.setText("Regeneration");
    menuSysKnowRegen.addActionListener(this::menuSysKnowRegen_actionPerformed);
    menuSysKnowConiferEncroach.setEnabled(false);
    menuSysKnowConiferEncroach.setText("Conifer Encroachment");
    menuSysKnowConiferEncroach.addActionListener(this::menuSysKnowConiferEncroach_actionPerformed);
    menuSysKnowVegTreat.setEnabled(false);
    menuSysKnowVegTreat.setText("Vegetative Treatments");
    menuSysKnowVegTreatSchedule.setEnabled(false);
    menuSysKnowVegTreatSchedule.setText("Treatment Schedule");
    menuSysKnowVegTreatSchedule.addActionListener(this::menuSysKnowVegTreatSchedule_actionPerformed);
    menuSysKnowVegTreatLogic.setEnabled(false);
    menuSysKnowVegTreatLogic.setText("Treatment Logic");
    menuSysKnowVegTreatLogic.addActionListener(this::menuSysKnowVegTreatLogic_actionPerformed);

    menuSysKnowRestoreDefaults.setEnabled(false);
    menuSysKnowRestoreDefaults.setToolTipText("Restores system knowledge to defaults.");
    menuSysKnowRestoreDefaults.setText("Restore All Defaults");
    menuSysKnowRestoreDefaults.addActionListener(this::menuSysKnowRestoreDefaults_actionPerformed);
    menuExportGISReburn.setText("Probability of Reburn");
    menuExportGISReburn.addActionListener(this::menuExportGISReburn_actionPerformed);
    menuImportInvalidReport.setEnabled(false);
    menuImportInvalidReport.setText("Invalid Units Report");
    menuImportInvalidReport.addActionListener(this::menuImportInvalidReport_actionPerformed);
    menuSysKnowWeatherEvent.setEnabled(false);
    menuSysKnowWeatherEvent.setText("Weather Ending Events");
    menuWeatherEventClassA.setText("Fires Less than .25 acres");
    menuWeatherEventClassA.addActionListener(this::menuWeatherEventClassA_actionPerformed);
    menuWeatherEventNotClassA.setText("Fires greater than .25 acres");
    menuWeatherEventNotClassA.addActionListener(this::menuWeatherEventNotClassA_actionPerformed);
    menuExportAttributes.setEnabled(false);
    menuExportAttributes.setText("Area Creation Files");
    menuExportAttributes.addActionListener(this::menuExportAttributes_actionPerformed);
    menuUtilityDatabaseTest.setEnabled(false);
    menuUtilityDatabaseTest.setText("Database test");
    menuUtilityDatabaseTest.addActionListener(this::menuUtilityDatabaseTest_actionPerformed);
    menuSysKnowUseRegenPulse.setText("Use Regen Pulse");
    menuSysKnowUseRegenPulse.addActionListener(this::menuSysKnowUseRegenPulse_actionPerformed);
    menuSysKnowFireSuppLogic.setEnabled(false);
    menuSysKnowFireSuppLogic.setText("Fire Suppression Logic");

    menuSysKnowFireSuppLogic.add(menuSysKnowFireSuppEvent);
    menuSysKnowFireSuppEvent.addActionListener(this::menuSysKnowFireSuppEvent_actionPerformed);
    menuSysKnowFireSuppEvent.setText("Event Probability");
    menuSysKnowFireSuppLogicClassA.setText("Class A");
    menuSysKnowFireSuppLogicClassA.addActionListener(this::menuSysKnowFireSuppLogicClassA_actionPerformed);
    menuSysKnowFireSuppLogicBeyondClassA.setText("Beyond Class A");
    menuSysKnowFireSuppLogicBeyondClassA.addActionListener(this::menuSysKnowFireSuppLogicBeyondClassA_actionPerformed);
    menuMagis.setEnabled(false);
    menuMagis.setText("Magis");

    menuMagisProcessTreatmentFiles.setEnabled(false);
    menuMagisProcessTreatmentFiles.setText("Process & Treatment Files");
    menuMagisProcessTreatmentFiles.addActionListener(this::menuMagisProcessTreatmentFiles_actionPerformed);
    menuMagisAllVegStates.setEnabled(false);
    menuMagisAllVegStates.setText("All Vegetative States");
    menuMagisAllVegStates.addActionListener(this::menuMagisAllVegStates_actionPerformed);
    menuUtilityDeleteUnits.setEnabled(false);
    menuUtilityDeleteUnits.setText("Delete Units...");
    menuUtilityDeleteUnits.addActionListener(this::menuUtilityDeleteUnits_actionPerformed);
    menuReportsFireSuppCostAll.setEnabled(false);
    menuReportsFireSuppCostAll.setText("Fire Supp Cost (all runs)");
    menuReportsFireSuppCostAll.addActionListener(this::menuReportsFireSuppCostAll_actionPerformed);
    menuSysKnowWSBWLogic.setEnabled(false);
    menuSysKnowWSBWLogic.setText("Western Spruce Budworm Logic");
    menuSysKnowWSBWLogic.addActionListener(this::menuSysKnowWSBWLogic_actionPerformed);
    menuGisUpdateSpread.setText("Update and Spread Files");
    menuGisUpdateSpread.addActionListener(this::menuGisUpdateSpread_actionPerformed);
    menuResultLandformUnit.setEnabled(false);
    menuResultLandformUnit.setText("Landform Unit Analysis");
    menuResultLandformUnit.addActionListener(this::menuResultLandformUnit_actionPerformed);
    menuResultLandformSum.setEnabled(false);
    menuResultLandformSum.setText("Landform Condition Summary");
    menuResultLandformSum.addActionListener(this::menuResultLandformSum_actionPerformed);
    menuSysKnowVegTreatDesired.setEnabled(false);
    menuSysKnowVegTreatDesired.setText("Desired Future Conditions");
    menuSysKnowVegTreatDesired.addActionListener(this::menuSysKnowVegTreatDesired_actionPerformed);
    menuUtilityGISFiles.setEnabled(false);
    menuUtilityGISFiles.setText("Copy GIS Files");
    menuUtilityGISFiles.addActionListener(this::menuUtilityGISFiles_actionPerformed);
    menuSysKnowFireSeason.setEnabled(false);
    menuSysKnowFireSeason.setText("Fire Season");
    menuSysKnowFireSeason.addActionListener(this::menuSysKnowFireSeason_actionPerformed);
    menuSysKnowCellPerc.setEnabled(false);
    menuSysKnowCellPerc.addActionListener(this::menuSysKnowCellPerc_actionPerformed);
    menuHelpUserGuide.setText("User\'s Guide");
    menuHelpUserGuide.addActionListener(this::menuHelpUserGuide_actionPerformed);
    menuSysKnowFireSuppProdRate.setText("Production Rate");
    menuSysKnowFireSuppProdRate.addActionListener(this::menuSysKnowFireSuppProdRate_actionPerformed);
    menuSysKnowFireSuppSpreadRate.setText("Spread Rate");
    menuSysKnowFireSuppSpreadRate.addActionListener(this::menuSysKnowFireSuppSpreadRate_actionPerformed);
    menuSysKnowFireOccMgmtZone.setText("Fire Occurrence and Management Zones");
    menuSysKnowFireOccMgmtZone.addActionListener(this::menuSysKnowFireSuppFireOccMgmtZone_actionPerformed);
    menuSysKnowSpeciesKnowledgeEditor.setEnabled(false);
    menuSysKnowSpeciesKnowledgeEditor.setText("Species Attribute Editor");
    menuSysKnowSpeciesKnowledgeEditor.addActionListener(this::menuSysKnowSpeciesKnowledgeEditor_actionPerformed);
    menuImportFSLandscape.setText("Run FS Landscape");
    menuImportFSLandscape.addActionListener(this::menuImportFSLandscape_actionPerformed);
    menuSysKnowImportProcesses.setEnabled(false);
    menuSysKnowImportProcesses.setText("Import Zone Processes");
    menuSysKnowImportProcesses.addActionListener(this::menuSysKnowImportProcesses_actionPerformed);
    menuSysKnowShowLegalProcesses.setEnabled(false);
    menuSysKnowShowLegalProcesses.setText("Show Zone Processes");
    menuSysKnowShowLegalProcesses.addActionListener(this::menuSysKnowShowLegalProcesses_actionPerformed);
    menuSysKnowShowZoneTreatments.setEnabled(false);
    menuSysKnowShowZoneTreatments.setText("Show Zone Treatments");
    menuSysKnowShowZoneTreatments.addActionListener(this::menuSysKnowShowZoneTreatments_actionPerformed);
    menuSysKnowImportZoneTreatments.setEnabled(false);
    menuSysKnowImportZoneTreatments.setText("Import Zone Treatments");
    menuSysKnowImportZoneTreatments.addActionListener(this::menuSysKnowImportZoneTreatments_actionPerformed);
    menuFileSaveZone.setEnabled(false);
    menuFileSaveZone.setText("Save Zone");
    menuFileSaveZone.addActionListener(this::menuFileSaveZone_actionPerformed);
    menuReportsAllStates.setEnabled(false);
    menuReportsAllStates.setText("All States Report");
    menuReportsAllStates.addActionListener(this::menuReportsAllStates_actionPerformed);
    buildSimpplleTypeFiles.setEnabled(true);
    buildSimpplleTypeFiles.setText("Build All SimpplleType Files");
    buildSimpplleTypeFiles.addActionListener(this::buildSimpplleTypeFiles_actionPerformed);
    buildSimpplleTypesSource.setActionCommand("Build  SimpplleTypes Source");
    buildSimpplleTypesSource.setText("Build SimpplleTypes Source");
    buildSimpplleTypesSource.addActionListener(this::buildSimpplleTypesSource_actionPerformed);
    menuSysKnowWindthrow.setEnabled(false);
    menuSysKnowWindthrow.setText("Windthrow");
    menuSysKnowWindthrow.addActionListener(this::menuSysKnowWindthrow_actionPerformed);
    menuSysKnowWildlifeBrowsing.setEnabled(false);
    menuSysKnowWildlifeBrowsing.setText("Wildlife Browsing");
    menuSysKnowWildlifeBrowsing.addActionListener(this::menuSysKnowWildlifeBrowsing_actionPerformed);
    menuSysKnowTussockMoth.setEnabled(false);
    menuSysKnowTussockMoth.setText("Tussock Moth");
    menuSysKnowTussockMoth.addActionListener(this::menuSysKnowTussockMoth_actionPerformed);
    menuUtilityTestNewDialog.setEnabled(false);
    menuUtilityTestNewDialog.setText("New Dialog Testing");
    menuUtilityTestNewDialog.addActionListener(this::menuUtilityTestNewDialog_actionPerformed);
    menuSysKnowProcessProbLogic.setEnabled(false);
    menuSysKnowProcessProbLogic.setText("Process Probability Logic");
    menuSysKnowProcessProbLogic.addActionListener(this::menuSysKnowProcessProbLogic_actionPerformed);
    menuSysKnowDisableWsbw.setEnabled(false);
    menuSysKnowDisableWsbw.setText("Disable Western Spruce Budworm");
    menuSysKnowDisableWsbw.addActionListener(this::menuSysKnowDisableWsbw_actionPerformed);
    //Disables WSBW logic manually, instead of using menu item directly above, since that has been hidden
    Wsbw.setEnabled(false);

    menuUtilityDatabaseManager.setEnabled(false);
    menuUtilityDatabaseManager.setText("DatabaseManager");
    menuUtilityDatabaseManager.addActionListener(this::menuUtilityDatabaseManager_actionPerformed);
    menuBisonGrazingLogic.setText("Bison Grazing Logic");
    menuBisonGrazingLogic.addActionListener(this::menuBisonGrazingLogic_actionPerformed);
    menuUtilityMemoryUse.setText("Memory Use Display");
    menuUtilityMemoryUse.addActionListener(this::menuUtilityMemoryUse_actionPerformed);
    menuSysKnowDoCompetition.setEnabled(false);
    menuSysKnowDoCompetition.setText("Lifeform Competition");
    menuSysKnowDoCompetition.addActionListener(this::menuSysKnowDoCompetition_actionPerformed);
    menuSysKnowGapProcessLogic.setEnabled(false);
    menuSysKnowGapProcessLogic.setText("Gap Process Logic");
    menuSysKnowGapProcessLogic.addActionListener(this::menuSysKnowGapProcessLogic_actionPerformed);
    menuSysKnowProducingSeedLogic.setEnabled(false);
    menuSysKnowProducingSeedLogic.setText("Producing Seed Logic");
    menuSysKnowProducingSeedLogic.addActionListener(this::menuSysKnowProducingSeedLogic_actionPerformed);
    menuSysKnowVegUnitFireTypeLogic.setEnabled(false);
    menuSysKnowVegUnitFireTypeLogic.setText("Vegetative -- Unit Fire Type Logic");
    menuSysKnowVegUnitFireTypeLogic.addActionListener(this::menuSysKnowVegUnitFireTypeLogic_actionPerformed);
    menuSysKnowInvasive.setEnabled(false);
    menuSysKnowInvasive.setText("Invasive Species Logic");
    menuSysKnowInvasiveLogicR1.setEnabled(false);
    menuSysKnowInvasiveLogicR1.setText("Region One Logic");
    menuSysKnowInvasiveLogicR1.addActionListener(this::menuSysKnowInvasiveLogicR1_actionPerformed);
    menuSysKnowInvasiveLogicMSU.setEnabled(false);
    menuSysKnowInvasiveLogicMSU.setText("Montana State University Logic");
    menuSysKnowInvasiveLogicMSU.addActionListener(this::menuSysKnowInvasiveLogicMSU_actionPerformed);
    menuSysKnowInvasiveLogicMesaVerdeNP.setEnabled(false);
    menuSysKnowInvasiveLogicMesaVerdeNP.setText("Mesa Verde NP");
    menuSysKnowInvasiveLogicMesaVerdeNP.addActionListener(this::menuSysKnowInvasiveLogicMesaVerdeNP_actionPerformed);
    menuUtilityMakeAreaMultipleLife.setEnabled(false);
    menuUtilityMakeAreaMultipleLife.setText("Change Area to Multiple Lifeform");
    menuUtilityMakeAreaMultipleLife.addActionListener(this::menuUtilityMakeAreaMultipleLife_actionPerformed);
    menuReportsTrackingSpecies.setEnabled(false);
    menuReportsTrackingSpecies.setText("Tracking Species Report");
    menuReportsTrackingSpecies.addActionListener(this::menuReportsTrackingSpecies_actionPerformed);
    menuUtilityCombineLSFiles.setText("Combine LS Files");
    menuUtilityCombineLSFiles.addActionListener(this::menuUtilityCombineLSFiles_actionPerformed);
    menuFile.add(menuSettings);
    menuFile.addSeparator();
    menuFile.add(menuFileSave);
    menuFile.addSeparator();
    menuFile.add(menuFileSaveZone);
    menuFile.addSeparator();
    menuFile.add(menuFileQuit);
    mainMenuBar.add(menuFile);
    mainMenuBar.add(menuSysKnow);
    mainMenuBar.add(menuImport);
    mainMenuBar.add(menuExport);
    mainMenuBar.add(menuReports);
    mainMenuBar.add(menuInterpretations);
    mainMenuBar.add(menuViewResult);
    mainMenuBar.add(menuUtility);
    mainMenuBar.add(menuHelp);
    this.setJMenuBar(mainMenuBar);
    this.getContentPane().add(statusBar, BorderLayout.SOUTH);
    this.getContentPane().add(lavaPanel);
    this.getContentPane().add(northPanel, BorderLayout.NORTH);
    northPanel.add(currentPanel, BorderLayout.SOUTH);
    currentPanel.add(zonePanel, null);
    zonePanel.add(zoneLabel, null);
    zonePanel.add(zoneValueLabel, null);
    currentPanel.add(areaPanel, null);
    areaPanel.add(areaLabel, null);
    areaPanel.add(areaValueLabel, null);
    areaPanel.add(areaInvalidLabel, null);
    northPanel.add(toolBar, BorderLayout.NORTH);
    toolBar.add(loadZone);
    toolBar.add(loadArea);
    toolBar.add(runSimulation);
    menuUtility.add(menuUtilityAreaName);
    menuUtility.add(menuUtilityUnitEditor);

    menuUtility.add(menuUtilityElevRelPos);
    menuUtilityElevRelPos.addActionListener(this::menuUtilityElevRelPos_actionPerformed);
    menuUtilityElevRelPos.setText("Elevation Relative Position ...");
    menuUtilityElevRelPos.setEnabled(false);

    menuUtility.add(menuUtilitySwapRowCol);
    menuUtilitySwapRowCol.addActionListener(this::menuUtilitySwapRowCol_actionPerformed);
    menuUtilitySwapRowCol.setText("Swap ROW/COL");
    menuUtilitySwapRowCol.setEnabled(false);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityCombineLSFiles);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityGISFiles);

    menuUtility.add(menuUtilityExportPathways);
    menuUtilityExportPathways.addActionListener(this::menuUtilityExportPathways_actionPerformed);
    menuUtilityExportPathways.setText("Export Pathways");
    menuUtilityExportPathways.setEnabled(false);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityDeleteUnits);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilitySimReady);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityMakeAreaMultipleLife);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityReset);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilityPrintArea);
    menuUtility.addSeparator();
    menuUtility.add(menuMagis);
    menuUtility.addSeparator();
    menuUtility.add(menuUtilitiesConsole);
    menuUtility.add(menuUtilityDatabaseTest);
    menuUtility.add(menuUtilityDatabaseManager);
    menuUtility.add(buildSimpplleTypeFiles);
    menuUtility.add(buildSimpplleTypesSource);
    menuUtility.add(menuUtilityTestNewDialog);
    menuUtility.add(menuUtilityMemoryUse);
    menuSysKnow.add(menuSysKnowPath);
    menuSysKnow.addSeparator();
    menuSysKnow.add(menuSysKnowVegProc);
    menuSysKnow.add(menuItemAquaticProcess);
    menuSysKnow.addSeparator();
    menuSysKnow.add(menuSysKnowVegTreat);
    menuSysKnow.add(menuSysKnowAquaticTreat);
    menuSysKnow.addSeparator();
    menuSysKnow.add(menuSysKnowRegionalClimate);
    menuSysKnow.addSeparator();
    menuSysKnow.add(menuSysKnowOpen);
    menuSysKnow.add(menuSysKnowSave);
    menuSysKnow.addSeparator();
    menuSysKnow.add(menuSysKnowRestoreDefaults);
    menuSysKnowPath.add(menuSysKnowPathVeg);
    menuSysKnowPath.add(menuSysKnowPathAquatic);
    menuSysKnowVegProc.add(menuSysKnowVegProcLock);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowFireEventLogic);
    menuSysKnowVegProc.add(menuSysKnowFireSeason);
    menuSysKnowVegProc.add(menuSysKnowFireSpread);
    menuSysKnowFireSpread.add(menuSysKnowCellPerc);
    menuSysKnowVegProc.add(menuSysKnowFireSuppLogic);
    menuSysKnowVegProc.add(menuSysKnowWeatherEvent);
    menuSysKnowVegProc.add(menuSysKnowVegUnitFireTypeLogic);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowSpeciesKnowledgeEditor);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuBisonGrazingLogic);

    menuSysKnowVegProc.add(menuSysKnowProcessProbLogic);
    menuSysKnowVegProc.add(menuSysKnowGapProcessLogic);
    menuSysKnowVegProc.add(menuSysKnowWindthrow);
    menuSysKnowVegProc.add(menuSysKnowWildlifeBrowsing);
    menuSysKnowVegProc.add(menuSysKnowTussockMoth);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowInvasive);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowUseRegenPulse);
    menuSysKnowVegProc.add(menuSysKnowRegen);
    menuSysKnowVegProc.add(menuSysKnowProducingSeedLogic);
    menuSysKnowVegProc.add(menuSysKnowConiferEncroach);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowDoCompetition);
    menuSysKnowVegProc.addSeparator();
    menuSysKnowVegProc.add(menuSysKnowShowLegalProcesses);
    menuSysKnowVegProc.add(menuSysKnowImportProcesses);
    menuImport.addSeparator();
    menuImport.add(menuImportCreate);
    menuImport.add(menuImportAttributeData);
    menuImport.addSeparator();
    menuImport.add(menuImportFixStates);
    menuImport.add(menuImportInvalidReport);
    menuImport.addSeparator();
    menuImport.add(menuImportFSLandscape);
    menuExport.add(menuExportGIS);
    menuExport.add(menuExportAttributes);
    menuExportGIS.add(menuGisUpdateSpread);
    menuExportGIS.add(menuExportGISDecadeProb);
    menuExportGIS.add(menuExportGISReburn);
    menuReports.add(menuReportsSummary);
    menuReports.add(menuReportsUnit);
    menuReports.add(menuReportsAllStates);
    menuReports.add(menuReportsTrackingSpecies);
    menuReports.add(menuReportsMult);
    menuReports.add(menuReportsFire);
    menuReports.add(menuReportsFireCost);
    menuReports.add(menuReportsFireSuppCostAll);
    menuReports.add(menuReportsEmission);
    menuReportsMult.add(menuReportsMultNormal);
    menuReportsMult.add(menuReportsMultSpecial);
    menuReportsMult.add(menuReportsMultOwner);
    menuInterpretations.add(menuInterpretProbCalc);
    menuInterpretations.add(menuInterpretRestoration);
    menuInterpretations.add(menuInterpretWildlife);
    menuViewResult.add(menuResultVegUnit);
    menuViewResult.add(menuResultVegSum);
    menuViewResult.addSeparator();
    menuViewResult.add(menuResultAquaticUnit);
    menuViewResult.add(menuResultAquaticSum);
    menuViewResult.addSeparator();
    menuViewResult.add(menuResultLandformUnit);
    menuViewResult.add(menuResultLandformSum);
    menuViewResult.addSeparator();
    menuViewResult.add(menuResultAquaticDiagram);
    menuHelp.add(menuHelpDocs);
    menuHelp.add(menuHelpUserGuide);
    menuHelp.add(menuHelpAbout);
    menuSysKnowVegTreat.add(menuSysKnowVegTreatSchedule);
    menuSysKnowVegTreat.add(menuSysKnowVegTreatLogic);
    menuSysKnowVegTreat.add(menuSysKnowVegTreatDesired);
    menuSysKnowVegTreat.addSeparator();
    menuSysKnowVegTreat.add(menuSysKnowShowZoneTreatments);
    menuSysKnowVegTreat.add(menuSysKnowImportZoneTreatments);
    menuSysKnowWeatherEvent.add(menuWeatherEventClassA);
    menuSysKnowWeatherEvent.add(menuWeatherEventNotClassA);
    menuSysKnowFireSuppLogic.add(menuSysKnowFireSuppLogicClassA);
    menuSysKnowFireSuppLogic.add(menuSysKnowFireSuppLogicBeyondClassA);
    menuSysKnowFireSuppLogic.addSeparator();
    menuSysKnowFireSuppLogic.add(menuSysKnowFireSuppProdRate);
    menuSysKnowFireSuppLogic.add(menuSysKnowFireSuppSpreadRate);
    menuSysKnowFireSuppLogic.add(menuSysKnowFireOccMgmtZone);

    menuMagis.add(menuMagisProcessTreatmentFiles);
    menuMagis.add(menuMagisAllVegStates);
    menuSysKnowInvasive.add(menuSysKnowInvasiveLogicR1);
    menuSysKnowInvasive.add(menuSysKnowInvasiveLogicMesaVerdeNP);
    menuSysKnowInvasive.add(menuSysKnowInvasiveLogicMSU);

    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        try {
          initFX(lavaPanel);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
  }

  public void refresh() {
    update(getGraphics());
  }

  /**
   * Clears the status message by setting it to empty string.
   */
  public void clearStatusMessage() {
    setStatusMessage("");
  }

  /**
   * Sets the status message to the string message in parameter.  Does not wait.
   *
   * @param msg the message to be displayed
   */
  public void setStatusMessage(String msg) {
    setStatusMessage(msg, false);
  }

  /**
   * If wait is enabled, shows a messageDialog which has the user press Ok to continue.
   *
   * @param msg  message to be displayed in the status bar.
   * @param wait if true will ask the user to press Ok to continue.
   */
  public void setStatusMessage(String msg, boolean wait) {
    statusBar.setText(msg);
    if (wait) {
      JOptionPane.showMessageDialog(this, "Press Ok to continue", "Profiling use",
          JOptionPane.INFORMATION_MESSAGE);
    }
    refresh();
  }

  /**
   * Enables the simulation controls by getting the current simulation and
   * setting the simulation JMenu items to enabled
   */
  public void enableSimulationControls() {
    Simulation simulation = Simpplle.getCurrentSimulation();

    menuReportsSummary.setEnabled(true);
    menuReportsUnit.setEnabled(true);
    menuReportsFire.setEnabled(true);
    menuReportsFireCost.setEnabled(simulation.fireSuppression());
    menuReportsEmission.setEnabled(true);
    menuExportGIS.setEnabled(true);
    menuFileSave.setEnabled(true);

    // We do not want the user to be able to edit units after a simulation.
    menuUtilityUnitEditor.setEnabled(false);

    menuUtilityDeleteUnits.setEnabled(false);

    if (simulation.isMultipleRun() &&
        simulation.existsMultipleRunSummary()) {
      menuReportsMult.setEnabled(true);
      if (simulation.trackOwnership()) {
        menuReportsMultOwner.setEnabled(true);
      }
      if (simulation.trackSpecialArea()) {
        menuReportsMultSpecial.setEnabled(true);
      }
      menuInterpretProbCalc.setEnabled(true);
    }
    if (simulation.isMultipleRun()) {
      menuExportGISDecadeProb.setEnabled(true);
    }
    menuImportAttributeData.setEnabled(false);

    menuReportsFireSuppCostAll.setEnabled(false);

    if (simulation.isDiscardData()) {
      menuReportsFire.setEnabled(false);
      menuExportGIS.setEnabled(false);
    }

    if ((simulation.isDiscardData() &&
        simulation.isDoAllStatesSummary() == false) ||
        (simulation.isMultipleRun() /*&& simulation.existsMultipleRunSummary()*/)) {
      menuReportsAllStates.setEnabled(false);
      menuReportsTrackingSpecies.setEnabled(false);
    } else {
      menuReportsAllStates.setEnabled(true);
      menuReportsTrackingSpecies.setEnabled(true);
    }
  }

  /**
   * Disables simulation controls by setting the enabled simulation methods of the JMenu items to false.
   */
  public void disableSimulationControls() {
    menuReportsSummary.setEnabled(false);
    menuReportsAllStates.setEnabled(false);
    menuReportsTrackingSpecies.setEnabled(false);
    menuReportsFire.setEnabled(false);
    menuReportsFireCost.setEnabled(false);
    menuReportsEmission.setEnabled(false);
    menuExportGIS.setEnabled(false);
    menuReportsMult.setEnabled(false);
    menuReportsMultOwner.setEnabled(false);
    menuReportsMultSpecial.setEnabled(false);
    menuExportGISDecadeProb.setEnabled(false);
    menuInterpretProbCalc.setEnabled(false);

    menuReportsFireSuppCostAll.setEnabled(true);
  }

  /**
   * Enables the area JMenu items.
   */
  public void enableAreaControls() {
    menuUtilityPrintArea.setEnabled(true);
    menuUtilityAreaName.setEnabled(true);
    runSimulation.setEnabled(true);
    menuResultVegUnit.setEnabled(true);
    menuResultVegSum.setEnabled(true);
    menuSysKnowVegTreatSchedule.setEnabled(true);
    menuSysKnowVegProcLock.setEnabled(true);
    menuUtilityUnitEditor.setEnabled(true);
    menuUtilityAreaName.setEnabled(true);
    menuUtilitySimReady.setEnabled(true);
    menuUtilityDeleteUnits.setEnabled(true);
    menuUtilityReset.setEnabled(true);
    menuUtilityElevRelPos.setEnabled(true);
    menuUtilitySwapRowCol.setEnabled(true);

    menuFileSave.setEnabled(true);
    menuImportFixStates.setEnabled(false);
    menuImportInvalidReport.setEnabled(false);
    menuImportAttributeData.setEnabled(true);
    menuInterpretWildlife.setEnabled(simpplle.comcode.WildlifeHabitat.isZoneValid());
    menuExportAttributes.setEnabled(true);

    menuMagisProcessTreatmentFiles.setEnabled(true);

    menuReportsFireSuppCostAll.setEnabled((Simpplle.getCurrentSimulation() == null));

    menuResultAquaticUnit.setEnabled((Simpplle.getCurrentArea().hasAquaticUnits()));

    menuResultLandformUnit.setEnabled((Simpplle.getCurrentArea().hasLandUnits()));

    menuReportsUnit.setEnabled(true);

    menuUtilityMakeAreaMultipleLife.setEnabled(true);

    menuUtilitySwapRowCol.setEnabled(true);
  }

  /**
   * LAVA Implementation. This method is invoked on the JavaFX thread.
   */
  private void initFX(JFXPanel fxPanel) throws IOException {
    Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
    Scene scene = new Scene(root);
    lavaPanel.setScene(scene);
  }
  /**
   * Disables Jmenu items by first checking if there is an invalid units and enabling or disabling Jmenu items to fix the states.
   * THen calls the  disableAreaControls() which sets the enabled methods of all area JMenu items to false.
   *
   * @param invalidUnits
   */
  public void disableAreaControls(boolean invalidUnits) {
    menuImportFixStates.setEnabled(invalidUnits);
    menuImportInvalidReport.setEnabled(invalidUnits);
    menuUtilityDeleteUnits.setEnabled(false);
    disableAreaControls();
  }

  /**
   * Sets the enabled methods of all area JMenu items to false.
   */
  public void disableAreaControls() {
    menuUtilityPrintArea.setEnabled(false);
    runSimulation.setEnabled(false);
    menuResultVegUnit.setEnabled(false);
    menuResultAquaticUnit.setEnabled(false);
    menuResultVegSum.setEnabled(false);
    menuSysKnowVegTreatSchedule.setEnabled(false);
    menuSysKnowVegProcLock.setEnabled(false);
    menuUtilityUnitEditor.setEnabled(false);
    menuUtilityAreaName.setEnabled(false);
    menuUtilitySimReady.setEnabled(false);
    menuInterpretWildlife.setEnabled(false);
    menuExportAttributes.setEnabled(false);
    menuUtilityDeleteUnits.setEnabled(false);
    menuUtilityReset.setEnabled(false);

    menuMagisProcessTreatmentFiles.setEnabled(true);

    menuFileSave.setEnabled(false);

    menuReportsFireSuppCostAll.setEnabled(false);

    menuUtilityMakeAreaMultipleLife.setEnabled(false);
    menuUtilitySwapRowCol.setEnabled(false);
  }

  /**
   * Gets the current area and checks if there are any invalid vegetative units.
   * If there are invalid Evu's  enables or Jmenu items to fix the states and lets the user know there are invalid Evu
   * Then calls the  disableAreaControls() which sets the enabled methods of all area JMenu items to false.
   * If there are it
   */
  public void updateAreaValidity() {
    if (Simpplle.getCurrentArea().hasInvalidVegetationUnits()) {
      disableAreaControls(true);
      areaInvalidLabel.setText("(Invalid)");
    } else {
      enableAreaControls();
      areaInvalidLabel.setText("");
    }
  }

  /**
   * Enables the zone JMenu items.  Unlike the simulation and area enable controls methods, this also has specific menu items for particular zones.
   */
  public void enableZoneControls() {
    loadArea.setEnabled(true);
    menuSysKnowFireEventLogic.setEnabled(true);
    menuSysKnowFireProb.setEnabled(true);
    menuSysKnowRegionalClimate.setEnabled(true);
    menuImportCreate.setEnabled(true);
    menuSysKnowPath.setEnabled(true);
    menuSysKnowPathVeg.setEnabled(true);
    menuSysKnowFireSuppLogic.setEnabled(true);
    menuSysKnowWeatherEvent.setEnabled(true);
    menuSysKnowVegTreat.setEnabled(true);
    menuSysKnowVegTreatLogic.setEnabled(true);
    menuMagis.setEnabled(true);
    menuMagisAllVegStates.setEnabled(true);
    menuSysKnowWSBWLogic.setEnabled(true);
    menuSysKnowRegen.setEnabled(true);
    menuSysKnowDisableWsbw.setEnabled(true);

    menuSysKnowFireSeason.setEnabled(true);
    menuSysKnowCellPerc.setEnabled(true);

    menuSysKnowPathAquatic.setEnabled(Simpplle.getCurrentZone().hasAquatics());

    menuUtilityGISFiles.setEnabled(System.getProperty("os.name").contains("Windows"));
    menuUtilityExportPathways.setEnabled(true);
    menuSysKnowSpeciesKnowledgeEditor.setEnabled(true);
    menuSysKnowOpen.setEnabled(true);
    menuSysKnowSave.setEnabled(true);
    menuSysKnowRestoreDefaults.setEnabled(true);

    menuSysKnowConiferEncroach.setEnabled(true);

    boolean doConiferEncroach =
        Simpplle.getCurrentZone().getId() == ValidZones.EASTSIDE_REGION_ONE ||
            Simpplle.getCurrentZone().getId() == ValidZones.TETON ||
            Simpplle.getCurrentZone().getId() == ValidZones.NORTHERN_CENTRAL_ROCKIES;

    menuSysKnowConiferEncroach.setVisible(doConiferEncroach);


    boolean enableZoneEditControls =
        Simpplle.getCurrentZone().getId() == ValidZones.COLORADO_FRONT_RANGE ||
            Simpplle.getCurrentZone().getId() == ValidZones.COLORADO_PLATEAU ||
            Simpplle.getCurrentZone().getId() == ValidZones.WESTERN_GREAT_PLAINS_STEPPE ||
            Simpplle.getCurrentZone().getId() == ValidZones.GREAT_PLAINS_STEPPE ||
            Simpplle.getCurrentZone().getId() == ValidZones.MIXED_GRASS_PRAIRIE;

    menuSysKnowImportProcesses.setEnabled(enableZoneEditControls);
    menuSysKnowShowLegalProcesses.setEnabled(enableZoneEditControls);
    menuSysKnowShowZoneTreatments.setEnabled(enableZoneEditControls);
    menuSysKnowImportZoneTreatments.setEnabled(enableZoneEditControls);
    menuFileSaveZone.setEnabled(enableZoneEditControls);

    menuSysKnowProcessProbLogic.setEnabled(true);
    menuSysKnowProcessProbLogic.setVisible(true);

    menuSysKnowImportProcesses.setVisible(enableZoneEditControls);
    menuSysKnowShowLegalProcesses.setVisible(enableZoneEditControls);
    menuSysKnowShowZoneTreatments.setVisible(enableZoneEditControls);
    menuSysKnowImportZoneTreatments.setVisible(enableZoneEditControls);
    menuFileSaveZone.setVisible(enableZoneEditControls);

    buildSimpplleTypeFiles.setVisible(JSimpplle.developerMode());
    buildSimpplleTypesSource.setVisible(JSimpplle.developerMode());

    boolean isColorado = (Simpplle.getCurrentZone() instanceof ColoradoFrontRange);
    menuSysKnowWindthrow.setVisible(isColorado);
    menuSysKnowWindthrow.setEnabled(isColorado);
    menuSysKnowWildlifeBrowsing.setVisible(isColorado);
    menuSysKnowWildlifeBrowsing.setEnabled(isColorado);
    menuSysKnowTussockMoth.setEnabled(isColorado);
    menuSysKnowTussockMoth.setVisible(isColorado);


    boolean isColoradoPlateau = (Simpplle.getCurrentZone() instanceof ColoradoPlateau);
    menuSysKnowWindthrow.setVisible(isColoradoPlateau);
    menuSysKnowWindthrow.setEnabled(isColoradoPlateau);
    menuSysKnowWildlifeBrowsing.setVisible(isColoradoPlateau);
    menuSysKnowWildlifeBrowsing.setEnabled(isColoradoPlateau);

    menuWeatherEventClassA.setEnabled(!(isColoradoPlateau));
    menuWeatherEventClassA.setVisible(!(isColoradoPlateau));

    boolean isWyoming = (RegionalZone.isWyoming());
    menuBisonGrazingLogic.setEnabled(isWyoming);
    menuBisonGrazingLogic.setVisible(isWyoming);
    menuSysKnowProcessProbLogic.setVisible(!isWyoming);

    menuSysKnowDoCompetition.setEnabled(true);
    menuSysKnowGapProcessLogic.setEnabled(true);
    menuSysKnowProducingSeedLogic.setEnabled(true);
    menuSysKnowVegUnitFireTypeLogic.setEnabled(true);

    menuSysKnowInvasive.setEnabled(true);
    menuSysKnowInvasiveLogicR1.setEnabled(false);
    menuSysKnowInvasiveLogicMesaVerdeNP.setEnabled(true);
    menuSysKnowInvasiveLogicMSU.setEnabled(true);

  }

  // Disabling of zone controls not necessary.
  // One currently cannot unload the current zone.
  public void setWaitState(String msg) {
    setCursor(Utility.getWaitCursor());
    setStatusMessage(msg);
  }

  /**
   * Sets the cursor to normal cursor location.
   */
  public void setNormalState() {
    Simpplle.clearStatusMessage();
    setCursor(Utility.getNormalCursor());
  }

  /**
   * Sets the dialog location in relation to the Simpplle Main frame
   *
   * @param dlg dialog whose location will be set.
   */
  public void setDialogLocation(JDialog dlg) {
    Dimension dlgSize = dlg.getPreferredSize();
    Dimension frmSize = getSize();
    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    Point loc = getLocation();
    int screen_height = dim.height;
    int screen_width = dim.width;

    int x, y;
    x = (frmSize.width - dlgSize.width) / 2 + loc.x;
    y = (loc.y + frmSize.height) - statusBar.getHeight() - dlgSize.height - 5;
    if (x < 0) {
      x = 0;
    }
    if (y < 0) {
      y = 0;
    }

    // If dialog appears off screen, set to the center
    if (x > screen_width || y > screen_height) {
      x = screen_width / 2;
      y = screen_height / 2;
    }
    dlg.setLocation(x, y);
  }

  // **************
  // Event Handlers
  // **************

  /**
   * File Exit action performed handler.  will write the properties file.  and close the hibernate connection.
   */
  private void fileExit_actionPerformed(ActionEvent e) {
    try {
      JSimpplle.getProperties().write();
    } catch (IOException ex) {
      ex.printStackTrace();
    }

    try {
      simpplle.comcode.DatabaseCreator.closeHibernate();
    } catch (SimpplleError ex) {
    }
    System.exit(0);
  }

  //Overridden so we can exit on System Close
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      fileExit_actionPerformed(null);
    }
  }

  /**
   * Displays the 'about OpenSimpplle' dialog
   *
   * @param e
   */
  private void menuHelpAbout_actionPerformed(ActionEvent e) {
    SimpplleMain_AboutBox dlg = new SimpplleMain_AboutBox(this);
    setDialogLocation(dlg);
    dlg.setModal(true);
    dlg.setVisible(true);
  }

  /**
   * Allows the user to select a zone, then enables zone controls, and disables
   * area controls.
   */
  private void loadZone_actionPerformed(ActionEvent e) {
    String str;
    simpplle.comcode.RegionalZone zone;

    NewZoneDialog dlg = new NewZoneDialog(this, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    if (Simpplle.getCurrentZone() != null && dlg.isNewZone()) {
      enableZoneControls();
      disableAreaControls();
      zone = Simpplle.getCurrentZone();
      if (zone.isHistoric()) {
        str = zone.getName() + " (Historic Pathways)";
      } else {
        str = zone.getName();
      }
      zoneValueLabel.setText(str);
      areaValueLabel.setText("");
    }
    refresh();
  }

  /**
   * Gets the area to be used depending on user input.  Choices are Sample, user defined,
   * previous, previous old, or none (if nothing selected)
   */
  private void loadArea_actionPerformed(ActionEvent e) {

    FileFilter areaFilter = new FileNameExtensionFilter("OpenSIMPPLLE Area Files (*.area, *.zip)", "area", "zip");
    FileFilter simFilter = new FileNameExtensionFilter("OpenSIMPPLLE Simulation Files (*.simdata)", "simdata");

    AreaChooser areaChooser = new AreaChooser(this);
    setDialogLocation(areaChooser);
    AreaType type = areaChooser.select();

    try {
      if (type == null) {
        setNormalState();
        return;
      }

      switch (type) {

        case SAMPLE:
          SampleAreaChooser sampleChooser = new SampleAreaChooser(this);
          sampleChooser.setLocationRelativeTo(this);
          sampleChooser.select();
          break;

        case SIMULATED:
          JFileChooser simAreaChooser = new JFileChooser(JSimpplle.getWorkingDir());
          simAreaChooser.setAcceptAllFileFilterUsed(false);
          simAreaChooser.setDialogTitle("Select an Area File");
          simAreaChooser.setFileFilter(areaFilter);

          int result = simAreaChooser.showOpenDialog(this);
          if (result == JFileChooser.APPROVE_OPTION) {
            File file = simAreaChooser.getSelectedFile();
            setWaitState("Loading User Defined Area...");
            setStatusMessage("Loading Area ...");
            comcode.loadUserArea(file);
            Simpplle.clearStatusMessage();
          } else {
            break;
          }

          JFileChooser simChooser = new JFileChooser(JSimpplle.getWorkingDir());
          simChooser.setAcceptAllFileFilterUsed(false);
          simChooser.setDialogTitle("Select a Simulation File");
          simChooser.setFileFilter(simFilter);

          result = simChooser.showOpenDialog(this);
          if (result != JFileChooser.APPROVE_OPTION) {
            break;
          }

          int choice = JOptionPane.showConfirmDialog(
              this, "Would you like to re-create the multiple run summary? (Last run only)",
              "Recreate Summary", JOptionPane.YES_NO_OPTION
          );

          if (choice == JOptionPane.YES_OPTION) {
            Simpplle.setRecreateMrSummary(true);
          } else {
            Simpplle.setRecreateMrSummary(false);
          }

          File simFile = simChooser.getSelectedFile();
          setWaitState("Loading Simulation...");
          comcode.loadSimulation(simFile);

          break;

        case USER:
          JFileChooser userChooser = new JFileChooser(JSimpplle.getWorkingDir());
          userChooser.setAcceptAllFileFilterUsed(false);
          userChooser.setDialogTitle("Select an Area File");
          userChooser.setFileFilter(areaFilter);

          int result2 = userChooser.showOpenDialog(this);
          if (result2 == JFileChooser.APPROVE_OPTION) {
            File file = userChooser.getSelectedFile();
            if (!file.getName().endsWith(".zip")) {
              JOptionPane.showMessageDialog(
                  this,
                  "This file format is deprecated and may be removed in a\n"
                      + "future release. Please save the area using the new format\n"
                      + "by selecting \"Save Area\" from the file menu.",
                  "Deprecation Warning",
                  JOptionPane.WARNING_MESSAGE);
            }
            setWaitState("Loading User Defined Area...");
            setStatusMessage("Loading Area ...");
            comcode.loadUserArea(file);
            Simpplle.clearStatusMessage();
          }
          break;
      }
    } catch (SimpplleError err) {
      err.printStackTrace();
      JOptionPane.showMessageDialog(this, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    } finally {
      setNormalState();
    }

    Area area = Simpplle.getCurrentArea();
    if (area != null) {
      if ((type == AreaType.SAMPLE) || (type == AreaType.USER)) {
        doInvalidAreaCheck();
      }

      FireEvent.setUseRegenPulse(area.getName().equalsIgnoreCase("cheesman"));
      menuSysKnowUseRegenPulse.setSelected(FireEvent.useRegenPulse());

      enableAreaControls();
      if (Simpplle.getCurrentSimulation() != null) {
        disableSimulationControls();
        enableSimulationControls();
      } else {
        disableSimulationControls();
      }

      areaValueLabel.setText(area.getName());
      area.setMultipleLifeformStatus();
      updateSpreadModels(area.hasKeaneAttributes());
      if (!Area.hasMultipleLifeforms() &&
          type != AreaType.SIMULATED &&
          type != AreaType.USER) {
        // Need to change Evu's to be single lifeform
        Evu[] evus = area.getAllEvu();
        for (Evu evu : evus) {
          if (evu != null) {
            evu.makeSingleLife();
          }
        }
      }
      TrackingSpeciesReportData.getInstance().initialize();

      FireSuppWeatherData.setMaxToAreaAcres();
    } else {
      disableAreaControls();
      disableSimulationControls();
    }
    refresh();
  }

  /**
   * Handles the action event when user chooses to run simulation.
   *
   * @param e
   */
  private void runSimulation_actionPerformed(ActionEvent e) {

    SimParam dlg = new SimParam(this, fireSpreadModels);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void actionMenuSettings(ActionEvent event) {
    PreferencesDialog dlg = new PreferencesDialog(this);
    setDialogLocation(dlg);
    dlg.setVisible(true);

    refresh();
  }

  /**
   * Resizes a component.
   *
   * @param e
   */
  private void this_componentResized(ComponentEvent e) {
    Dimension d = getSize();

    if (d.width < MIN_WIDTH && d.height < MIN_HEIGHT) {
      setSize(MIN_WIDTH, MIN_HEIGHT);
    } else if (d.width < MIN_WIDTH) {
      setSize(MIN_WIDTH, d.height);
    } else if (d.height < MIN_HEIGHT) {
      setSize(d.width, MIN_HEIGHT);
    }
  }

  /**
   * Used to restore the units to initial conditions.
   * It is used to restore the units to their original state, as well as deleting any simulation related stuff.
   * This is no longer used before a simulation, the reseting of things is done as needed when the simulation is run. The name resetSimulation is kept primary for consistency with prior versions of this software.
   */
  private void menuUtilityReset_actionPerformed(ActionEvent e) {
    Simpplle.resetSimulation();
    disableSimulationControls();
    JOptionPane.showMessageDialog(this, "Reset Complete", "Reset Complete",
        JOptionPane.INFORMATION_MESSAGE);
  }

  private void menuReportsSummary_actionPerformed(ActionEvent e) {
    ReportOption dlg = new ReportOption(this, "Choose an Option", true, false);

    setDialogLocation(dlg);
    dlg.setVisible(true);

    int selection = dlg.getSelection();
    if (selection == ReportOption.NO_SELECTION) {
      return;
    }

    File outfile;

    outfile = Utility.getSaveFile(this, "Summary Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
        comcode.summaryReport(outfile, selection, dlg.isCombineLifeforms());
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuReportsUnit_actionPerformed(ActionEvent e) {
    File outfile;
    Area area = Simpplle.getCurrentArea();

    outfile = Utility.getSaveFile(this, "Individual Unit Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
        area.printIndividualSummary(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }

  }

  private void menuReportsAllStates_actionPerformed(ActionEvent e) {
    File rulesFile = null;
    if (Simulation.getInstance().isDiscardData() == false) {

      String msg =
          "Do you wish to load a file with rules to customize this report?";
      int choice = JOptionPane.showConfirmDialog(this, msg,
          "Load Customization File",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE);

      rulesFile = null;

      if (choice == JOptionPane.YES_OPTION) {
        rulesFile = Utility.getOpenFile(this, "All States Customization file");
      }
    }


    File outfile = Utility.getSaveFile(this, "All States Report Output");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
        if (Simulation.getInstance().isDoAllStatesSummary() == false) {
          Simulation.getInstance().doAllStatesSummaryAllTimeSteps(rulesFile);
        }
        if (rulesFile != null) {
          Reports.generateAllStatesReport(rulesFile, outfile);
        } else {
          Reports.generateAllStatesReport(outfile);
        }
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }

  }

  /**
   * Handles the event when tracking species menu item is selected.  It creates a new Tracking Species report dialog
   *
   * @param e
   */
  private void menuReportsTrackingSpecies_actionPerformed(ActionEvent e) {
    TrackingSpeciesReportDlg dlg = new TrackingSpeciesReportDlg(this, "Tracking Species Report Categories", true);
    dlg.setVisible(true);

    TrackingSpeciesReportData data = TrackingSpeciesReportData.getInstance();
    if (data == null || data.hasData() == false) {
      return;
    }

    File outfile = Utility.getSaveFile(this, "Tracking Species Report Output");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
//        if (Simulation.getInstance().isDoTrackingSpeciesReport() == false) {
        Simulation.getInstance().doTrackingSpeciesReportAllTimeSteps();
//        }
        Reports.generateTrackingSpeciesReport(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }

  }

  private void menuReportsMultNormal_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Multiple Run Summary Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      comcode.multipleRunSummaryReport(outfile);
      setNormalState();
    }
  }

  private void menuReportsMultSpecial_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Multiple Run Summary Report By Special Area");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      comcode.saMultipleRunSummaryReport(outfile);
      setNormalState();
    }
  }

  private void menuReportsMultOwner_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Multiple Run Summary Report By Ownership");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      comcode.ownershipMultipleRunSummaryReport(outfile);
      setNormalState();
    }
  }

  private void menuReportsFire_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Detailed Fire Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      comcode.fireSpreadReport(outfile);
      setNormalState();
    }
  }

  private void menuReportsFireCost_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Fire Suppression Cost Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      comcode.fireSuppressionCostReport(outfile);
      setNormalState();
    }
  }

  private void menuReportsEmission_actionPerformed(ActionEvent e) {
    ReportOption dlg = new ReportOption(this, "Choose an Option", true);

    setDialogLocation(dlg);
    dlg.setVisible(true);

    int selection = dlg.getSelection();
    if (selection == ReportOption.NO_SELECTION) {
      return;
    }

    File outfile = Utility.getSaveFile(this, "Emissions Report");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
        if (selection == Simpplle.FORMATTED) {
          comcode.emissionsReport(outfile);
        } else if (selection == Simpplle.CDF) {
          comcode.emissionsReportCDF(outfile);
        } else {
          setNormalState();
          return;
        }
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Failure",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuGisUpdateSpread_actionPerformed(ActionEvent e) {
    Lifeform lifeform = null;
    if (Area.hasMultipleLifeforms()) {
      LifeformTypeChooser dlg = new LifeformTypeChooser(this, "Chooser Lifeform", true);
      dlg.setVisible(true);

      lifeform = dlg.getChosenLife();
      if (dlg.okPushed() == false) {
        return;
      }
    }


    File outfile = null;


    JFileChooser chooser = new JFileChooser(JSimpplle.getWorkingDir());
    chooser.setDialogTitle("GIS Files prefix?");
    if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      outfile = chooser.getSelectedFile();
    }

    if (outfile != null) {
      setWaitState("Generating GIS Update & Spread Files ...");
      try {
        comcode.createGisUpdateSpreadFiles(outfile, lifeform);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Failure", JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuExportGISDecadeProb_actionPerformed(ActionEvent e) {
    File outfile = null;

    JFileChooser chooser = new JFileChooser(JSimpplle.getWorkingDir());
    chooser.setDialogTitle("GIS Decade Probability Files prefix?");
    if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      outfile = chooser.getSelectedFile();
    }

    if (outfile != null) {
      setWaitState("Generating GIS Decade Probability Files ...");
      try {
        comcode.createGisDecadeProbabilityFiles(outfile);

      } catch (SimpplleError ex) {
        JOptionPane.showMessageDialog(this, ex.getMessage(), "Failure", JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  /**
   * Re-burn function is a hoped for future improvement in OpenSimpplle.
   *
   * @param e
   */
  private void menuExportGISReburn_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Probability of Reburn File prefix?");
    if (outfile != null) {
      setWaitState("Generating GIS Reburn Probability File ...");
      try {
        Simpplle.getCurrentArea().produceReburnProbabilityFile(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error writing file",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuExportAttributes_actionPerformed(ActionEvent e) {
    String msg = "This will export two files, *.spatialrelate and *.attributesall.\n" +
        "These files are needed to create an area using the import function.\n" +
        "In the following dialog please provide a filename prefix.\n\n" +
        "**Existing files will be overwritten.**";
    JOptionPane.showMessageDialog(this, msg, "Information", JOptionPane.INFORMATION_MESSAGE);


    File outfile;
    MyFileFilter extFilter = new MyFileFilter("spatialrelate",
        "SIMPPLLE Area Creation Files (*.spatialrelate)");

    outfile = Utility.getSaveFile(this, "Export Area Creation files.", extFilter);
    if (outfile != null) {
      setWaitState("Exporting Area Creation Files ...");
      try {
        Simpplle.getCurrentArea().exportCreationFiles(outfile);

        // Error was not generated, provide feedback for success
        JOptionPane.showMessageDialog(this, "Export area creation files complete.",
            "Export Finished", JOptionPane.INFORMATION_MESSAGE);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Error writing file",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuUtilityPrintArea_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Print Current Area to File");
    if (outfile != null) {
      setWaitState("Printing Area to File ...");
      try {
        comcode.printCurrentArea(outfile);
      } catch (SimpplleError ex) {
        JOptionPane.showMessageDialog(this, ex.getMessage(), "",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuResultVegUnit_actionPerformed(ActionEvent e) {
    if (EvuAnalysis.isOpen()) {
      return;
    }

    EvuAnalysis dlg = new EvuAnalysis(this, "Vegetative Unit Analysis", false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuResultVegSum_actionPerformed(ActionEvent e) {
    VegSummary dlg = new VegSummary(this, "Vegetative Condition Summary", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuFileSave_actionPerformed(ActionEvent e) {

    JFileChooser chooser = new JFileChooser(JSimpplle.getWorkingDir());
    chooser.setAcceptAllFileFilterUsed(false);
    chooser.setDialogTitle("Save Current Area");
    chooser.setFileFilter(new MyFileFilter("zip", "OpenSIMPPLLE Area (*.zip)"));
    int result = chooser.showSaveDialog(this);
    if (result != JFileChooser.APPROVE_OPTION) {
      return;
    }

    File file = chooser.getSelectedFile();
    String path = file.getAbsolutePath();
    if (path.endsWith(".area")) {
      path = path.substring(0, path.lastIndexOf('.'));
    }

    try {
      setWaitState("Saving Current Area ...");
      if (!path.toLowerCase().endsWith(".zip")) {
        path += ".zip";
      }
      comcode.saveCurrentArea(new File(path));
    } catch (SimpplleError error) {
      JOptionPane.showMessageDialog(this, error.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }

    if (Simpplle.getCurrentSimulation() != null) {
      try {
        Simpplle.setStatusMessage("Saving Simulation...");
        comcode.saveSimulation(new File(path + "-1.simdata"), 1);
      } catch (SimpplleError error) {
        JOptionPane.showMessageDialog(this, error.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    }

    JOptionPane.showMessageDialog(this, "Successfully saved area", "Success", JOptionPane.INFORMATION_MESSAGE);

    setNormalState();

  }

  private void menuSysKnowFireEventLogic_actionPerformed(ActionEvent e) {
    FireEventLogicDialog dlg = new FireEventLogicDialog(this, "Fire Spread, Type, and Spotting", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowFireProb_actionPerformed(ActionEvent e) {
    FireSpreadProb dlg = new FireSpreadProb(this, "Extreme Fire Spread Probability", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowFireSuppLogicBeyondClassA_actionPerformed(ActionEvent e) {
    FireSuppBeyondClassALogicDlg dlg = new FireSuppBeyondClassALogicDlg(this, "Fire Suppression Logic for Fires Beyond Class A", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowFireSuppLogicClassA_actionPerformed(ActionEvent e) {
    FireSuppClassALogicDlg dlg = new FireSuppClassALogicDlg(this, "Fire Suppression for Class A Fires Logic", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuWeatherEventClassA_actionPerformed(ActionEvent e) {
    String title = "Weather Ending Events -- Fires < 0.25 acres";
    FireSuppWeatherClassALogicDlg dlg = new FireSuppWeatherClassALogicDlg(this, title, false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuWeatherEventNotClassA_actionPerformed(ActionEvent e) {
    String title = "Weather Ending Events -- Fires > 0.25 acres";
    FireSuppWeather dlg = new FireSuppWeather(this, title, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowVegTreatSchedule_actionPerformed(ActionEvent e) {
    TreatmentSchedule dlg;
    dlg = new TreatmentSchedule(this, "Treatment Schedule", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowVegTreatLogic_actionPerformed(ActionEvent e) {
    if (!arePathwaysLoaded()) {
      return;
    }

    TreatmentLogic dlg;

    dlg = new TreatmentLogic(this, "Treatment Logic", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }


  private void menuSysKnowRegionalClimate_actionPerformed(ActionEvent e) {
    ClimateDialogWrapper dlg;
    dlg = new ClimateDialogWrapper(this, "Regional Climate Schedule", true);
    setDialogLocation(dlg.getDialog());
    dlg.getDialog().setVisible(true);
    refresh();
  }

  private void menuSysKnowVegProcLock_actionPerformed(ActionEvent e) {
    ProcessSchedule dlg = new ProcessSchedule(this, "Process Schedule", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  public boolean isVegPathwayDlgOpen() {
    return vegPathwayDlgOpen;
  }

  private void setVegPathwayDlgOpen() {
    menuSysKnowPathVeg.setEnabled(false);
    vegPathwayDlgOpen = true;
  }

  public void setVegPathwayDlgClosed() {
    menuSysKnowPathVeg.setEnabled(true);
    vegPathwayDlgOpen = false;
  }

  public boolean isAquaticPathwayDlgOpen() {
    return aquaticPathwayDlgOpen;
  }

  private void setAquaticPathwayDlgOpen() {
    menuSysKnowPathAquatic.setEnabled(false);
    aquaticPathwayDlgOpen = true;
  }

  public void setAquaticPathwayDlgClosed() {
    menuSysKnowPathAquatic.setEnabled(true);
    aquaticPathwayDlgOpen = false;
  }

  private void menuSysKnowPathVeg_actionPerformed(ActionEvent e) {
    if (isVegPathwayDlgOpen()) {
      return;
    }

    Pathway dlg = new Pathway(this, false);
    setDialogLocation(dlg);
    setVegPathwayDlgOpen();
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowPathAquatic_actionPerformed(ActionEvent e) {
    if (isAquaticPathwayDlgOpen()) {
      return;
    }

    AquaticPathway dlg = new AquaticPathway(this, "Aquatic Pathways", false);
    setDialogLocation(dlg);
    setAquaticPathwayDlgOpen();
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowFireSeason_actionPerformed(ActionEvent e) {
    FireSeason dlg = new FireSeason(this, "Fire Season Probabilities", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuSysKnowCellPerc_actionPerformed(ActionEvent e) {
    KeaneCellPercolation dlg = new KeaneCellPercolation(this);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private boolean deleteSimulationCheck() {
    String msg =
      "An area is loaded that has simulation data.\n" +
      "If unit data is made invalid by loading of one or more knowledge files " +
      "then the state will be marked as invalid, and a result the area as well.\n" +
      "** In addition all simulation data will be removed from memory. **\n\n" +
      "Do you wish to continue?\n";

    int choice = JOptionPane.showConfirmDialog(this,msg,
                                           "Warning: Simulation Data exists.",
                                           JOptionPane.YES_NO_OPTION,
                                           JOptionPane.WARNING_MESSAGE);


    return (choice == JOptionPane.NO_OPTION || choice == JOptionPane.CLOSED_OPTION);
  }

  private void doInvalidAreaCheck() {
    Area area = Simpplle.getCurrentArea();

    if (area.hasInvalidVegetationUnits()) {
      String msg =
          "Invalid data was found in the units after loading\n" +
              "In addition any simulation data that may have existed has\n" +
              "been erased from memory\n" +
              "The area can be made valid again by either running the Unit Editor\n" +
              "found under the Utilities menu of the main application window, or\n" +
              "by loading a system knowledge file that contains the missing data\n";

      JOptionPane.showMessageDialog(this, msg, "Invalid units found",
          JOptionPane.INFORMATION_MESSAGE);
      markAreaInvalid();
    } else {
      markAreaValid();
    }
  }

  private void menuSysKnowOpen_actionPerformed(ActionEvent e) {
    Area area = Simpplle.getCurrentArea();

    if (area != null && Simpplle.getCurrentSimulation() != null) {
      if (deleteSimulationCheck()) {
        return;
      }
    }

    SystemKnowledgeLoadSave dlg = new  SystemKnowledgeLoadSave(this, "2Load User Knowledge", true, false);
    setDialogLocation(dlg);
    File file = dlg.selectFile();
    SystemKnowledge.loadedFile = file;
    if (file != null) {
      dlg.setVisible(true);
    } else {
      return;
    }

    // Update the units and check for invalid ones.
    if (area != null && dlg.isDialogCanceled() == false) {
      area.updatePathwayData();
      doInvalidAreaCheck();

      boolean changed = area.updateFmzData();
      if (changed) {
        String msg = "Fmz's in the currently loaded area that\n" +
            "referred to fmz's not currently loaded\n" +
            "were changed to the default fmz.\n\n" +
            "If this is not desired load the correct\n" +
            "fmz data file for the area and then\n" +
            "reload the current area.";
        JOptionPane.showMessageDialog(this, msg, "Warning",
            JOptionPane.WARNING_MESSAGE);
      }
    }
  }

  private void menuSysKnowSave_actionPerformed(ActionEvent e) {
    SystemKnowledgeLoadSave dlg = new SystemKnowledgeLoadSave(this, "Save User Knowledge", true, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);

  }

  private void menuSysKnowRestoreDefaults_actionPerformed(ActionEvent e) {
    Area area = Simpplle.getCurrentArea();

    if (area != null && Simpplle.getCurrentSimulation() != null) {
      if (deleteSimulationCheck()) {
        return;
      }
    }

    String msg =
        "This option will restore system knowledge to the defaults.\n" +
            "This has essentially the same effect as reloading the current zone.\n" +
            "\nDo you wish to continue?";

    int choice = JOptionPane.showConfirmDialog(this, msg,
        "Restore System Knowledge Defaults",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);

    if (choice == JOptionPane.NO_OPTION || choice == JOptionPane.CLOSED_OPTION) { return; }

    try {

      RegionalZone zone = Simpplle.getCurrentZone();
      zone.loadKnowledge();
      FireEvent.resetExtremeData();

    } catch (SimpplleError err) {
      JOptionPane.showMessageDialog(this, err.getMessage(),
          "Could not Restore defaults",
          JOptionPane.ERROR_MESSAGE);
    }

    // Update the units and check for invalid ones.
    if (area != null) {
      area.updatePathwayData();
      doInvalidAreaCheck();

      boolean changed = area.updateFmzData();
      if (changed) {
        msg = "Fmz's in the currently loaded area that\n" +
            "referred to fmz's not currently loaded\n" +
            "were changed to the default fmz.\n\n" +
            "If this is not desired load the correct\n" +
            "fmz data file for the area and then\n" +
            "reload the current area.";
        JOptionPane.showMessageDialog(this, msg, "Warning",
            JOptionPane.WARNING_MESSAGE);
      }
    }

  }

  private void menuImportCreate_actionPerformed(ActionEvent e) {

    // Display a warning if an existing area will be replaced
    if (Simpplle.getCurrentArea() != null) {
      int option = JOptionPane.showConfirmDialog(
          this,
          "The currently loaded area will be replaced.\n\nDo you wish to continue?",
          "Replace Current Area",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE
      );
      if (option == JOptionPane.NO_OPTION || option == JOptionPane.CLOSED_OPTION) {
        return;
      }
    }

    // Choose the spatial relations file
    FileFilter spatialExt = new FileNameExtensionFilter(
        "SIMPPLLE Spatial Relations (*.spatialrelate)",
        "spatialrelate"
    );

    JFileChooser spatialChooser = new JFileChooser(JSimpplle.getWorkingDir());
    spatialChooser.setAcceptAllFileFilterUsed(false);
    spatialChooser.addChoosableFileFilter(spatialExt);
    spatialChooser.setFileFilter(spatialExt);
    spatialChooser.setDialogTitle("Choose a Spatial Relations File");

    int option = spatialChooser.showOpenDialog(this);
    if (option == JFileChooser.CANCEL_OPTION) {
      return;
    }

    // Choose the unit attributes file
    FileFilter attribExt = new FileNameExtensionFilter(
        "SIMPPLLE Unit Attributes (*.attributesall)",
        "attributesall"
    );

    JFileChooser attribChooser = new JFileChooser(spatialChooser.getCurrentDirectory());
    attribChooser.setAcceptAllFileFilterUsed(false);
    attribChooser.addChoosableFileFilter(attribExt);
    attribChooser.setFileFilter(attribExt);
    attribChooser.setDialogTitle("Choose a Unit Attributes File");

    option = attribChooser.showOpenDialog(this);
    if (option == JFileChooser.CANCEL_OPTION) {
      return;
    }

    // Clear the current area and simulation
    comcode.removeCurrentArea();
    Simpplle.resetSimulation();

    // Update the user interface to reflect no area being loaded
    disableAreaControls();
    disableSimulationControls();
    areaValueLabel.setText("");
    Simpplle.setStatusMessage("Creating New Area ...");

    // Create the area
    Area area = new Area(AreaType.USER);

    // Load the spatial relations first. The order is important.
    File spatialFile = spatialChooser.getSelectedFile();
    File spatialLog = new File(spatialFile.getAbsolutePath() + ".log");
    SpatialRelateLoader loader = new SpatialRelateLoader(spatialFile, spatialLog);
    try {
      loader.load(area);
    } catch (IOException | ParseError | SimpplleError ex) {
      ex.printStackTrace();
      return;
    }

    // Load the unit attributes last.
    File attribFile = attribChooser.getSelectedFile();
    File attribLog = new File(attribFile.getAbsolutePath() + ".log");
    AttributesAllLoader attrLoader = new AttributesAllLoader(attribFile, attribLog);
    try {
      attrLoader.load(area);
    } catch (IOException | ParseError | SimpplleError ex) {
      ex.printStackTrace();
      return;
    }

    // If we made it this far, then the area loaded correctly. Set it as the new current area.
    Simpplle.setCurrentArea(area);
    Simpplle.clearStatusMessage();

    // Define the elevation relative to the position
    area.setElevationRelativePositionDefault();
    ElevationRelativePosition dlg = new ElevationRelativePosition(
        simpplle.JSimpplle.getSimpplleMain(),
        "Elevation Relative Position",
        true,
        area
    );
    dlg.setVisible(true);
    int elevRelativePos = dlg.getValue();

    // Complete initialization of the area
    area.initPolygonWidth();
    area.calcRelativeSlopes();
    area.setElevationRelativePosition(elevRelativePos);
    area.setMultipleLifeformStatus();
    String fileName = attribFile.getName();
    String areaName = fileName.substring(0, fileName.lastIndexOf("."));
    area.setName(areaName);
    areaValueLabel.setText(areaName);
    updateSpreadModels(area.hasKeaneAttributes());

    // Display a warning if the area contains values that aren't in the zone
    if (area.hasInvalidVegetationUnits()) {
      JOptionPane.showMessageDialog(
          this,
          "One or more of the units had invalid attribute data.\n"
              + "It was most likely caused by one of the following:\n"
              + "  -- an invalid habitat type group\n"
              + "  -- an invalid species, size class, or density\n"
              + "  -- other invalid attribute data\n\n"
              + "Please look at the log file for details.\n\n"
              + "The import menu has three options that will help\n"
              + "to fix problems.\n"
              + "  1. Fix Incorrect States\n"
              + "       Attempt to fix incorrect states.\n"
              + "  2. Edit Units\n"
              + "       Allows editing of unit attribute data.\n"
              + "  3. Import Attribute Data\n"
              + "       Add data to existing units.\n\n"
              + "Please note save of the area will not be allowed\n"
              + "until all errors have been fixed.\n",
          "Area Import Partially Succeeded",
          JOptionPane.WARNING_MESSAGE
      );
      area.fixEmptyDataUnits();
      markAreaInvalid();
    } else {
      JOptionPane.showMessageDialog(
          this,
          "Creation of the area was successful.",
          "Area Import Succeeded",
          JOptionPane.INFORMATION_MESSAGE
      );
      markAreaValid();
    }

    refresh();

  }

  /**
   * Marks an area invalid and allows users to import fix states, edit units, or
   * print invalid report.
   */
  public void markAreaInvalid() {
    areaInvalidLabel.setText("(invalid)");
    disableAreaControls();
    disableSimulationControls();
    menuImportFixStates.setEnabled(true);
    menuImportInvalidReport.setEnabled(true);
    menuUtilityUnitEditor.setEnabled(true);
  }

  /**
   * Marks the area as valid, then enables area controls.
   *
   * @see #markAreaInvalid()
   */
  public void markAreaValid() {
    areaInvalidLabel.setText("");
    enableAreaControls();
    menuImportFixStates.setEnabled(false);
    menuImportInvalidReport.setEnabled(false);
  }

  private void menuImportAttributeData_actionPerformed(ActionEvent e) {
    String msg;
    File inputFile;
    MyFileFilter extFilter;
    Area area = Simpplle.getCurrentArea();

    extFilter = new MyFileFilter("attributes",
        "SIMPPLLE Atrribute Files (*.attributes)");

    inputFile = Utility.getOpenFile(this, "Attributes Import File?", extFilter);
    if (inputFile == null) {
      refresh();
      return;
    }

    setWaitState("Adding attribute Data ...");
    try {
      comcode.importAttributeData(inputFile);
    } catch (SimpplleError err) {
      JOptionPane.showMessageDialog(this, err.getError(), "Import Failed",
          JOptionPane.ERROR_MESSAGE);
    }
    setNormalState();

    if (area.hasInvalidVegetationUnits()) {
      msg = "Adding Attributes was only partially successful.\n" +
          "One or more of the units had invalid attribute data.\n" +
          "It was most likely caused by one of the following:\n" +
          "  -- an invalid habitat type group\n" +
          "  -- an invalid species, size class, or density\n" +
          "  -- other invalid attribute data\n\n" +
          "Please look at the following log file for details:\n\n" +
          "The import menu has two options that will help " +
          "to fix problems.\n" +
          "  1. Fix Incorrect States\n" +
          "       Attempt to fix incorrect states.\n" +
          "  2. Edit Units\n" +
          "       Allows editing of unit attribute data.\n\n" +
          "Please note save of the area will not be allowed\n" +
          "Until \"ALL\" Errors have been fixed.\n";
      JOptionPane.showMessageDialog(this, msg, "Partial Success",
          JOptionPane.WARNING_MESSAGE);
      menuImportFixStates.setEnabled(true);
      menuImportInvalidReport.setEnabled(true);
    }
    // Import was successful.
    else {
      msg =
          "Adding Attritues was successful.\n" +
              "Please give the area a name using the \"Change Area Name\"" +
              " function under the utility menu.\n\n" +
              "*** Do not to forget to save the area (File Menu) ***";
      JOptionPane.showMessageDialog(this, msg, "Success",
          JOptionPane.INFORMATION_MESSAGE);
      markAreaValid();
      areaInvalidLabel.setText("");
      enableAreaControls();
      disableSimulationControls();

      menuImportFixStates.setEnabled(false);
      menuImportInvalidReport.setEnabled(false);
    }
  }

  private void menuImportFixStates_actionPerformed(ActionEvent e) {
    RegionalZone zone = Simpplle.getCurrentZone();
    Area area = Simpplle.getCurrentArea();
    int zoneId = zone.getId();

    if (zoneId != ValidZones.EASTSIDE_REGION_ONE) {
      JOptionPane.showMessageDialog(this, "Not available for the zone yet.",
          "Not Yet Implemented",
          JOptionPane.INFORMATION_MESSAGE);
      return;
    } else {
      setWaitState("Attempting to fix incorrect states ...");
      Simpplle.getCurrentArea().fixIncorrectStates();
      setNormalState();
    }
    updateAreaValidity();

    if (area.hasInvalidVegetationUnits()) {
      String msg =
          "Fix of incorrect states was only partially successful.\n" +
              "Remaining errors can be fixed by using the \"Edit Units\"\n" +
              "menu item under the Import Menu, or by using the \"Unit Editor\"\n" +
              "menu item under the Utilities Menu";
      JOptionPane.showMessageDialog(this, msg, "Fixing of States Unsuccessful",
          JOptionPane.INFORMATION_MESSAGE);
    } else {
      String msg =
          "Fixing of incorrect states was completely successful.\n" +
              "Remember to name the new area by using \"Change Area Name\"\n" +
              "under the Utility Menu.  Then save the area using \"Save Area\"\n" +
              "under the File Menu.";
      JOptionPane.showMessageDialog(this, msg, "Fixing of States Successful",
          JOptionPane.INFORMATION_MESSAGE);
      markAreaValid();
      areaInvalidLabel.setText("");
      enableAreaControls();
      disableSimulationControls();

      menuImportFixStates.setEnabled(false);
      menuImportInvalidReport.setEnabled(false);
    }
  }

  private void menuImportInvalidReport_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Invalid Units Report File");
    if (outfile != null) {
      setWaitState("Generating Report ...");
      try {
        Simpplle.getCurrentArea().printInvalidUnits(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(),
            "Unable to write file",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuInterpretProbCalc_actionPerformed(ActionEvent e) {
    String title = "Attribute Probability Calculator";
    AttributeProbabilityCalculator dlg =
        new AttributeProbabilityCalculator(this, title, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuInterpretRestoration_actionPerformed(ActionEvent e) {
    RestorationReport dlg = new RestorationReport(this, "Ecosystem Restoration", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuInterpretWildlife_actionPerformed(ActionEvent e) {
    String title = "Wildlife Habitat Interpretations";
    WildlifeHabitat dlg = new WildlifeHabitat(this, title, false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuUtilitySimReady_actionPerformed(ActionEvent e) {
    String msg =
        "This will take the current state of units at the end of the simulation,\n" +
            "and make this state the new vegetative state for the unit.\n" +
            "In addition the simulation will be reset, thus removing all simulation data.\n\n" +
            "Do you wish to continue?";
    int choice = JOptionPane.showConfirmDialog(this, msg, "Make Area Simulation Ready",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    if (choice == JOptionPane.YES_OPTION) {
      disableSimulationControls();
      Simpplle.makeAreaSimulationReady();
      enableAreaControls();
    }
  }

  // Editor found in utility menu
  private void menuUtilityUnitEditor_actionPerformed(ActionEvent e) {
    EvuEditor dlg = new EvuEditor(this, "", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuUtilityAreaName_actionPerformed(ActionEvent e) {
    String msg = "New Area Name";
    String title = "New Area Name";
    String name = JOptionPane.showInputDialog(this, msg, title,
        JOptionPane.PLAIN_MESSAGE);
    if (name != null) {
      Simpplle.getCurrentArea().setName(name);
      String str = Simpplle.getCurrentArea().getName();
      areaValueLabel.setText(str);
    }
    refresh();
  }

  private void menuUtilitiesConsole_actionPerformed(ActionEvent e) {
    ConsoleMessages dlg = new ConsoleMessages(this, "Console Messages", false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuUtilityMemoryUse_actionPerformed(ActionEvent e) {
    MemoryDisplay frame = new MemoryDisplay();
    frame.setVisible(true);
  }

  private void menuUtilityGISFiles_actionPerformed(ActionEvent e) {
    CopyGis dlg = new CopyGis(this, "Copy GIS Files", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuUtilityDatabaseTest_actionPerformed(ActionEvent e) {

    try {
      File workDir = simpplle.JSimpplle.getWorkingDir();
      simpplle.comcode.DatabaseCreator.initHibernate(true, new File(workDir, "test"));
      Simpplle.getCurrentArea().writeSimulationDatabase();
      simpplle.comcode.DatabaseCreator.closeHibernate();
    } catch (SimpplleError ex) {
      ex.printStackTrace();
      System.out.println(ex.getMessage());
    }
  }

  private void menuUtilityDatabaseManager_actionPerformed(ActionEvent e) {
    MyFileFilter extFilter = new MyFileFilter("data",
        "Simpplle Simulation Database Files (*.data)");
    File outfile = Utility.getOpenFile(this, "User Defined Area File?", extFilter);
    if (outfile != null) {
      outfile = simpplle.comcode.Utility.stripExtension(outfile);
    }

    String url = "jdbc:hsqldb:file:" + outfile;
    DatabaseManagerSwing.main(new String[]{"-url", url});
  }


  private void menuSysKnowUseRegenPulse_actionPerformed(ActionEvent e) {
    FireEvent.setUseRegenPulse(menuSysKnowUseRegenPulse.isSelected());
  }

  private void menuSysKnowConiferEncroach_actionPerformed(ActionEvent e) {
    ConiferEncroachmentDialog dlg = new ConiferEncroachmentDialog(this, "Conifer Encroachment Logic", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }


  private void menuMagisProcessTreatmentFiles_actionPerformed(ActionEvent e) {
    File outfile;
    outfile = Utility.getSaveFile(this, "Magis Process/Treatment Files Prefix?");

    if (outfile != null) {
      setWaitState("Generating Magis Process and Treatment Files ...");
      try {
        Simpplle.getCurrentArea().magisProcessAndTreatmentFiles(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(), "Problem writing files",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuMagisAllVegStates_actionPerformed(ActionEvent e) {
    String msg = "File to Output Vegetative States to?";
    MyFileFilter extFilter = new MyFileFilter("txt",
        "Text Files (*.txt)");

    File outfile = Utility.getSaveFile(this, msg, extFilter);
    if (outfile != null) {
      try {
        setWaitState("Writing All Vegetative Types...");
        HabitatTypeGroup.magisAllVegTypes(outfile);
      } catch (SimpplleError err) {
        JOptionPane.showMessageDialog(this, err.getMessage(),
            "Problems generating file",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuUtilityDeleteUnits_actionPerformed(ActionEvent e) {
    RemoveUnits dlg = new RemoveUnits(this, "Delete Units", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuReportsFireSuppCostAll_actionPerformed(ActionEvent e) {
//    File         simFile, simPrefix, outfile;
//    String       str;
//    int          index;
//    PrintWriter  fout;
//    Simulation   simulation = new Simulation();
//    MyFileFilter  extFilter = new MyFileFilter("area",
//                                               "Simpplle Area Files (*.area)");
//
//    simFile = Utility.getOpenFile(this,"Select (Any) Simulation File?",extFilter);
//    if (simFile != null) {
//      try {
//        // Find the Simulation Prefix
//        str   = simFile.toString();
//        index = str.indexOf(".area");
//        if (Character.isDigit(str.charAt(index-1)) == false) {
//          throw new SimpplleError(simFile.toString() + " is not a valid saved simulation.");
//        }
//        while (str.charAt(index) != '-') { index--; }
//        simPrefix = new File(str.substring(0,index));
//
//        // Make output file name
//        outfile = simpplle.comcode.Utility.makeSuffixedPathname(simPrefix,"-ls-cost","txt");
//
//        // Read the Simulation Data
//        simulation.readAllSimulationFiles(simPrefix);
//
//        // Write out the Report
//        fout = new PrintWriter(new FileWriter(outfile));
//
//        Simpplle.setStatusMessage("Writing Fire Suppression Cost Report to: " + outfile.toString());
//        simulation.getMultipleRunSummary().asciiFireSuppressionCostSummaryReport(fout,simulation);
//
//        fout.flush();
//        fout.close();
//
//        Simpplle.clearStatusMessage();
//      }
//      catch (Exception err) {
//        JOptionPane.showMessageDialog(this,err.getMessage(),"Error",
//                                      JOptionPane.ERROR_MESSAGE);
//      }
//    }
  }

  private void menuSysKnowWSBWLogic_actionPerformed(ActionEvent e) {
    if (Simpplle.getCurrentZone() instanceof ColoradoFrontRange ||
        Simpplle.getCurrentZone() instanceof ColoradoPlateau) {
      String title = "Western Spruce Budworm";
      String text =
          "Needs to be Locked in through the 'Lock in Processes' Screen";

      JTextAreaDialog dlg = new JTextAreaDialog(this, title, true, text);
      setDialogLocation(dlg);
      dlg.setVisible(true);
      return;
    }

    if (JSimpplle.developerMode() == false) {
      JOptionPane.showMessageDialog(this, "Under Construction",
          "Under Construction",
          JOptionPane.INFORMATION_MESSAGE);
      return;
    }

    if (Simpplle.getCurrentZone().getUserProbProcesses() == null) {
      JOptionPane.showMessageDialog(this, "Nothing Defined for this zone",
          "No Processes Found",
          JOptionPane.INFORMATION_MESSAGE);
      return;
    }

    InsectDiseaseLogic dlg = new InsectDiseaseLogic(this);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private boolean arePathwaysLoaded() {
    Vector v = HabitatTypeGroup.getAllLoadedTypes();
    if (v == null || v.size() == 0) {
      String msg = "Please load some pathways first";
      JOptionPane.showMessageDialog(this, msg, "No Pathways Loaded",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  private void menuSysKnowRegen_actionPerformed(ActionEvent e) {
    RegenerationLogic.setDefaultEcoGroup(RegenerationLogic.FIRE);
    RegenerationLogic.setDefaultEcoGroup(RegenerationLogic.SUCCESSION);
    if (arePathwaysLoaded() == false) {
      return;
    }
    if (RegenerationLogic.isDataPresent(RegenerationLogic.FIRE) == false) {
      RegenerationLogic.makeBlankLogic(RegenerationLogic.FIRE);
    }
    if (RegenerationLogic.isDataPresent(RegenerationLogic.SUCCESSION) == false) {
      RegenerationLogic.makeBlankLogic(RegenerationLogic.SUCCESSION);
    }

    RegenerationLogicDialog dlg = new RegenerationLogicDialog(this);
    dlg.setLocationRelativeTo(this);
    dlg.setVisible(true);

  }


  private void menuResultLandformSum_actionPerformed(ActionEvent e) {

  }

  private void menuResultLandformUnit_actionPerformed(ActionEvent e) {
    UnitAnalysis dlg = new UnitAnalysis(this, "Unit Analysis", false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }


  private void menuSysKnowVegTreatDesired_actionPerformed(ActionEvent e) {

  }


  private void menuResultAquaticUnit_actionPerformed(ActionEvent e) {
    if (EauAnalysis.isOpen()) {
      return;
    }

    EauAnalysis dlg = new EauAnalysis(this, "Aquatic Unit Analysis", false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void menuResultAquaticSum_actionPerformed(ActionEvent e) {

  }

  private void menuHelpUserGuide_actionPerformed(ActionEvent e) {

    //Verify that OpenSimpplle Can access browser, if not create popup with link
    if (Desktop.isDesktopSupported()) {
      Desktop desktop = Desktop.getDesktop();
      URI link = URI.create(GUIDE_URL);
      try {
        desktop.browse(link);
      } catch (IOException noBrowse) {
        noBrowse.printStackTrace();
      }
    } else {
      //First msg is text, second msg is link in JTextField so it can be selected and copied
      JLabel msg1 = new JLabel("OpenSimpplle cannot access your browser. For the User's Guide, please go to: ");
      JTextField msg2 = new JTextField(GUIDE_URL);
      //Prevent accidentally deleting link
      msg2.setEditable(false);
      //Add one component in dialog, each part of the message within that component
      JPanel textPanel = new JPanel();
      textPanel.add(msg1);
      textPanel.add(msg2);
      JOptionPane.showMessageDialog(this, textPanel, "", JOptionPane.INFORMATION_MESSAGE);
    }
  }


  private void menuSysKnowFireSuppProdRate_actionPerformed(ActionEvent e) {
    String title = "Fire Suppression Line Production Rate Logic";
    FireSuppProductionRateLogicDlg dlg = new FireSuppProductionRateLogicDlg(this, title, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowFireSuppSpreadRate_actionPerformed(ActionEvent e) {
    String title = "Fire Suppression Spread Rate Logic";
    FireSuppSpreadRateLogicDlg dlg = new FireSuppSpreadRateLogicDlg(this, title, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowFireSuppFireOccMgmtZone_actionPerformed(ActionEvent e) {
    String title = "Fire Occurrence and Management Zones";
    FmzEditor dlg = new FmzEditor(this, title, true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowSpeciesKnowledgeEditor_actionPerformed(ActionEvent e) {
    String title = "Species Knowledge Editor";
    SpeciesKnowledgeEditor dlg = new SpeciesKnowledgeEditor(this, title, false);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuImportFSLandscape_actionPerformed(ActionEvent e) {
    doFSLandscape();
  }

  private void doFSLandscape() {
    try {
      String osName = System.getProperty("os.name");
      String[] cmd = new String[7];
      File dir;
      if (JSimpplle.developerMode()) {
        dir = new File("C:\\MyDocuments\\MyProjects\\SIMPPLLE-installAnywhere\\gis\\fslandscape");
      } else {
        dir = RegionalZone.getSystemFSLandscapeDiretory();
      }
      File batchFile = new File(dir, "fslandscape.bat");

      if (osName.equalsIgnoreCase("Windows NT") ||
          osName.equalsIgnoreCase("Windows 2000") ||
          osName.equalsIgnoreCase("Windows XP")) {
        cmd[0] = "cmd.exe";
        cmd[1] = "/c";
        cmd[2] = "start";
        cmd[3] = "/i/d";
        cmd[4] = JSimpplle.getWorkingDir().getAbsolutePath();
        cmd[5] = batchFile.getAbsolutePath();
        cmd[6] = dir.getAbsolutePath();
      } else {
        return;
      }

      Runtime rt = Runtime.getRuntime();

      StringBuffer strBuf = new StringBuffer();
      strBuf.append("Running");
      for (int i = 0; i < cmd.length; i++) {
        strBuf.append(" " + cmd[i]);
      }

      setWaitState(strBuf.toString());
      java.lang.Process proc = rt.exec(cmd);
      // any error message?
      StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");


      // any output?
      StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");

      // kick them off
      errorGobbler.start();
      outputGobbler.start();

      // any error?
      @SuppressWarnings("unused")
      int exitVal = proc.waitFor();
      setNormalState();
//      System.out.println("ExitValue: " + exitVal);
    } catch (Throwable t) {
      t.printStackTrace();
    }
  }

  private void menuSysKnowImportProcesses_actionPerformed(ActionEvent e) {
    File filename = Utility.getOpenFile(this, "Import Process Definition File");
    if (filename == null) {
      return;
    }

    try {
      simpplle.comcode.Process.importLegalFile(filename);
    } catch (SimpplleError ex) {
      JOptionPane.showMessageDialog(this, ex.getMessage(), "", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void menuSysKnowShowLegalProcesses_actionPerformed(ActionEvent e) {
    ProcessType[] processes = simpplle.comcode.Process.getLegalProcesses();
    StringBuffer strBuf = new StringBuffer();

    for (int i = 0; i < processes.length; i++) {
      strBuf.append(processes[i].toString());
      strBuf.append((processes[i].isSpreading() ? " -- spreading\n" : " -- not spreading\n"));
    }

    JTextAreaDialog dlg = new JTextAreaDialog(this, "Legal Processes", false, strBuf.toString());
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowImportZoneTreatments_actionPerformed(ActionEvent e) {
    File filename = Utility.getOpenFile(this, "Import Treatment Definition File");
    if (filename == null) {
      return;
    }

    try {
      simpplle.comcode.Treatment.importLegalFile(filename);
    } catch (SimpplleError ex) {
      JOptionPane.showMessageDialog(this, ex.getMessage(), "", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void menuSysKnowShowZoneTreatments_actionPerformed(ActionEvent e) {
    TreatmentType[] treatments = simpplle.comcode.Treatment.getLegalTreatments();
    StringBuffer strBuf = new StringBuffer();

    for (int i = 0; i < treatments.length; i++) {
      strBuf.append(treatments[i].toString());
      strBuf.append("\n");
    }

    JTextAreaDialog dlg = new JTextAreaDialog(this, "Legal Treatments", false, strBuf.toString());
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuFileSaveZone_actionPerformed(ActionEvent e) {
    MyFileFilter extFilter = new MyFileFilter("simpplle_zone",
        "Simpplle Zone Files (*.simpplle_zone)");
    File zoneName = Utility.getSaveFile(this, "Zone Name", extFilter);
    if (zoneName == null) {
      return;
    }

    try {
      SystemKnowledge.saveZoneKnowledge(zoneName);
    } catch (SimpplleError ex) {
      JOptionPane.showMessageDialog(this, ex.getMessage(), "Problems saving",
          JOptionPane.ERROR_MESSAGE);
    }
  }

  private void buildSimpplleTypeFiles_actionPerformed(ActionEvent e) {
    try {
      File prefix = Utility.getSaveFile(this, "Simpplle Types Prefix");
      if (prefix == null) {
        return;
      }
      HabitatTypeGroup.makeAllSimpplleTypeFiles(prefix);
    } catch (SimpplleError ex) {
      JOptionPane.showMessageDialog(this, ex.getMessage(), "Problems writing file",
          JOptionPane.ERROR_MESSAGE);
    }
  }

  private void buildSimpplleTypesSource_actionPerformed(ActionEvent e) {
    try {
      File filename = Utility.getOpenFile(this, "Simpplle Types File");
      if (filename == null) {
        return;
      }
      HabitatTypeGroup.makeAllSimpplleTypesSourceFiles(filename);
    } catch (SimpplleError ex) {
      JOptionPane.showMessageDialog(this, ex.getMessage(), "Problems writing file",
          JOptionPane.ERROR_MESSAGE);
    }
  }

  private void menuSysKnowWindthrow_actionPerformed(ActionEvent e) {
    String title = "Windthrow";
    String text = "Needs to be Locked in through the 'Lock in Processes' Screen";

    JTextAreaDialog dlg = new JTextAreaDialog(this, title, true, text);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowWildlifeBrowsing_actionPerformed(ActionEvent e) {
    String title = "Wildlife Browsing";
    String text = "Needs to be Locked in through the 'Lock in Processes' Screen";

    JTextAreaDialog dlg = new JTextAreaDialog(this, title, true, text);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowTussockMoth_actionPerformed(ActionEvent e) {
    String title = "Tussock Moth";
    String text = "Needs to be Locked in through the 'Lock in Processes' Screen";

    JTextAreaDialog dlg = new JTextAreaDialog(this, title, true, text);
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuUtilityTestNewDialog_actionPerformed(ActionEvent e) {
    Simpplle.getCurrentArea().validateLifeformStorageMatch();
  }

  private void menuSysKnowDisableWsbw_actionPerformed(ActionEvent e) {
    Wsbw.setEnabled(false);
    System.out.println(Wsbw.isEnabled());
  }

  private void menuSysKnowProcessProbLogic_actionPerformed(ActionEvent e) {
    ProcessProbabilityLogicDialog dlg =
        new ProcessProbabilityLogicDialog(this, "Process Probability Logic", true);
    dlg.setVisible(true);
  }

  private void menuBisonGrazingLogic_actionPerformed(ActionEvent e) {
    String title = "Bison Grazing Logic";
    BisonGrazingLogicEditor dlg = new BisonGrazingLogicEditor(this, title, true);

    dlg.initialize();
    setDialogLocation(dlg);
    dlg.setVisible(true);
  }

  private void menuSysKnowDoCompetition_actionPerformed(ActionEvent e) {
    DoCompetitionDlg dlg = new DoCompetitionDlg(this, "Lifeform Competition", true);
    dlg.setVisible(true);
  }

  private void menuSysKnowGapProcessLogic_actionPerformed(ActionEvent e) {
    GapProcessLogicDlg dlg = new GapProcessLogicDlg(this, "Gap Process Logic", true);
    dlg.setVisible(true);

  }

  private void menuSysKnowProducingSeedLogic_actionPerformed(ActionEvent e) {
    ProducingSeedLogicDlg dlg = new ProducingSeedLogicDlg(this, "Producing Seed Logic", true);
    dlg.setVisible(true);
  }

  private void menuSysKnowVegUnitFireTypeLogic_actionPerformed(ActionEvent e) {
    VegUnitFireTypeLogicDlg dlg = new VegUnitFireTypeLogicDlg(this, "Veg Unit Fire Type Logic", true);
    dlg.setVisible(true);
  }

  // Unused?..
  private void menuSysKnowInvasiveLogicR1_actionPerformed(ActionEvent e) {
  }

  private void menuSysKnowInvasiveLogicMSU_actionPerformed(ActionEvent e) {
    InvasiveSpeciesMSULogicDialog dlg =
        new InvasiveSpeciesMSULogicDialog(this, "Invasive Species Logic (Montana State University)", true);
    dlg.setVisible(true);
  }

  private void menuSysKnowInvasiveLogicMesaVerdeNP_actionPerformed(ActionEvent e) {
    InvasiveSpeciesLogicDialog dlg =
        new InvasiveSpeciesLogicDialog(this, "Invasive Species Logic (Mesa Verde NP)", true);
    dlg.setVisible(true);
  }

  private void menuUtilityMakeAreaMultipleLife_actionPerformed(ActionEvent e) {
    int response = JOptionPane.showConfirmDialog(null,
        "This option allows the simulation to run invasive " +
            "species logic when the area file does not contain " +
            "fields for multiple-lifeforms.\n Selecting this option " +
            "will modify the output files to include these empty fields.\n\n" +
            "Do you wish to continue?", "Confirm",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    if (response == JOptionPane.YES_OPTION) {
      Simpplle.getCurrentArea().makeMultipleLifeforms();
    }
  }

  private void menuUtilityCombineLSFiles_actionPerformed(ActionEvent e) {
    String[] suffixes = new String[]{"-ls"};
    MyFileFilter extFilter = new MyFileFilter(suffixes, "txt",
        "Landcape Summary Files (*-ls.txt)");

    File[] files = Utility.getOpenFiles(this, "LS Files to Combine", extFilter);
    if (files == null) {
      return;
    }

    try {
      String outfile = LandscapeSummaryFileCombiner.combineLandscapeSummaryFiles(files);

      JOptionPane.showMessageDialog(this, outfile, "File Successfully Written",
          JOptionPane.ERROR_MESSAGE);

    } catch (SimpplleError err) {
      JOptionPane.showMessageDialog(this, err.getMessage(), "Error",
          JOptionPane.ERROR_MESSAGE);
    } finally {
      setNormalState();
    }
  }

  private void menuUtilityExportPathways_actionPerformed(ActionEvent e) {
    File outfile;

    outfile = Utility.getSaveFile(this, "Export Pathways to Text File");
    if (outfile != null) {
      setWaitState("Exporting Pathways ...");
      try {
        HabitatTypeGroup.exportGISTable(outfile);
      } catch (SimpplleError ex) {
        JOptionPane.showMessageDialog(this, ex.getMessage(), "",
            JOptionPane.ERROR_MESSAGE);
      }
      setNormalState();
    }
  }

  private void menuUtilityElevRelPos_actionPerformed(ActionEvent e) {
    Area area = Simpplle.getCurrentArea();
    ElevationRelativePosition dlg = new ElevationRelativePosition(simpplle.JSimpplle.getSimpplleMain(), "Elevation Relative Position", true, area);

    dlg.setVisible(true);

    int value = dlg.getValue();

    area.setElevationRelativePosition(value);
  }

  private void menuUtilitySwapRowCol_actionPerformed(ActionEvent e) {
    String msg =
        "This will swap the Row and Column values in the units.\n\n" +
            "Do you wish to continue?";
    int choice = JOptionPane.showConfirmDialog(this, msg, "Swap Row/Column",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    if (choice == JOptionPane.YES_OPTION) {
      Simpplle.getCurrentArea().swapRowColumn();
    }
  }

  private void menuSysKnowFireSuppEvent_actionPerformed(ActionEvent e) {
    FireSuppEventLogicDlg dlg = new FireSuppEventLogicDlg(this, "Fire Suppression Event Probability", true);
    setDialogLocation(dlg);
    dlg.setVisible(true);
    refresh();
  }

  private void updateSpreadModels(boolean hasKeane) {
    if (hasKeane) {
      if (fireSpreadModels.contains("KEANE"))
        return;
      fireSpreadModels.add("KEANE");
      Collections.reverse(fireSpreadModels);
    } else {
      if (fireSpreadModels.contains("KEANE"))
        fireSpreadModels.remove("KEANE");
    }
  }

  public JLabel getStatusBar() {
    return statusBar;
  }
}
