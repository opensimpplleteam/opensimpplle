package simpplle.gui;

import simpplle.comcode.StatusReporter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Reports application status to users using the standard output stream.
 */
public class PrintStatusReporter implements StatusReporter {

  public PrintStatusReporter() { }

  @Override
  public void report(String status) {
    ZonedDateTime time = ZonedDateTime.now();
    System.out.println(time.format(DateTimeFormatter.ofPattern("HH:mm:ss")) + "  " + status);
  }

  @Override
  public void clear() { }
}
