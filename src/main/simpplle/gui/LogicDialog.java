/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.SimpplleError;
import simpplle.comcode.SystemKnowledge;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines UI elements and behavior common to all logic dialogs. All logic dialogs contain a menu
 * bar with a file menu, an action menu, a knowledge source menu, and zero or more tabbed panes
 * for viewing and editing the logic. Common behavior includes saving, loading, moving, duplicating,
 * adding, and deleting rules.
 */

public class LogicDialog extends JDialog {

  protected List<String> panelKinds = new ArrayList<>();
  protected List<LogicPanel> tabPanels = new ArrayList<>();
  protected String currentPanelKind;
  protected LogicPanel currentPanel;
  protected SystemKnowledge.Kinds sysKnowKind;

  protected JPanel mainPanel = new JPanel(new BorderLayout());
  protected JTabbedPane tabbedPane = new JTabbedPane();
  protected JMenu menuFile = new JMenu();
  protected JMenuBar menuBar = new JMenuBar();
  protected JMenuItem menuFileOpen = new JMenuItem();
  protected JMenuItem menuFileClose = new JMenuItem();
  protected JMenuItem menuFileSave = new JMenuItem();
  protected JMenuItem menuFileSaveAs = new JMenuItem();
  protected JMenuItem menuFileLoadDefault = new JMenuItem();
  protected JMenu menuAction = new JMenu();
  protected JMenuItem menuActionMoveRuleUp = new JMenuItem();
  protected JMenuItem menuActionMoveRuleDown = new JMenuItem();
  protected JMenuItem menuActionDeleteSelectedRule = new JMenuItem();
  protected JMenuItem menuActionDuplicateSelectedRule = new JMenuItem();

  /**
   * Creates a logic dialog.
   *
   * @param owner parent frame
   * @param title a title naming the logic being edited
   * @param modal whether dialog blocks user input to other top-level windows when shown
   */
  protected LogicDialog(Frame owner, String title, boolean modal) {
    super(owner, title, modal);
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public LogicDialog() {
    this(new Frame(), "LogicDialog", false);
  }

  private void jbInit() throws Exception {

    menuFileOpen.setText("Open");
    menuFileOpen.addActionListener(this::open);

    menuFileClose.setText("Close");
    menuFileClose.addActionListener(this::close);

    menuFileSave.setText("Save");
    menuFileSave.addActionListener(this::save);

    menuFileSaveAs.setText("Save As");
    menuFileSaveAs.addActionListener(this::saveAs);

    menuFileLoadDefault.setText("Load Default");
    menuFileLoadDefault.addActionListener(this::loadDefault);

    JMenuItem menuFileQuit = new JMenuItem("Close Dialog");
    menuFileQuit.addActionListener(this::quit);

    menuFile.setText("File");
    menuFile.add(menuFileOpen);
    menuFile.add(menuFileClose);
    menuFile.addSeparator();
    menuFile.add(menuFileSave);
    menuFile.add(menuFileSaveAs);
    menuFile.addSeparator();
    menuFile.add(menuFileLoadDefault);
    menuFile.addSeparator();
    menuFile.add(menuFileQuit);

    menuActionMoveRuleUp.setText("Move Rule Up");
    menuActionMoveRuleUp.addActionListener(this::moveRuleUp);

    menuActionMoveRuleDown.setText("Move Rule Down");
    menuActionMoveRuleDown.addActionListener(this::moveRuleDown);

    JMenuItem menuActionInsertNewRule = new JMenuItem("Insert New Rule");
    menuActionInsertNewRule.addActionListener(this::insertNewRule);

    menuActionDeleteSelectedRule.setText("Delete Selected Rule");
    menuActionDeleteSelectedRule.addActionListener(this::deleteSelectedRule);

    menuActionDuplicateSelectedRule.setText("Duplicate Selected Rule");
    menuActionDuplicateSelectedRule.addActionListener(this::duplicateSelectedRule);

    menuAction.setText("Action");
    menuAction.add(menuActionMoveRuleUp);
    menuAction.add(menuActionMoveRuleDown);
    menuAction.addSeparator();
    menuAction.add(menuActionInsertNewRule);
    menuAction.add(menuActionDeleteSelectedRule);
    menuAction.add(menuActionDuplicateSelectedRule);

    tabbedPane.addChangeListener(this::tabChanged);

    JMenuItem menuKnowledgeSourceEdit = new JMenuItem("Display/Edit");
    menuKnowledgeSourceEdit.addActionListener(this::menuKnowledgeSourceEdit);

    JMenu menuKnowledgeSource = new JMenu("Knowledge Source");
    menuKnowledgeSource.add(menuKnowledgeSourceEdit);

    menuBar.add(menuFile);
    menuBar.add(menuAction);
    menuBar.add(menuKnowledgeSource);

    setJMenuBar(menuBar);

    JPanel tabsPanel = new JPanel(new BorderLayout());
    tabsPanel.add(tabbedPane, BorderLayout.CENTER);

    mainPanel.setPreferredSize(new Dimension(825, 400));
    mainPanel.add(tabsPanel, BorderLayout.CENTER);

    getContentPane().add(mainPanel);

  }

  public void addPanel(String name, LogicPanel panel) {

    panelKinds.add(name);
    tabPanels.add(panel);

    tabbedPane.add(panel, name);

  }

  /**
   * Updates the Abstract Logic Dialog.
   */
  protected void updateDialog() {
    update(getGraphics());
  }

  /**
   * Enables or disables menu items based on the current state. This method enables and disables
   * the save and "close" menu items depending on if knowledge is loaded or has changed.
   */
  protected void updateMenuItems() {
    boolean hasFile = (SystemKnowledge.getFile(sysKnowKind) != null);
    menuFileSave.setEnabled(SystemKnowledge.hasChangedOrUserData(sysKnowKind) && hasFile);
    menuFileClose.setEnabled(hasFile);
  }

  /**
   * If open is selected from JMenu, will open the file containing this type of 
   * @param e
   */
  public void open(ActionEvent e) {
    SystemKnowledgeFiler.openFile(this,sysKnowKind,menuFileSave,menuFileClose);
    //  This line must be done first!
    for (LogicPanel currPanel : tabPanels) {
      currPanel.dataModel.fireTableStructureChanged();
      currPanel.initColumns();
    }

    updateMenuItems();
    currentPanel.updatePanel();
    updateDialog();
  }
  /**
   * Loads default logic from System Knowledge class.  
   * @throws SimpplleError catches the error thrown from SystemKnowledge loadZoneKnowledge
   */
  protected void loadDefaults() {
    try {
      String msg = "This will load the default Logic.\n\n" +
                   "Do you wish to continue?";
      String title = "Load Default Logic";

      if (Utility.askYesNoQuestion(this,msg,title)) {
        SystemKnowledge.loadZoneKnowledge(sysKnowKind);
        //  This line must be done first!
        currentPanel.dataModel.fireTableStructureChanged();
        currentPanel.initColumns();
        updateMenuItems();
        currentPanel.updatePanel();
        updateDialog();
        menuFileSave.setEnabled(isSaveNeeded());
        menuFileClose.setEnabled(false);
      }
    }
    catch (simpplle.comcode.SimpplleError err) {
      JOptionPane.showMessageDialog(this, err.getError(), "Unable to load file",
                                    JOptionPane.ERROR_MESSAGE);
    }
  }
  /**
   * If close is chosen from menu file close loads defaults.
   * @param e
   */
  public void close(ActionEvent e) {
    loadDefaults();
  }
  /**
   * Checks if save file menu should be enabled.  
   * @return true if System Knowledge has a file and system knowledge changed or has user data input.  
   */
  protected boolean isSaveNeeded() {
    return ( (SystemKnowledge.getFile(sysKnowKind) != null) &&
             (SystemKnowledge.hasChangedOrUserData(sysKnowKind)) );
  }
  /**
   * If save menu item selected, SystemKnowledgeFiler class saves to a file using SystemKnowledge getFile method.  
   * @param e save menu item selected
   * @see SystemKnowledgeFiler()
   */
  public void save(ActionEvent e) {
    File outfile = SystemKnowledge.getFile(sysKnowKind);
    SystemKnowledgeFiler.saveFile(this, outfile, sysKnowKind, menuFileSave, menuFileClose);
  }
  /**
   * 'Save as' function sends to overloaded SystemKnowledgeFiler.saveFile method.
   * @param e Save As menu item selected
   */
  public void saveAs(ActionEvent e) {
    SystemKnowledgeFiler.saveFile(this, sysKnowKind, menuFileSave, menuFileClose);
  }

  private void loadDefault(ActionEvent e) {
    loadDefaults();
  }

  public void quit(ActionEvent e) {
    setVisible(false);
  }
  /**
   * Moves selected rule containing row up.
   * @param e "Move Rule Up" selected
   */
  private void moveRuleUp(ActionEvent e) {
    currentPanel.moveRowUp();
    updateDialog();
  }
  /**
   * Moves selected rule containing row down.
   * @param e "Move Rule Down" selected
   */
  private void moveRuleDown(ActionEvent e) {
    currentPanel.moveRowDown();
    updateDialog();
  }
  /**
   * Inserts a new row in the currentPanel.
   * @param e "Insert New Rule" menu item selected
   */
  private void insertNewRule(ActionEvent e) {
    currentPanel.insertRow();
    updateDialog();
  }
  /**
   * Deletes selected rule containing row.  
   * @param e "Delete Selected Rule" selected
   */
  private void deleteSelectedRule(ActionEvent e) {
    currentPanel.deleteSelectedRow();
    updateDialog();
  }
  /**
   * Duplicates selected rule containing row.
   * @param e "Duplicate Selected Rule" selected
   */
  private void duplicateSelectedRule(ActionEvent e) {
    currentPanel.duplicateSelectedRow();
    updateDialog();
  }
  /**
   * Handles the changing of tabs
   * @param e ChangeEvent
   */
  public void tabChanged(ChangeEvent e) {

    int index = tabbedPane.getSelectedIndex();

    currentPanelKind = panelKinds.get(index);
    currentPanel = tabPanels.get(index);

    tabPanels.get(index).updatePanel();

    updateMenuItems();

  }

  public void menuKnowledgeSourceEdit(ActionEvent e) {
    String str = SystemKnowledge.getKnowledgeSource(sysKnowKind);
    String title = "Knowledge Source";

    KnowledgeSource dlg = new KnowledgeSource(JSimpplle.getSimpplleMain(),title,true,str);
    dlg.setVisible(true);

    String newKnowledge = dlg.getText();
    if (newKnowledge != null) {
      SystemKnowledge.setKnowledgeSource(sysKnowKind,newKnowledge);
    }
  }
}
