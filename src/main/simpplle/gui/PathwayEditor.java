/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import simpplle.comcode.HabitatTypeGroup;
import simpplle.comcode.Process;
import simpplle.comcode.ProcessType;
import simpplle.comcode.Species;
import simpplle.comcode.VegetativeType;

/**
 * This class defines Pathways Editor, a type of JDialog.
 * It allows users to edit a vegetative type next state.
 * It Contains an inner class to construct a listitem object which sets the process and next state
 *
 * @author Documentation by Brian Losi
 * <p>Original source code authorship: Kirk A. Moeller
 */

public class PathwayEditor extends JDialog {

  private static Color BORDER_HIGHLIGHT = Color.white;
  private static Color BORDER_SHADOW = new Color(148, 145, 140);

  private Frame theFrame;
  private HabitatTypeGroup pathwayHtGrp;
  private VegetativeType pathwayState;

  /**
   * Inner class which defines a list item to be used in Pathway Editor.
   *
   */
  private static class ListItem {
    public Process        process;
    public VegetativeType nextState;
/**
 * Constructor for
 * @param process process of this list item
 * @param nextState the next state of the list item.
 */
    public ListItem(Process process, VegetativeType nextState) {
      this.process   = process;
      this.nextState = nextState;
    }

/**
 * Creates a string of the process and next state.  This is displayed in a text
 */
    public String toString() {
      return process.toString() + " ---> " + nextState.toString();
    }
  }

  private ListItem editListItem;

  private JList nextStateList = new JList();
  private JLabel speciesText = new JLabel("XERIC-FS-SHRUBS");
  private JLabel htGrpText = new JLabel("A1");
  private JLabel currentStateText = new JLabel("XERIC-FS-SHRUBS/CLOSED-TALL-SHRUB10/1");
  private JLabel sizeClassText = new JLabel("CLOSED-TALL-SHRUB");
  private JLabel densityText = new JLabel("1");
  private JLabel ageText = new JLabel("10");
  private JLabel processValue = new JLabel("STAND-REPLACING-FIRE");

  private JButton showListPB = new JButton("Show List");
  private JButton saveEditPB = new JButton("Save Edits");
  private JButton processDeletePB = new JButton("Delete Selected");

  private JPopupMenu listPopupMenu = new JPopupMenu();

  private JTextField nextStateVegTypeValue = new JTextField("XERIC-FS-SHRUBS/CLOSED-TALL-SHRUB10/1");

  /**
   * Primary constructor for Pathway Editor, takes in the owner frame, title, modality and an additional variable for vegetative type.
   * @param frame the JFrame owner of the dialog
   * @param title title of dialog
   * @param modal specifies whether dialog blocks user input to other top-level windows when shown
   * @param vegType
   */
  public PathwayEditor(Frame frame, String title, boolean modal, VegetativeType vegType) {
    super(frame, title, modal);
    try {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
    this.theFrame = frame;
    this.pathwayState = vegType;
    initialize();
  }

  /**
   * Overloaded Pathway Editor constructor.  Sets the owner to null, the title to empty string, modality to modeless, and vegetative type object to null.
   */
  public PathwayEditor() {
    this(null, "", false, null);
  }

  void jbInit() throws Exception {

    JPanel mainPanel = new JPanel(new BorderLayout());
    JPanel northPanel = new JPanel(new BorderLayout());
    JPanel nextStatePanel = new JPanel(new BorderLayout());
    JPanel nextStateListPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel editPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel agePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel densityPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel sizeClassPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel SpeciesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel currentStatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel htGrpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel processPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel nextStateVegTypePanel = new JPanel(new FlowLayout());
    JPanel currentSelectionPanel = new JPanel();
    JPanel VegetativeTypeInfoPanel = new JPanel();
    JPanel saveEditPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JPanel processPBPanel = new JPanel();

    JLabel speciesLabel = new JLabel("Species           ");
    JLabel htGrpLabel = new JLabel("Ecological Grouping");
    JLabel currentStateLabel = new JLabel("Vegetative Type   ");
    JLabel sizeClassLabel = new JLabel("Size Class        ");
    JLabel densityLabel = new JLabel("Density           ");
    JLabel ageLabel = new JLabel("Age               ");
    JLabel processLabel = new JLabel("Process        ");
    JLabel nextStateVegTypeLabel = new JLabel("Vegetative Type");

    JScrollPane nextStateListScroll = new JScrollPane(nextStateList);

    JMenuItem contextMenuEdit = new JMenuItem("Edit");
    JMenuItem contextMenuDelete = new JMenuItem("Delete");

    JButton processAddPB = new JButton("Add");

    TitledBorder titledBorder1 = new TitledBorder(BorderFactory.createEtchedBorder(BORDER_HIGHLIGHT, BORDER_SHADOW),"Next Vegetative State");
    TitledBorder processPBBorder = new TitledBorder(BorderFactory.createEmptyBorder(),"Processes");

    nextStateList.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    nextStateList.setToolTipText("Double-Click item to change or Right-Click for Menu");
    nextStateList.setPrototypeCellValue("STAND-REPLACING-FIRE ---> XERIC_FS_SHRUBS/CLOSED-TALL-SHRUB10/1");
    nextStateList.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        nextStateList_mouseClicked(e);
      }
      public void mousePressed(MouseEvent e) {
        nextStateList_mousePressed(e);
      }
      public void mouseReleased(MouseEvent e) {
        nextStateList_mouseReleased(e);
      }
    });
    currentSelectionPanel.setLayout(new BoxLayout(currentSelectionPanel, BoxLayout.Y_AXIS));
    currentSelectionPanel.setBorder(titledBorder1);
    VegetativeTypeInfoPanel.setLayout(new BoxLayout(VegetativeTypeInfoPanel, BoxLayout.Y_AXIS));
    speciesText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    speciesText.setForeground(Color.blue);
    speciesLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    htGrpLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    htGrpText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    htGrpText.setForeground(Color.blue);
    currentStateText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    currentStateText.setForeground(Color.blue);
    currentStateLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    sizeClassText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    sizeClassText.setForeground(Color.blue);
    sizeClassLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    densityText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    densityText.setForeground(Color.blue);
    densityLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    ageText.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    ageText.setForeground(Color.blue);
    ageLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 14));
    VegetativeTypeInfoPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    contextMenuEdit.addActionListener(this::contextMenuEdit_actionPerformed);
    contextMenuDelete.addActionListener(this::deleteProcessNextState);
    listPopupMenu.setInvoker(nextStateList);
    titledBorder1.setTitleFont(new java.awt.Font("Dialog", Font.ITALIC, 12));
    showListPB.setEnabled(false);
    showListPB.setMargin(new Insets(0, 0, 0, 0));
    showListPB.addActionListener(this::showListPB_actionPerformed);
    nextStateVegTypeLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));
    nextStateVegTypeValue.setEnabled(false);
    nextStateVegTypeValue.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    nextStateVegTypeValue.setColumns(40);
    nextStateVegTypeValue.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyTyped(KeyEvent e) {
        nextStateVegTypeValue_keyTyped(e);
      }
    });
    nextStateVegTypeValue.addActionListener(this::nextStateVegTypeValue_actionPerformed);
    processLabel.setFont(new java.awt.Font("Monospaced", Font.BOLD, 12));
    processValue.setFont(new java.awt.Font("Monospaced", Font.PLAIN, 12));
    processValue.setForeground(Color.blue);
    saveEditPB.setEnabled(false);
    saveEditPB.setMargin(new Insets(0, 0, 0, 0));
    saveEditPB.addActionListener(this::saveEditPB_actionPerformed);

    GridLayout gridLayout1 = new GridLayout();
    gridLayout1.setRows(2);
    gridLayout1.setVgap(5);
    processPBPanel.setLayout(gridLayout1);

    processAddPB.addActionListener(this::processAddPB_actionPerformed);
    processDeletePB.setEnabled(false);
    processDeletePB.addActionListener(this::deleteProcessNextState);
    processPBPanel.setBorder(processPBBorder);
    processPBBorder.setTitleFont(new java.awt.Font("Monospaced", Font.BOLD, 12));

    getContentPane().add(mainPanel);
    mainPanel.add(northPanel, BorderLayout.NORTH);
    northPanel.add(VegetativeTypeInfoPanel, BorderLayout.WEST);
    VegetativeTypeInfoPanel.add(htGrpPanel, null);
    htGrpPanel.add(htGrpLabel, null);
    htGrpPanel.add(htGrpText, null);
    VegetativeTypeInfoPanel.add(currentStatePanel, null);
    currentStatePanel.add(currentStateLabel, null);
    currentStatePanel.add(currentStateText, null);
    VegetativeTypeInfoPanel.add(SpeciesPanel, null);
    SpeciesPanel.add(speciesLabel, null);
    SpeciesPanel.add(speciesText, null);
    VegetativeTypeInfoPanel.add(sizeClassPanel, null);
    sizeClassPanel.add(sizeClassLabel, null);
    sizeClassPanel.add(sizeClassText, null);
    VegetativeTypeInfoPanel.add(densityPanel, null);
    densityPanel.add(densityLabel, null);
    densityPanel.add(densityText, null);
    VegetativeTypeInfoPanel.add(agePanel, null);
    agePanel.add(ageLabel, null);
    agePanel.add(ageText, null);
    mainPanel.add(nextStatePanel, BorderLayout.CENTER);
    nextStatePanel.add(nextStateListPanel, BorderLayout.CENTER);
    nextStateListPanel.add(nextStateListScroll, null);
    nextStateListPanel.add(processPBPanel, null);
    processPBPanel.add(processAddPB, null);
    processPBPanel.add(processDeletePB, null);
    nextStatePanel.add(editPanel, BorderLayout.NORTH);
    editPanel.add(currentSelectionPanel, null);
    currentSelectionPanel.add(processPanel, null);
    processPanel.add(processLabel, null);
    processPanel.add(processValue, null);
    currentSelectionPanel.add(nextStateVegTypePanel, null);
    nextStateVegTypePanel.add(nextStateVegTypeLabel, null);
    nextStateVegTypePanel.add(nextStateVegTypeValue, null);
    nextStateVegTypePanel.add(showListPB, null);
    currentSelectionPanel.add(saveEditPanel, null);
    saveEditPanel.add(saveEditPB, null);
    listPopupMenu.add(contextMenuEdit);
    listPopupMenu.add(contextMenuDelete);
  }

  /**
 * Initializes the Pathway Editor.
 */
  private void initialize() {
    updateDialog();
    processValue.setText("");
    nextStateVegTypeValue.setText("");
  }

  private void updateDialog() {
    setPathwayStateInfo();
    fillNextStateList();
  }

  /**
 * Sets the pertinent Pathway state info.  These are the Habitat group, vegetative type current state, current state, species, size class, density (canopy coverage), and age.
 *
 */
  private void setPathwayStateInfo() {
    pathwayHtGrp = pathwayState.getHtGrp();

    htGrpText.setText(pathwayHtGrp.toString());
    currentStateText.setText(pathwayState.getCurrentState());
    speciesText.setText(pathwayState.getSpecies().toString());
    sizeClassText.setText(pathwayState.getSizeClass().toString());
    densityText.setText(pathwayState.getDensity().toString());
    ageText.setText(Integer.toString(pathwayState.getAge()));
  }

  /**
 * Gets all the processes in the vegetative type, and creates a list item array of all the processes.  Then gets the next states of the processes
 * and creates a listItem array of those
 * Pathway states are required to have at least one process next state.  Therefore the processes list will never be empty.
 */
  private void fillNextStateList() {
    Vector<ListItem> listItems = new Vector<>();

    for (Process p : pathwayState.getProcesses()) {
      VegetativeType nextState = pathwayState.getProcessNextState(p);
      listItems.add(new ListItem(p, nextState));
    }
    nextStateList.setListData(listItems);
  }

  /**
 * Edits the next step by creating a temporary listitem to hold a selected list item, enables the Show List button and next state veg type value text field which allows
 * users to edit the next state.   This is how the user can select a process --> next state to edit.
 */
  private void editNextState() {

    editListItem = (ListItem) nextStateList.getSelectedValue();
    if (editListItem == null) { return; }

    showListPB.setEnabled(true);
    nextStateVegTypeValue.setEnabled(true);

    processValue.setText(editListItem.process.toString());
    nextStateVegTypeValue.setText(editListItem.nextState.toString());
  }

  /**
 * Saves the edits by creating a new vegetative type from the user entered text (made uppercase), edits the list items array with the new vegetative type
 * then updates the dialog.
 */
  private void saveEdits() {
    VegetativeType newNextState =
       pathwayHtGrp.getVegetativeType(nextStateVegTypeValue.getText().toUpperCase());

    if (newNextState == null) {
      JOptionPane.showMessageDialog(this,"Invalid Next State",
                                    "Invalid Next State", JOptionPane.ERROR_MESSAGE);
      return;
    }

    editListItem.nextState = newNextState;
    pathwayState.setProcessNextState(editListItem.process,newNextState);

    processValue.setText("");
    nextStateVegTypeValue.setText("");
    showListPB.setEnabled(false);
    saveEditPB.setEnabled(false);
    nextStateVegTypeValue.setEnabled(false);
    editListItem = null;

    update(getGraphics());
  }

  /**
   * Handles the double mouse click in the next state list.  Allows users to choose a next state to edit.
   * @param e
   */
  private void nextStateList_mouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2 && !nextStateList.isSelectionEmpty()) {
      editNextState();
    }
  }

  /**
   * Makes sure user wants to edit the next state, then saves the edits if user is positive of changes.
   * @param e
   */
  private void nextStateVegTypeValue_actionPerformed(ActionEvent e) {
    String msg = "Are you sure you want to save edits?";
    int choice = JOptionPane.showConfirmDialog(this,msg,"Save Edits",
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);
    if (choice == JOptionPane.YES_OPTION) {
      saveEdits();
    }
  }

  /**
   * Saves the edited next vegetative state when 'Save Edits' button pressed.
   * @param e 'Save Edits'
   */
  private void saveEditPB_actionPerformed(ActionEvent e) {
    saveEdits();
  }

  /**
   * Edits the context menu to
   * @param e
   */
  private void contextMenuEdit_actionPerformed(ActionEvent e) {
    editNextState();
  }

  /**
   * Deletes selected process next state
   * @param e
   */
  private void deleteProcessNextState(ActionEvent e) {
    String msg;

    ListItem item = (ListItem) nextStateList.getSelectedValue();
    if (item.process.toString().equals("SUCCESSION")) {
      msg = "Deleting the process SUCCESSION is not allowed";
      JOptionPane.showMessageDialog(this,"Illegal selection",msg,
                                    JOptionPane.ERROR_MESSAGE);
      nextStateList.clearSelection();
      return;
    }

    msg = "Are you sure you want to delete the selected " +
                 "Process Next State?";
    int choice = JOptionPane.showConfirmDialog(this,msg,"Delete Selected Item",
                                               JOptionPane.YES_NO_OPTION,
                                               JOptionPane.QUESTION_MESSAGE);
    if (choice == JOptionPane.YES_OPTION) {
      pathwayState.removeProcessNextState(item.process);
      updateDialog();
    }
    nextStateList.clearSelection();
  }

  /**
   * Displays a list popup menu if next state is selected.  Menu will popup at the x and y coordinates of component that invoked it.
   * @param e
   */
  private void maybePopupMenu(MouseEvent e) {
    if (e.isPopupTrigger() && !nextStateList.isSelectionEmpty()) {
      listPopupMenu.show(e.getComponent(),e.getX(),e.getY());
    }
  }

  /**
   * Handles mouse press in in next state list.  This will give the user the ability to delete a process if a next state is selected.
   * @param e
   */
  private void nextStateList_mousePressed(MouseEvent e) {
    processDeletePB.setEnabled((!nextStateList.isSelectionEmpty()));
    maybePopupMenu(e);
  }

  private void nextStateList_mouseReleased(MouseEvent e) {
    processDeletePB.setEnabled((!nextStateList.isSelectionEmpty()));
    maybePopupMenu(e);
  }

  /**
   * If a next state vegetative type is typed into the next state vegetative type text field, enables the saveEdit buton.
   * @param e
   */
  private void nextStateVegTypeValue_keyTyped(KeyEvent e) {
    saveEditPB.setEnabled(true);
  }

  /**
   * Once the process --> next state is selected, if the user pushes the 'Show List' button makes the vegetative type chooser.  This creates a new instance of vegetative type chooserwhich allows user to
   * see a list of vegetative types (species/size class/density) to choose from.
   * @param e
   */
  private void showListPB_actionPerformed(ActionEvent e) {
    String  title   = "Vegetative Type Chooser";
    Species species = editListItem.nextState.getSpecies();
    VegetativeTypeChooser dlg = new VegetativeTypeChooser(this, title, true,
                                                          pathwayHtGrp, species);

    dlg.setVisible(true);

    VegetativeType selection = dlg.getSelection();
    if (selection != null) {
      nextStateVegTypeValue.setText(selection.toString());
      saveEditPB.setEnabled(true);
    }
  }

  /**
   * Method to add a process to a pathway.  Creates a new list selection dialog of all the summary porcesses, gets the user selected object if one exists,
   * gets the next state of the vegetative type and adds it and the process to the pathway state.
   * @param e
   */
  private void processAddPB_actionPerformed(ActionEvent e) {
    String title = "Add a Process";
    ListSelectionDialog dlg;

    dlg = new ListSelectionDialog(theFrame,title,true,Process.getSummaryProcesses());
    dlg.setLocation(getLocation());
    dlg.setVisible(true);

    ProcessType result = (ProcessType)dlg.getSelection();
    if (result != null) {
      Process p = Process.findInstance(result);
      Process succession = Process.findInstance(ProcessType.SUCCESSION);
      VegetativeType nextState = pathwayState.getProcessNextState(succession);
      if (nextState == null) {
        pathwayState.addProcessNextState(p, pathwayState);
      } else {
        pathwayState.addProcessNextState(p, nextState);
      }
    }
    updateDialog();
  }
}
