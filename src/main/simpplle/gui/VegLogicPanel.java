/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import javax.swing.table.TableColumn;

import simpplle.comcode.*;

/**
 * The base class for all vegetative logic panels.
 */
public class VegLogicPanel extends LogicPanel {

  /**
   * Creates a vegetative logic panel.
   *
   * @param dialog the dialog that this panel has been placed in
   * @param kind the kind of logic that this panel is presenting
   * @param logicInst the logic displayed within this panel
   * @param sysKnowKind a kind of represented system knowledge
   */
  public VegLogicPanel(LogicDialog dialog,
                       String kind,
                       AbstractBaseLogic logicInst,
                       SystemKnowledge.Kinds sysKnowKind) {
    super(dialog,kind,logicInst,sysKnowKind);
  }

  /**
   * Initializes columns common to all vegetative logic panels. This includes:
   *
   * <li>Row Col (from LogicPanel superclass)
   * <li>Ecological Grouping
   * <li>Species
   * <li>Size Class
   * <li>Density
   * <li>Process
   * <li>Treatment
   * <li>Season
   * <li>Moisture
   * <li>Temperature
   * <li>Tracking Species
   * <li>Ownership
   * <li>Special Area
   * <li>Road Status
   * <li>Trail Status
   * <li>Landtype
   *
   * @param column the table column to initialize
   * @param identifier the column identifier
   */
  protected void initBaseColumn(TableColumn column, int identifier) {
    if (identifier == BaseLogic.ECO_GROUP_COL) {

      column.setIdentifier(BaseLogic.ECO_GROUP_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.ECO_GROUP_COL,"Ecological Grouping",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.SPECIES_COL) {

      column.setIdentifier(BaseLogic.SPECIES_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.SPECIES_COL,"Species",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.SIZE_CLASS_COL) {

      column.setIdentifier(BaseLogic.SIZE_CLASS_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.SIZE_CLASS_COL,"Size Class",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.DENSITY_COL) {

      column.setIdentifier(BaseLogic.DENSITY_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.DENSITY_COL,"Density",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.PROCESS_COL) {

      column.setIdentifier(BaseLogic.PROCESS_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.PROCESS_COL,"Process",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.TREATMENT_COL) {

      column.setIdentifier(BaseLogic.TREATMENT_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.TREATMENT_COL,"Treatment",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.SEASON_COL) {

      column.setIdentifier(BaseLogic.SEASON_COL);
      column.setCellRenderer(new MyJComboBoxRenderer(Season.values()));
      column.setCellEditor(new MyJComboBoxEditor(Season.values()));

    } else if (identifier == BaseLogic.MOISTURE_COL) {

      column.setIdentifier(BaseLogic.MOISTURE_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.MOISTURE_COL,"Moisture",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.TEMP_COL) {

      column.setIdentifier(BaseLogic.TEMP_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.TEMP_COL,"Temperature",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.TRACKING_SPECIES_COL) {

      column.setIdentifier(BaseLogic.TRACKING_SPECIES_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.TRACKING_SPECIES_COL,"Tracking Species",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.OWNERSHIP_COL) {

      column.setIdentifier(BaseLogic.OWNERSHIP_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.OWNERSHIP_COL,"Ownership",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.SPECIAL_AREA_COL) {

      column.setIdentifier(BaseLogic.SPECIAL_AREA_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.SPECIAL_AREA_COL,"Special Area",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.ROAD_STATUS_COL) {

      column.setIdentifier(BaseLogic.ROAD_STATUS_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.ROAD_STATUS_COL,"Road Status",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.TRAIL_STATUS_COL) {

      column.setIdentifier(BaseLogic.TRAIL_STATUS_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.TRAIL_STATUS_COL,"Trail Status",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else if (identifier == BaseLogic.LANDTYPE_COL) {

      column.setIdentifier(BaseLogic.LANDTYPE_COL);
      column.setCellEditor(new MyJButtonEditor(dialog,logicTable,dataModel,BaseLogic.LANDTYPE_COL,"Land Type",true));
      column.setCellRenderer(new MyJTextAreaRenderer());

    } else {

      super.initBaseColumn(column, identifier);

    }
  }
}


