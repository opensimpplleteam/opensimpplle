/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.Area;
import simpplle.comcode.SimpplleError;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

/**
 * A sample area chooser dialog prompts the user to select a sample area from the current zone.
 */

public class SampleAreaChooser extends JDialog {

  private JList<Area> areas = new JList<>();
  private Area selection;

  public SampleAreaChooser(Frame frame) {

    super(frame, "Select an Area", true);

    try  {
      jbInit();
      pack();
    } catch(Exception ex) {
      ex.printStackTrace();
    }

    areas.setListData(JSimpplle.getComcode().getSampleAreas());

  }

  void jbInit() throws Exception {

    areas.setBorder(BorderFactory.createRaisedBevelBorder());
    areas.setMinimumSize(new Dimension(161, 200));
    areas.setForeground(Color.white);
    areas.setBackground(Color.darkGray);
    areas.setSelectionBackground(Color.white);
    areas.setSelectionForeground(Color.orange);
    areas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    areas.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        clickedItem(e);
      }
    });

    JButton okButton = new JButton("Ok");
    okButton.setMinimumSize(new Dimension(73, 27));
    okButton.setPreferredSize(new Dimension(73, 27));
    okButton.addActionListener(this::pressedOk);

    JButton cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(this::pressedCancel);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());
    buttonPanel.add(okButton, null);
    buttonPanel.add(cancelButton, null);

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout());
    mainPanel.setMinimumSize(new Dimension(300, 237));
    mainPanel.setPreferredSize(new Dimension(300, 237));
    mainPanel.add(areas, BorderLayout.CENTER);
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);

    getContentPane().add(mainPanel);

  }

  public Area select() {
    setVisible(true);
    return selection;
  }

  private void clickedItem(MouseEvent e) {
    if (e.getClickCount() == 2 && !areas.isSelectionEmpty()) {
      load();
      setVisible(false);
      dispose();
    }
  }

  private void pressedOk(ActionEvent event) {
    if (areas.isSelectionEmpty()) {
      JOptionPane.showMessageDialog(this,
                                    "Please Select an Area",
                                    "No Area Selected",
                                    JOptionPane.WARNING_MESSAGE);
    } else {
      load();
      setVisible(false);
      dispose();
    }
  }

  private void pressedCancel(ActionEvent event) {
    setVisible(false);
    dispose();
  }

  private void load() {
    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    selection = areas.getSelectedValue();
    try {
      JSimpplle.getComcode().loadSampleArea(selection);
    } catch (SimpplleError err) {
      JOptionPane.showMessageDialog(this, err.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
  }
}

