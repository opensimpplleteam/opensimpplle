/*
 * The University of Montana owns copyright of the designated documentation contained
 * within this file as part of the software product designated by Uniform Resource Identifier
 * UM-OpenSIMPPLLE-1.0. By copying this file the user accepts the University of Montana
 * Open Source License Contract pertaining to this documentation and agrees to abide by all
 * restrictions, requirements, and assertions contained therein. All Other Rights Reserved.
 */

package simpplle.gui;

import simpplle.JSimpplle;
import simpplle.comcode.AreaType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * The area chooser dialog prompts the user to select a type of area to load.
 */

public class AreaChooser extends JDialog {

  private JButton sampleArea      = new JButton("Sample Area");
  private JButton simulatedArea   = new JButton("Simulated Area");
  private JButton userDefinedArea = new JButton("User Defined Area");
  private JButton cancelButton    = new JButton("Cancel");

  private AreaType selection;

  public AreaChooser(Frame frame) {

    super(frame, "Select a Type of Area", true);

    try  {
      jbInit();
      pack();
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    if (JSimpplle.getComcode().getSampleAreas() == null) {
      sampleArea.setEnabled(false);
    } else {
      sampleArea.setEnabled(true);
    }
  }

  private void jbInit() throws Exception {

    Insets buttonMargin = new Insets(2, 5, 2, 5);

    sampleArea.setMargin(buttonMargin);
    sampleArea.setToolTipText("Loads a sample area within this geographic region.");
    sampleArea.addActionListener(this::selectSample);

    simulatedArea.setMargin(buttonMargin);
    simulatedArea.setToolTipText("Loads a user-defined area with simulation data.");
    simulatedArea.addActionListener(this::selectSimulated);

    userDefinedArea.setMargin(buttonMargin);
    userDefinedArea.setToolTipText("Loads a user-defined area.");
    userDefinedArea.addActionListener(this::selectUserDefined);

    GridLayout gridLayout = new GridLayout(4, 1);
    gridLayout.setColumns(1);
    gridLayout.setRows(3);
    gridLayout.setVgap(4);

    JPanel selectionPanel = new JPanel();
    selectionPanel.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
    selectionPanel.setLayout(gridLayout);
    selectionPanel.add(sampleArea, null);
    selectionPanel.add(simulatedArea, null);
    selectionPanel.add(userDefinedArea, null);

    cancelButton.addActionListener(this::cancel);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setBorder(BorderFactory.createEtchedBorder());
    buttonPanel.setLayout(new FlowLayout());
    buttonPanel.add(cancelButton, null);

    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout());
    mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    mainPanel.add(selectionPanel, BorderLayout.NORTH);

    getContentPane().add(mainPanel);

  }

  public AreaType select() {
    setVisible(true);
    return selection;
  }

  private void selectSample(ActionEvent event) {
    selection = AreaType.SAMPLE;
    setVisible(false);
    dispose();
  }

  private void selectSimulated(ActionEvent event) {
    selection = AreaType.SIMULATED;
    setVisible(false);
    dispose();
  }

  private void selectUserDefined(ActionEvent event) {
    selection = AreaType.USER;
    setVisible(false);
    dispose();
  }

  private void cancel(ActionEvent event) {
    setVisible(false);
    dispose();
  }
}
