package simpplle.view;

import javafx.event.Event;

/**
 * An event indicating map interaction.
 */
public class MapEvent extends Event {

    /**
     * The offset of the left edge of the map in pixels
     */
    public double leftOffset;

    /**
     * The offset of the right edge of the map in pixels
     */
    public double topOffset;

    /**
     * The zoom level
     */
    public int zoomLevel;

    /**
     * Creates a new map event.
     *
     * @param source the tile map where the event originated
     */
    public MapEvent(TileMap source) {
        super(source, NULL_SOURCE_TARGET, Event.ANY);
    }
}
