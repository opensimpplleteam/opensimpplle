package simpplle.view;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Created by John on 3/24/2017.
 */
public class Inspector {

    private Group group;
    private Text text;
    private Rectangle rect;

    /**
     * Create the inspector layer.
     */
    public Inspector() {
        group = new Group();

        Line[] lines = new Line[4];
        lines[0] = new Line(0,0,0,-20);
        lines[1] = new Line(0,0,0,20);
        lines[2] = new Line(0,0,-20,0);
        lines[3] = new Line(0,0,20,0);
        for (int i = 0; i < 4; i++) {
            lines[i].setStrokeWidth(2.0);
            lines[i].setStroke(Color.WHITE);
            group.getChildren().add(lines[i]);
        }

        rect = new Rectangle(5, 5, 40, 25);
        rect.setFill(Color.YELLOW);
        group.getChildren().add(rect);

        text = new Text(10, 20, "0");
        group.getChildren().add(text);
    }

    /**
     * Get the JavaFX group containing the inspector.
     *
     * @return the JavaFX group containing the inspector
     */
    public Group getGroup() { return group; }

    /**
     * Set the text label of the inspector
     *
     * @param msg the message to display
     */
    public void setLabel(String msg) {
        text.setText(msg);
    }
}
