package simpplle.view;

import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import simpplle.model.ColorMap;
import simpplle.model.Frame;

/**
 * A tile map displays a region of a map.
 */
public class TileMap extends Pane {

    /**
     * The tiles managed by this map
     */
    private Tile[][] tiles;

    /**
     * The clipping shape for the pane
     */
    private Rectangle clip;

    /**
     * The number of columns in the tile map
     */
    private int ncols;

    /**
     * The number of rows in the tile map
     */
    private int nrows;

    /**
     * The frame drawn by this tile map
     */
    private Frame frame;

    /**
     * The color map applied to the tiles
     */
    private ColorMap colorMap;

    /**
     * Flag indicating if the map can be zoomed and panned
     */
    private boolean canNavigate;

    //TODO Replace separate coordinate variables with a coordinate class (Look into JavaFX offerings vs custom)

    /**
     * Tile position offset in pixels
     */
    private double offsetX;
    private double offsetY;

    private double priorOffsetX;
    private double priorOffsetY;

    /**
     * Tile position offset at start of pan in pixels
     */
    private double offsetStartX;
    private double offsetStartY;

    /**
     * Mouse position at start of pan in pixels
     */
    private double dragStartX;
    private double dragStartY;

    /**
     * The zoom level (-1 = 50%, 0 = 100%, 1 = 200%)
     */
    private int zoomLevel;

    /**
     * The dimensions of the map in pixels
     */
    private int mapWidth;
    private int mapHeight;

    /**
     * The event handler for this tile map
     */
    private EventHandler<MapEvent> handler;

    /**
     * Creates a new tile map.
     *
     * @param maxWidth the maximum visible width in pixels
     * @param maxHeight the maximum visible height in pixels
     * @param tileWidth the width of each tile in pixels
     * @param tileHeight the height of each tile in pixels
     */
    public TileMap(int maxWidth, int maxHeight, int tileWidth, int tileHeight) {

        // Find the minimum number of tiles that fit in the visible area (round up)
        nrows = (maxHeight - 1) / tileHeight + 1;
        ncols = (maxWidth - 1) / tileWidth + 1;

        // Add an extra row and column so tiles always cover visible area
        nrows++;
        ncols++;

        // Compute the dimensions of the tile map
        mapWidth = ncols * tileWidth;
        mapHeight = nrows * tileHeight;

        ObservableList<Node> children = getChildren();

        // here is where build 2d array of tiles
        tiles = new Tile[nrows][ncols];
        for (int row = 0; row < nrows; row++) {
            for (int col = 0; col < ncols; col++) {
                Tile tile = new Tile(tileWidth, tileHeight);
                children.add(tile);
                tiles[row][col] = tile;
            }
        }

        setOnMouseDragged(this::mouseDragged);
        setOnMousePressed(this::mousePressed);

        clip = new Rectangle(getWidth(), getHeight());
        setClip(clip);

        setMaxWidth(Double.MAX_VALUE);
        setMaxHeight(Double.MAX_VALUE);

        canNavigate = true;

        reposition();

    }

    /**
     * Increases the zoom level.
     */
    public void zoomIn() {
        zoomLevel++;
        redraw();
    }

    /**
     * Decreases the zoom level.
     */
    public void zoomOut() {
        zoomLevel--;
        redraw();
    }

    /**
     * Resets the zoom level to 100%.
     */
    public void zoomReset() {
        zoomLevel = 0;
        redraw();
    }

    /**
     * Pans to a position in the tile map.
     *
     * @param leftOffset offset of the left of the map in pixels
     * @param topOffset offset of the top of the map in pixels
     */
    public void panTo(double leftOffset, double topOffset) {

        offsetStartX = offsetX;
        offsetStartY = offsetY;

        offsetX = leftOffset;
        offsetY = topOffset;

        reposition();

    }

    /**
     * Freezes navigation within the map.
     */
    public void freeze() {
        canNavigate = true;
    }

    /**
     * Unfreezes navigation within the map.
     */
    public void unfreeze() {
        canNavigate = false;
    }

    public void setHandler(EventHandler<MapEvent> handler) {
        this.handler = handler;
    }

    /**
     * Dragging with the primary mouse button pans the tiles in the map.
     *
     * @param e a mouse event
     */
    private void mouseDragged(MouseEvent e) {
        if (e.isPrimaryButtonDown() && canNavigate) {

            // Compute the mouse movement
            double deltaX = e.getSceneX() - dragStartX;
            double deltaY = e.getSceneY() - dragStartY;

            // Update the map offset
            offsetX = offsetStartX + deltaX;
            offsetY = offsetStartY + deltaY;

            // Reposition the tiles
            reposition();

            // Call the handler
            if (handler != null) {

                MapEvent event = new MapEvent(this);

                event.leftOffset = offsetX;
                event.topOffset = offsetY;
                event.zoomLevel = zoomLevel;

                handler.handle(event);

            }
        }
    }

    /**
     * Pressing the primary mouse button prepares panning.
     *
     * @param e a mouse event
     */
    private void mousePressed(MouseEvent e) {
        if (e.isPrimaryButtonDown()) {
            dragStartX = e.getSceneX();
            dragStartY = e.getSceneY();
            offsetStartX = offsetX;
            offsetStartY = offsetY;
        }
    }

    /**
     * Repositions every tile in the map. Tiles pushed out of view are reused on the opposite side of the map. This
     * is performed mathematically; the cycling of tiles does not involve shuffling any memory.
     */
    private void reposition() {
        for (int row = 0; row < nrows; row++) {
            for (int col = 0; col < ncols; col++) {

                Tile tile = tiles[row][col];

                // Compute the new unwrapped position
                double newX = offsetX + col * tile.getWidth();
                double newY = offsetY + row * tile.getHeight();

                // Compute the old unwrapped position
                double oldX = priorOffsetX + col * tile.getWidth();
                double oldY = priorOffsetY + row * tile.getHeight();

                // Determine if the tile will wrap
                boolean wrapX = Math.floor(oldX / mapWidth) != Math.floor(newX / mapWidth);
                boolean wrapY = Math.floor(oldY / mapHeight) != Math.floor(newY / mapHeight);

                // Redraw if tile will wrap
                if (wrapX || wrapY) {
                    tile.draw(frame,
                              colorMap,
                              (int) (col * tile.getWidth()),
                              (int) (row * tile.getHeight()),
                              (int) tile.getWidth(),
                              (int) tile.getHeight(),
                              colorMap.getColor(0, Color.GRAY));
                }

                // Wrap the new offset
                newX %= mapWidth;
                newY %= mapHeight;
                if (newX < 0) newX += mapWidth;
                if (newY < 0) newY += mapHeight;

                // Set the new offset
                tile.setTranslateX(newX);
                tile.setTranslateY(newY);
            }
        }

        priorOffsetX = offsetX;
        priorOffsetY = offsetY;

    }

    /**
     * Redraws all tiles within the map. Currently this only redraws all tiles in their initial position.
     */
    private void redraw() {
        for (int row = 0; row < nrows; row++) {
            for (int col = 0; col < ncols; col++) {
                Tile tile = tiles[row][col];
                tile.draw(frame,
                    colorMap,
                    (int) (col * tile.getWidth()),
                    (int) (row * tile.getHeight()),
                    (int) tile.getWidth(),
                    (int) tile.getHeight(),
                    colorMap.getColor(0, Color.GRAY));
            }
        }
    }

    public void resizeMap(double clipWidth, double clipHeight) {
        clip.setWidth(clipWidth);
        clip.setHeight(clipHeight);
    }

    /**
     *
     *
     * @param frame frame passed in by viewerController and acquired from a layer inside of a simulation object
     * @param colorMap also passed in by viewerController, but acquired from landscape object
     */
    public void setFrame(Frame frame, ColorMap colorMap) {
        // switched by Doug and Ed from using the actual passed object to making a copy
        //this.frame = frame;
        /** Eduard Shokur:
         * Just to make sure nothing gets broken by doing changes on the frame object,
         * a copy (by value) of the object was made and then the rotate operations were applied to this copy
          */
        this.frame = new Frame(frame); // this makes a copy of the frame (copy constructor)
        this.frame.rotate();

        // the following was always there
        this.colorMap = colorMap;
        redraw();
    }
}
