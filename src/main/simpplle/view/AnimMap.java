package simpplle.view;

import simpplle.controller.PlaybackController;
import simpplle.controller.ViewerController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import simpplle.model.ColorMap;
import simpplle.model.Frame;
import simpplle.model.Layer;
import java.util.ArrayList;

/**
 * Created by John on 4/30/2017.
 */
public class AnimMap extends Pane {

    private ViewerController viewer;
    private PlaybackController playback;

    private Layer layer;
    private ColorMap colorMap;

    private ArrayList<WritableImage> imgFrames;
    private ImageView imgView;

    private double speed;
    private double frameWidth, frameHeight;
    private int timestep;

    private Timeline tline;

    public AnimMap() {
        imgFrames = new ArrayList<>();
        imgView = new ImageView();
        getChildren().add(imgView);

        tline = new Timeline(new KeyFrame(
                Duration.millis(1000),
                ae -> showNext()));
        tline.setCycleCount(Animation.INDEFINITE);
        tline.setRate(3);

        frameWidth = 300;
        frameHeight = 300;
    }

    public void setSize(double newWidth, double newHeight) {
        this.frameWidth = newWidth;
        this.frameHeight = newHeight;
    }

    public void setSpeed(double speed) {
        tline.setRate(speed);
    }

    public void start() {
        imgFrames.clear();
        timestep = viewer.getTimestep();
        this.layer = viewer.getSimulation().getLayer(viewer.getMapAttrib());
        this.colorMap = viewer.getLandscape().getColorMap(viewer.getMapAttrib());

        int startX = 0;
        int startY = 0;
        int endX = (int) frameWidth; //??????????
        int endY = (int)  frameHeight; // ????????
        endX = Math.min(endX, viewer.getLandscape().getNumCols());
        endY = Math.min(endY, viewer.getLandscape().getNumRows());

        for (int i = 0; i < layer.numFrames(); i++) {
            Tile t = new Tile((int) frameWidth, (int) frameHeight);
            GraphicsContext ctx = t.getGraphicsContext2D();
            ctx.setFill(Color.GRAY);
            ctx.fillRect(0, 0, (int) frameWidth, (int) frameHeight);
            Frame frame = new Frame(layer.getFrame(i));

            /**
             * Comment by Ed Shokur:
             * When each frame was created from the raw data, the x and y axis were flipped.
             * Before passing each frame into t.draw, we flip the frame using frame.rotate, ensuring that
             * the frame drawn is drawn correctly
             * Because flipping the drawing of the frame makes frame height and frame width wrong
             * We switched the endY and endX
             */
            frame.rotate();
            endY = frame.getDim().getRows();
            endX = frame.getDim().getColumns();

            t.draw(frame,
                    colorMap,
                    startX,
                    startY,
                    endX,
                    endY,
                    Color.GRAY);
            WritableImage img = new WritableImage((int) frameWidth, (int) frameHeight);
            t.snapshot(null, img);
            imgFrames.add(img);
        }
        tline.play();
    }

    public void stop() {
        tline.stop();
        viewer.setTimestep(timestep);
    }

    public void showNext() {
        timestep++;
        timestep = timestep % layer.numFrames();
        imgView.setImage(imgFrames.get(timestep));
        // tell viewer/playback which timestep this is
    }

    public void setPlaybackController(PlaybackController playback) {
        this.playback = playback;
    }

    public void setViewer(ViewerController viewer) {
        this.viewer = viewer;

        setSize(frameWidth, frameHeight);
    }
}
