package simpplle.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import simpplle.model.ColorMap;
import simpplle.model.Frame;

/**
 * A tile displays a region of a frame.
 */
public class Tile extends Canvas {

    //---------------------------------
    // Temporary: For debug coloring
    //---------------------------------
    private static int tileCount;
    private int id;
    //---------------------------------

    private GraphicsContext context;

    /**
     * Creates a new tile.
     *
     * @param width the width of the tile in pixels
     * @param height the height of the tile in pixels
     */
    public Tile(int width, int height) {

        super(width, height);

        context = getGraphicsContext2D();

        id = (tileCount * 16) % 255;
        tileCount++;

    }

    /**
     * Draws a rectangular from a frame.
     *
     * @param frame a frame to render from
     * @param colorMap a color map to apply to the frame
     * @param top index of the top row
     * @param left index of the left column
     * @param width number of columns to draw
     * @param height number of rows to draw
     */
    public void draw(Frame frame, ColorMap colorMap, int top, int left, int width, int height, Color missing) {

        // TODO Render cells to tile
        // render grabs a chunk of the frame and returns a 2d array of colors
        Color[][] data = frame.render(top, top + height, left, left + width, colorMap, missing);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                context.setFill(data[y][x]);
                context.fillRect(y, x, 1, 1);
            }
        }
        
    }

}
