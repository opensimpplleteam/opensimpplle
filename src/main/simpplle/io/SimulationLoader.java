package simpplle.io;

import simpplle.model.Landscape;
import simpplle.model.Simulation;

import java.io.IOException;
import java.util.List;

/**
 * Loads one or more simulations into a landscape.
 */
public interface SimulationLoader {

    List<Simulation> load(Landscape l) throws IOException;

}
