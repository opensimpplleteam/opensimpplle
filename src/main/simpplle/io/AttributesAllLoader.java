package simpplle.io;

import simpplle.model.Dimensions;
import simpplle.model.Landscape;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Loads a landscape from an OpenSIMPPLLE attributesAll file.
 */
public class AttributesAllLoader implements LandscapeLoader {

    private File file;

    /**
     * Creates a landscape loader for an attributesAll file.
     *
     * @param file an *.attributesAll file
     */
    public AttributesAllLoader(File file) {
        this.file = file;
    }

    /**
     * Reads attributes from the landscape file.
     * Adds colorMaps to the landscape
     * @return a landscape, or null if the file is empty
     * @throws IOException if the file isn't found
     */
    @Override
    public Landscape load() throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(this.file));

        String line = reader.readLine();

        // If the file is empty return from read method
        if (line == null) {
            return null;
        }

        // Ignore header
        if (line.equalsIgnoreCase("begin vegetation") || line.equalsIgnoreCase("begin vegetation-new") ) {
            line = reader.readLine();
        }

        int minRow = Integer.MAX_VALUE;
        int minCol = Integer.MAX_VALUE;
        int maxRow = Integer.MIN_VALUE;
        int maxCol = Integer.MIN_VALUE;

        // Loop until end of file or "END" token
        while ( line != null && !line.equalsIgnoreCase("end")) {

            String[] values = line.split(",");

            int row = Integer.valueOf(values[1]);
            if (row < minRow) minRow = row;
            if (row > maxRow) maxRow = row;

            int col = Integer.valueOf(values[2]);
            if (col < minCol) minCol = col;
            if (col > maxCol) maxCol = col;

            line = reader.readLine();
        }

        int nrows = maxRow - minRow + 1;
        int ncols = maxCol - minCol + 1;

        // Create a landscape

        Landscape landscape = new Landscape(new Dimensions(nrows, ncols));

        // Add default color maps

        landscape.addColorMap("Age");
        landscape.addColorMap("Density");
        landscape.addColorMap("Process");
        landscape.addColorMap("Size Class");
        landscape.addColorMap("Species");

        return landscape;

    }
}
