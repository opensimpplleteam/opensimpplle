package simpplle.io;

import java.io.File;
import java.io.IOException;

/**
 * The loader factory makes loaders for landscapes, simulations, and color maps.
 */
public class LoaderFactory {

    public static ColorMapLoader createColorMapLoader(File file) throws IOException {
        if (file.getName().endsWith(".clr")) {
            return new MMaPPitColorLoader(file);
        } else {
            return new LavaColorLoader(file);
        }
    }

    public static LandscapeLoader createLandscapeLoader(File file) {
        return new AttributesAllLoader(file);
    }

    public static SimulationLoader createSimulationLoader(File simulationDirectory, File landscapeFile) {
        return new EvuSimDataLoader(simulationDirectory, landscapeFile);
    }

}
