package simpplle.io;

import javafx.scene.paint.Color;
import simpplle.model.*;

import java.io.*;
import java.util.*;

/**
 * EvuSimDataLoader loads results from an OpenSIMPPLLE simulation into a Landscape's hashmap of Strings and Simulations.
 */
public class EvuSimDataLoader implements SimulationLoader {

    private static final String DENSITY_PATH   = "DENSITY.csv";
    private static final String PROCESS_PATH   = "PROCESS.csv";
    private static final String SIZECLASS_PATH = "SIZECLASS.csv";
    private static final String SPECIES_PATH   = "SPECIES.csv";

    private Map<Integer, String> densityLookup;
    private Map<Integer, String> processLookup;
    private Map<Integer, String> sizeClassLookup;
    private Map<Integer, String> speciesLookup;

    private File simulationDirectory;
    private File attributesFile;

    private int rowOffset;
    private int colOffset;

    private Map<Integer, Coordinate> coordinates;

    /** Comment By Eduard Shokur:
     * Creates a simulation loader. Loading a simulation file requires a simulation directory and an attributes file for parameters.
     *
     * @param simulationDirectory an OpenSIMPPLLE simulation directory
     * @param attributesFile an OpenSIMPPLLE *.attributesAll file which returns a landscape object
     * @throws IOException
     */
    public EvuSimDataLoader(File simulationDirectory, File attributesFile) {
        this.simulationDirectory = simulationDirectory;
        this.attributesFile = attributesFile;
    }

    /**
     * Loads one or more OpenSIMPPLLE simulations into a landscape.
     *
     * @param landscape a landscape to load the simulations into
     * @return a list of one or more loaded simulations
     * @throws IOException if a file doesn't exist
     */
    @Override
    public List<Simulation> load(Landscape landscape) throws IOException {

        List<Simulation> simulations = new ArrayList<>();
/**
 * Eduard Shokur:
 * If simulation directory name is "textdata" then simulationdirectory will be the same as was assigned during EvuSimdataLoader
 * initialization;
 * otherwise, a new File instance with "textdata" directory is initialized  and assigned to simulationDirectory; see:
 * https://stackoverflow.com/questions/10336899/what-is-a-question-mark-and-colon-operator-used-for
 */
        simulationDirectory = (simulationDirectory.getName().equalsIgnoreCase("textdata"))
                ? simulationDirectory : new File(simulationDirectory, "textdata");

        /** How LAVA works Overview By Eduard Shokur, part 1
         * The densityLookup, processLookup, sizeCLassLookup, speciesLookup lines are executed because
         * a Landscape object was created in the LoadController once "accept" was received from UI.
         * The landscape object has two hashmaps: String->ColorMap hashmap and String->Simulations hashmap .
         * After String->ColorMap hashmap is initialized, a line in simLoader.load(landscape) in LoadController,
         * loads String->Simulations hashmap of the before mentioned landscape object.
         * To create the Simulation, the tables below are populated for use in the loadSimulation func
         */

        densityLookup   = loadLookupTable(simulationDirectory, DENSITY_PATH);
        processLookup   = loadLookupTable(simulationDirectory, PROCESS_PATH);
        sizeClassLookup = loadLookupTable(simulationDirectory, SIZECLASS_PATH);
        speciesLookup   = loadLookupTable(simulationDirectory, SPECIES_PATH);

        // Determine the row offset, column offset, and map cells to coordinates

        rowOffset = Integer.MAX_VALUE;
        colOffset = Integer.MAX_VALUE;
        //by Eduard Shokur: slink is key, Coordinates object containing rows/cols is value
        coordinates = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(attributesFile))) {

            // Skip section name

            reader.readLine();

            // Read until the end of the file or section

            String line = reader.readLine();
            while (line != null && !line.equalsIgnoreCase("END")) {

                String[] values = line.split(",");

                int slink = Integer.parseInt(values[0]);
                int row = Integer.parseInt(values[1]);
                int col = Integer.parseInt(values[2]);

                // Update the row and column offsets

                if (row < rowOffset) rowOffset = row;
                if (col < colOffset) colOffset = col;

                // Store the coordinate

                coordinates.put(slink, new Coordinate(col, row));

                line = reader.readLine();

            }
        }

        // Load all simulation runs
        // makes an arraylist of all Simulation objects
        for (File file : findSimulationFiles(simulationDirectory)) {
            Simulation simulation = loadSimulation(landscape, file);
            simulations.add(simulation);
        }

        return simulations;

    }

    /**
     * Loads a lookup table from a directory into a hash map.
     *
     * @param parent a parent directory containing a lookup table
     * @param tablePath a path to the table within the directory
     * @return a map of indices to string values;
     * @throws IOException if the file doesn't exist
     */
    private Map<Integer, String> loadLookupTable(File parent, String tablePath) throws IOException {

        Map<Integer, String> map = new HashMap<>();

        File file = new File(parent, tablePath);

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            // Skip the header
            reader.readLine();

            // Load values into the map
            String line = reader.readLine();
            while (line != null) {
                String[] values = line.split(",");
                map.put(Integer.parseInt(values[0]), values[1]);
                line = reader.readLine();
            }
        }
        return map;

    }
    /**
     * Finds all simulation files (EVU_SIM_DATA) in the provided directory.
     *
     * @param directory a directory containing simulations
     * @return a list of simulation files
     */
    private List<File> findSimulationFiles(File directory) {
        List<File> files = new ArrayList<>();
        for (File file : directory.listFiles()) {
            if (file.isFile() && file.getName().matches("(?i).*EVU_*SIM_*DATA\\d*.*")) {
                files.add(file);
            }
        }
        return files;

    }

    /**
     * Loads a simulation with zero or more frames into a landscape.
     *
     * @param landscape a landscape to load the simulation into
     * @param file an EVU_SIM_DATA simulation file
     * @return a simulation
     * @throws IOException if the file doesn't exist
     */
    private Simulation loadSimulation(Landscape landscape, File file) throws IOException {

        Color missing = Landscape.DEFAULT_COLOR;

        // Count number of frames

        int numFrames;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            String header = reader.readLine();
//  Eduard Shokur: this loop finds which column in the header contains timestamp and saves the index of that column
            int index = 0;
            String[] columns = header.split(",");
            for (int i = 0; i < columns.length; i++) {
                if (columns[i].equalsIgnoreCase("timestep")) {
                    index = i;
                    break;
                }
            }
// By Eduard Shokur: we loop through all the lines and in each line we check if the step is greater than current maxStep, if so,
// we update our maxStep
            int maxStep = 0;
            String line = reader.readLine();
            while (line != null) {
                String[] values = line.split(",");
                int step = Integer.parseInt(values[index]);
                if (maxStep < step) {
                    maxStep = step;
                }
                line = reader.readLine();
            }

            numFrames = maxStep + 1;

        }

        // Load attribute values

        int numRows = landscape.getNumRows();
        int numCols = landscape.getNumCols();

        String[][][] age       = new String[numFrames][numRows][numCols];
        String[][][] density   = new String[numFrames][numRows][numCols];
        String[][][] process   = new String[numFrames][numRows][numCols];
        String[][][] sizeClass = new String[numFrames][numRows][numCols];
        String[][][] species   = new String[numFrames][numRows][numCols];

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            // Store the indices of required columns

            int colAge       = 0;
            int colDensity   = 0;
            int colProcess   = 0;
            int colSizeClass = 0;
            int colSlink     = 0;
            int colSpecies   = 0;
            int colTimestep  = 0;
// By Eduard Shokur: we read through EVU_SIM_DATA1.csv and we find which column in the csv contains
// age, density_id, etc and update the appropriate variable
            String[] columns = reader.readLine().split(",");
            for (int i = 0; i < columns.length; i++) {
                switch (columns[i].toLowerCase()) {
                    case "age":          colAge       = i; break;
                    case "density_id":   colDensity   = i; break;
                    case "process_id":   colProcess   = i; break;
                    case "sizeclass_id": colSizeClass = i; break;
                    case "slink":        colSlink     = i; break;
                    case "species_id":   colSpecies   = i; break;
                    case "timestep":     colTimestep  = i; break;
                }
            }

            // Read values for each attribute

            String line = reader.readLine();
// Eduard Shokur: each line in EVU_SIM_DATA1.csv is read
            while (line != null) {

                String[] values = line.split(",");
/**
 * Eduard Shokur:
 * Rows, cols and timestep are read in; real location of rows/cols is calculated relative to x & y offset.
 *  The resulting rows & cols values of each timestep of each Layer
 * are saved into the proper in a 3d array
 * See Notes below for Layers and Frames
 */
                int slink       = Integer.parseInt(values[colSlink]);
                int densityID   = Integer.parseInt(values[colDensity]);
                int processID   = Integer.parseInt(values[colProcess]);
                int sizeClassID = Integer.parseInt(values[colSizeClass]);
                int speciesID   = Integer.parseInt(values[colSpecies]);
                int timestep    = Integer.parseInt(values[colTimestep]);


                Coordinate coordinate = coordinates.get(slink);
                int row = coordinate.y - rowOffset;
                int col = coordinate.x - colOffset;

                age       [timestep][row][col] = values[colAge];
                density   [timestep][row][col] = densityLookup.get(densityID);
                process   [timestep][row][col] = processLookup.get(processID);
                sizeClass [timestep][row][col] = sizeClassLookup.get(sizeClassID);
                species   [timestep][row][col] = speciesLookup.get(speciesID);
                line = reader.readLine();
            }
        }

        // Add a simulation to the landscape

        Simulation simulation = landscape.addSimulation(file.getName(), numFrames);

        simulation.runName = file.getName();

        // Load values into frames
// Eduard Shokur: add name to simulation
        Layer layerAge       = simulation.addLayer("Age");
        Layer layerDensity   = simulation.addLayer("Density");
        Layer layerProcess   = simulation.addLayer("Process");
        Layer layerSizeClass = simulation.addLayer("Size Class");
        Layer layerSpecies   = simulation.addLayer("Species");
/** By Eduard Shokur:
 * The overall picture of how LAVA simulation works part 2
 * Landscape contains colorMap table which is a table Strings (keys) and ColorMap object (value)
 * Landscape also has a table of Strings (keys) and Simulation (value)
 * Layer is a collection of Frames that have same Dimension; age is one example of a layer.
 * Frame represents a timestep in the simulation for a particular process;
 * Frame stores a table of indices for a single time step. Indices within a frame correspond to entries in a
 * color map. Each value in the indices array corresponds to the index number in colorMap containing
 * the color for that [row][col] in that specific frame
 * Simulation object contains a table which has Layer string (key) and Layer object (value)
 * Simulation also contains number of frames and Dimension object
 */

        ColorMap colorAge       = landscape.getColorMap("Age");
        ColorMap colorDensity   = landscape.getColorMap("Density");
        ColorMap colorProcess   = landscape.getColorMap("Process");
        ColorMap colorSizeClass = landscape.getColorMap("Size Class");
        ColorMap colorSpecies   = landscape.getColorMap("Species");
/*
 * Eduard Shokur: Each frame contains an array of indices which correspond to location in the colorMap;
 */
        for (int i = 0; i < numFrames; i++) {
// this assigns to a variable a collection of frames of a particular layer
            Frame frameAge       = layerAge.getFrame(i);
            Frame frameDensity   = layerDensity.getFrame(i);
            Frame frameProcess   = layerProcess.getFrame(i);
            Frame frameSizeClass = layerSizeClass.getFrame(i);
            Frame frameSpecies   = layerSpecies.getFrame(i);

/*
 * Overview of how LAVA works by Eduard Shokur part 3:
 * Explanation of Parameters passed into the frameObjectName.load()
 * From left to right:
 * 1: 3D array containing timesteps of the each Layer and the rows/cols locations and corresponding frame number.
 * Variable i is the same as frame number in each layer of that landscape.
 * 2: The still empty colorMap object of the landscape is passed in as a second parameter
 * 3: Default color to be used, if no matching entry in ColorMap is found
 */
            frameAge.load(age[i], colorAge, colorAge.getColor(i, missing));
            frameDensity.load(density[i], colorDensity, missing);
            frameProcess.load(process[i], colorProcess, missing);
            frameSizeClass.load(sizeClass[i], colorSizeClass, missing);
            frameSpecies.load(species[i], colorSpecies, missing);
        }
        return simulation;
    }
}
