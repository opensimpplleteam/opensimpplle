package simpplle.io;

import javafx.scene.paint.Color;
import simpplle.model.ColorMap;
import simpplle.model.Landscape;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * A color loader for loading colors in from a MMaPPit color file
 */
public class MMaPPitColorLoader implements ColorMapLoader {

    private File file;

    public MMaPPitColorLoader(File file) {
        this.file = file;
    }

    /**
     *
     * TODO Place this into it's own class that implements LavaColorLoader
     * loads color files from MMaPPit formats.
     *
     * A MMaPPit color file contains information for a single color map
     *
     * It contains a header for number of colors denoted by:
     * NColors
     *  (int)
     *
     *  Then a secondary header with column names:
     *  LabelColorIndex(i),ColmnName(s),LabelString(s),LabelMinVal(i),LabelMaxVal(i),R,G,B
     *
     * @param landscape
     * @throws IOException
     */
    @Override
    public void load(Landscape landscape) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            if (file.getName().matches("(?i).+\\.clr")) {

                // read first, second, third and fourth line to get past column headers
                String line = reader.readLine(); line = reader.readLine(); line = reader.readLine(); line = reader.readLine();

                ColorMap map = landscape.getColorMap(line.split(",")[1]);

                while (line != null) {

                    String[] values = line.split(",");

                    int r = Integer.parseInt(values[5]);
                    int g = Integer.parseInt(values[6]);
                    int b = Integer.parseInt(values[7]);

                    map.add(values[2], Color.rgb(r, g, b));
                    line = reader.readLine();

                }
            }
        }
    }
}
