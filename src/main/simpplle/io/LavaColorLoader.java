package simpplle.io;

import javafx.scene.paint.Color;
import simpplle.model.ColorMap;
import simpplle.model.Landscape;

import java.io.*;
import java.util.*;

/**
 * A color loader for loading colors in from a LAVA color file
 */
public class LavaColorLoader implements ColorMapLoader {

    private File file;

    public LavaColorLoader(File file) {
        this.file = file;
    }

    /**
     * Loads attributes into color maps contained within the landscape.
     *
     * This method should only be used with the new format of color files
     * produced by the lava application.  User defined color files are supported.
     *
     * Defined color files should have this format:
     * Color map name indicated by "-name-" followed by a newline
     * Attributes in the color map indicated by "name,h:s:b" followed by a newline
     *
     * EXAMPLE
     * -Process-
     * FIRE,10:10:10
     * MPB,255:255:255
     * -SPECIES-
     * LODGEPOLE,100:153:88
     * PONDEROSA,83:252:104
     *
     * @param landscape a landscape object, should contain a number of colormaps
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void load(Landscape landscape) throws IOException {

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            ColorMap colorMap = null;

            String line = reader.readLine();

            while (line != null) {

                if (line.matches("-.+-")) {

                    String name = line.replace("-","");

                    colorMap = landscape.getColorMap(name);

                    if (colorMap == null) {
                        colorMap = landscape.addColorMap(name);
                    }

                } else if (colorMap != null) {

                    String[] tokens = line.split(",");

                    double h = Double.parseDouble(tokens[1]);
                    double s = Double.parseDouble(tokens[2]);
                    double b = Double.parseDouble(tokens[3]);

                    colorMap.setColor(tokens[0], Color.hsb(h, s, b));

                }

                line = reader.readLine();

            }
        }
    }

    /**
     * Saves color map information to a file
     *
     * The format of the file is as follows:
     *
     * -Process-
     * FIRE,10:10:10
     * MPB,255:255:255
     * -SPECIES-
     * LODGEPOLE,100:153:88
     * PONDEROSA,83:252:104
     *
     * @param landscape a landscape with populated color maps
     * @throws IOException if an I/O error occurs
     */
    public void save(Landscape landscape) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));

        for (String name : landscape.getColorMapNames()) {

            ColorMap map = landscape.getColorMap(name);

            writer.write("-" + name + "-");
            writer.newLine();

            for (int i = 0; i < map.size(); i++) {

                Color c = map.getColor(i, Color.web("#666"));

                String value = map.getValue(i, "NOT FOUND");

                if (value != null) {

                    double h = c.getHue();
                    double s = c.getSaturation();
                    double b = c.getBrightness();

                    writer.write(value + "," + h + "," + s + "," + b);
                    writer.newLine();

                }

            }
        }

        writer.close();

    }
}
