package simpplle.io;

import simpplle.model.Landscape;

import java.io.IOException;

/**
 * Loads a color map.
 */
public interface ColorMapLoader {

    void load(Landscape landscape) throws IOException;

}
