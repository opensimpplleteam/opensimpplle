package simpplle.io;

import simpplle.model.Landscape;

import java.io.IOException;

/**
 * Loads a landscape.
 */
public interface LandscapeLoader {

    Landscape load() throws IOException;

}
/////