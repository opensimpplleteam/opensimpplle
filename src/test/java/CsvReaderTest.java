import org.junit.Test;
import simpplle.comcode.CsvReader;

import java.io.File;
import java.io.FileWriter;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;

public class CsvReaderTest {

  private static final String PREFIX = "test";
  private static final String SUFFIX = ".tmp";
  private static final double DELTA = 0.0000001;

  @Test
  public void getBoolean() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write("true\n");
    writer.write("TRUE\n");
    writer.write("false\n");
    writer.write("FALSE\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord();
    assertTrue(reader.getBoolean("field", false));
    reader.nextRecord();
    assertTrue(reader.getBoolean("field", false));
    reader.nextRecord();
    assertFalse(reader.getBoolean("field", true));
    reader.nextRecord();
    assertFalse(reader.getBoolean("field", true));

  }

  @Test
  public void getByte() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Byte.MAX_VALUE + "\n");
    writer.write(Byte.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getByte("field", (byte) 0), Byte.MAX_VALUE);
    reader.nextRecord(); assertEquals(reader.getByte("field", (byte) 0), Byte.MIN_VALUE);

  }

  @Test
  public void getChar() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write("A\n");
    writer.write("あ\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getChar("field", '\u0000'), 'A');
    reader.nextRecord(); assertEquals(reader.getChar("field", '\u0000'), 'あ');

  }

  @Test
  public void getDouble() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Double.MAX_VALUE + "\n");
    writer.write(Double.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getDouble("field", 0.0), Double.MAX_VALUE, DELTA);
    reader.nextRecord(); assertEquals(reader.getDouble("field", 0.0), Double.MIN_VALUE, DELTA);

  }

  @Test
  public void getFloat() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Float.MAX_VALUE + "\n");
    writer.write(Float.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getFloat("field", 0.0f), Float.MAX_VALUE, DELTA);
    reader.nextRecord(); assertEquals(reader.getFloat("field", 0.0f), Float.MIN_VALUE, DELTA);

  }

  @Test
  public void getInteger() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Integer.MAX_VALUE + "\n");
    writer.write(Integer.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getInteger("field", 0), Integer.MAX_VALUE);
    reader.nextRecord(); assertEquals(reader.getInteger("field", 0), Integer.MIN_VALUE);

  }

  @Test
  public void getLong() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Long.MAX_VALUE + "\n");
    writer.write(Long.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getLong("field", 0L), Long.MAX_VALUE);
    reader.nextRecord(); assertEquals(reader.getLong("field", 0L), Long.MIN_VALUE);

  }

  @Test
  public void getShort() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write(Short.MAX_VALUE + "\n");
    writer.write(Short.MIN_VALUE + "\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getShort("field", (short) 0), Short.MAX_VALUE);
    reader.nextRecord(); assertEquals(reader.getShort("field", (short) 0), Short.MIN_VALUE);

  }

  @Test
  public void getString() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write("A b c\n");
    writer.close();

    CsvReader reader = new CsvReader(file);
    reader.nextRecord(); assertEquals(reader.getString("field", ""), "A b c");

  }

  @Test
  public void hasField() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("a,b");
    writer.close();

    CsvReader reader = new CsvReader(file);
    assertTrue(reader.hasField("a"));
    assertTrue(reader.hasField("b"));
    assertFalse(reader.hasField("c"));

  }

  @Test
  public void nextRecord() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("field\n");
    writer.write("value");
    writer.close();

    CsvReader reader = new CsvReader(file);
    assertTrue(reader.nextRecord());
    assertFalse(reader.nextRecord());

  }

}