import org.junit.Test;
import simpplle.comcode.Ini;

import java.io.File;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IniTest {

  private static final float FLOAT_EPSILON = 0.0000001f;
  private static final double DOUBLE_EPSILON = 0.0000001;

  private static final String PREFIX = "temp";
  private static final String SUFFIX = ".ini";

  @Test
  public void sections() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[A]\n");
    writer.write(". = .\n");
    writer.write("[B]\n");
    writer.close();

    Set<String> sections = new HashSet<>();
    sections.add("A");

    Ini ini = new Ini(file);
    assertTrue(ini.hasSection("A"));
    assertFalse(ini.hasSection("B"));
    assertEquals(ini.sections(), sections);

  }

  @Test
  public void keys() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[A]\n");
    writer.write("1 = .\n");
    writer.write("[B]\n");
    writer.write("1 = .\n");
    writer.write("2 = .\n");
    writer.write("[C]\n");
    writer.write("1 = .\n");
    writer.write("2 = .\n");
    writer.write("3 = .\n");
    writer.close();

    Set<String> keysA = new HashSet<>();
    keysA.add("1");

    Set<String> keysB = new HashSet<>();
    keysB.add("1");
    keysB.add("2");

    Set<String> keysC = new HashSet<>();
    keysC.add("1");
    keysC.add("2");
    keysC.add("3");

    Ini ini = new Ini(file);
    assertTrue(ini.hasKey("A","1"));
    assertTrue(ini.hasKey("B","1"));
    assertTrue(ini.hasKey("B","2"));
    assertTrue(ini.hasKey("C","1"));
    assertTrue(ini.hasKey("C","2"));
    assertTrue(ini.hasKey("C","3"));
    assertTrue(ini.keys("A").equals(keysA));
    assertTrue(ini.keys("B").equals(keysB));
    assertTrue(ini.keys("C").equals(keysC));

  }

  @Test
  public void getBoolean() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = true\n");
    writer.write("2 = True\n");
    writer.write("3 = false\n");
    writer.write("4 = False\n");
    writer.close();

    Ini ini = new Ini(file);
    assertTrue(ini.getBoolean("group", "1", false));
    assertTrue(ini.getBoolean("group", "2", false));
    assertFalse(ini.getBoolean("group", "3", true));
    assertFalse(ini.getBoolean("group", "4", true));

  }

  @Test
  public void getByte() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Byte.MAX_VALUE + "\n");
    writer.write("2 = " + Byte.MIN_VALUE + "\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getByte("group", "1", (byte) 0), Byte.MAX_VALUE);
    assertEquals(ini.getByte("group", "2", (byte) 0), Byte.MIN_VALUE);

  }

  @Test
  public void getChar() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = A\n");
    writer.write("2 = あ\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getChar("group", "1", '\u0000'), 'A');
    assertEquals(ini.getChar("group", "2", '\u0000'), 'あ');

  }

  @Test
  public void getDouble() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Double.MAX_VALUE + "\n");
    writer.write("2 = " + Double.MIN_VALUE + "\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getDouble("group", "1", 0.0), Double.MAX_VALUE, DOUBLE_EPSILON);
    assertEquals(ini.getDouble("group", "2", 0.0), Double.MIN_VALUE, DOUBLE_EPSILON);

  }

  @Test
  public void getFloat() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Float.MAX_VALUE + "\n");
    writer.write("2 = " + Float.MIN_VALUE + "\n");
    writer.close();

    final double epsilon = 0.00000001f;

    Ini ini = new Ini(file);
    assertEquals(ini.getFloat("group", "1", 0.0f), Float.MAX_VALUE, FLOAT_EPSILON);
    assertEquals(ini.getFloat("group", "2", 0.0f), Float.MIN_VALUE, FLOAT_EPSILON);

  }

  @Test
  public void getInteger() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Integer.MAX_VALUE + "\n");
    writer.write("2 = " + Integer.MIN_VALUE + "\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getInteger("group", "1", 0), Integer.MAX_VALUE);
    assertEquals(ini.getInteger("group", "2", 0), Integer.MIN_VALUE);

  }

  @Test
  public void getLong() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Long.MAX_VALUE + "\n");
    writer.write("2 = " + Long.MIN_VALUE + "\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getLong("group", "1", 0L), Long.MAX_VALUE);
    assertEquals(ini.getLong("group", "2", 0L), Long.MIN_VALUE);

  }

  @Test
  public void getShort() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = " + Short.MAX_VALUE + "\n");
    writer.write("2 = " + Short.MIN_VALUE + "\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getShort("group", "1", (short) 0), Short.MAX_VALUE);
    assertEquals(ini.getShort("group", "2", (short) 0), Short.MIN_VALUE);

  }

  @Test
  public void getString() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    FileWriter writer = new FileWriter(file);
    writer.write("[group]\n");
    writer.write("1 = A b c\n");
    writer.close();

    Ini ini = new Ini(file);
    assertEquals(ini.getString("group", "1", ""), "A b c");

  }

  @Test
  public void putBoolean() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putBoolean("group", "1", true);
    ini.putBoolean("group", "2", false);
    ini.save(file);

    ini = new Ini(file);
    assertTrue(ini.getBoolean("group", "1", false));
    assertFalse(ini.getBoolean("group", "2", true));

  }

  @Test
  public void putByte() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putByte("group", "1", Byte.MAX_VALUE);
    ini.putByte("group", "2", Byte.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getByte("group", "1", (byte) 0), Byte.MAX_VALUE);
    assertEquals(ini.getByte("group", "2", (byte) 0), Byte.MIN_VALUE);

  }

  @Test
  public void putChar() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putChar("group", "1", 'A');
    ini.putChar("group", "2", 'あ');
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getChar("group", "1", '\u0000'), 'A');
    assertEquals(ini.getChar("group", "2", '\u0000'), 'あ');

  }

  @Test
  public void putDouble() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putDouble("group", "1", Double.MAX_VALUE);
    ini.putDouble("group", "2", Double.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getDouble("group", "1", 0.0), Double.MAX_VALUE, DOUBLE_EPSILON);
    assertEquals(ini.getDouble("group", "2", 0.0), Double.MIN_VALUE, DOUBLE_EPSILON);

  }

  @Test
  public void putFloat() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putFloat("group", "1", Float.MAX_VALUE);
    ini.putFloat("group", "2", Float.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getFloat("group", "1", 0.0f), Float.MAX_VALUE, FLOAT_EPSILON);
    assertEquals(ini.getFloat("group", "2", 0.0f), Float.MIN_VALUE, FLOAT_EPSILON);

  }

  @Test
  public void putInteger() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putInteger("group", "1", Integer.MAX_VALUE);
    ini.putInteger("group", "2", Integer.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getInteger("group", "1", 0), Integer.MAX_VALUE);
    assertEquals(ini.getInteger("group", "2", 0), Integer.MIN_VALUE);

  }

  @Test
  public void putLong() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putLong("group", "1", Long.MAX_VALUE);
    ini.putLong("group", "2", Long.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getLong("group", "1", 0L), Long.MAX_VALUE);
    assertEquals(ini.getLong("group", "2", 0L), Long.MIN_VALUE);

  }

  @Test
  public void putShort() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putShort("group", "1", Short.MAX_VALUE);
    ini.putShort("group", "2", Short.MIN_VALUE);
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getShort("group", "1", (short) 0), Short.MAX_VALUE);
    assertEquals(ini.getShort("group", "2", (short) 0), Short.MIN_VALUE);

  }

  @Test
  public void putString() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    Ini ini = new Ini();
    ini.putString("group", "1", "A b c");
    ini.save(file);

    ini = new Ini(file);
    assertEquals(ini.getString("group", "1", ""), "A b c");

  }
}
