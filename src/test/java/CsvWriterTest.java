import org.junit.Test;
import simpplle.comcode.CsvWriter;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CsvWriterTest {

  private static final String PREFIX = "test";
  private static final String SUFFIX = ".tmp";
  private static final double DELTA = 0.0000001;

  @Test
  public void putBoolean() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putBoolean("field", true);
    writer.storeRecord();
    writer.putBoolean("field", false);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals("true",  lines.get(1));
    assertEquals("false", lines.get(2));

  }
  @Test
  public void putByte() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putByte("field", Byte.MAX_VALUE);
    writer.storeRecord();
    writer.putByte("field", Byte.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Byte.MAX_VALUE, Byte.parseByte(lines.get(1)));
    assertEquals(Byte.MIN_VALUE, Byte.parseByte(lines.get(2)));

  }

  @Test
  public void putChar() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putChar("field", 'A');
    writer.storeRecord();
    writer.putChar("field", 'あ');
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals("A", lines.get(1));
    assertEquals("あ", lines.get(2));

  }

  @Test
  public void putDouble() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putDouble("field", Double.MAX_VALUE);
    writer.storeRecord();
    writer.putDouble("field", Double.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Double.MAX_VALUE, Double.parseDouble(lines.get(1)), DELTA);
    assertEquals(Double.MIN_VALUE, Double.parseDouble(lines.get(2)), DELTA);

  }

  @Test
  public void putFloat() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putFloat("field", Float.MAX_VALUE);
    writer.storeRecord();
    writer.putFloat("field", Float.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Float.MAX_VALUE, Float.parseFloat(lines.get(1)), DELTA);
    assertEquals(Float.MIN_VALUE, Float.parseFloat(lines.get(2)), DELTA);

  }

  @Test
  public void putInteger() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putInteger("field", Integer.MAX_VALUE);
    writer.storeRecord();
    writer.putInteger("field", Integer.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Integer.MAX_VALUE, Integer.parseInt(lines.get(1)));
    assertEquals(Integer.MIN_VALUE, Integer.parseInt(lines.get(2)));

  }

  @Test
  public void putLong() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putLong("field", Long.MAX_VALUE);
    writer.storeRecord();
    writer.putLong("field", Long.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Long.MAX_VALUE, Long.parseLong(lines.get(1)));
    assertEquals(Long.MIN_VALUE, Long.parseLong(lines.get(2)));

  }

  @Test
  public void putShort() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putShort("field", Short.MAX_VALUE);
    writer.storeRecord();
    writer.putShort("field", Short.MIN_VALUE);
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals(Short.MAX_VALUE, Short.parseShort(lines.get(1)));
    assertEquals(Short.MIN_VALUE, Short.parseShort(lines.get(2)));

  }

  @Test
  public void putString() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putString("field", "A b c");
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals("A b c", lines.get(1));

  }

  @Test
  public void storeRecord() throws Exception {

    File file = File.createTempFile(PREFIX, SUFFIX);

    CsvWriter writer = new CsvWriter(file, new String[] {"field"});
    writer.putString("field", "A");
    writer.storeRecord();
    writer.storeRecord();
    writer.close();

    List<String> lines = Files.readAllLines(file.toPath());
    assertEquals("field", lines.get(0));
    assertEquals("A", lines.get(1));
    assertEquals("", lines.get(2));

  }
}
